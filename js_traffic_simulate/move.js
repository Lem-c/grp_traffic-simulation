//The movement of vehicles
//Get vehicles
var car_1 = document.getElementById("car");
var car_2 = document.getElementById("car_2");

//Get traffic lights
var redLight = document.getElementsByClassName("red")[0];
var greenLight = document.getElementsByClassName("green")[0];
var yellowLight = document.getElementsByClassName("yellow")[0];

//The different driver
function driver_1(ele,target){
	//clear the timer 
	clearInterval(ele.timer);

	var speed = target>ele.offsetLeft?10:-10;
	ele.timer = setInterval(function () {
		//Get difference of current value of target value
		var val = target - ele.offsetLeft;
		ele.style.left = ele.offsetLeft + speed + "px";
		//Compare the step length with the target value
		if(Math.abs(val)<Math.abs(speed)){
			ele.style.left = target + "px";
			clearInterval(ele.timer);
		}
	},30)
}

function driver_2(ele, target){
	//clear the timer
	clearInterval(ele.timer);
	
	var speed = target>ele.offsetTop?3:-3;
	ele.timer = setInterval(function () {
		//Get difference of current value of target value
		var val = target - ele.offsetTop;
		ele.style.top = ele.offsetTop + speed + "px";
		//Compare the step length with the target value
		if(Math.abs(val)<Math.abs(speed)){
			ele.style.top = target + "px";
			clearInterval(ele.timer);
		}
	},30)
}

function moveVehicle_1(){
	driver_1(car_1, 720);
}

function moveVehicle_2(){
	driver_2(car_2, 600);
}

//The traffic light controller
function changeLight(light, target){
	var time = target;
	clearInterval(light.timer);
	
	light.timer = setInterval(function () {
		time -= 1;
		if(time<0){
			light.style.display = "none";
			clearInterval(light.timer);
		}else{
			light.style.display = "inline";
		}
	},50)
}

function crossRode_1(times){
	var i=1;
	
	for(i; i<times+1; ){
		changeLight(greenLight, 45);
		//changeLight(yellowLight, 15);
		console.log("hello");
		greenLight.style.display = "inline";

		i++;
	}
}

//The actions
moveVehicle_1();
moveVehicle_2();
crossRode_1(3);