#ifndef PIEGRAPH_H
#define PIEGRAPH_H

#include <QtCharts/QChartView>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>

class PieGraph : public QChartView
{
    Q_OBJECT
public:
    PieGraph(QWidget *parent = 0);

    //设置标题
    void SetTitle(QString _title);
    //设置数值
    void UpdateValue(QList<int> _values);
    //初始化饼图分部数量
    void InitPartCounts(QStringList _labelList);

private:
    QChart *mChart;
    QPieSeries *mPieSeries;

    int mPartCount;
    QStringList mPartLabelList;

    //颜色容器
    QList<QColor> mColors;
    QColor GetColorUnique();
};

#endif // PIEGRAPH_H
