#include "piegraph.h"

PieGraph::PieGraph(QWidget *parent) :
    QChartView(new QChart(), parent)
{
    mChart = new QChart();
    mChart->setAnimationOptions(QChart::SeriesAnimations);

    mPieSeries = new QPieSeries();

    mChart->addSeries(mPieSeries);
    mChart->setTitle("Simple Title");
    mChart->legend()->setVisible(true); //����ͼ��Ϊ��ʾ״̬
    mChart->legend()->setAlignment(Qt::AlignBottom);//����ͼ������ʾλ���ڵײ�
    setChart(mChart);

    mPartCount = 0;

    mChart->setTheme(QChart::ChartThemeBlueCerulean);
}

void PieGraph::SetTitle(QString _title)
{
    mChart->setTitle(_title);
}

void PieGraph::UpdateValue(QList<int> _values)
{
    int _sum = 0;
    for (auto _value : _values)
    {
        _sum += _value;
    }
    for (int i = 0; i < _values.count(); i++)
    {
        float _percent = (_values[i]/(float)_sum)*100.00;
        QString _percentStr = QString::number(_percent, 'f', 2);
//        QString _label = mPieSeries->slices().at(i)->label();
        mPieSeries->slices().at(i)->setLabel(mPartLabelList[i] + ":" +_percentStr + "%");
        mPieSeries->slices().at(i)->setValue(_values[i]);
    }
}

void PieGraph::InitPartCounts(QStringList _labelList)
{
    mPartCount = _labelList.count();
    mPartLabelList = _labelList;
    for (int i = 0; i < mPartCount; i++)
    {
        mPieSeries->append(_labelList[i],i+1);
    }
    for (int i = 0; i < mPartCount; i++)
    {
        QColor _color = GetColorUnique();
        QPieSlice *slice = mPieSeries->slices().at(i);
//        slice->setExploded();
        slice->setLabelVisible();
        slice->setPen(QPen(_color, 2));
        slice->setBrush(_color);
    }
}

QColor PieGraph::GetColorUnique()
{
    QColor qc(rand()%256,rand()%256,rand()%256);
    if(mColors.contains(qc))
    {
        GetColorUnique();
    }
    else {
        return qc;
    }
}

