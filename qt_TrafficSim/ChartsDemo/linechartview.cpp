#include "linechartview.h"
#include "chart.h"

LineChartView::LineChartView(QWidget *parent) :
    QChartView(new QChart(), parent)
{
    Chart *chart = new Chart;
    chart->setTitle("Dynamic spline chart");
    chart->legend()->hide();
    chart->setAnimationOptions(QChart::AllAnimations);
    setChart(chart);
    chart->setTheme(QChart::ChartThemeBlueCerulean);
    chart->legend()->setVisible(true); //设置图例为显示状态
    chart->legend()->setAlignment(Qt::AlignBottom);//设置图例的显示位置在底部
}
