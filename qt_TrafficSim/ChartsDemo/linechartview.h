#ifndef LINECHARTVIEW_H
#define LINECHARTVIEW_H

#include <QChartView>
#include <QObject>

class LineChartView : public QChartView
{
    Q_OBJECT
public:
    LineChartView(QWidget *parent = 0);
};

#endif // LINECHARTVIEW_H
