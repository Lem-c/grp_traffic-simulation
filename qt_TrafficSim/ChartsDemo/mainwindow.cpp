#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "barchart.h"
#include <QTime>
#include <qrandom.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    startTimer(1000);

    //直方图设置
    //设置标题
    ui->barChart->SetTitle("Simple Title");
    //定义横坐标
    QStringList _clist;
    _clist<<"a"<<"b"<<"c"<<"d";
    ui->barChart->AddCateGories(_clist);
    //初始化直方图数据
    QList<int> _valueList;
    _valueList<<4<<6<<9<<10;
    ui->barChart->UpdateBarValue(_valueList);

    //饼图设置
    ui->pieGraph->SetTitle("Pie Graph");
    ui->pieGraph->InitPartCounts(_clist);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    QTime time;
    time= QTime::currentTime();
    srand(time.msec()+time.second()*1000);
    int n = rand() % 10;
    QList<int> _valueList;
    _valueList<<n+1<<n+2<<n+3<<n+4;
    ui->barChart->UpdateBarValue(_valueList);
    ui->pieGraph->UpdateValue(_valueList);
}

