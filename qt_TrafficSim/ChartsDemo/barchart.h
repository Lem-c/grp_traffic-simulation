#ifndef BARCHART_H
#define BARCHART_H

#include <QtCharts/QChartView>
#include <QBarSet>
#include <QObject>
#include <QMap>
#include <QBarSeries>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <QObject>

class BarChart : public QChartView
{
    Q_OBJECT
public:
    BarChart(QWidget *parent = 0);

    //设置标题
    void SetTitle(QString _title);
    //增加横坐标描述
    void AddCateGories(QStringList _stringList);
    //修改各个柱状数值
    void UpdateBarValue(QList<int> _valueList);

private:
    QChart *mChart;
    QBarSeries *mBarSeries;
    QValueAxis *mAxisY;
    QBarCategoryAxis * mBarCategoryAxis;
    QBarSet *mBarSet;

};

#endif // BARCHART_H
