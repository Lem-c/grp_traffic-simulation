#include "barchart.h"


BarChart::BarChart(QWidget *parent) :
    QChartView(new QChart(), parent)
{
    mChart = new QChart();
    mChart->setAnimationOptions(QChart::NoAnimation);
    mBarSet = new QBarSet(QString::fromLocal8Bit("吞吐量"));
    mBarSeries = new QBarSeries();
    mBarCategoryAxis = new QBarCategoryAxis();

    mAxisY = new QValueAxis();
    mAxisY->setRange(0,10);

    mChart->addSeries(mBarSeries);
    mChart->legend()->setVisible(true); //设置图例为显示状态
    mChart->legend()->setAlignment(Qt::AlignBottom);//设置图例的显示位置在底部
    setChart(mChart);

//    QPalette pal = palette();
//    pal.setColor(QPalette::Window, QRgb(0x40434a));
//    pal.setColor(QPalette::WindowText, QRgb(0xd6d6d6));
//    setPalette(pal);
    mChart->setTheme(QChart::ChartThemeBlueCerulean);
}

void BarChart::SetTitle(QString _title)
{
    mChart->setTitle(_title);
}

void BarChart::AddCateGories(QStringList _stringList)
{
    mBarCategoryAxis->append(_stringList);
//    mChart->createDefaultAxes();//创建默认的左侧的坐标轴（根据 QBarSet 设置的值）
    for(int i = 0; i < _stringList.count(); i++)
    {
        mBarSet->append(3);
    }
    mBarSeries->append(mBarSet);
    mChart->setAxisX(mBarCategoryAxis, mBarSeries);//设置坐标轴
    mChart->setAxisY(mAxisY,mBarSeries);
}

void BarChart::UpdateBarValue(QList<int> _valueList)
{
    for(int i = 0; i < _valueList.count(); i++)
    {
        mBarSet->replace(i, _valueList[i]);
    }
}
