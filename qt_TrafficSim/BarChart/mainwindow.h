#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include <map>
#include <cstdlib>
#include <QMap>
#include <QList>
#include <QLabel>
#include <QVBoxLayout>
#include "barchartsbuilder.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void CreateDate(unsigned int uiCnt);
    void ResetLabel();

private slots:
    void pushRand();
    void pushClear();
    //void autoUpdate();
    void sltTooltip(bool status, int index, QBarSet *barset);
    //void timerUpdate();


private:
    std::vector<QLabel*>    m_vecToolTips;
    Ui::MainWindow          *ui;
    BarChartBuilder         *m_BarChart;
    QTimer m_timer;



};

#endif // MAINWINDOW_H
