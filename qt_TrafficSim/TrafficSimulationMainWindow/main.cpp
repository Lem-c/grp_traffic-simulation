#include "views/softwaremainwindowview.h"

#include <QObject>
#include <QApplication>
#include <QFile>
#include <QTimer>
#include <QTextStream>
#include <ctime>
#include "utilities/uiexception.h"
#include "simulation/trafficsimulator.h"
#include "simulation/trafficconfigurationmaker.h"
#include "simulation/trafficsimulationview.h"
#include "simulation/trafficsimulatorthread.h"
#include "utilities/stylesheetparser.h"
#include <iostream>

//int main(int argc, char** argv) {
//    const int refreshTimeInterval = 20;

//    QApplication application(argc, argv);

//    QSurfaceFormat fmt;
//    fmt.setSamples(4);
//    QSurfaceFormat::setDefaultFormat(fmt);

//    srand((unsigned int) time(NULL));

//    TrafficSimulator trafficSimulator;

//    TrafficSceneConfiguration trafficSceneConfiguration(2, 2);

//    trafficSimulator.setUpSimulatorWith(trafficSceneConfiguration);

//    TrafficSimulationView trafficSimulationView;
//    trafficSimulationView.setModel(&trafficSimulator);

//    SimulationThread simulationThread(&trafficSimulator);
//    simulationThread.start(refreshTimeInterval);

//    QTimer simulationTimer;
//    QObject::connect(&simulationTimer, &QTimer::timeout, &trafficSimulationView, [&]() {
//        trafficSimulationView.update();
//    });
//    simulationTimer.start(refreshTimeInterval);

//    trafficSimulationView.show();

//    return application.exec();
//} 

int main(int argc, char *argv[])
{
    QApplication application(argc, argv);

    ProjectModel projectModel;
    UsedFilesTrackerModel usedFilesTrackerModel;
    TrafficDataModel trafficDataModel;

    SoftwareMainWindowView softwareMainWindowView;
    softwareMainWindowView.setModels(&projectModel, &usedFilesTrackerModel, &trafficDataModel);

    // set style
    application.setStyle("fusion");
    application.setStyleSheet(StyleSheetParser::parseStyleSheet(":resources/styleSheets/Unity.qss"));

    softwareMainWindowView.setWindowIcon(QIcon(":/resources/windowIcon.png"));
    softwareMainWindowView.show();

    return application.exec();
}
