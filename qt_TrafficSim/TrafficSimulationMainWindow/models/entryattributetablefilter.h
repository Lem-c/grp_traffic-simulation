#ifndef ENTRYATTRIBUTETABLEFILTER_H
#define ENTRYATTRIBUTETABLEFILTER_H

#include <QSortFilterProxyModel>

/**
* @class <EntryAttributeTableFilter> [ENTRYATTRIBUTETABLEFILTER_H] [<ENTRYATTRIBUTETABLEFILTER_H]
* @brief The model response for the entry data stat and display as a table
* @note Extends the QSortFilterProxyModel
* Sort the table with a given filter
* The attribute will show on a table
* Set the enableFiltering and entryNumber
*/
class EntryAttributeTableFilter : public QSortFilterProxyModel
{
    Q_OBJECT
public:

    /** @brief The constructor of EntryAttributeTableFilter class
    * Accepts a QObject as parent
    * filter the table with enableFiltering and according to the entryNumber
    */
    explicit EntryAttributeTableFilter(QObject* parent = nullptr);

    /**  @brief filter the row according to the source row and the parent
       * @param[in] sourceRow : The source row
       * @param[in] sourceParent : The sourcre parent
       */
    virtual bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

    /**  @brief set method for the entry number
       * @param[in] entryNumber : set the entry number
       */
    inline void setEntryNumber(const int entryNumber);

    /**  @brief set method for whether enable the filtering
       * @param[in] enable : bool value of true of false
       */
    inline void setEnableFiltering(bool enable);

    /**  @brief get method for whether enableFiltering
       */
    inline bool getEnableFiltering();
    inline int getEntryNumber();

private:
    bool enableFiltering;
    int entryNumber;
};

void EntryAttributeTableFilter::setEnableFiltering(bool enable) {
    this->enableFiltering = enable;
    this->invalidateRowsFilter();
}

void EntryAttributeTableFilter::setEntryNumber(const int entryNumber) {
    this->entryNumber = entryNumber;
    if (enableFiltering) {
        this->invalidateRowsFilter();
    }
}

bool EntryAttributeTableFilter::getEnableFiltering(){
    return enableFiltering;
}

int EntryAttributeTableFilter::getEntryNumber(){
    return entryNumber;
}

#endif // ENTRYATTRIBUTETABLEFILTER_H
