#ifndef TRAFFICLIGHTATTRIBUTETABLEFILTER_H
#define TRAFFICLIGHTATTRIBUTETABLEFILTER_H

#include <QSortFilterProxyModel>

/**
* @class <TrafficLightAttributeTableFilter> [TRAFFICLIGHTATTRIBUTETABLEFILTER_H] [<TRAFFICLIGHTATTRIBUTETABLEFILTER_H]
* @brief The model response for the presentation of the traffic light
* @note Extends the QSortFilterProxyModel
* Display all data in a table
*/
class TrafficLightAttributeTableFilter : public QSortFilterProxyModel
{

    Q_OBJECT

public:

    /** @brief The constructor of TrafficLightAttributeTableFilter class
    * Accepts a QObject as parent
    */
    explicit TrafficLightAttributeTableFilter(QObject* parent = nullptr);

    /**  @brief filter the accepted row
       * @param[in] sourceRow : the source of the row
       * @param[in] sourceParent : the parent of source
       */
    virtual bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

    /**  @brief set the intersection number display value
       * @param[in] intersectionNumber : the number of intersection
       */
    inline void setIntersectionNumber(const int intersectionNumber);

    /**  @brief set the squence number display value
       * @param[in] sequenceNumber : the number of sequenceNumber
       */
    inline void setSequenceNumber(const int sequenceNumber);

    /**  @brief set whether enable filter the table
       * @param[in] enable : bool value of true or false
       */
    inline void setEnableFiltering(bool enable);

    /**  @brief get whether enable filter the table
       */
    inline bool getEnableFiltering();

    /**  @brief get the number of intersection
       */
    inline int getIntersectionNumber();

    /**  @brief get \the number of sequence
       */
    inline int getSequenceNumber();

private:
    bool enableFiltering;
    int intersectionNumber;
    int sequenceNumber;
};

void TrafficLightAttributeTableFilter::setEnableFiltering(bool enable) {
    this->enableFiltering = enable;
    this->invalidateRowsFilter();
}

void TrafficLightAttributeTableFilter::setIntersectionNumber(const int intersectionNumber) {
    this->intersectionNumber = intersectionNumber;
    if (enableFiltering) {
        this->invalidateRowsFilter();
    }
}

void TrafficLightAttributeTableFilter::setSequenceNumber(const int sequenceNumber) {
    this->sequenceNumber = sequenceNumber;
    if (enableFiltering) {
        this->invalidateRowsFilter();
    }
}

bool TrafficLightAttributeTableFilter::getEnableFiltering(){
    return enableFiltering;
}

int TrafficLightAttributeTableFilter::getIntersectionNumber(){
    return intersectionNumber;
}

int TrafficLightAttributeTableFilter::getSequenceNumber(){
    return sequenceNumber;
}

#endif // TRAFFICLIGHTATTRIBUTETABLEFILTER_H
