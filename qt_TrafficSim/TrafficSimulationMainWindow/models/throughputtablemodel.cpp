#include "throughputtablemodel.h"

const int ThroughputTableModel::columnNumber = 3;

ThroughputTableModel::ThroughputTableModel(QObject *parent)
    : QAbstractTableModel(parent), trafficDataModel(nullptr),
      horizontalHeaders({"Intersection Number", "Entry Number", "Throughput"})
{

}

void ThroughputTableModel::setSourceModel(TrafficDataModel* trafficDataModel) {
    this->trafficDataModel = trafficDataModel;

    connect(trafficDataModel, &TrafficDataModel::vehicleInputEntryChangedAt, this, &ThroughputTableModel::refreshModel);
}

int ThroughputTableModel::rowCount(const QModelIndex& parent) const {
    if (!trafficDataModel) {
        return 0;
    }

    if (parent.isValid()) {
        return 0;
    }

    return trafficDataModel->getThroughputEntries().size();
}

int ThroughputTableModel::columnCount(const QModelIndex& parent) const {
    if (!trafficDataModel) {
        return 0;
    }

    if (parent.isValid()) {
        return 0;
    }

    return columnNumber;
}

QVariant ThroughputTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            if (section >= 0 && section < horizontalHeaders.size()) {
                return horizontalHeaders.at(section);
            } else {
                throw UiException("Invalid section number.");
            }
        } else if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }
    } else {
        if (role == Qt::DisplayRole) {
            return section + 1;
        } else if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }
    }

    return QVariant();
}

QVariant ThroughputTableModel::data(const QModelIndex &index, int role) const {
    if (!trafficDataModel) {
        return 0;
    }

    if (!index.isValid()) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        switch (index.column()) {
            case 0:
                return std::get<0>(trafficDataModel->getThroughputEntries().at(index.row()));
            case 1:
                return std::get<1>(trafficDataModel->getThroughputEntries().at(index.row()));
            case 2:
                return std::get<2>(trafficDataModel->getThroughputEntries().at(index.row()));
            default:
                throw UiException("Invalid column number.");
        }
    }

    return QVariant();
}

Qt::ItemFlags ThroughputTableModel::flags(const QModelIndex& index) const {
    if (!index.isValid()) {
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

void ThroughputTableModel::refreshModel() {
    beginResetModel();
    endResetModel();
}
