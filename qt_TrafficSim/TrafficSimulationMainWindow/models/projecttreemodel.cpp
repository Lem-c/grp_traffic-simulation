#include "projecttreemodel.h"
#include <QString>

ProjectTreeModel::ProjectTreeModel(QObject* parent)
    : QAbstractItemModel(parent), projectModel(nullptr), projects(nullptr),
                    rootItem(new ProjectTreeModelItem(
                                          {QString("Project Attribute"), QString("Details")}))
{

}


ProjectTreeModel::~ProjectTreeModel() {
    delete rootItem;
}

int ProjectTreeModel::columnCount(const QModelIndex& parent) const {
    if (parent.isValid()) {
        return static_cast<ProjectTreeModelItem*>(parent.internalPointer())->getColumnNumber();
    }

    return rootItem->getColumnNumber();
}

int ProjectTreeModel::rowCount(const QModelIndex &parent) const {
    if (parent.column() > 0) {
        return 0;
    }

    ProjectTreeModelItem* parentItem;
    if (!parent.isValid()) {
        parentItem = rootItem;
    } else {
        parentItem = static_cast<ProjectTreeModelItem*>(parent.internalPointer());
    }

    return parentItem->getChildrenNumber();
}

QVariant ProjectTreeModel::data(const QModelIndex& index, int role) const {
    if (!index.isValid()) {
        return QVariant();
    }

    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    return static_cast<ProjectTreeModelItem*>(
                index.internalPointer())->data(index.column());
}

Qt::ItemFlags ProjectTreeModel::flags(const QModelIndex &index) const {
    if (!index.isValid()) {
        return Qt::NoItemFlags;
    }

    return QAbstractItemModel::flags(index);
}

QVariant ProjectTreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        return rootItem->data(section);
    }

    return QVariant();
}

QModelIndex ProjectTreeModel::index(int row, int column, const QModelIndex &parent) const {
    if (!hasIndex(row, column, parent)) {
        return QModelIndex();
    }

    ProjectTreeModelItem* parentItem;
    if (!parent.isValid()) {
        parentItem = rootItem;
    } else {
        parentItem = static_cast<ProjectTreeModelItem*>(parent.internalPointer());
    }

    ProjectTreeModelItem* childItem = parentItem->getChildAt(row);
    if (childItem) {
        return createIndex(row, column, childItem);
    }

    return QModelIndex();
}

QModelIndex ProjectTreeModel::parent(const QModelIndex &index) const {
    if (!index.isValid()) {
        return QModelIndex();
    }

    ProjectTreeModelItem* childItem = static_cast<ProjectTreeModelItem*>(index.internalPointer());
    ProjectTreeModelItem* parentItem = childItem->getParent();

    if (parentItem == rootItem) {
        return QModelIndex();
    }

    return createIndex(parentItem->row(), 0, parentItem);
}

void ProjectTreeModel::setSourceModel(const ProjectModel* projectModel) {
    this->projectModel = projectModel;
    projects = &(projectModel->getProjects());
    refreshModel();
}

void ProjectTreeModel::refreshModel() {
    if (projectModel) {
        beginResetModel();

        rootItem->clearChildren();

        int i = 1;
        for (const Project& project : *projects) {
            ProjectTreeModelItem* projectItem = new ProjectTreeModelItem({QString("Project %1").arg(i), QString()}, rootItem);
            rootItem->appendChild(projectItem);

            projectItem->appendChild(new ProjectTreeModelItem({QString("Project Name"), project.getProjectName()}, projectItem));
            projectItem->appendChild(new ProjectTreeModelItem({QString("User Name"), project.getUserName()}, projectItem));
            projectItem->appendChild(new ProjectTreeModelItem({QString("Horizontal Intersection Number"), project.getHorizontalIntersectionNumber()}, projectItem));
            projectItem->appendChild(new ProjectTreeModelItem({QString("Vertical Intersection Number"), project.getVerticalInersectionNumber()}, projectItem));
            projectItem->appendChild(new ProjectTreeModelItem({QString("Create Date"), project.getCreateDate().toString()}, projectItem));
            projectItem->appendChild(new ProjectTreeModelItem({QString("Last Modified Date"), project.getLastModifiedDate().toString()}, projectItem));

            ProjectTreeModelItem* projectEntryAttributesItem = new ProjectTreeModelItem({QString("Entry Attributes"), QString()}, projectItem);
            projectItem->appendChild(projectEntryAttributesItem);

            int j = 1;
            for (const ProjectAttribute* projectAttribute : project.getEntryAttributes()) {
                const EntryAttribute* entryAttribute = dynamic_cast<const EntryAttribute*>(projectAttribute);
                ProjectTreeModelItem* projectEntryAttributeItem = new ProjectTreeModelItem({QString("Attribute %1").arg(j), QString()}, projectEntryAttributesItem);
                projectEntryAttributesItem->appendChild(projectEntryAttributeItem);

                projectEntryAttributeItem->appendChild(new ProjectTreeModelItem({QString("Entry Number"),
                                                                                 entryAttribute->getEntryNumber()}, projectEntryAttributeItem));
                projectEntryAttributeItem->appendChild(new ProjectTreeModelItem({QString("Vehicle Initial Speed"),
                                                                                 entryAttribute->getVehicleInitialSpeed()}, projectEntryAttributeItem));

                j++;
            }

            ProjectTreeModelItem* projectTrafficLightAttributesItem = new ProjectTreeModelItem({QString("Traffic Light Attributes"), QString()}, projectItem);
            projectItem->appendChild(projectTrafficLightAttributesItem);

            int k = 1;
            for (const ProjectAttribute* projectAttribute : project.getTrafficLightAttributes()) {
                const TrafficLightAttribute* trafficLightAttribute = dynamic_cast<const TrafficLightAttribute*>(projectAttribute);
                ProjectTreeModelItem* projectTrafficLightAttributeItem = new ProjectTreeModelItem({QString("Attribute %1").arg(k), QString()}, projectTrafficLightAttributesItem);
                projectTrafficLightAttributesItem->appendChild(projectTrafficLightAttributeItem);

                projectTrafficLightAttributeItem->appendChild(new ProjectTreeModelItem({QString("Intersection Number"),
                                                                                 trafficLightAttribute->getIntersectionNumber()}, projectTrafficLightAttributeItem));
                projectTrafficLightAttributeItem->appendChild(new ProjectTreeModelItem({QString("Sequence Number"),
                                                                                 trafficLightAttribute->getRoadSequenceNumber()}, projectTrafficLightAttributeItem));
                projectTrafficLightAttributeItem->appendChild(new ProjectTreeModelItem({QString("Green Duration"),
                                                                                 trafficLightAttribute->getGreenDuration()}, projectTrafficLightAttributeItem));
                projectTrafficLightAttributeItem->appendChild(new ProjectTreeModelItem({QString("Yellow Duration"),
                                                                                 trafficLightAttribute->getYellowDuration()}, projectTrafficLightAttributeItem));
                projectTrafficLightAttributeItem->appendChild(new ProjectTreeModelItem({QString("Red Duration"),
                                                                                 trafficLightAttribute->getRedDuration()}, projectTrafficLightAttributeItem));

                k++;
            }

            i++;
        }

        endResetModel();
    }
}
