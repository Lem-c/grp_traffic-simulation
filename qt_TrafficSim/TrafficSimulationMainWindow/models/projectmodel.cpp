#include "projectmodel.h"
#include "../utilities/project.h"
#include "../views/saveprojectdialogview.h"
#include "../utilities/uiexception.h"
#include <QMessageBox>
#include <sstream>

ProjectModel::ProjectModel() :
    projects()
{

}

ProjectModel::~ProjectModel() {
    for (Project& project : projects) {
        for (ProjectAttribute* entryAttribute : project.getEntryAttributes()) {
            delete entryAttribute;
        }

        for (ProjectAttribute* trafficLightAttribute : project.getTrafficLightAttributes()) {
            delete trafficLightAttribute;
        }

        delete &project.getTrafficSceneConfiguration();
    }
}

const Project* ProjectModel::getProjectBy(const QString& projectName) const {
    for (const Project& project : projects) {
        if (QString::compare(project.getProjectName(), projectName) == 0) {
            return &project;
        }
    }

    return nullptr;
}

Project* ProjectModel::getChangeableProjectBy(const QString& projectName) {
    for (Project& project : projects) {
        if (QString::compare(project.getProjectName(), projectName) == 0) {
            return &project;
        }
    }

    return nullptr;
}

bool ProjectModel::containProject(const QString &projectName) const {
    for (const Project& project : projects) {
        if (QString::compare(project.getProjectName(), projectName) == 0) {
            return true;
        }
    }
    return false;
}


QStringList ProjectModel::getProjectNames() const {
    QStringList stringList;
    for (const Project& project : projects) {
        stringList << project.getProjectName();
    }

    return stringList;
}

void ProjectModel::saveProjectInto(const QString& projectName, const QString& pathToFile) {
    const Project* projectToBeSaved = getProjectBy(projectName);
    if (!projectToBeSaved) {
        throw UiException("Project name is not found.");
    }

    QFile file(pathToFile);
    JsonCoder::saveProjectInto(*projectToBeSaved, file);
}

void ProjectModel::loadProjectFrom(const QString& pathToFile) {
    QFile file(pathToFile);
    Project projectLoadedFromFile(JsonCoder::loadProjectFrom(file));
    if (containProject(projectLoadedFromFile.getProjectName())) {
        Project* originalProject = getChangeableProjectBy(projectLoadedFromFile.getProjectName());
        *originalProject = projectLoadedFromFile;
        emit projectReset(*originalProject);
    } else {
        projects.append(projectLoadedFromFile);
        emit projectAddedAt(projects.size() - 1, projectLoadedFromFile);
    }
}

void ProjectModel::addProjectAt(const int order, const Project& project) {
    if (!containProject(project.getProjectName())) {
        projects.insert(order, project);
        emit projectAddedAt(order, project);
    } else {
        throw UiException("Project already exists. Cannot add a new project with the same name as one existing project.");
    }
}

//void ProjectModel::addProjectAttributeAt(const ProjectAttributeIndex& attributeIndex) {
//    Project* project = nullptr;
//    if (!(project = getChangeableProjectBy(attributeIndex.getProjectName()))) {
//        throw UiException("Cannot find the project by projectName");
//    }

//    if (attributeIndex.getAttributeType() == ProjectAttribute::Type::EntryAttributeType) {
//        project->addAttributeAt(attributeIndex.getAttributeType(), attributeIndex.getOrder(), new EntryAttribute());
//    } else if (attributeIndex.getAttributeType() == ProjectAttribute::Type::TrafficLightAttributeType) {
//        project->addAttributeAt(attributeIndex.getAttributeType(), attributeIndex.getOrder(), new TrafficLightAttribute());
//    } else {
//        throw UiException("Invalid attribute type.");
//    }

//    emit projectAttributeAddedAt(attributeIndex);
//}

void ProjectModel::setProjectAttributeBy(const ProjectAttributeIndex& attributeIndex, const QVariant& newValue) {
    Project* project = nullptr;
    if (!(project = getChangeableProjectBy(attributeIndex.getProjectName()))) {
        throw UiException("Cannot find the project by projectName");
    }

    project->setAttributeBy(attributeIndex.getAttributeType(), attributeIndex.getOrder(),
                            attributeIndex.getSubAttributeNumber(), newValue);

    emit projectAttributeChangedAt(attributeIndex);
}

//void ProjectModel::removeProjectAttributeAt(const ProjectAttributeIndex& attributeIndex) {
//    Project* project = nullptr;
//    if (!(project = getChangeableProjectBy(attributeIndex.getProjectName()))) {
//        throw UiException("Cannot find the project by projectName");
//    }

//    project->removeAttributeAt(attributeIndex.getAttributeType(), attributeIndex.getOrder());

//    emit projectAttributeRemovedAt(attributeIndex);
//}

