#ifndef VEHICLEINPUTTABLEMODEL_H
#define VEHICLEINPUTTABLEMODEL_H

#include <QAbstractTableModel>
#include "trafficdatamodel.h"

/**
* @class <VehicleInputTableModel> [VEHICLEINPUTTABLEMODEL_H] [<VEHICLEINPUTTABLEMODEL_H]
* @brief The model response for the presentation of the vehicle input table
* @note Extends the QAbstractTableModel
* Display all data in a table
*/
class VehicleInputTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:

    /** @brief The constructor of VehicleInputTableModel class
    * Accepts a QObject as parent
    * Set the header and the data model
    */
    explicit VehicleInputTableModel(QObject* parent = nullptr);

    /**  @brief count the number of row
       * @param[in] parent : A QModelIndex
       */
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    /**  @brief count the number of column
       * @param[in] parent : A QModelIndex
       */
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    /**  @brief get the data from the model
       * @param[in] index : A QModelIndex
       * @param[in] role : The QT Displayrole
       */
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

    /**  @brief get the flag from the model
       * @param[in] index : A QModelIndex
       */
    virtual Qt::ItemFlags flags(const QModelIndex& index) const override;

    /**  @brief header data
       * @param[in] orientation : the orientation
       * @param[in] section : A QModelIndex
       */
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    /**  @brief set the data flow source model
       * @param[in] trafficDataModel : the model set as the data flow
       */
    void setSourceModel(TrafficDataModel* trafficDataModel);

    /** @brief The couumn number of the medel*/
    static const int columnNumber;

public slots:
    void refreshModel();

private:
    TrafficDataModel* trafficDataModel;
    QList<QString> horizontalHeaders;
};

#endif // VEHICLEINPUTTABLEMODEL_H
