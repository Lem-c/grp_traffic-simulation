#ifndef VEHICLEINPUTCHARTMODEL_H
#define VEHICLEINPUTCHARTMODEL_H

#include <QChart>
#include <QPieSeries>
#include <QPieSlice>
#include "trafficdatamodel.h"

/**
* @class <VehicleInputChartModel> [VEHICLEINPUTCHARTMODEL_H] [<VEHICLEINPUTCHARTMODEL_]
* @brief The model present the vehicle input
* @note Extends the QChart
* Display all data in a chart
*/
class VehicleInputChartModel : public QChart
{
    Q_OBJECT
public:

    /** @brief The constructor of VehicleInputChartModel class
    * Accepts a QObject as parent
    * A set windowFLags
    */
    VehicleInputChartModel(QGraphicsItem* parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());

    /** @brief The de-constructor of VehicleInputChartModel class
     *  delete pieSeries;
    */
    ~VehicleInputChartModel();

    /**  @brief set the source model of vehicle input chart
       * @param[in] trafficDataModel : a data model will be set
       */
    void setSourceModel(const TrafficDataModel* trafficDataModel);

//    void addBreakdownSeries(QPieSeries *breakdownSeries, QColor color);
private:
    QPieSeries* pieSeries;

//private:
//    void recalculateAngles();
//    void updateLegendMarkers();
};

#endif // VEHICLEINPUTCHARTMODEL_H
