#ifndef THROUGHPUTOVERVIEWCHARTMODEL_H
#define THROUGHPUTOVERVIEWCHARTMODEL_H

#include <QChart>
#include <QBarSet>
#include <QHorizontalStackedBarSeries>
#include <QValueAxis>
#include <QBarCategoryAxis>
#include "trafficdatamodel.h"

/**
* @class <ThroughputOverviewChartModel> [THROUGHPUTOVERVIEWCHARTMODEL_H] [<THROUGHPUTOVERVIEWCHARTMODEL_H]
* @brief The model response for vehicles throughput and display it in the chart
* @note Extends the QChart
*/
class ThroughputOverviewChartModel : public QChart
{
public:

    /** @brief The constructor of ThroughputOverviewChartModel class
    * Accepts a QGraphicsItem as parent
    * check and set flags
    * Set the title animation and theme for the chart
    */
    ThroughputOverviewChartModel(QGraphicsItem* parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());

    /** @brief The de-constructor of ThroughputOverviewChartModel class
    *     delete sequence0BarSet;
          delete sequence1BarSet;
          delete sequence2BarSet;
          delete sequence3BarSet;
          delete horizontalStackedBarSeries;
          delete xAxis;
          delete yAxis;
    */
    ~ThroughputOverviewChartModel();

    /**  @brief Set the source of the model
       * @param[in] trafficDataModel : set the data flow from this
       */
    void setSourceModel(TrafficDataModel* trafficDataModel);

private:
    QBarSet* sequence0BarSet;
    QBarSet* sequence1BarSet;
    QBarSet* sequence2BarSet;
    QBarSet* sequence3BarSet;
    QHorizontalStackedBarSeries* horizontalStackedBarSeries;
    QValueAxis* xAxis;
    QBarCategoryAxis* yAxis;
};



//#include <QWidget>

//class HorizontalColumnModel;

//namespace Ui {
//class HorizontalColumnView;
//}

//class ThroughputOverviewChartView : public QWidget
//{
//    Q_OBJECT

//public:
//    explicit ThroughputOverviewChartView(QWidget *parent = nullptr);
//    ~ThroughputOverviewChartView();

//    void setChart(HorizontalColumnModel* _HorizontalColumnModel);

//private:
//    Ui::HorizontalColumnView *ui;
//};

#endif // THROUGHPUTOVERVIEWCHARTMODEL_H
