#ifndef THROUGHPUTCHARTMODEL_H
#define THROUGHPUTCHARTMODEL_H

#include <QChart>
#include <QBarSet>
#include <QBarSeries>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include "trafficdatamodel.h"

/**
* @class <ThroughputChartModel> [THROUGHPUTCHARTMODEL_H] [<THROUGHPUTCHARTMODEL_H]
* @brief The chart response the throuhtput of curren time period
* @note Extends the QChart
* The value connect with vehicle data
* @class trafficdatamodel "trafficdatamodel.h"
*/
class ThroughputChartModel : public QChart
{
    Q_OBJECT
public:

    /**  @brief define the type of entryID, avoid the overlap<int, int>
       */
    typedef std::pair<int, int> EntryId;

    /**  @brief The construcotr of throughtPutChartModel
       * @param[in] parent : the draw canvas
       * @param[in] flags : the qt window flag
       */
    ThroughputChartModel(QGraphicsItem* parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());

    /**  @brief The de-constructor of class
       */
    ~ThroughputChartModel();
    void setSourceModel(const TrafficDataModel* trafficDataModel);

private:
    QList<EntryId> entryIds;
    QList<QString> intersectionAndSequenceNumbers;
    QBarSet* barSet;
    QBarSeries* barSeries;
    QBarCategoryAxis* xAxis;
    QValueAxis* yAxis;
};

#endif // THROUGHPUTCHARTMODEL_H
