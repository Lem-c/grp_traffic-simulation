#ifndef PROJECTMODEL_H
#define PROJECTMODEL_H

#include <QList>
#include <QFile>
#include <QTableView>
#include <QPlainTextEdit>
#include "../utilities/jsoncoder.h"
#include "../utilities/project.h"
#include "../utilities/projectattribute.h"
#include "../utilities/projectattributeindex.h"

class SaveProjectDialogView;

/**
* @class <ProjectModel> [PROJECTMODEL_H] [<PROJECTMODEL_H]
* @brief The class made for the project set and edition
* @note Extends the QObject
* Multi reletaed infomation about the project
* set and reste the project
* check state
* save and load project json file
*/
class ProjectModel : public QObject
{

    Q_OBJECT

public:
    /** @brief The constructor of ProjectModel class
    *   Set project object
    */
    ProjectModel();

    /** @brief The de-constructor of ProjectModel class
        *   delete entry and traffic attibute
        *   delete configuration object
        */
    ~ProjectModel();

    /**  @brief method get the project
       * @param[in] projectName : search the project according to the project
       *                          name
       */
    const Project* getProjectBy(const QString& projectName) const;

    /**  @brief Method to check whether project list has current project
       * @param[in] projectName : check whether has this project in the list
       */
    bool containProject(const QString& projectName) const;

    /**  @brief Check whether project is empty
       * check whether this project is emoty or not set yet
       */
    inline bool isEmpty() const;

    /**  @brief Ruturn current project list
       * return current project.
       */
    inline const QList<Project>& getProjects() const;

    /**  @brief get the name of the project
       * @return the project name in the type of QString.
       */
    QStringList getProjectNames() const;

    /**  @brief Add the project into the give order/position
       * @param[in] order : the insert order of added project
       * @param[in] project : the project will be add in
       */
    void addProjectAt(const int order, const Project& project);

    /**  @brief Set the project attribute
       * @param[in] attributeIndex : the base of the sort index
       * @param[in] newValue : the new value will be set to the project
       */
    void setProjectAttributeBy(const ProjectAttributeIndex& attributeIndex, const QVariant& newValue);
//    void removeProjectAttributeAt(const ProjectAttributeIndex& attributeIndex);

    /**  @brief Save current project into the disk
       * @param[in] projectName : The save name of the project file
       * @param[in] pathTofile : The file directory of saved project
       */
    void saveProjectInto(const QString& projectName, const QString& pathTofile);

    /**  @brief Load a project from the disk with JSON type
       * @param[in] pathTofile : The file directory of saved project
       */
    void loadProjectFrom(const QString& pathToFile);

signals:
    void projectAddedAt(const int order, const Project& addedProject);
    void projectReset(const Project& resetProject);
//    void projectAttributeAddedAt(const ProjectAttributeIndex& attributeIndex);
    void projectAttributeChangedAt(const ProjectAttributeIndex& attributeIndex);
//    void projectAttributeRemovedAt(const ProjectAttributeIndex& attributeIndex);

private:
    QList<Project> projects;

    Project* getChangeableProjectBy(const QString& projectName);
};

bool ProjectModel::isEmpty() const {
    return projects.isEmpty();
}

const QList<Project>& ProjectModel::getProjects() const {
    return projects;
}

#endif // PROJECTMODEL_H
