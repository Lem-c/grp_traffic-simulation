#ifndef TRAFFICDATAMODEL_H
#define TRAFFICDATAMODEL_H

#include <QObject>
#include "../utilities/uiexception.h"

/**
* @class <TrafficDataModel> [TRAFFICDATAMODEL_H] [<uiexception.h]
* @brief The model contains all traffic simulation data
* @note Extends the QObject
* Will be used by another model to get the data inside and present the
* result by them
*/
class TrafficDataModel : public QObject
{
    Q_OBJECT
public:
    typedef std::pair<int, double> AverageSpeedEntry;
    typedef std::tuple<int, int, double> ThroughputEntry;
    typedef std::pair<int, double> VehicleInputEntry;


    /** @brief The constructor of TrafficDataModel class
    * Has default speed, throughput entry and vehicle input
    */
    TrafficDataModel();

    /**  @brief Add a average speed information at current data
       * @param[in] time : Thhe added time
       * @param[in] acerageSpeed : Add average speed for the table
       */
    inline void addAverageSpeed(int time, double averageSpeed);

    /**  @brief Add throught entry according to the intersection and sequence
       * @param[in] intersectionNumber : The intersection number
       * @param[in] sequenceNumber : The sequence number
       * @param[in] throughpu : the new throughput
       */
    inline void addThroughputEntryBy(int intersectionNumber, int sequenceNumber, double throughput);

    /**  @brief According to the given data, set the throughput entry
       * @param[in] intersectionNumber : The intersection number
       * @param[in] sequenceNumber : The sequence number
       * @param[in] throughpu : the new throughput
       */
    inline void setThroughputEntryBy(int intersectionNumber, int sequenceNumber, double newThroughput);

    /**  @brief Add vehicle input entry by parameters
       * @param[in] entryNumber : The entry number
       * @param[in] vehicleInput : The input value of vehicle
       */
    inline void addVehicleInputEntryBy(int entryNumber, double vehicleInput);

    /**  @brief Set vehicle input entry by parameters
       * @param[in] entryNumber : The entry number
       * @param[in] vehicleInput : The input value of vehicle
       */
    inline void setVehicleInputEntryBy(int entryNumber, double newVehicleInput);

    inline const QList<ThroughputEntry>& getThroughputEntries() const;
    inline const QList<VehicleInputEntry>& getVehicleInputEntries() const;

signals:
    void averageSpeedChangedAt(int time, double averageSpeed);
    void throughputEntryChangedAt(int intersectionNumber, int sequenceNumber, double newThroughput);
    void vehicleInputEntryChangedAt(int entryNumber, double newVehicleInput);


private:
    QList<AverageSpeedEntry> averageSpeeds;
    QList<ThroughputEntry> throughputEntries;
    QList<VehicleInputEntry> vehicleInputEntries;
};

void TrafficDataModel::addAverageSpeed(int time, double averageSpeed) {
    averageSpeeds.append(AverageSpeedEntry(time, averageSpeed));

    emit averageSpeedChangedAt(time, averageSpeed);
}

void TrafficDataModel::addThroughputEntryBy(int intersectionNumber, int sequenceNumber, double throughput) {
    throughputEntries.append(ThroughputEntry(intersectionNumber, sequenceNumber, throughput));
}

void TrafficDataModel::setThroughputEntryBy(int intersectionNumber, int sequenceNumber, double newThroughput) {
    QList<ThroughputEntry>::iterator targetEntry = std::find_if(throughputEntries.begin(), throughputEntries.end(),
                                                                [=](const ThroughputEntry& throughputEntry) {
        return std::get<0>(throughputEntry) == intersectionNumber && std::get<1>(throughputEntry) == sequenceNumber;
    });

    if (targetEntry == throughputEntries.end()) {
        throw UiException("Cannot find the throughput entry.");
    }

    std::get<2>(*targetEntry) = newThroughput;

    emit throughputEntryChangedAt(intersectionNumber, sequenceNumber, newThroughput);
}

void TrafficDataModel::addVehicleInputEntryBy(int entryNumber, double vehicleInput) {
    vehicleInputEntries.append(VehicleInputEntry(entryNumber, vehicleInput));
}

void TrafficDataModel::setVehicleInputEntryBy(int entryNumber, double newVehicleInput) {
    QList<VehicleInputEntry>::iterator targetEntry = std::find_if(vehicleInputEntries.begin(), vehicleInputEntries.end(),
                                                                [=](const VehicleInputEntry& vehicleInputEntry) {
        return vehicleInputEntry.first == entryNumber;
    });

    if (targetEntry == vehicleInputEntries.end()) {
        throw UiException("Cannot find the vehicle input entry.");
    }

    targetEntry->second = newVehicleInput;

    emit vehicleInputEntryChangedAt(entryNumber, newVehicleInput);
}

const QList<TrafficDataModel::ThroughputEntry>& TrafficDataModel::getThroughputEntries() const {
    return throughputEntries;
}

const QList<TrafficDataModel::VehicleInputEntry>& TrafficDataModel::getVehicleInputEntries() const {
    return vehicleInputEntries;
}

#endif // TRAFFICDATAMODEL_H
