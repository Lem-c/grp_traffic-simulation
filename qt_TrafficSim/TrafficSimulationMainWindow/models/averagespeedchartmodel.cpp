#include "averagespeedchartmodel.h"
#include "../utilities/uiexception.h"

AverageSpeedChartModel::AverageSpeedChartModel(QGraphicsItem* parent, Qt::WindowFlags flags) :
    QChart(parent, flags), splineSeries(new QSplineSeries()), xAxis(new QValueAxis()), yAxis(new QValueAxis())
{
    this->setTitle("Average Speed Chart");
    this->setAnimationOptions(QChart::NoAnimation);
    this->setTheme(QChart::ChartThemeDark);
}

AverageSpeedChartModel::~AverageSpeedChartModel() {
    delete splineSeries;
    delete xAxis;
    delete yAxis;
}

void AverageSpeedChartModel::setSourceModel(const TrafficDataModel* trafficDataModel) {
    splineSeries->append(0.0, 0.0);

    xAxis->setTitleText("Time");
    yAxis->setTitleText("Average Speed");
    this->legend()->hide();

    this->addSeries(splineSeries);
    this->addAxis(xAxis, Qt::AlignBottom);
    this->addAxis(yAxis, Qt::AlignLeft);
    splineSeries->attachAxis(xAxis);
    splineSeries->attachAxis(yAxis);
    xAxis->setRange(0, 3000);
    yAxis->setRange(0, 150);
//    this->createDefaultAxes();

    connect(trafficDataModel, &TrafficDataModel::averageSpeedChangedAt, this, [=](int time, double averageSpeed) {
        splineSeries->append(time, averageSpeed);
        if (time >= xAxis->max()) {
            xAxis->setMax(time + 3000);
            xAxis->setMin(xAxis->max() - 3000);
        }
    });
}

//void AverageSpeedChartModel::addNewRecord(const TimeSpeedPair& record) {
//    if (series.at(series.count() - 1).x() >= record.first) {
//        throw UiException("Record's time is smaller than the latest one.");
//    }

//    series.append(record.first, record.second);
//    this->scroll(record.first - series.at(series.count() - 2).x(), 0);
//}
