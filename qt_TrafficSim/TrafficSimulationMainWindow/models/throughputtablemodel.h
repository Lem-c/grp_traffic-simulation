#ifndef THROUGHPUTTABLEMODEL_H
#define THROUGHPUTTABLEMODEL_H

#include <QAbstractItemModel>
#include "trafficdatamodel.h"

/**
* @class <ThroughputTableModel> [THROUGHPUTTABLEMODEL_H] [<THROUGHPUTTABLEMODEL_H]
* @brief The model response for the presentation of the throughput data
* @note Extends the QAbstractTableModel
* Display all data in a table
*/
class ThroughputTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:

    /** @brief The constructor of ThroughputTableModel class
    * Accepts a QObject as parent
    */
    explicit ThroughputTableModel(QObject *parent = nullptr);

    /**  @brief count the number of the row
       * @param[in] parent : The source model parent
       */
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    /**  @brief count the number of the column
       * @param[in] parent : The source model parent
       */
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    /**  @brief The throughput data flow method
       * @param[in] index
       * @param[in] role
       */
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

    /**  @brief Get the item flags from index value
       * @param[in] index : the modex index value
       */
    virtual Qt::ItemFlags flags(const QModelIndex& index) const override;

    /**  @brief Get header Data from the section and orientation with role
       * @param[in] section : The source section
       * @param[in] orientation : The sourcre orientation
       * @param[in] role : role value
       */
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    /**  @brief Set the data flow from the data model
       * @param[in] trafficDataModel : The data model will be set
       */
    void setSourceModel(TrafficDataModel* trafficDataModel);

    /** @brief The column number will be count*/
    static const int columnNumber;

public slots:
    void refreshModel();

private:
    TrafficDataModel* trafficDataModel;
    QList<QString> horizontalHeaders;
};

#endif // THROUGHPUTTABLEMODEL_H
