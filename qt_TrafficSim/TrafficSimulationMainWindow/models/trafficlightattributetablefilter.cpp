#include "trafficlightattributetablefilter.h"

TrafficLightAttributeTableFilter::TrafficLightAttributeTableFilter(QObject* parent) :
    QSortFilterProxyModel(parent), enableFiltering(false), intersectionNumber(0), sequenceNumber(0)
{

}

bool TrafficLightAttributeTableFilter::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const {
    if (!enableFiltering) {
        return true;
    }

    QModelIndex index0 = sourceModel()->index(sourceRow, 0, sourceParent);
    QModelIndex index1 = sourceModel()->index(sourceRow, 1, sourceParent);

    return sourceModel()->data(index0).toInt() == intersectionNumber &&
            sourceModel()->data(index1).toInt() == sequenceNumber;
}
