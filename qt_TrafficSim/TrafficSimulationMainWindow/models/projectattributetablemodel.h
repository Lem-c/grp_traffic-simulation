#ifndef PROJECTATTRIBUTETABLEMODEL_H
#define PROJECTATTRIBUTETABLEMODEL_H

#include <QAbstractTableModel>
#include "../utilities/projectattribute.h"
#include "../utilities/project.h"

class ProjectModel;

/**
* @class <ProjectAttributeTableModel> [PROJECTATTRIBUTETABLEMODEL_H] [<PROJECTATTRIBUTETABLEMODEL_H]
* @brief The model response for view the projetc attribute in table
* @note Extends the QAbstractTableModel
* Fill the  table with project attributes
* Including the project modle, select project name, selected project
* QList<ProjectAttribute*>* for point reference the project
* attributeType in type of projectAttribute
* QList<QString> to save the project header horizontal
*/
class ProjectAttributeTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:

    /** @brief The constructor of ProjectAttributeTableModel class
    * Accepts a QObject as parent
    * attributeType to set the attribute type of the project
    */
    explicit ProjectAttributeTableModel(ProjectAttribute::Type attributeType, QObject* parent = nullptr);

    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    virtual bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
    virtual Qt::ItemFlags flags(const QModelIndex& index) const override;
//    virtual bool insertRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;
//    virtual bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;    

    void setSourceModel(ProjectModel* projectModel);

public slots:
    void refreshModel();
    void setSelectedProjectName(const QString& projectName);

private:
    ProjectModel* projectModel;
    QString selectedProjectName;
    const Project* selectedProject;
    const QList<ProjectAttribute*>* selectedProjectAttributes;
    ProjectAttribute::Type attributeType;
    QList<QString> horizontalHeaders;
};

#endif // PROJECTATTRIBUTETABLEMODEL_H
