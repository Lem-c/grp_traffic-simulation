#include "vehicleinputtablemodel.h"

const int VehicleInputTableModel::columnNumber = 2;

VehicleInputTableModel::VehicleInputTableModel(QObject *parent)
    : QAbstractTableModel(parent), trafficDataModel(nullptr), horizontalHeaders({"Entry Number", "Current Vehicle Input"})
{

}

void VehicleInputTableModel::setSourceModel(TrafficDataModel* trafficDataModel) {
    this->trafficDataModel = trafficDataModel;

    connect(trafficDataModel, &TrafficDataModel::vehicleInputEntryChangedAt, this, &VehicleInputTableModel::refreshModel);
}

int VehicleInputTableModel::rowCount(const QModelIndex& parent) const {
    if (!trafficDataModel) {
        return 0;
    }

    if (parent.isValid()) {
        return 0;
    }

    return trafficDataModel->getVehicleInputEntries().size();
}

int VehicleInputTableModel::columnCount(const QModelIndex& parent) const {
    if (!trafficDataModel) {
        return 0;
    }

    if (parent.isValid()) {
        return 0;
    }

    return columnNumber;
}

QVariant VehicleInputTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            if (section >= 0 && section < horizontalHeaders.size()) {
                return horizontalHeaders.at(section);
            } else {
                throw UiException("Invalid section number.");
            }
        } else if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }
    } else {
        if (role == Qt::DisplayRole) {
            return section + 1;
        } else if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }
    }

    return QVariant();
}

QVariant VehicleInputTableModel::data(const QModelIndex &index, int role) const {
    if (!trafficDataModel) {
        return 0;
    }

    if (!index.isValid()) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        return index.column() == 0 ? trafficDataModel->getVehicleInputEntries().at(index.row()).first :
                                     trafficDataModel->getVehicleInputEntries().at(index.row()).second;
    }

    return QVariant();
}

Qt::ItemFlags VehicleInputTableModel::flags(const QModelIndex& index) const {
    if (!index.isValid()) {
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

void VehicleInputTableModel::refreshModel() {
    beginResetModel();
    endResetModel();
}
