#include "projectattributetablemodel.h"
#include "../utilities/uiexception.h"
#include "../utilities/entryattribute.h"
#include "../utilities/trafficlightattribute.h"
#include "projectmodel.h"
#include <typeinfo>

ProjectAttributeTableModel::ProjectAttributeTableModel(ProjectAttribute::Type attributeType, QObject* parent)
    : QAbstractTableModel(parent), projectModel(nullptr),
      selectedProjectName(QString()), selectedProject(nullptr),
      selectedProjectAttributes(nullptr),
      attributeType(attributeType), horizontalHeaders()
{
    if (attributeType == ProjectAttribute::Type::EntryAttributeType) {
        horizontalHeaders << "Entry Number" << "Vehicle Enter Rate";
    } else if (attributeType == ProjectAttribute::Type::TrafficLightAttributeType) {
        horizontalHeaders << "Intersection Number" << "Sequence Number" << "Green Duration"
                          << "Yellow Duration" << "Red Duration";
    } else {
        throw UiException("Unsupported attribute type.");
    }
}

void ProjectAttributeTableModel::setSourceModel(ProjectModel* projectModel) {
    this->projectModel = projectModel;
}

void ProjectAttributeTableModel::setSelectedProjectName(const QString& projectName) {
    beginResetModel();

    selectedProjectName = projectName;
    selectedProject = projectModel->getProjectBy(projectName);
    if (attributeType == ProjectAttribute::Type::EntryAttributeType) {
        selectedProjectAttributes = &(selectedProject->getEntryAttributes());
    } else {
        selectedProjectAttributes = &(selectedProject->getTrafficLightAttributes());
    }

    endResetModel();
}

int ProjectAttributeTableModel::rowCount(const QModelIndex& parent) const {
    if (!projectModel) {
        return 0;
    }

    if (parent.isValid()) {
        return 0;
    }

    return selectedProjectAttributes->size();
}

int ProjectAttributeTableModel::columnCount(const QModelIndex& parent) const {
    if (!projectModel) {
        return 0;
    }

    if (parent.isValid()) {
        return 0;
    }

    switch (attributeType) {
       case ProjectAttribute::Type::EntryAttributeType:
            return EntryAttribute::subAttributeCount;
       case ProjectAttribute::Type::TrafficLightAttributeType:
            return TrafficLightAttribute::subAttributeCount;
       default:
            throw UiException("Unsupported attribute type.");
    }
}

QVariant ProjectAttributeTableModel::data(const QModelIndex& index, int role) const {
    if (!projectModel) {
        return 0;
    }

    if (!index.isValid()) {
        return QVariant();
    }

    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        return selectedProjectAttributes->at(index.row())->getSubAttributeBy(index.column());
    }

    return QVariant();
}

bool ProjectAttributeTableModel::setData(const QModelIndex& index, const QVariant& value, int role) {
    if (!projectModel) {
        return false;
    }

    if (index.isValid() && role == Qt::EditRole) {
        projectModel->setProjectAttributeBy(ProjectAttributeIndex(selectedProjectName,
                                                                  attributeType, index.row(), index.column()),
                                            value);

        emit dataChanged(index.siblingAtRow(0), index.siblingAtRow(rowCount() - 1), QList<int>() << role);

        return true;
    }

    return false;
}

Qt::ItemFlags ProjectAttributeTableModel::flags(const QModelIndex& index) const {
    if (!index.isValid()) {
        return Qt::NoItemFlags;
    }

    if (attributeType == ProjectAttribute::Type::EntryAttributeType) {
        if (index.column() == 0) {
            return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
        }
    } else {
        if (index.column() == 0 || index.column() == 1 || index.column() == 3) {
            return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
        }
    }
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

//bool ProjectAttributeTableModel::insertRows(int row, int count, const QModelIndex& parent) {
//    if (!projectModel) {
//        return false;
//    }

//    if (row < 0 || count < 1 || row > rowCount()) {
//        return false;
//    }

//    beginInsertRows(parent, row, row + count - 1);

//    int i = row;
//    while (i < row + count) {
//            projectModel->addProjectAttributeAt(ProjectAttributeIndex(selectedProjectName,
//                                                                      attributeType, i));
//        i++;
//    }

//    endInsertRows();

//    return true;
//}

//bool ProjectAttributeTableModel::removeRows(int row, int count, const QModelIndex& parent) {
//    if (!projectModel) {
//        return false;
//    }

//    if (row < 0 || count < 1 || row + count > rowCount()) {
//        return false;
//    }

//    beginRemoveRows(parent, row, row + count - 1);

//    int i = row;
//    while (i < row + count) {
//        projectModel->removeProjectAttributeAt(ProjectAttributeIndex(selectedProjectName,
//                                                                     attributeType, i));
//        i++;
//    }

//    endRemoveRows();

//    return true;
//}

QVariant ProjectAttributeTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            if (section >= 0 && section < horizontalHeaders.size()) {
                return horizontalHeaders.at(section);
            } else {
                throw UiException("Invalid section number.");
            }
        } else if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }
    } else {
        if (role == Qt::DisplayRole) {
            return section + 1;
        } else if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }
    }

    return QVariant();
}

void ProjectAttributeTableModel::refreshModel() {
    beginResetModel();
    endResetModel();
}
