#include "throughputchartmodel.h"

ThroughputChartModel::ThroughputChartModel(QGraphicsItem* parent, Qt::WindowFlags flags) :
    QChart(parent, flags), entryIds(),
    barSet(new QBarSet("Intersections")), barSeries(new QBarSeries()),
    xAxis(new QBarCategoryAxis()), yAxis(new QValueAxis())
{
    this->setTheme(QChart::ChartThemeDark);
    this->setTitle("Throughput Chart");
    this->setAnimationOptions(QChart::NoAnimation);
}

ThroughputChartModel::~ThroughputChartModel() {
    delete barSet;
    delete barSeries;
    delete xAxis;
    delete yAxis;
}

void ThroughputChartModel::setSourceModel(const TrafficDataModel* trafficDataModel) {
    for (const TrafficDataModel::ThroughputEntry& throughputEntry : trafficDataModel->getThroughputEntries()) {
        entryIds.append(EntryId(std::get<0>(throughputEntry), std::get<1>(throughputEntry)));
        barSet->append(std::get<2>(throughputEntry));
        intersectionAndSequenceNumbers.append(QString::fromStdString(std::to_string(std::get<0>(throughputEntry))) +
                                              QString(", ") + QString::fromStdString(std::to_string(std::get<1>(throughputEntry))));
    }

    barSeries->append(barSet);

    this->addSeries(barSeries);

    xAxis->append(intersectionAndSequenceNumbers);
    this->addAxis(xAxis, Qt::AlignBottom);
    barSeries->attachAxis(xAxis);

    yAxis->setRange(0, 100);
    this->addAxis(yAxis, Qt::AlignLeft);
    barSeries->attachAxis(yAxis);

    this->legend()->hide();

    connect(trafficDataModel, &TrafficDataModel::throughputEntryChangedAt, this, [=](int intersectionNumber, int sequenceNumber, double newThroughput) {
        int i = 0;
        for (EntryId& entryId : entryIds) {
            if (entryId.first == intersectionNumber && entryId.second == sequenceNumber) {
                break;
            }
            i++;
        }

        barSet->replace(i, newThroughput);
    });
}
