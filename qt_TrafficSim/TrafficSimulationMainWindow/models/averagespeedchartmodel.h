#ifndef AVERAGESPEEDCHARTMODEL_H
#define AVERAGESPEEDCHARTMODEL_H

#include <QChart>
#include <QSplineSeries>
#include <QValueAxis>
#include "trafficdatamodel.h"

/**
* @class <AverageSpeedChartModel> [AVERAGESPEEDCHARTMODEL_H] [<AVERAGESPEEDCHARTMODEL_H]
* @brief The model response for the average speed present
* @note Extends the QChart
*
* The speed will show on a pire chart like dash board
* Set the x and y axis to locate
*/
class AverageSpeedChartModel : public QChart
{
    Q_OBJECT
public:
    typedef std::pair<int, double> TimeSpeedPair;
    /** @brief
    *The constructor of average speed chart
    *Accepts a wight item as parent to draw on and a window flag
    */
    AverageSpeedChartModel(QGraphicsItem* parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());
    /** @brief The de-constructor of class*/
    ~AverageSpeedChartModel();

    /**  @brief set the data source model of the model
       * @param[in] trafficDataModel : The accept data model(data flow)
       */
    void setSourceModel(const TrafficDataModel* trafficDataModel);

private:
    QSplineSeries* splineSeries;
    QValueAxis* xAxis;
    QValueAxis* yAxis;
};

#endif // AVERAGESPEEDCHARTMODEL_H
