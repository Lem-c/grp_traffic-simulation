#include "throughputoverviewchartmodel.h"

ThroughputOverviewChartModel::ThroughputOverviewChartModel(QGraphicsItem* parent, Qt::WindowFlags flags) :
    QChart(parent, flags), sequence0BarSet(new QBarSet("Sequence 0")),
    sequence1BarSet(new QBarSet("Sequence 1")), sequence2BarSet(new QBarSet("Sequence 2")),
    sequence3BarSet(new QBarSet("Sequence 3")), horizontalStackedBarSeries(new QHorizontalStackedBarSeries()),
    xAxis(new QValueAxis()), yAxis(new QBarCategoryAxis())
{
    this->setTitle("Throughput Overview");
    this->setAnimationOptions(QChart::NoAnimation);
    this->setTheme(QChart::ChartThemeDark);
}

ThroughputOverviewChartModel::~ThroughputOverviewChartModel() {
    delete sequence0BarSet;
    delete sequence1BarSet;
    delete sequence2BarSet;
    delete sequence3BarSet;
    delete horizontalStackedBarSeries;
    delete xAxis;
    delete yAxis;
}

void ThroughputOverviewChartModel::setSourceModel(TrafficDataModel* trafficDataModel) {
    QList<QString> intersections;
    for (const TrafficDataModel::ThroughputEntry& throughputEntry : trafficDataModel->getThroughputEntries()) {
        if (std::get<1>(throughputEntry) == 0) {
            intersections.append(QString("Sec:") + QString::number(std::get<0>(throughputEntry)));
        }

        switch (std::get<1>(throughputEntry)) {
            case 0:
                sequence0BarSet->append(std::get<2>(throughputEntry));
                break;
            case 1:
                sequence1BarSet->append(std::get<2>(throughputEntry));
                break;
            case 2:
                sequence2BarSet->append(std::get<2>(throughputEntry));
                break;
            case 3:
                sequence3BarSet->append(std::get<2>(throughputEntry));
                break;

            default:
                throw UiException("Invalid sequence number.");
        }
    }

    yAxis->append(intersections);
    xAxis->setRange(0, 400);

    horizontalStackedBarSeries->append(sequence0BarSet);
    horizontalStackedBarSeries->append(sequence1BarSet);
    horizontalStackedBarSeries->append(sequence2BarSet);
    horizontalStackedBarSeries->append(sequence3BarSet);
    this->addSeries(horizontalStackedBarSeries);

    this->addAxis(yAxis, Qt::AlignLeft);
    horizontalStackedBarSeries->attachAxis(yAxis);
    this->addAxis(xAxis, Qt::AlignBottom);
    horizontalStackedBarSeries->attachAxis(xAxis);

    this->legend()->setVisible(true);
    this->legend()->setAlignment(Qt::AlignBottom);

    connect(trafficDataModel, &TrafficDataModel::throughputEntryChangedAt, this, [=](int intersectionNumber, int sequenceNumber, double newThroughput) {
        QBarSet* targetBarSet = nullptr;
        switch (sequenceNumber) {
            case 0:
                targetBarSet = sequence0BarSet;
                break;
            case 1:
                targetBarSet = sequence1BarSet;
                break;
            case 2:
                targetBarSet = sequence2BarSet;
                break;
            case 3:
                targetBarSet = sequence3BarSet;
                break;

            default:
                throw UiException("Invalid sequence number.");
        }

        targetBarSet->replace(intersectionNumber, newThroughput);
    });
}







//#include "horizontalcolumnview.h"
//#include "horizontalcolumnmodel.h"

//HorizontalColumnView::HorizontalColumnView(QWidget *parent) :
//    QWidget(parent),
//    ui(new Ui::HorizontalColumnView)
//{
//    ui->setupUi(this);
//}

//HorizontalColumnView::~HorizontalColumnView()
//{
//    delete ui;
//}

//void HorizontalColumnView::setChart(HorizontalColumnModel *_HorizontalColumnModel)
//{
//    ui->horizontalColumn->layout()->addWidget(_HorizontalColumnModel);
//}
