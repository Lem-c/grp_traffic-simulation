#ifndef USEDFILESTRACKERMODEL_H
#define USEDFILESTRACKERMODEL_H

#include <QObject>
#include <QFile>
#include <QAction>
#include <utility>

/**
* @class <UsedFilesTrackerModel> [USEDFILESTRACKERMODEL_H] [<USEDFILESTRACKERMODEL_H]
* @brief The model response for tracking the used file and present it
* @note Extends the QObject
* Track the used file in the directory
*/
class UsedFilesTrackerModel : public QObject
{
    Q_OBJECT
public:
    typedef std::pair<int, QString> Record;

    /** @brief The constructor of UsedFilesTrackerModel class
    * Accepts a QObject as parent
    */
    explicit UsedFilesTrackerModel(QObject* parent = nullptr);

    /** @brief The de-constructor of UsedFilesTrackerModel class
    * delete itself
    */
    ~UsedFilesTrackerModel();

    /**  @brief get record of recently used file
       * @return A list of record files
       */
    inline const QList<Record>& getRecentlyUsedFilesRecords() const;

    /**  @brief check from string whether contains the record file
       * @param[in] filePath ; The path save the record file
       */
    inline bool containRecord(const QString& filePath) const;

    /**  @brief get record file by file path
       * @param[in] filePath ; The path save the record file
       */
    inline const Record* getRecordBy(const QString& filePath) const;

    /**  @brief add new record to a string
       * @param[in] filePath ; The path save the record file
       */
    void addNewRecord(const QString& filePath);

    /** @brief a string store the file path*/
    const static QString pathToRecordFile;

signals:
    void recordsSequenceChanged(const QList<UsedFilesTrackerModel::Record>& recentlyUsedFilesRecords);
    void newRecordAdded(const UsedFilesTrackerModel::Record& newRecord);

private:
    QFile recordFile;
    QList<Record> recentlyUsedFilesRecords;

    Record* getRecordBy(const QString& filePath);
};

const QList<UsedFilesTrackerModel::Record>& UsedFilesTrackerModel::getRecentlyUsedFilesRecords() const {
    return recentlyUsedFilesRecords;
}

bool UsedFilesTrackerModel::containRecord(const QString& filePath) const {
    for (const Record& record : recentlyUsedFilesRecords) {
        if (!QString::compare(record.second, filePath)) {
            return true;
        }
    }

    return false;
}

const UsedFilesTrackerModel::Record* UsedFilesTrackerModel::getRecordBy(const QString& filePath) const {
    for (const Record& record : recentlyUsedFilesRecords) {
        if (!QString::compare(record.second, filePath)) {
            return &record;
        }
    }

    return nullptr;
}

#endif // USEDFILESTRACKERMODEL_H
