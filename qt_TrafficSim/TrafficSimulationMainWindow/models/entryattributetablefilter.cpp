#include "entryattributetablefilter.h"

EntryAttributeTableFilter::EntryAttributeTableFilter(QObject* parent)
    : QSortFilterProxyModel(parent), enableFiltering(false), entryNumber(0)
{

}

bool EntryAttributeTableFilter::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const {
    if (!enableFiltering) {
        return true;
    }

    QModelIndex index0 = sourceModel()->index(sourceRow, 0, sourceParent);

    return sourceModel()->data(index0).toInt() == entryNumber;
}
