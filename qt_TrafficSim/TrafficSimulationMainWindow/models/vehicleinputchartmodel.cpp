#include "vehicleinputchartmodel.h"
#include <QPieLegendMarker>

VehicleInputChartModel::VehicleInputChartModel(QGraphicsItem* parent, Qt::WindowFlags flags) :
    QChart(parent, flags), pieSeries(new QPieSeries())
{
    this->setTitle("Vehicle Input Pie Chart");
    this->setTheme(QChart::ChartThemeDark);
    this->setAnimationOptions(QChart::NoAnimation);
    legend()->setVisible(false);
    QChart::addSeries(pieSeries);
}

VehicleInputChartModel::~VehicleInputChartModel() {
//    delete pieSeries;
}

void VehicleInputChartModel::setSourceModel(const TrafficDataModel* trafficDataModel) {
    for (const TrafficDataModel::VehicleInputEntry& vehicleInputEntry : trafficDataModel->getVehicleInputEntries()) {
        pieSeries->append(QString("Entry%1: %2").arg(QString::number(vehicleInputEntry.first), QString::number(vehicleInputEntry.second, 'f', 2)),
                          vehicleInputEntry.second);
    }

    for (QPieSlice* pieSlice : pieSeries->slices()) {
        pieSlice->setLabelVisible(true);
    }

    this->addSeries(pieSeries);

    this->legend()->setVisible(true);
    this->legend()->setAlignment(Qt::AlignBottom);

    connect(trafficDataModel, &TrafficDataModel::vehicleInputEntryChangedAt, this, [=](int entryNumber, double newVehicleInput) {
        QList<QPieSlice*> pieSlices = pieSeries->slices();
        QList<QPieSlice*>::iterator targetSlice = std::find_if(pieSlices.begin(), pieSlices.end(), [=](QPieSlice* pieSlice) {
            return pieSlice->label().contains(QString("Entry%1").arg(QString::number(entryNumber)));
        });

        if (targetSlice == pieSlices.end()) {
            return;
        }

        (*targetSlice)->setValue(newVehicleInput);
        (*targetSlice)->setLabel(QString("Entry%1: %2").arg(QString::number(entryNumber), QString::number(newVehicleInput, 'f', 2)));
    });
//    QPieSeries *series1 = new QPieSeries();
//    series1->setName("Fossil fuels");
//    series1->append("Oil", 353295);
//    series1->append("Coal", 188500);
//    series1->append("Natural gas", 148680);
//    series1->append("Peat", 94545);

//    QPieSeries *series2 = new QPieSeries();
//    series2->setName("Renewables");
//    series2->append("Wood fuels", 319663);
//    series2->append("Hydro power", 45875);
//    series2->append("Wind power", 1060);

//    QPieSeries *series3 = new QPieSeries();
//    series3->setName("Others");
//    series3->append("Nuclear energy", 238789);
//    series3->append("Import energy", 37802);
//    series3->append("Other", 32441);
//    //![1]

//    //![2]
//    addBreakdownSeries(series1, Qt::red);
//    addBreakdownSeries(series2, Qt::darkGreen);
//    addBreakdownSeries(series3, Qt::darkBlue);
}

//void VehicleInputChartModel::addBreakdownSeries(QPieSeries *breakdownSeries, QColor color)
//{
//    QFont font("Arial", 8);

//    // add breakdown series as a slice to center pie
//    MainSlice *mainSlice = new MainSlice(breakdownSeries);
//    mainSlice->setName(breakdownSeries->name());
//    mainSlice->setValue(breakdownSeries->sum());
//    pieSeries->append(mainSlice);

//    // customize the slice
//    mainSlice->setBrush(color);
//    mainSlice->setLabelVisible();
//    mainSlice->setLabelColor(Qt::white);
//    mainSlice->setLabelPosition(QPieSlice::LabelInsideHorizontal);
//    mainSlice->setLabelFont(font);

//    // position and customize the breakdown series
//    breakdownSeries->setPieSize(0.8);
//    breakdownSeries->setHoleSize(0.7);
//    breakdownSeries->setLabelsVisible();
//    const auto slices = breakdownSeries->slices();
//    for (QPieSlice *slice : slices) {
//        color = color.lighter(115);
//        slice->setBrush(color);
//        slice->setLabelFont(font);
//    }

//    // add the series to the chart
//    QChart::addSeries(breakdownSeries);

//    // recalculate breakdown donut segments
//    recalculateAngles();

//    // update customize legend markers
//    updateLegendMarkers();
//}

//void VehicleInputChartModel::recalculateAngles()
//{
//    qreal angle = 0;
//    const auto slices = pieSeries->slices();
//    for (QPieSlice *slice : slices) {
//        QPieSeries *breakdownSeries = qobject_cast<MainSlice *>(slice)->breakdownSeries();
//        breakdownSeries->setPieStartAngle(angle);
//        angle += slice->percentage() * 360.0; // full pie is 360.0
//        breakdownSeries->setPieEndAngle(angle);
//    }
//}

//void VehicleInputChartModel::updateLegendMarkers()
//{
//    // go through all markers
//    const auto allseries = series();
//    for (QAbstractSeries *series : allseries) {
//        const auto markers = legend()->markers(series);
//        for (QLegendMarker *marker : markers) {
//            QPieLegendMarker *pieMarker = qobject_cast<QPieLegendMarker *>(marker);
//            if (series == pieSeries) {
//                // hide markers from main series
//                pieMarker->setVisible(false);
//            } else {
//                // modify markers from breakdown series
//                pieMarker->setLabel(QString("%1 %2%")
//                                    .arg(pieMarker->slice()->label())
//                                    .arg(pieMarker->slice()->percentage() * 100, 0, 'f', 2));
//                pieMarker->setFont(QFont("Arial", 8));
//            }
//        }
//    }
//}
