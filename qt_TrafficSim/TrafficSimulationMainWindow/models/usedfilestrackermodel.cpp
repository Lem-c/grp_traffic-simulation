#include "usedfilestrackermodel.h"

#include <QIODevice>
#include <QDateTime>
#include <QRegularExpression>
#include "../utilities/uiexception.h"

const QString UsedFilesTrackerModel::pathToRecordFile("D:\\UNNC\\GRP_project\\Traffic_sim\\grp_sim\\grp_traffic-simulation\\qt_TrafficSim\\TrafficSimulationMainWindow\\resources\\recently_used_files_record.txt");

UsedFilesTrackerModel::UsedFilesTrackerModel(QObject* parent)
    : QObject(parent), recordFile(pathToRecordFile), recentlyUsedFilesRecords()
{
    if (recordFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream readRecordStream(&recordFile);
        QStringList allRecords(readRecordStream.readAll().split(QRegularExpression("[\n,\t]"),
                                                                Qt::SkipEmptyParts));
        recordFile.close();

        int i = 0;
        int j = 1;
        for (const QString& record : allRecords) {
            if (j % 2 != 0 && !containRecord(record)) {
                recentlyUsedFilesRecords.append(Record(i, record));
                i++;
            }

            j++;
        }

        if (!recentlyUsedFilesRecords.isEmpty()) {
            std::sort(recentlyUsedFilesRecords.begin(), recentlyUsedFilesRecords.end(),
                      [](const Record& record1,
                      const Record& record2) {
                            return record1.first > record2.first;
            });
        }

    } else {
        throw UiException("Cannot open the record file.");
    }
}

UsedFilesTrackerModel::~UsedFilesTrackerModel() {

}

UsedFilesTrackerModel::Record* UsedFilesTrackerModel::getRecordBy(const QString& filePath) {
    for (Record& record : recentlyUsedFilesRecords) {
        if (!QString::compare(record.second, filePath)) {
            return &record;
        }
    }

    return nullptr;
}

void UsedFilesTrackerModel::addNewRecord(const QString& filePath) {
    if (recordFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
        QTextStream writeRecordStream(&recordFile);
        writeRecordStream << filePath << ",\t"
                          << QDateTime::currentDateTime().toString(Qt::TextDate)
                          << "\n";
        recordFile.close();

        Record* recordPtr = getRecordBy(filePath);
        if (!recordPtr) {
            Record newRecord(recentlyUsedFilesRecords.isEmpty() ? 0 :
                             recentlyUsedFilesRecords.front().first + 1,
                             filePath);

            recentlyUsedFilesRecords.push_front(newRecord);

            emit newRecordAdded(newRecord);

        } else {
            recordPtr->first = recentlyUsedFilesRecords.front().first + 1;
            std::sort(recentlyUsedFilesRecords.begin(), recentlyUsedFilesRecords.end(),
                      [](const Record& record1,
                      const Record& record2) {
                            return record1.first > record2.first;
            });

            emit recordsSequenceChanged(recentlyUsedFilesRecords);
        }

    } else {
        throw UiException("Cannot open the record file.");
    }
}
