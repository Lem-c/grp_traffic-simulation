#ifndef PROJECTTREEMODEL_H
#define PROJECTTREEMODEL_H

#include <QAbstractItemModel>
#include "projectmodel.h"
#include "../utilities/projecttreemodelitem.h"

/**
* @class <ProjectTreeModel> [PROJECTTREEMODEL_H] [<PROJECTTREEMODEL_H]
* @brief The model response for the project information store
* @note Extends the QAbstractItemModel
* @class projecttreemodelitem "../utilities/projecttreemodelitem.h"
*
*/
class ProjectTreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:

    /** @brief The constructor of ProjectTreeModel class
        * Accepts a QObject as parent, default set as nullptr
        */
    explicit ProjectTreeModel(QObject* parent = nullptr);

    /** @brief The de-constructor of ProjectTreeModel class
        * delete the rootItem
        */
    ~ProjectTreeModel();

    /**     @brief check if a valid data and then return it
        *   @param[in] index : the index of the rable
        *   @param[in] role : whicl role to return
        */
    virtual QVariant data(const QModelIndex &index, int role) const override;

    /**     @brief get the flag at related index
        *   @param[in] index : the index of the rable
        */
    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;

    /**     @brief check if a valid data and then return it
        *   @param[in] section : the section
        *   @param[in] orientation : the toward position
        */
    virtual QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    /**     @brief return the index of (row,column) project model
        *   @param[in] row : the checked row value
        *   @param[in] column : the checked column value
        */
    virtual QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;

    /**     @brief get the parent of the current project
        *   @param[in] index : the index in current model
        */
    virtual QModelIndex parent(const QModelIndex &index) const override;

    /**     @brief get the count of the row number
        *   @param[in] parent : the parent of current node
        */
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    /**     @brief get the count of the column number
        *   @param[in] parent : the parent of current node
        */
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    /**     @brief set the source model of the project
        *   @param[in] projetcModel : a project will be set as source
        */
    void setSourceModel(const ProjectModel* projectModel);

public slots:
    void refreshModel();

private:
    const ProjectModel* projectModel;
    const QList<Project>* projects;
    ProjectTreeModelItem* rootItem;
};

#endif // PROJECTTREEMODEL_H
