#ifndef ENTRYATTRIBUTE_H
#define ENTRYATTRIBUTE_H

#include "projectattribute.h"

/**
 * @class EntryAttribute entryattribute.h
 * @brief Input entry attributes
 * 
 * @author YiCun Duan
 */
class EntryAttribute : public ProjectAttribute
{
public:
    /**
     * @brief Construct a new Entry Attribute object
     * 
     * @param[in] entryNumber : The number of entry
     * @param[in] vehicleInitialSpeed : The initial speed of the vehicle
     * @param[in] correspondingRoadIndex : The index of the corresponding road
     */
    EntryAttribute(int entryNumber, double vehicleInitialSpeed, unsigned int correspondingRoadIndex);

    /**
     * @brief Destroy the Entry Attribute object
     */
    virtual ~EntryAttribute() = default;

    /**
     * @brief Get the Entry Number
     * 
     * @return int 
     */
    inline int getEntryNumber() const;

    /**
     * @brief Get the Vehicle Initial Speed
     * 
     * @return double 
     */
    inline double getVehicleInitialSpeed() const;

    /**
     * @brief Get the Corresponding Road Index object
     * 
     * @return unsigned int 
     */
    inline unsigned int getCorrespondingRoadIndex() const;

    /**
     * @brief Get the Sub Attribute By object
     * 
     * @param[in] subAttributeNumber : the sub attribute number
     * @return QVariant 
     */
    virtual QVariant getSubAttributeBy(const int subAttributeNumber) const override;

    /**
     * @brief Set the Sub Attribute By object
     * 
     * @param[in] subAttributeNumber : the sub attribute number
     * @param[in] value 
     */
    virtual void setSubAttributeBy(const int subAttributeNumber, const QVariant& value) override;

    /**
     * @brief Number of sub attributes
     * 
     */
    static const int subAttributeCount;

private:
    /**
     * @brief The number of entry
     * 
     */
    int entryNumber;
    /**
     * @brief The initial speed of the vehicle
     * 
     */
    double vehicleInitialSpeed;

    /**
     * @brief The corresponding road index
     * 
     */
    unsigned int correspondingRoadIndex;
};

int EntryAttribute::getEntryNumber() const {
    return entryNumber;
}

double EntryAttribute::getVehicleInitialSpeed() const {
    return vehicleInitialSpeed;
}

unsigned int EntryAttribute::getCorrespondingRoadIndex() const {
    return correspondingRoadIndex;
}




#endif // ENTRYATTRIBUTE_H
