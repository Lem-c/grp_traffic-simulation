#include "stylesheetparser.h"

#include <QFile>
#include <QMap>
#include "uiexception.h"

QString StyleSheetParser::parseStyleSheet(const char* pathToStyleSheet)
{
    QFile styleFile(pathToStyleSheet);
    if(styleFile.open(QIODevice::ReadOnly))
    {
      QString styleSheet(styleFile.readAll());
      QMap <QString, QString> colorValue;
      QList <QString> key;
      while (styleSheet.indexOf("*/") != -1)
      {// remove comment
        int i = styleSheet.indexOf("/*");
        int j = styleSheet.indexOf("*/");
//        qDebug()<<styleSheet.mid(i,j+2-i);
        styleSheet.remove(i,j+2-i);
      }
      while (styleSheet.indexOf("$") != -1) {// put values into the container
        int i = styleSheet.indexOf("$");
        int j = styleSheet.indexOf("=");
        int k = styleSheet.indexOf(";");
        if(j > k || j == -1){
            break;
        }
        colorValue[styleSheet.mid(i,j-i)] = styleSheet.mid(j+1,k-j-1);
        key<<styleSheet.mid(i,j-i);
        styleSheet.remove(0,k+1);
      }
      for (int i = 0;i<key.length();i++)
      {
        while (styleSheet.indexOf(key[i]) != -1)
        {
           styleSheet.replace(styleSheet.indexOf(key[i]),key[i].length(),colorValue[key[i]]);
        }
      }
//      qDebug()<<styleSheet;
      styleFile.close();
      return styleSheet;
    } else {
        throw UiException("Cannot open the Style Sheet");
    }
}
