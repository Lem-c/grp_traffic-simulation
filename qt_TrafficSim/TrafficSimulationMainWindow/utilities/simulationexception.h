#ifndef SIMULATIONEXCEPTION_H
#define SIMULATIONEXCEPTION_H

#include <exception>
#include <string>

/**
 * @class SimulationException 
 * @brief Tree model that 
 * 
 * @author YiCun Duan
 */
class SimulationException : public std::exception
{
public:
    /**
     * @brief Construct a new Simulation Exception
     * 
     * @param[in] errorMessage : the error message of the exception
     */
    SimulationException(const std::string& errorMessage);

    /**
     * @brief Return the error message of the exception
     * 
     * @return const char* 
     */
    virtual const char* what() const noexcept override;

private:
    /**
     * @brief The error message
     * 
     */
    std::string errorMessage;
};

#endif // SIMULATIONEXCEPTION_H
