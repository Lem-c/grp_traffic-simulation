#include "simulationexception.h"

SimulationException::SimulationException(const std::string& errorMessage) :
    errorMessage(errorMessage)
{

}

const char* SimulationException::what() const noexcept {
    return errorMessage.c_str();
}
