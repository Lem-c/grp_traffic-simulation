#ifndef PROJECTATTRIBUTE_H
#define PROJECTATTRIBUTE_H

#include <QVariant>

/**
 * @class ProjectAttribute projectattribute.h
 * @brief Stores the attributes and properties of the project
 * 
 * @author YiCun Duan
 */
class ProjectAttribute
{
public:
    /**
     * @brief Enum of the attribute types
     * 
     */
    enum Type {
      EntryAttributeType, /** @brief enum value EntryAttributeType */
      TrafficLightAttributeType, /** @brief enum value TrafficLightAttributeType */
      NullType /** @brief enum value NullType */
    };

    /**
     * @brief Destroy the Project Attribute object
     * 
     */
    virtual ~ProjectAttribute();

    /**
     * @brief Get the Sub Attribute By object
     * 
     * @param[in] subAttributeNumber : the sub attribute number
     * @return QVariant 
     */
    virtual QVariant getSubAttributeBy(const int subAttributeNumber) const = 0;

    /**
     * @brief Set the Sub Attribute By object
     * 
     * @param[in] subAttributeNumber : the sub attribute number
     * @param[in] value : the value entered
     */
    virtual void setSubAttributeBy(const int subAttributeNumber, const QVariant& value) = 0;

protected:
    /**
     * @brief Construct a new Project Attribute object
     * 
     */
    ProjectAttribute();
};

#endif // PROJECTATTRIBUTE_H
