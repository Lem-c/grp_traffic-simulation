#ifndef PROJECT_H
#define PROJECT_H

#include <QString>
#include <QDateTime>
#include <QList>
#include <QFile>

#include "jsoncoder.h"
#include "entryattribute.h"
#include "trafficlightattribute.h"
#include "../simulation/trafficsceneconfiguration.h"

/**
 * @class Project project.h
 * @brief Stores the attributes and properties of the project
 * 
 * @author YiCun Duan
 */
class Project
{    
public:
    /**
     * @brief Construct a new Project object
     * 
     * @param[in] projectName : the project name
     * @param[in] userName : user name
     * @param[in] horizonIntersectionNumber : number of horizontal intersection
     * @param[in] verticalIntersectionNumber : number of vertical intersection
     */
    Project(const QString& projectName, const QString& userName, int horizonIntersectionNumber,
                int verticalIntersectionNumber);

    /**
     * @brief Destroy the Project object
     * 
     */
    ~Project();

    /**
     * @brief Get the Project Name Object
     * 
     * @return const QString& 
     */
    inline const QString& getProjectName() const;

    /**
     * @brief Get the User Name Object
     * 
     * @return const QString& 
     */
    inline const QString& getUserName() const;

    /**
     * @brief Get the Create Date object
     * 
     * @return const QDateTime& 
     */
    inline const QDateTime& getCreateDate() const;

    /**
     * @brief Get the Last Modified Date object
     * 
     * @return const QDateTime& 
     */
    inline const QDateTime& getLastModifiedDate() const;

    /**
     * @brief Get the Horizontal Intersection Number object
     * 
     * @return int 
     */
    inline int getHorizontalIntersectionNumber() const;

    /**
     * @brief Get the Vertical Inersection Number object
     * 
     * @return int 
     */
    inline int getVerticalInersectionNumber() const;

    /**
     * @brief Get the Entry Attributes object
     * 
     * @return const QList<ProjectAttribute*>& 
     */
    inline const QList<ProjectAttribute*>& getEntryAttributes() const;

    /**
     * @brief Get the Traffic Light Attributes object
     * 
     * @return const QList<ProjectAttribute*>& 
     */
    inline const QList<ProjectAttribute*>& getTrafficLightAttributes() const;

//    void addAttributeAt(ProjectAttribute::Type attributeType, const int order, ProjectAttribute* entryAttribute);
//    void removeAttributeAt(ProjectAttribute::Type attributeType, const int order);

    /**
     * @brief Set the Attribute of the project
     * 
     * @param[in] attributeType : type of the attribute
     * @param[in] order : the order of the project attribute
     * @param[in] subAttributeOrder : the sub attribute of the project
     * @param[in] newValue : the new value entered
     */
    void setAttributeBy(ProjectAttribute::Type attributeType, const int order, const int subAttributeOrder, const QVariant& newValue);

    /**
     * @brief Refresh the modified date
     * 
     */
    void refreshModifiedDate();

    /**
     * @brief Get the Traffic Scene Configuration object
     * 
     * @return const TrafficSceneConfiguration& 
     */
    inline const TrafficSceneConfiguration& getTrafficSceneConfiguration() const;

private:
    /**
     * @brief Project name
     * 
     */
    QString projectName;
    
    /**
     * @brief User name
     * 
     */
    QString userName;

    /**
     * @brief Date of creation
     * 
     */
    QDateTime createDate;

    /**
     * @brief Last modified date
     * 
     */
    QDateTime lastModifiedDate;

    /**
     * @brief The number of horizontal intersection
     * 
     */
    int horizontalIntersectionNumber;

    /**
     * @brief The number of vertical intersection
     * 
     */
    int verticalIntersectionNumber;
    
    /**
     * @brief Traffic scene configuration
     * 
     */
    TrafficSceneConfiguration* trafficSceneConfiguration;

    /**
     * @brief List of entry attributes
     * 
     */
    QList<ProjectAttribute*> entryAttributes;

    /**
     * @brief List of traffic light attributes
     * 
     */
    QList<ProjectAttribute*> trafficLightAttributes;

    /**
     * @brief Initialize the project attributes
     * 
     */
    void initProjectAttributes();

//    int getAliasRoadSequenceNumberFor(int roadSequenceNumber) const;

    /**
     * @brief Save project into designated project file 
     * 
     * @param[in] project : the project name
     * @param[in] projectFile : the project file
     */
    friend void JsonCoder::saveProjectInto(const Project& project, QFile& projectFile);
    friend Project JsonCoder::loadProjectFrom(QFile& projectFile);
};


const QString& Project::getProjectName() const {
    return projectName;
}

const QString& Project::getUserName() const {
    return userName;
}

const QDateTime& Project::getCreateDate() const {
    return createDate;
}

const QDateTime& Project::getLastModifiedDate() const {
    return lastModifiedDate;
}

int Project::getHorizontalIntersectionNumber() const {
    return horizontalIntersectionNumber;
}

int Project::getVerticalInersectionNumber() const {
    return verticalIntersectionNumber;
}

const QList<ProjectAttribute*>& Project::getEntryAttributes() const {
    return entryAttributes;
}

const QList<ProjectAttribute*>& Project::getTrafficLightAttributes() const {
    return trafficLightAttributes;
}

const TrafficSceneConfiguration& Project::getTrafficSceneConfiguration() const {
    return *trafficSceneConfiguration;
}

#endif // PROJECT_H
