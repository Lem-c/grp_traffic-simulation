#ifndef JSONCODER_H
#define JSONCODER_H

#include <QString>
#include <QDateTime>
#include <QFile>

/**
 * @class JsonCoder jsoncoder.h
 * @brief Save and load project's json file
 * 
 * @author YiCun Duan
 */
class Project;

namespace JsonCoder
{   
    /**
     * @brief Save project
     * 
     * @param[in] project : the project name
     * @param[in] projectFile : the project file
     */
    void saveProjectInto(const Project& project, QFile& projectFile);

    /**
     * @brief Load project
     * 
     * @param[in] projectFile : the project file
     * @return Project 
     */
    Project loadProjectFrom(QFile& projectFile);
};

#endif // JSONCODER_H
