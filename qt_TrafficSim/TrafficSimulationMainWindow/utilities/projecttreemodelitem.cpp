#include "projecttreemodelitem.h"
#include "uiexception.h"

ProjectTreeModelItem::ProjectTreeModelItem(const QList<QVariant>& data, ProjectTreeModelItem* parentItem) :
    children(), dataList(data), parent(parentItem)
{

}

ProjectTreeModelItem::~ProjectTreeModelItem() {
    for (ProjectTreeModelItem* child : children) {
        delete child;
    }
}

void ProjectTreeModelItem::appendChild(ProjectTreeModelItem* child) {
    children.append(child);
}

ProjectTreeModelItem* ProjectTreeModelItem::getChildAt(const int row) {
    if (row < 0 || row >= children.size()) {
        return nullptr;
    }

    return children.at(row);
}

int ProjectTreeModelItem::getChildrenNumber() const {
    return children.size();
}

int ProjectTreeModelItem::getColumnNumber() const {
    return dataList.size();
}

QVariant ProjectTreeModelItem::data(const int column) const {
    if (column < 0 || column >= dataList.size()) {
        return QVariant();
    }

    return dataList.at(column);
}

int ProjectTreeModelItem::row() const {
    if (parent) {
        return parent->children.indexOf(const_cast<ProjectTreeModelItem*>(this));
    }

    return 0;
}

ProjectTreeModelItem* ProjectTreeModelItem::getParent() {
    return parent;
}
