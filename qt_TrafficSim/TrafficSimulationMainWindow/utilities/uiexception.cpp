#include "uiexception.h"

UiException::UiException(const std::string& errorMessage) :
    errorMessage(errorMessage)
{

}

const char* UiException::what() const noexcept {
    return errorMessage.c_str();
}
