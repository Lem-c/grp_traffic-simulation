#ifndef PROJECTATTRIBUTEINDEX_H
#define PROJECTATTRIBUTEINDEX_H

#include <QString>
#include "projectattribute.h"

/**
 * @class ProjectAttributeIndex projectattributeindex.h
 * @brief The index of the ProjectAttribute
 * 
 */
class ProjectAttributeIndex
{
public:
    /**
     * @brief Construct a new Project Attribute Index object
     * 
     * @param projectName : the project name
     * @param attributeType : the type of the attribute
     * @param order : the order of the attribute
     * @param attributeNumber : the number of the attribute
     */
    ProjectAttributeIndex(const QString& projectName = QString(),
                          ProjectAttribute::Type attributeType = ProjectAttribute::Type::NullType,
                          const int order = NullOrder, const int attributeNumber = NullAttributeNumber);

    /**
     * @brief Get the Project Name object
     * 
     * @return const QString& 
     */
    inline const QString& getProjectName() const;

    /**
     * @brief Get the type of the attribute
     * 
     * @return ProjectAttribute::Type 
     */
    inline ProjectAttribute::Type getAttributeType() const;

    /**
     * @brief Get the order of the object
     * 
     * @return int 
     */
    inline int getOrder() const;

    /**
     * @brief Get the Sub Attribute Number object
     * 
     * @return int 
     */
    inline int getSubAttributeNumber() const;
    
    static const int NullOrder;
    static const int NullAttributeNumber;

private:
    /**
     * @brief Project name
     * 
     */
    QString projectName;
    
    /**
     * @brief The type of the attribute
     * 
     */
    ProjectAttribute::Type attributeType;

    /**
     * @brief The order of the project
     * 
     */
    int order;

    /**
     * @brief The sub attribute number
     * 
     */
    int subAttributeNumber;
};

const QString& ProjectAttributeIndex::getProjectName() const {
    return projectName;
}

ProjectAttribute::Type ProjectAttributeIndex::getAttributeType() const {
    return attributeType;
}

int ProjectAttributeIndex::getOrder() const {
    return order;
}

int ProjectAttributeIndex::getSubAttributeNumber() const {
    return subAttributeNumber;
}

#endif // PROJECTATTRIBUTEINDEX_H
