#ifndef STYLESHEETPARSER_H
#define STYLESHEETPARSER_H

#include <QString>

/**
 * @class StyleSheetParser
 * @brief Tree model that 
 * 
 * @author YiCun Duan
 */
namespace StyleSheetParser {
    /**
     * @brief Parse the style sheet
     * 
     * @param[in] pathToStyleSheet : the path of the style sheet
     * @return QString 
     */
    QString parseStyleSheet(const char* pathToStyleSheet);
}

#endif // STYLESHEETPARSER_H
