#include "project.h"
#include "entryattribute.h"
#include "uiexception.h"
#include "../simulation/intersection.h"

//Project::Project():
//    userName(""), createDate(QDateTime::currentDateTime()), lastModifiedDate(QDateTime::currentDateTime()),
//    horizontalIntersectionNumber(1), verticalIntersectionNumber(1),
//    trafficSceneConfiguration(nullptr),
//    entryAttributes(), trafficLightAttributes()
//{
//    this->trafficSceneConfiguration = new TrafficSceneConfiguration(horizontalIntersectionNumber,
//                                                                    verticalIntersectionNumber);
//    initProjectAttributes();
//}

Project::Project(const QString& projectName, const QString& userName, int horizonIntersectionNumber,
            int verticalIntersectionNumber):
    projectName(projectName), userName(userName), createDate(QDateTime::currentDateTime()), lastModifiedDate(QDateTime::currentDateTime()),
    horizontalIntersectionNumber(horizonIntersectionNumber), verticalIntersectionNumber(verticalIntersectionNumber),
    trafficSceneConfiguration(nullptr),
    entryAttributes(), trafficLightAttributes()
{
    if (horizonIntersectionNumber <= 0 || verticalIntersectionNumber <= 0) {
        throw UiException("Invalid horizontalIntersectionNumber or verticalIntersectionNumber.");
    }

    this->trafficSceneConfiguration = new TrafficSceneConfiguration(horizonIntersectionNumber,
                                                                    verticalIntersectionNumber);
    initProjectAttributes();
}

Project::~Project() {

}

//void Project::addAttributeAt(ProjectAttribute::Type attributeType, const int order, ProjectAttribute* projectAttribute) {
//    if (attributeType != ProjectAttribute::Type::EntryAttributeType &&
//            attributeType != ProjectAttribute::Type::TrafficLightAttributeType) {
//        throw UiException("Unsupported attribute type.");
//    }

//    if (order < 0) {
//        throw UiException("Attribute order cannot less than zero.");
//    }

//    switch (attributeType) {
//        case ProjectAttribute::Type::EntryAttributeType:
//            if (order > entryAttributes.size()) {
//                throw UiException("Attribute order cannot be larger than the container's size.");
//            }

//            if (!dynamic_cast<EntryAttribute*>(projectAttribute)) {
//                throw UiException("attributeType is not equal to the actual type of projectAttribute.");
//            }

//            entryAttributes.insert(order, projectAttribute);
//            break;
//        case ProjectAttribute::Type::TrafficLightAttributeType:
//            if (order > trafficLightAttributes.size()) {
//                throw UiException("Attribute order cannot be larger than the container's size.");
//            }

//            if (!dynamic_cast<TrafficLightAttribute*>(projectAttribute)) {
//                throw UiException("attributeType is not equal to the actual type of projectAttribute.");
//            }

//            trafficLightAttributes.insert(order, projectAttribute);
//            break;
//        default:
//            throw UiException("Unsupported attribute type.");
//    }
//}

//void Project::removeAttributeAt(ProjectAttribute::Type attributeType, const int order) {
//    if (attributeType != ProjectAttribute::Type::EntryAttributeType &&
//            attributeType != ProjectAttribute::Type::TrafficLightAttributeType) {
//        throw UiException("Unsupported attribute type.");
//    }

//    if (order < 0) {
//        throw UiException("Attribute order cannot less than zero.");
//    }

//    switch (attributeType) {
//        case ProjectAttribute::Type::EntryAttributeType:
//            if (order >= entryAttributes.size()) {
//                throw UiException("Attribute order cannot be larger than or equal to container's size.");
//            }

//            delete entryAttributes.at(order);
//            entryAttributes.removeAt(order);
//            break;
//        case ProjectAttribute::Type::TrafficLightAttributeType:
//            if (order >= trafficLightAttributes.size()) {
//                throw UiException("Attribute order cannot be larger than or equal to container's size.");
//            }

//            delete trafficLightAttributes.at(order);
//            trafficLightAttributes.removeAt(order);
//            break;
//        default:
//            throw UiException("Unsupported attribute type.");
//    }
//}

void Project::initProjectAttributes() {
    // set traffic light attributes
    for (Intersection* intersection : trafficSceneConfiguration->getIntersections()) {
        int intersectionSequenceNumber = 0;
        int roadSequenceNumber = 0;
        while (intersectionSequenceNumber < 8) {
            const TrafficSceneConfiguration::RoadSetting* curRoad0 = nullptr;
            const TrafficSceneConfiguration::RoadSetting* curRoad1 = nullptr;
            unsigned int curRoad0RoadIndex = 0;
            unsigned int curRoad1RoadIndex = 0;
            if (intersectionSequenceNumber % 2 == 0) {
                curRoad0 = intersection->getTwowayRoadBy(intersectionSequenceNumber)->getRightRoadEndRoadSetting();
                curRoad1 = intersection->getTwowayRoadBy(intersectionSequenceNumber)->getLeftRoadEndRoadSetting();
                curRoad0RoadIndex = intersection->getTwowayRoadBy(intersectionSequenceNumber)->getRightRoadEndRoadIndex();
                curRoad1RoadIndex = intersection->getTwowayRoadBy(intersectionSequenceNumber)->getLeftRoadEndRoadIndex();
            } else {
                curRoad0 = intersection->getTwowayRoadBy(intersectionSequenceNumber)->getLeftRoadEndRoadSetting();
                curRoad1 = intersection->getTwowayRoadBy(intersectionSequenceNumber)->getRightRoadEndRoadSetting();
                curRoad0RoadIndex = intersection->getTwowayRoadBy(intersectionSequenceNumber)->getLeftRoadEndRoadIndex();
                curRoad1RoadIndex = intersection->getTwowayRoadBy(intersectionSequenceNumber)->getRightRoadEndRoadIndex();
            }

            // test curRoad0
            if (std::get<TrafficSceneConfiguration::RoadSettingIndex::HasTrafficLight>(*curRoad0)) {
                bool hasAliasRoad = false;
                switch(intersectionSequenceNumber / 2) {
                case 0:
                    if (intersection->getUpIntersection()) {
                        hasAliasRoad = true;
                    }
                    break;
                case 1:
                    if (intersection->getLeftIntersection()) {
                        hasAliasRoad = true;
                    }
                    break;
                case 2:
                    if (intersection->getDownIntersection()) {
                        hasAliasRoad = true;
                    }
                    break;
                case 3:
                    if (intersection->getRightIntersection()) {
                        hasAliasRoad = true;
                    }
                    break;
                }

                trafficLightAttributes.append(new TrafficLightAttribute(intersection->getIntersectionNumber(),
                                                                        roadSequenceNumber,
                                                                        std::get<TrafficSceneConfiguration::RoadSettingIndex::GreenDuration>(*curRoad0),
                                                                        0,
                                                                        std::get<TrafficSceneConfiguration::RoadSettingIndex::RedDuration>(*curRoad0),
                                                                        curRoad0RoadIndex, hasAliasRoad));
            }
            roadSequenceNumber++;

            // test curRoad1
            if (std::get<TrafficSceneConfiguration::RoadSettingIndex::HasTrafficLight>(*curRoad1)) {
                bool hasAliasRoad = false;
                switch(intersectionSequenceNumber / 2) {
                case 0:
                    if (intersection->getUpIntersection()) {
                        hasAliasRoad = true;
                    }
                    break;
                case 1:
                    if (intersection->getLeftIntersection()) {
                        hasAliasRoad = true;
                    }
                    break;
                case 2:
                    if (intersection->getDownIntersection()) {
                        hasAliasRoad = true;
                    }
                    break;
                case 3:
                    if (intersection->getRightIntersection()) {
                        hasAliasRoad = true;
                    }
                    break;
                }

                trafficLightAttributes.append(new TrafficLightAttribute(intersection->getIntersectionNumber(),
                                                                        roadSequenceNumber,
                                                                        std::get<TrafficSceneConfiguration::RoadSettingIndex::GreenDuration>(*curRoad1),
                                                                        0,
                                                                        std::get<TrafficSceneConfiguration::RoadSettingIndex::RedDuration>(*curRoad1),
                                                                        curRoad1RoadIndex, hasAliasRoad));
            }
            roadSequenceNumber++;

            intersectionSequenceNumber++;
        }
    }

    // set entry attribute
    int i = horizontalIntersectionNumber - 1;
    while (i >= 0) {
        Intersection* curIntersection = trafficSceneConfiguration->getIntersections()[i];
        const TrafficSceneConfiguration::RoadSetting* leftRoadStartRoadSetting = curIntersection->getTwowayRoadBy(1)->getLeftRoadStartRoadSetting();
        entryAttributes.append(new EntryAttribute(entryAttributes.size(), std::get<TrafficSceneConfiguration::RoadSettingIndex::VehicleEnterRate>(*leftRoadStartRoadSetting),
                                                  curIntersection->getTwowayRoadBy(1)->getLeftRoadStartRoadIndex()));

        const TrafficSceneConfiguration::RoadSetting* rightRoadStartRoadSetting = curIntersection->getTwowayRoadBy(1)->getRightRoadStartRoadSetting();
        entryAttributes.append(new EntryAttribute(entryAttributes.size(), std::get<TrafficSceneConfiguration::RoadSettingIndex::VehicleEnterRate>(*rightRoadStartRoadSetting),
                                                  curIntersection->getTwowayRoadBy(1)->getRightRoadStartRoadIndex()));
        i--;
    }

    i = 0;
    while (i <= horizontalIntersectionNumber *
           (verticalIntersectionNumber - 1)) {
        Intersection* curIntersection = trafficSceneConfiguration->getIntersections()[i];
        const TrafficSceneConfiguration::RoadSetting* leftRoadStartRoadSetting = curIntersection->getTwowayRoadBy(3)->getLeftRoadStartRoadSetting();
        entryAttributes.append(new EntryAttribute(entryAttributes.size(), std::get<TrafficSceneConfiguration::RoadSettingIndex::VehicleEnterRate>(*leftRoadStartRoadSetting),
                                                  curIntersection->getTwowayRoadBy(3)->getLeftRoadStartRoadIndex()));

        const TrafficSceneConfiguration::RoadSetting* rightRoadStartRoadSetting = curIntersection->getTwowayRoadBy(3)->getRightRoadStartRoadSetting();
        entryAttributes.append(new EntryAttribute(entryAttributes.size(), std::get<TrafficSceneConfiguration::RoadSettingIndex::VehicleEnterRate>(*rightRoadStartRoadSetting),
                                                  curIntersection->getTwowayRoadBy(3)->getRightRoadStartRoadIndex()));
        i += horizontalIntersectionNumber;
    }

    i = horizontalIntersectionNumber *
            (verticalIntersectionNumber - 1);
    while (i < horizontalIntersectionNumber * verticalIntersectionNumber) {
        Intersection* curIntersection = trafficSceneConfiguration->getIntersections()[i];
        const TrafficSceneConfiguration::RoadSetting* leftRoadStartRoadSetting = curIntersection->getTwowayRoadBy(5)->getLeftRoadStartRoadSetting();
        entryAttributes.append(new EntryAttribute(entryAttributes.size(), std::get<TrafficSceneConfiguration::RoadSettingIndex::VehicleEnterRate>(*leftRoadStartRoadSetting),
                                                  curIntersection->getTwowayRoadBy(5)->getLeftRoadStartRoadIndex()));

        const TrafficSceneConfiguration::RoadSetting* rightRoadStartRoadSetting = curIntersection->getTwowayRoadBy(5)->getRightRoadStartRoadSetting();
        entryAttributes.append(new EntryAttribute(entryAttributes.size(), std::get<TrafficSceneConfiguration::RoadSettingIndex::VehicleEnterRate>(*rightRoadStartRoadSetting),
                                                  curIntersection->getTwowayRoadBy(5)->getRightRoadStartRoadIndex()));
        i++;
    }

    i = horizontalIntersectionNumber * verticalIntersectionNumber - 1;
    while (i >= trafficSceneConfiguration->getHorizontalIntersectionNumber() - 1) {
        Intersection* curIntersection = trafficSceneConfiguration->getIntersections()[i];
        const TrafficSceneConfiguration::RoadSetting* leftRoadStartRoadSetting = curIntersection->getTwowayRoadBy(7)->getLeftRoadStartRoadSetting();
        entryAttributes.append(new EntryAttribute(entryAttributes.size(), std::get<TrafficSceneConfiguration::RoadSettingIndex::VehicleEnterRate>(*leftRoadStartRoadSetting),
                                                  curIntersection->getTwowayRoadBy(7)->getLeftRoadStartRoadIndex()));

        const TrafficSceneConfiguration::RoadSetting* rightRoadStartRoadSetting = curIntersection->getTwowayRoadBy(7)->getRightRoadStartRoadSetting();
        entryAttributes.append(new EntryAttribute(entryAttributes.size(), std::get<TrafficSceneConfiguration::RoadSettingIndex::VehicleEnterRate>(*rightRoadStartRoadSetting),
                                                  curIntersection->getTwowayRoadBy(7)->getRightRoadStartRoadIndex()));
        i -= horizontalIntersectionNumber;
    }
}

void Project::setAttributeBy(ProjectAttribute::Type attributeType, const int order, const int subAttributeOrder, const QVariant& newValue) {
    if (attributeType != ProjectAttribute::Type::EntryAttributeType &&
            attributeType != ProjectAttribute::Type::TrafficLightAttributeType) {
        throw UiException("Unsupported attribute type.");
    }

    if (order < 0) {
        throw UiException("Attribute order cannot less than zero.");
    }


    if (attributeType == ProjectAttribute::Type::EntryAttributeType) {
        if (order >= entryAttributes.size()) {
            throw UiException("Attribute order cannot be larger than or equal to container's size.");
        }

        if (subAttributeOrder == 0) {
            throw UiException("Can't change entry number");
        }

        entryAttributes.at(order)->setSubAttributeBy(subAttributeOrder, newValue);

        EntryAttribute* entryAttributeToBeChanged = dynamic_cast<EntryAttribute*>(entryAttributes.at(order));

        trafficSceneConfiguration->changeRoadSettingBy(entryAttributeToBeChanged->getCorrespondingRoadIndex(), TrafficSceneConfiguration::RoadSettingIndex::VehicleEnterRate, newValue);
    } else if (attributeType == ProjectAttribute::Type::TrafficLightAttributeType) {
        if (order >= trafficLightAttributes.size()) {
            throw UiException("Attribute order cannot be larger than or equal to container's size.");
        }

        if (subAttributeOrder != 2 && subAttributeOrder != 4) {
            throw UiException("Can't change traffic light attribute except green/red light duration.");
        }

        trafficLightAttributes.at(order)->setSubAttributeBy(subAttributeOrder, newValue);

        TrafficLightAttribute* trafficLightAttributeToBeChanged = dynamic_cast<TrafficLightAttribute*>(trafficLightAttributes.at(order));

        if (trafficLightAttributeToBeChanged->getHasAliasRoad()) {
            TrafficLightAttribute* aliasTrafficLightAttribute = dynamic_cast<TrafficLightAttribute*>(*std::find_if(trafficLightAttributes.begin(), trafficLightAttributes.end(),
                                                                             [=](ProjectAttribute* curProjectAttribute) {
                                                                                TrafficLightAttribute* curTrafficLightAttribute = dynamic_cast<TrafficLightAttribute*>(curProjectAttribute);
                                                                                return curTrafficLightAttribute->getHasAliasRoad() &&
                                                                                         curTrafficLightAttribute->getCorrespondingRoadIndex() == trafficLightAttributeToBeChanged->getCorrespondingRoadIndex() &&
                                                                                         curTrafficLightAttribute->getIntersectionNumber() != trafficLightAttributeToBeChanged->getIntersectionNumber() &&
                                                                                         curTrafficLightAttribute->getRoadSequenceNumber() != trafficLightAttributeToBeChanged->getRoadSequenceNumber();
                                                                             }));
            aliasTrafficLightAttribute->setSubAttributeBy(subAttributeOrder, newValue);
        }

        if (subAttributeOrder == 2) {
            trafficSceneConfiguration->changeRoadSettingBy(trafficLightAttributeToBeChanged->getCorrespondingRoadIndex(), TrafficSceneConfiguration::RoadSettingIndex::GreenDuration, newValue);
        } else if (subAttributeOrder == 4) {
            trafficSceneConfiguration->changeRoadSettingBy(trafficLightAttributeToBeChanged->getCorrespondingRoadIndex(), TrafficSceneConfiguration::RoadSettingIndex::RedDuration, newValue);
        }
    } else {
        throw UiException("Unsupported attribute type.");
    }
}

void Project::refreshModifiedDate() {
    lastModifiedDate = QDateTime::currentDateTime();
}

//int Project::getAliasRoadSequenceNumberFor(int roadSequenceNumber) const {
//    if (roadSequenceNumber < 0 || roadSequenceNumber > 15) {
//        throw UiException("Invalid road sequence number, can't find its alias.");
//    }

//    if (roadSequenceNumber == 0) {
//        return 11;
//    } else if (roadSequenceNumber == 1) {
//        return 10;
//    } else if (roadSequenceNumber == 2) {
//        return 9;
//    } else if (roadSequenceNumber == 3) {
//        return 8;
//    } else if (roadSequenceNumber == 4) {
//        return 15;
//    } else if (roadSequenceNumber == 5) {
//        return 14;
//    } else if (roadSequenceNumber == 6) {
//        return 13;
//    } else if (roadSequenceNumber == 7) {
//        return 12;
//    } else if (roadSequenceNumber == 8) {
//        return 3;
//    } else if (roadSequenceNumber == 9) {
//        return 2;
//    } else if (roadSequenceNumber == 10) {
//        return 1;
//    } else if (roadSequenceNumber == 11) {
//        return 0;
//    } else if (roadSequenceNumber == 12) {
//        return 7;
//    } else if (roadSequenceNumber == 13) {
//        return 6;
//    } else if (roadSequenceNumber == 14) {
//        return 5;
//    } else if (roadSequenceNumber == 15) {
//        return 4;
//    }

//    throw UiException("Invalid road sequence number, can't find its alias.");
//}
