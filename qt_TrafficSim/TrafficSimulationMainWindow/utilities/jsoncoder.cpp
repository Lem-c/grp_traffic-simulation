#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonValue>
#include <QIODevice>
#include <QFile>
#include <QDir>
#include <QDateTime>
#include <QTextStream>
#include <sstream>

#include "project.h"
#include "entryattribute.h"
#include "jsoncoder.h"
#include "uiexception.h"

void JsonCoder::saveProjectInto(const Project& project, QFile& projectFile) {
    QJsonObject jsonObject;
    jsonObject.insert("projectName", project.getProjectName());
    jsonObject.insert("userName", project.getUserName());
    jsonObject.insert("createDate", project.getCreateDate().toString(Qt::TextDate));
    jsonObject.insert("lastModifiedDate", project.getLastModifiedDate().toString(Qt::TextDate));
    jsonObject.insert("horizontalIntersectionNumber", project.getHorizontalIntersectionNumber());
    jsonObject.insert("verticalIntersectionNumber", project.getVerticalInersectionNumber());

    QJsonArray entryNumbers;
    QJsonArray vehicleInitialSpeeds;
    for (ProjectAttribute* entryAttribute : project.getEntryAttributes()) {
        auto entryAttributePtr = dynamic_cast<EntryAttribute*>(entryAttribute);
        entryNumbers.append(entryAttributePtr->getEntryNumber());
        vehicleInitialSpeeds.append(entryAttributePtr->getVehicleInitialSpeed());
    }
    jsonObject.insert("entryNumbers", entryNumbers);
    jsonObject.insert("vehicleInitialSpeeds", vehicleInitialSpeeds);

    QJsonArray intersectionNumbers;
    QJsonArray sequenceNumbers;
    QJsonArray greenDurations;
    QJsonArray yellowDurations;
    QJsonArray redDurations;
    for (ProjectAttribute* trafficLightAttribute : project.getTrafficLightAttributes()) {
        auto trafficLightAttributePtr = dynamic_cast<TrafficLightAttribute*>(trafficLightAttribute);
        intersectionNumbers.append(trafficLightAttributePtr->getIntersectionNumber());
        sequenceNumbers.append(trafficLightAttributePtr->getRoadSequenceNumber());
        greenDurations.append(trafficLightAttributePtr->getGreenDuration());
        yellowDurations.append(trafficLightAttributePtr->getYellowDuration());
        redDurations.append(trafficLightAttributePtr->getRedDuration());
    }
    jsonObject.insert("intersectionNumbers", intersectionNumbers);
    jsonObject.insert("sequenceNumbers", sequenceNumbers);
    jsonObject.insert("greenDurations", greenDurations);
    jsonObject.insert("yellowDurations", yellowDurations);
    jsonObject.insert("redDurations", redDurations);

    QJsonDocument document;
    document.setObject(jsonObject);
    QByteArray bytes = document.toJson(QJsonDocument::Indented);

    if(projectFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
    {
       QTextStream iStream(&projectFile);
       iStream << bytes;
       projectFile.close();
    }
    else
    {
       throw UiException("Cannot open the project json file.");
    }
}

Project JsonCoder::loadProjectFrom(QFile& projectFile){
    if(projectFile.open(QIODevice::ReadOnly)) {
        QByteArray bytes = projectFile.readAll();
        projectFile.close();

        QJsonDocument document = QJsonDocument::fromJson(bytes);

        if(document.isObject())
        {
            QJsonObject jsonObject = document.object();

            QString projectFileName = jsonObject.value("projectName").toString();
            QString userName = jsonObject.value("userName").toString();
            QDateTime createDate = QDateTime::fromString(jsonObject.value("createDate").toString(),
                                                         Qt::TextDate);
            QDateTime lastModifiedDate = QDateTime::fromString(jsonObject.value("lastModifiedDate").toString(),
                                                                Qt::TextDate);
            int horizonIntersectionNumber = jsonObject.value("horizontalIntersectionNumber").toInt();
            int verticalIntersectionNumber = jsonObject.value("verticalIntersectionNumber").toInt();

            QJsonArray entryNumbers = jsonObject.value("entryNumbers").toArray();
            QJsonArray vehicleInitialSpeeds = jsonObject.value("vehicleInitialSpeeds").toArray();

            QJsonArray intersectionNumbers = jsonObject.value("intersectionNumbers").toArray();
            QJsonArray sequenceNumbers = jsonObject.value("sequenceNumbers").toArray();
            QJsonArray greenDurations = jsonObject.value("greenDurations").toArray();
            QJsonArray yellowDurations = jsonObject.value("yellowDurations").toArray();
            QJsonArray redDurations = jsonObject.value("redDurations").toArray();

            Project project(projectFileName, userName, horizonIntersectionNumber, verticalIntersectionNumber);
            project.createDate = createDate;
            project.lastModifiedDate = lastModifiedDate;

            int i = 0;
            while (i < entryNumbers.size()) {
                project.setAttributeBy(ProjectAttribute::Type::EntryAttributeType, i, 1, vehicleInitialSpeeds.at(i).toDouble());
//                project.addAttributeAt(ProjectAttribute::Type::EntryAttributeType, i, new EntryAttribute(entryNumbers.at(i).toInt(), vehicleInitialSpeeds.at(i).toDouble()));
                i++;
            }

            i = 0;
            while (i < intersectionNumbers.size()) {
                project.setAttributeBy(ProjectAttribute::Type::TrafficLightAttributeType, i, 2, greenDurations.at(i).toDouble());
                project.setAttributeBy(ProjectAttribute::Type::TrafficLightAttributeType, i, 4, redDurations.at(i).toDouble());
//                project.addAttributeAt(ProjectAttribute::Type::TrafficLightAttributeType, i, new TrafficLightAttribute(intersectionNumbers.at(i).toInt(),
//                                                                           sequenceNumbers.at(i).toInt(),
//                                                                           greenDurations.at(i).toDouble(),
//                                                                           yellowDurations.at(i).toDouble(),
//                                                                           redDurations.at(i).toDouble()));

                i++;
            }

            return project;
        }
        else
        {
            throw UiException("Cannot parse the project json file.");
        }
    }
    else
    {
        throw UiException("Cannot open the project json file.");
    }

}
