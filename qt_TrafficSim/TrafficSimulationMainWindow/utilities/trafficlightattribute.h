#ifndef TRAFFICLIGHTATTRIBUTE_H
#define TRAFFICLIGHTATTRIBUTE_H

#include "projectattribute.h"
#include "uiexception.h"

/**
 * @class TrafficLightAttribute trafficLightAttribute.h
 * @brief Stores the attribute of the traffic light
 * 
 * @author YiCun Duan
 */
class TrafficLightAttribute : public ProjectAttribute
{
public:
    /**
     * @brief Construct a new Traffic Light Attribute object
     * 
     * @param[in] intersectionNumber : the number of intersection
     * @param[in] roadSequenceNumber : the sequence number of the road
     * @param[in] greenDuration : the duration of the green light
     * @param[in] yellowDuration : the duration of the yellow light
     * @param[in] redDuration : the duration of the red light
     * @param[in] correspondingRoadIndex : the corresponding road index
     * @param[in] hasAliasRoadIndex : whether the road has alias
     */
    TrafficLightAttribute(int intersectionNumber, int roadSequenceNumber,
                          double greenDuration, double yellowDuration,
                          double redDuration,
                          unsigned int correspondingRoadIndex,
                          bool hasAliasRoadIndex);

    /**
     * @brief Destroy the Traffic Light Attribute object
     * 
     */
    virtual ~TrafficLightAttribute() = default;

    /**
     * @brief Get the number of intersections
     * 
     * @return int 
     */
    inline int getIntersectionNumber() const;

    /**
     * @brief Get the sequence number of the road
     * 
     * @return int 
     */
    inline int getRoadSequenceNumber() const;

    /**
     * @brief Get the duration of the green light
     * 
     * @return double 
     */
    inline double getGreenDuration() const;

    /**
     * @brief Get the duration of the yellow light
     * 
     * @return double 
     */
    inline double getYellowDuration() const;

    /**
     * @brief Get the duration of the red light
     * 
     * @return double 
     */
    inline double getRedDuration() const;

    /**
     * @brief Get the corresponding road index
     * 
     * @return unsigned int 
     */
    inline unsigned int getCorrespondingRoadIndex() const;
    
    /**
     * @brief Get the Has Alias Road object
     * 
     * @return true 
     * @return false 
     */
    inline bool getHasAliasRoad() const;

    /**
     * @brief Get the Sub Attribute By object
     * 
     * @param[in] subAttributeNumber : the number of sub attribute
     * @return QVariant 
     */
    virtual QVariant getSubAttributeBy(const int subAttributeNumber) const override;

    /**
     * @brief Set the Sub Attribute By object
     * 
     * @param[in] subAttributeNumber : the number of the sub attribute
     * @param[in] value : the new value entered
     */
    virtual void setSubAttributeBy(const int subAttributeNumber, const QVariant& value) override;

    /**
     * @brief sub attribute count
     * 
     */
    static const int subAttributeCount;

private:
    /**
     * @brief Number of intersection
     * 
     */
    int intersectionNumber;

    /**
     * @brief Sequence number of the road
     * 
     */
    int roadSequenceNumber;

    /**
     * @brief The duration of the green light
     * 
     */
    double greenDuration;

    /**
     * @brief The duration of the yellow light
     * 
     */
    double yellowDuration;

    /**
     * @brief The duration of the red light
     * 
     */
    double redDuration;

    /**
     * @brief The corresponding road index of the traffic light
     * 
     */
    unsigned int correspondingRoadIndex;

    /**
     * @brief Whether the road has alias
     * 
     */
    bool hasAliasRoad;
};

int TrafficLightAttribute::getIntersectionNumber() const {
    return intersectionNumber;
}

int TrafficLightAttribute::getRoadSequenceNumber() const {
    return roadSequenceNumber;
}

double TrafficLightAttribute::getGreenDuration() const {
    return greenDuration;
}

double TrafficLightAttribute::getYellowDuration() const {
    return yellowDuration;
}

double TrafficLightAttribute::getRedDuration() const {
    return redDuration;
}

unsigned int TrafficLightAttribute::getCorrespondingRoadIndex() const {
    return correspondingRoadIndex;
}

bool TrafficLightAttribute::getHasAliasRoad() const {
    return hasAliasRoad;
}

#endif // TRAFFICLIGHTATTRIBUTE_H
