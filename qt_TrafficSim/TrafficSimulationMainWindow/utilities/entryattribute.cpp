#include "entryattribute.h"
#include "uiexception.h"


const int EntryAttribute::subAttributeCount = 2;

//EntryAttribute::EntryAttribute() :
//    ProjectAttribute(), entryNumber(0), vehicleInitialSpeed(0.0),
//    intersectionNumber(-1), sequenceNumber(-1), left(false)
//{

//}

EntryAttribute::EntryAttribute(int entryNumber, double vehicleInitialSpeed, unsigned int correspondingRoadIndex) :
    ProjectAttribute(), entryNumber(entryNumber), vehicleInitialSpeed(vehicleInitialSpeed),
    correspondingRoadIndex(correspondingRoadIndex)
{

}

QVariant EntryAttribute::getSubAttributeBy(const int subAttributeOrder) const {
    switch (subAttributeOrder) {
        case 0:
            return entryNumber;
        case 1:
            return vehicleInitialSpeed;
        default:
            throw UiException("attributeOrder is invalid.");
    }
}

void EntryAttribute::setSubAttributeBy(const int subAttributeOrder, const QVariant& value) {
    switch (subAttributeOrder) {
        case 0:
            throw UiException("Can't change the entry number.");
        case 1:
            vehicleInitialSpeed = value.toDouble();
            break;
        default:
            throw UiException("attributeOrder is invalid.");
    }
}
