#include "trafficlightattribute.h"
#include "uiexception.h"

const int TrafficLightAttribute::subAttributeCount = 5;

//TrafficLightAttribute::TrafficLightAttribute() :
//    ProjectAttribute(), intersectionNumber(0), sequenceNumber(0),
//    greenDuration(0.0), yellowDuration(0.0), redDuration(0.0),
//    aliasIntersectionNumber(NullAliasIntersectionNumber), aliasSequenceNumber(NullAliasSequenceNumber)
//{

//}

TrafficLightAttribute::TrafficLightAttribute(int intersectionNumber, int roadSequenceNumber,
                                             double greenDuration, double yellowDuration,
                                             double redDuration,
                                             unsigned int correspondingRoadIndex,
                                             bool hasAliasRoadIndex):
    ProjectAttribute(),
    intersectionNumber(intersectionNumber), roadSequenceNumber(roadSequenceNumber),
    greenDuration(greenDuration), yellowDuration(yellowDuration), redDuration(redDuration),
    correspondingRoadIndex(correspondingRoadIndex),
    hasAliasRoad(hasAliasRoadIndex)
{

}

QVariant TrafficLightAttribute::getSubAttributeBy(const int subAttributeNumber) const {
    switch (subAttributeNumber) {
        case 0:
            return intersectionNumber;
        case 1:
            return roadSequenceNumber;
        case 2:
            return greenDuration;
        case 3:
            return yellowDuration;
        case 4:
            return redDuration;
        default:
            throw UiException("attributeNumber is invalid.");
    }
}

void TrafficLightAttribute::setSubAttributeBy(const int subAttributeNumber, const QVariant &value) {
    switch (subAttributeNumber) {
        case 0:
            throw UiException("Can't change intersection number of traffic light attribute.");
        case 1:
            throw UiException("Can't change road sequence number of traffic light attribute.");
        case 2:
            greenDuration = value.toDouble();
            break;
        case 3:
            yellowDuration = value.toDouble();
            break;
        case 4:
            redDuration = value.toDouble();
            break;
        default:
            throw UiException("attributeNumber is invalid.");
    }
}
