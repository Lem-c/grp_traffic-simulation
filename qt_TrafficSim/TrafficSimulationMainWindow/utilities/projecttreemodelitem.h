#ifndef PROJECTTREEMODELITEM_H
#define PROJECTTREEMODELITEM_H

#include <QVariant>
#include <QList>

/**
 * @class Project project.h
 * @brief Tree model that 
 * 
 * @author YiCun Duan
 */
class ProjectTreeModelItem
{
public:
    /**
     * @brief Construct a new Project Tree Model Item object
     * 
     * @param[in] data : the data of the project
     * @param[in] parentItem : the parent of the object
     */
    explicit ProjectTreeModelItem(const QList<QVariant>& data, ProjectTreeModelItem* parentItem = nullptr);
    
    /**
     * @brief Destroy the Project Tree Model Item object
     * 
     */
    ~ProjectTreeModelItem();
    
    /**
     * @brief Append a child
     * 
     * @param[in] child : the child object to be entered
     */
    void appendChild(ProjectTreeModelItem* child);

    /**
     * @brief Get the Child at designated row
     * 
     * @param[in] row : the row index of the child
     * @return ProjectTreeModelItem* 
     */
    ProjectTreeModelItem* getChildAt(const int row);

    /**
     * @brief Clear children
     * 
     */
    inline void clearChildren();

    /**
     * @brief Get the number of children
     * 
     * @return int 
     */
    int getChildrenNumber() const;

    /**
     * @brief Get the number of column
     * 
     * @return int 
     */
    int getColumnNumber() const;

    /**
     * @brief Get data of the designated column
     * 
     * @param[in] column : the column index of the data
     * @return QVariant 
     */
    QVariant data(const int column) const;

    /**
     * @brief Get the row of the current object
     * 
     * @return int 
     */
    int row() const;
    
    /**
     * @brief Get the Parent of this object
     * 
     * @return ProjectTreeModelItem* 
     */
    ProjectTreeModelItem* getParent();

private:
    /**
     * @brief List of children of the tree
     * 
     */
    QList<ProjectTreeModelItem*> children;
    
    /**
     * @brief List of the data
     * 
     */
    QList<QVariant> dataList;

    /**
     * @brief Parent of this object
     * 
     */
    ProjectTreeModelItem* parent;
};

void ProjectTreeModelItem::clearChildren() {
    for (ProjectTreeModelItem* child : children) {
        delete child;
    }

    children.clear();
}

#endif // PROJECTTREEMODELITEM_H
