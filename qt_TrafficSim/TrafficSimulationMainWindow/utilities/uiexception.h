#ifndef UIEXCEPTION_H
#define UIEXCEPTION_H

#include <exception>
#include <QString>
#include <string>

/**
 * @class UiException uiexception.h
 * @brief The uiexception
 * 
 * @author YiCun Duan
 */
class UiException : public std::exception
{
public:
    /**
     * @brief Construct a new Ui Exception object
     * 
     * @param[in] errorMessage : the error message of the exception
     */
    UiException(const std::string& errorMessage);

    /**
     * @brief Get the error message of the exception
     * 
     * @return const char* 
     */
    virtual const char* what() const noexcept override;

private:
    /**
     * @brief Error message of the exception
     * 
     */
    std::string errorMessage;
};

#endif // UIEXCEPTION_H
