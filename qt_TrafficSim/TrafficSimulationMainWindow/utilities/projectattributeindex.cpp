#include "projectattributeindex.h"

const int ProjectAttributeIndex::NullOrder = -1;
const int ProjectAttributeIndex::NullAttributeNumber = -1;

ProjectAttributeIndex::ProjectAttributeIndex(const QString& projectName, ProjectAttribute::Type attributeType,
                                             const int order, const int attributeNumber) :
    projectName(projectName), attributeType(attributeType), order(order), subAttributeNumber(attributeNumber)
{

}
