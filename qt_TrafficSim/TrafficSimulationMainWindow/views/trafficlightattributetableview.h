#ifndef TRAFFICLIGHTATTRIBUTETABLEVIEW_H
#define TRAFFICLIGHTATTRIBUTETABLEVIEW_H

#include <QDialog>
#include "../models/trafficlightattributetablefilter.h"

#include "../models/projectattributetablemodel.h"

QT_BEGIN_NAMESPACE
namespace Ui { class TrafficLightAttributeTableView; }
QT_END_NAMESPACE

/**
 *  @class TrafficLightAttributeTableView trafficlightattributetableview.h
 *  @brief This class if for the table view of the traffic light attribute
 * 
 *  @author YiCun Duan
 */
class TrafficLightAttributeTableView : public QDialog
{

    Q_OBJECT

public:
    /**
     * @brief Construct a new Traffic Light Attribute Table View object
     * 
     * @param parent : the parent class
     */
    explicit TrafficLightAttributeTableView(QWidget* parent = nullptr);
    /**
     * @brief Destroy the Traffic Light Attribute Table View object
     * 
     */
    ~TrafficLightAttributeTableView();

    /**
     * @brief Set the Models object
     * 
     * @param trafficLightAttributeTableModel : the traffic light attribute table model
     * @param projectModel : the project model
     * @param filterModel : the filter model
     */
    void setModels(ProjectAttributeTableModel* trafficLightAttributeTableModel,
                   ProjectModel* projectModel, TrafficLightAttributeTableFilter* filterModel);

private slots:
//    void handleAddButtonOnClicked();
//    void handleDeleteButtonOnClicked();

private:
    Ui::TrafficLightAttributeTableView* ui;
};

#endif // TRAFFICLIGHTATTRIBUTETABLEVIEW_H
