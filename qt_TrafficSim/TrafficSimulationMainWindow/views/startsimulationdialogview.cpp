#include "startsimulationdialogview.h"
#include "ui_startsimulationdialogview.h"

StartSimulationDialogView::StartSimulationDialogView(QWidget* parent):
    QDialog(parent), ui(new Ui::StartSimulationDialogView()), projectModel(nullptr),
    selectedProjectName("")
{
    ui->setupUi(this);
}

StartSimulationDialogView::~StartSimulationDialogView() {
    delete ui;
}

void StartSimulationDialogView::setSourceModel(ProjectModel* projectModel) {
    this->projectModel = projectModel;

    ui->projectSelectionComboBox->addItems(projectModel->getProjectNames());

    connect(ui->confirmButton, &QAbstractButton::clicked, this, &StartSimulationDialogView::handleConfirmButtonOnClicked);
}

void StartSimulationDialogView::handleConfirmButtonOnClicked() {
    selectedProjectName = ui->projectSelectionComboBox->currentText();
    this->accept();
}
