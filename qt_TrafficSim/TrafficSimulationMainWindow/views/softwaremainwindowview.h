#ifndef SOFTWAREMAINWINDOWVIEW_H
#define SOFTWAREMAINWINDOWVIEW_H

#include <QMainWindow>
#include <QAction>
#include <QList>
#include <QString>
#include <QChartView>
#include <QColor>
#include <sstream>
#include "../models/projectmodel.h"
#include "createnewprojectdialogview.h"
#include "../models/usedfilestrackermodel.h"
#include "../models/projecttreemodel.h"
#include "../models/averagespeedchartmodel.h"
#include "../models/throughputchartmodel.h"
#include "../models/vehicleinputtablemodel.h"
#include "../models/throughputtablemodel.h"
#include "../simulation/trafficsimulationview.h"
#include "../simulation/trafficsimulatorthread.h"

class PaintingHelper;
class GLWidget;
class SaveProjectDialogView;
class VehicleInputChartModel;
class AverageSpeedGaugeView;
class ThroughputOverviewChartModel;
class HorizontalColumnModel;

QT_BEGIN_NAMESPACE
namespace Ui { class SoftwareMainWindowView; }
QT_END_NAMESPACE

/**
 *  @class SoftwareMainWindowView softwaremainwindowview.h
 *  @brief This class is for main window view of the software
 * 
 *  @author YiCun Duan
 */
class SoftwareMainWindowView : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Construct a new Software Main Window View object
     * 
     * @param parent : the parent class
     */
    SoftwareMainWindowView(QWidget* parent = nullptr);
    /**
     * @brief Destroy the Software Main Window View object
     * 
     */
    ~SoftwareMainWindowView();

    /**
     * @brief Set the Models object
     * 
     * @param projectModel : the project model
     * @param usedFilesTrackerModel : the used files tracker model
     * @param trafficDataModel : the traffic data model
     */
    void setModels(ProjectModel* projectModel, UsedFilesTrackerModel* usedFilesTrackerModel, TrafficDataModel* trafficDataModel);

    /**
     * @brief 
     * 
     */
    static const int simulationRefreshTimeInterval;


protected:
#ifndef QT_NO_CONTEXMENU
    void contextMenuEvent(QContextMenuEvent* event) override;
#endif

private slots:
    void createNewProject();
    void loadProject();
    void saveProject();
    void manageEntryAttributes();
    void manageTrafficLightAttributes();
    void defaultStyle();
    void unityStyle();
    void showHelpManual();
    void aboutUs();
    void showRightClickMenu();
    void updateRecentlyUsedFilesMenuWith(const QList<UsedFilesTrackerModel::Record>& recentlyUsedFilesRecords);
    void addNewRecordIntoRecentlyUsedFilesMenu(const UsedFilesTrackerModel::Record& newRecord);
    void stopSimulation();
    void pauseSimulation();
    void startSimulation();

private:
    Ui::SoftwareMainWindowView* ui;
    QChartView* averageSpeedChartView;
    QChartView* throughputChartView;
    QChartView* vehicleInputChartView;
    AverageSpeedGaugeView* averageSpeedGaugeView;
    QChartView* throughputOverviewChartView;
    TrafficSimulationView* trafficSimulationView;
    QAction* actionRightClick;
    QMenu* recentlyUsedFilesMenu;
    QActionGroup* styleActionsGroup;
    QToolBar* simulationControlToolBar;
    QAction* actionStop;
    QAction* actionPause;
    QAction* actionStart;

    ProjectTreeModel* projectTreeModel;
    TrafficDataModel* trafficDataModel;
    AverageSpeedChartModel* averageSpeedChartModel;
    ThroughputChartModel* throughputChartModel;
    VehicleInputChartModel* vehicleInputChartModel;
    VehicleInputTableModel* vehicleInputTableModel;
    ThroughputTableModel* throughputTableModel;
    ThroughputOverviewChartModel* throughputOverviewChartModel;
    TrafficSimulator* trafficSimulator;
    TrafficSimulatorThread* trafficSimulatorThread;

    bool firstRunSimulation;
    bool simulationIsStopped;
    bool simulationIsPaused;
    bool simulationIsRunning;

//    PaintingHelper* paintingHelper;
    QTimer* simulationViewRefreshTimer;
    int totalTime;
//    GLWidget* glWidget;

    ProjectModel* projectModel;
    UsedFilesTrackerModel* usedFilesTrackerModel;

    QString getProjectsDetails();
};

#endif // SOFTWAREMAINWINDOWVIEW_H
