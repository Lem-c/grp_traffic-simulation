#include "createnewprojectdialogview.h"
#include "ui_createnewprojectdialogview.h"
#include "../models/projectmodel.h"
#include "../utilities/project.h"
#include <QFileDialog>
#include <QFlag>

CreateNewProjectDialogView::CreateNewProjectDialogView(QWidget* parent)
    : QDialog(parent), ui(new Ui::CreateNewProjectDialogView()), projectModel(nullptr)
{
    ui->setupUi(this);
    ui->horizontalIntersectionNumberSpinBox->setRange(1, 5);
    ui->verticalIntersectionNumberSpinBox->setRange(1, 5);
}

CreateNewProjectDialogView::~CreateNewProjectDialogView() {
    delete ui;
}

void CreateNewProjectDialogView::setSourceModel(ProjectModel* projectModel) {
    this->projectModel = projectModel;

    connect(ui->confirmButton, &QAbstractButton::clicked, this, &CreateNewProjectDialogView::handleConfirmButtonOnClicked);
    connect(ui->cancelButton, &QAbstractButton::clicked, this, &CreateNewProjectDialogView::handleCancelButtonOnClicked);
//    connect(ui->setDatabaseButton, &QAbstractButton::clicked, this, &CreateNewProjectDialogView::handleSetDatabaseButtonOnClicked);
}

void CreateNewProjectDialogView::handleConfirmButtonOnClicked() {
    QString projectName = ui->projectNameLineEdit->text();
    QString userName = ui->userNameLineEdit->text();
    int horizontalIntersectionNumber = ui->horizontalIntersectionNumberSpinBox->value();
    int verticalIntersectionNumber = ui->verticalIntersectionNumberSpinBox->value();
//    QString dataBasePath = ui->databasePathLabel->text();

    projectModel->addProjectAt(projectModel->getProjects().size(), Project(projectName, userName, horizontalIntersectionNumber, verticalIntersectionNumber));
    this->accept();
}

void CreateNewProjectDialogView::handleCancelButtonOnClicked() {
    this->reject();
}

//void CreateNewProjectDialogView::handleSetDatabaseButtonOnClicked() {
//    const QFileDialog::Options options = QFlag(QFileDialog::DontUseCustomDirectoryIcons);
//    QString selectedFilter;
//    QString fileName = QFileDialog::getOpenFileName(this,
//                                QString::fromStdString("Get Database"),
//                                QString::fromStdString("Please enter the database path"),
//                                QString::fromStdString("All File (*);;Database File (*.db)"),
//                                &selectedFilter,
//                                options);
////    if (!fileName.isEmpty()) {
////        ui->databasePathLabel->setText(fileName);
////    }
//}
