#ifndef CREATENEWPROJECTDIALOGVIEW_H
#define CREATENEWPROJECTDIALOGVIEW_H

#include <QDialog>

class ProjectModel;
class Project;

QT_BEGIN_NAMESPACE
namespace Ui { class CreateNewProjectDialogView; }
QT_END_NAMESPACE

/**
 *  @class CreateNewProjectDialogView createnewprojectdialogview.h
 *  @brief This class is for new project dialog view
 * 
 *  @author YiCun Duan
 */
class CreateNewProjectDialogView : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Create a New Project Dialog View object
     * 
     * @param parent : the parent class
     */
    explicit CreateNewProjectDialogView(QWidget* parent = nullptr);
    /**
     * @brief Destroy the Create New Project Dialog View object
     * 
     */
    ~CreateNewProjectDialogView();

    /**
     * @brief Set the Source Model object
     * 
     * @param projectModel : the project model to be set as source model
     */
    void setSourceModel(ProjectModel* projectModel);

private slots:
    void handleConfirmButtonOnClicked();
    void handleCancelButtonOnClicked();
//    void handleSetDatabaseButtonOnClicked();

private:
    Ui::CreateNewProjectDialogView* ui;
    ProjectModel* projectModel;
};

#endif // CREATENEWPROJECTDIALOGVIEW_H
