#ifndef SAVEPROJECTDIALOGVIEW_H
#define SAVEPROJECTDIALOGVIEW_H

#include <QDialog>

class ProjectModel;

QT_BEGIN_NAMESPACE
namespace Ui { class SaveProjectDialogView; }
QT_END_NAMESPACE

/**
 *  @class SaveProjectDialogView saveprojectdialogview.h
 *  @brief This class saves the project dialog view
 * 
 *  @author YiCun Duan
 */
class SaveProjectDialogView : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Construct a new Save Project Dialog View object
     * 
     * @param parent : the parent class
     */
    explicit SaveProjectDialogView(QWidget* parent = nullptr);
    /**
     * @brief Destroy the Save Project Dialog View object
     * 
     */
    ~SaveProjectDialogView();

    /**
     * @brief Set the Source Model object
     * 
     * @param projectModel : the project model
     */
    void setSourceModel(ProjectModel* projectModel);

private slots:
    void handleSaveFileButtonOnClicked();

private:
    Ui::SaveProjectDialogView* ui;
    ProjectModel* projectModel;
};

#endif // SAVEPROJECTDIALOGVIEW_H
