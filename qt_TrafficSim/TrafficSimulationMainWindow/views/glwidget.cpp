#include "glwidget.h"
#include "paintinghelper.h"

#include <QPainter>
#include <QTimer>

GLWidget::GLWidget(QWidget* parent, PaintingHelper* paintingHelper)
    : QOpenGLWidget(parent), paintingHelper(paintingHelper)
{
    elapsed = 0;
//    setFixedSize(parent->rect().width(), parent->rect().height());
//    setAutoFillBackground(false);
}

void GLWidget::animate()
{
    elapsed = (elapsed + qobject_cast<QTimer*>(sender())->interval()) % 1000;
    update();
}


void GLWidget::paintEvent(QPaintEvent* event)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);
    paintingHelper->paint(&painter, event, elapsed);
    painter.end();
}
