#ifndef PAINTINGHELPER_H
#define PAINTINGHELPER_H

#include <QWidget>
#include <QBrush>
#include <QFont>
#include <QPen>

/**
 *  @class PaintingHelper paintinghelper.h
 *  @brief This class is the helps paint the scene
 * 
 *  @author YiCun Duan
 */
class PaintingHelper
{
public:
    /**
     * @brief Construct a new Painting Helper object
     * 
     */
    PaintingHelper();

    /**
     * @brief 
     * 
     * @param painter : the painter object
     * @param event : the painted event
     * @param elapsed
     */
    void paint(QPainter* painter, QPaintEvent* event, int elapsed);

private:
    QBrush background;
    QBrush circleBrush;
    QFont textFont;
    QPen circlePen;
    QPen textPen;
};

#endif // PAINTINGHELPER_H
