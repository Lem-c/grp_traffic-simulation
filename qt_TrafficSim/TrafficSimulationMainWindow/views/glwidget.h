#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>

class PaintingHelper;

/**
 *  @class GLWidget glwidget.h
 *  @brief This class is for new project dialog view
 * 
 *  @author YiCun Duan
 */
class GLWidget : public QOpenGLWidget
{
    Q_OBJECT

public:
    /**
     * @brief Construct a new GLWidget object
     * 
     * @param parent : the parent class
     * @param paintingHelper : the painting helper
     */
    GLWidget(QWidget* parent, PaintingHelper* paintingHelper);

public slots:
    /**
     * @brief the function that helps update the animation
     * 
     */
    void animate();

protected:
    /**
     * @brief paints the event on the scene
     * 
     * @param event 
     */
    void paintEvent(QPaintEvent* event) override;

private:
    PaintingHelper* paintingHelper;
    int elapsed;
};

#endif // GLWIDGET_H
