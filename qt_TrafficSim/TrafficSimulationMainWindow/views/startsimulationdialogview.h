#ifndef STARTSIMULATIONDIALOGVIEW_H
#define STARTSIMULATIONDIALOGVIEW_H

#include <QDialog>
#include "../models/projectmodel.h"

QT_BEGIN_NAMESPACE
namespace Ui {class StartSimulationDialogView; }
QT_END_NAMESPACE

/**
 *  @class StartSimulationDialogView startsimulationdialogview.h
 *  @brief This class is for main window view of the software
 * 
 *  @author YiCun Duan
 */
class StartSimulationDialogView : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Construct a new Start Simulation Dialog View object
     * 
     * @param parent : the parent class
     */
    explicit StartSimulationDialogView(QWidget* parent = nullptr);
    /**
     * @brief Destroy the Start Simulation Dialog View object
     * 
     */
    ~StartSimulationDialogView();

    /**
     * @brief Set the Source Model object
     * 
     * @param projectModel : the project model
     */
    void setSourceModel(ProjectModel* projectModel);

    /**
     * @brief Get the Selected Project Name object
     * 
     * @return const QString& 
     */
    inline const QString& getSelectedProjectName() const;

private slots:
    void handleConfirmButtonOnClicked();

private:
    Ui::StartSimulationDialogView* ui;
    ProjectModel* projectModel;
    QString selectedProjectName;
};

const QString& StartSimulationDialogView::getSelectedProjectName() const {
    return selectedProjectName;
}

#endif // STARTSIMULATIONDIALOGVIEW_H
