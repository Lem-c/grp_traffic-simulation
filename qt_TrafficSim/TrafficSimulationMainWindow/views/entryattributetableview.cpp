#include "entryattributetableview.h"
#include "ui_entryattributetableview.h"
#include "../models/projectmodel.h"
#include <qlistview.h>

EntryAttributeTableView::EntryAttributeTableView(QWidget* parent)
    : QDialog(parent), ui(new Ui::EntryAttributeTableView())
{
    ui->setupUi(this);

    ui->entryAttributeTableView->horizontalHeader()->setSectionsMovable(false);
    ui->entryAttributeTableView->horizontalHeader()->setSectionsClickable(false);
    ui->entryAttributeTableView->horizontalHeader()->setHighlightSections(false);


    ui->entryAttributeTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->entryAttributeTableView->setAlternatingRowColors(true);

}

EntryAttributeTableView::~EntryAttributeTableView() {
    delete ui;
}

void EntryAttributeTableView::setModels(ProjectAttributeTableModel* entryAttributeTableModel,
                                        ProjectModel* projectModel, EntryAttributeTableFilter* filterModel) {
    ui->projectComboBox->setView(new QListView());
    ui->projectComboBox->addItems(projectModel->getProjectNames());
    ui->projectComboBox->setCurrentIndex(0);
    //ui->projectComboBox->setStyleSheet("QComboBox {combobox-popup:0;}");


    entryAttributeTableModel->setSourceModel(projectModel);
    entryAttributeTableModel->setSelectedProjectName(ui->projectComboBox->currentText());

    entryAttributeTableModel->setParent(ui->entryAttributeTableView);
    filterModel->setParent(ui->entryAttributeTableView);
    filterModel->setSourceModel(entryAttributeTableModel);
    if (ui->filterCheckBox->isChecked()) {
        filterModel->setEnableFiltering(true);
    } else {
        filterModel->setEnableFiltering(false);
    }
    ui->entryAttributeTableView->setModel(filterModel);

    ui->entryAttributeTableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->entryAttributeTableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

//    connect(ui->addButton, &QPushButton::clicked, this, &EntryAttributeTableView::handleAddButtonOnClicked);
//    connect(ui->deleteButton, &QPushButton::clicked, this, &EntryAttributeTableView::handleDeleteButtonOnClicked);
    connect(ui->quitButton, &QPushButton::clicked, this, &EntryAttributeTableView::close);

    connect(ui->projectComboBox, &QComboBox::currentTextChanged,
            entryAttributeTableModel, &ProjectAttributeTableModel::setSelectedProjectName);

    connect(ui->entryNumberSpinBox, &QSpinBox::valueChanged, filterModel, &EntryAttributeTableFilter::setEntryNumber);
    connect(ui->filterCheckBox, &QCheckBox::stateChanged, filterModel, &EntryAttributeTableFilter::setEnableFiltering);
}

//void EntryAttributeTableView::handleAddButtonOnClicked() {
//    if (ui->entryAttributeTableView->currentIndex().isValid()) {
//        ui->entryAttributeTableView->model()->insertRow(ui->entryAttributeTableView->currentIndex().row());
//    } else {
//        ui->entryAttributeTableView->model()->insertRow(0);
//    }
//}

//void EntryAttributeTableView::handleDeleteButtonOnClicked() {
//    if (ui->entryAttributeTableView->currentIndex().isValid()) {
//        ui->entryAttributeTableView->model()->removeRow(ui->entryAttributeTableView->currentIndex().row());
//    } else {
//        ui->entryAttributeTableView->model()->removeRow(0);
//    }

//}

