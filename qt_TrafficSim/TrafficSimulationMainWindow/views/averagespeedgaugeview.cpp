﻿#pragma execution_character_set("utf-8")

#include "averagespeedgaugeview.h"
#include <QPainter>
#include <QtMath>
#include <QDebug>
#include <QFontMetrics>
#include <QPainterPath>

AverageSpeedGaugeView::AverageSpeedGaugeView(QWidget* parent) :
    QOpenGLWidget(parent)
{
    minValue = 0;
    maxValue = 100;
    value = 0;
    precision = 0;

    scaleMajor = 10;
    scaleMinor = 10;
    startAngle = 45;
    endAngle = 45;

    animation = false;
    animationStep = 0.5;

    outerCircleColor = QColor(80, 80, 80);
    innerCircleColor = QColor(60, 60, 60);

    pieColorStart = QColor(24, 189, 155);
    pieColorMid = QColor(218, 218, 0);
    pieColorEnd = QColor(255, 107, 107);

    coverCircleColor = QColor(100, 100, 100);
    scaleColor = QColor(255, 255, 255);
    pointerColor = QColor(255, 107, 107);
    centerCircleColor = QColor(250, 250, 250);
    textColor = QColor(0, 0, 0);

    showOverlay = true;
    overlayColor = QColor(255, 255, 255, 60);

    circleWidth = 15;
    pieStyle = PieStyle_Three;
    pointerStyle = PointerStyle_Indicator;

    reverse = false;
    currentValue = 0;
//    timer = new QTimer(this);
//    timer->setInterval(10);
//    connect(timer, &QTimer::timeout, this, &AverageSpeedGaugeView::updateValue);

    setFont(QFont("Arial", 8));
}

AverageSpeedGaugeView::~AverageSpeedGaugeView()
{
//    if (timer->isActive()) {
//        timer->stop();
//    }
}

void AverageSpeedGaugeView::setSourceModel(const TrafficDataModel *trafficDataModel)
{
    connect(trafficDataModel, &TrafficDataModel::averageSpeedChangedAt, this, [=](int time, double averageSpeed) {
//        qDebug()<<"averageSpeed :"<<averageSpeed;
        setValue(averageSpeed);
    });
}

void AverageSpeedGaugeView::paintEvent(QPaintEvent *)
{
    int width = this->width();
    int height = this->height();
    int side = qMin(width, height);

    //绘制准备工作,启用反锯齿,平移坐标轴中心,等比例缩放
    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    painter.translate(width / 2, height / 2);
    painter.scale(side / 200.0, side / 200.0);

    //画背景
    drawBackground(&painter);
    //绘制外圆
    drawOuterCircle(&painter);
    //绘制内圆
    drawInnerCircle(&painter);
    //绘制饼圆
    drawColorPie(&painter);
    //绘制覆盖圆 用以遮住饼圆多余部分
    drawCoverCircle(&painter);
    //绘制刻度线
    drawScale(&painter);
    //绘制刻度值
    drawScaleNum(&painter);

    //根据指示器形状绘制指示器
    if (pointerStyle == PointerStyle_Circle) {
        drawPointerCircle(&painter);
    } else if (pointerStyle == PointerStyle_Indicator) {
        drawPointerIndicator(&painter);
    } else if (pointerStyle == PointerStyle_IndicatorR) {
        drawPointerIndicatorR(&painter);
    } else if (pointerStyle == PointerStyle_Triangle) {
        drawPointerTriangle(&painter);
    }

    //绘制指针中心圆外边框
    drawRoundCircle(&painter);
    //绘制指针中心圆
    drawCenterCircle(&painter);
    //绘制当前值
    drawValue(&painter);
    //绘制遮罩层
    drawOverlay(&painter);
}

void AverageSpeedGaugeView::drawOuterCircle(QPainter *painter)
{
    int radius = 99;
    painter->save();
    painter->setPen(Qt::NoPen);
    painter->setBrush(outerCircleColor);
    painter->drawEllipse(-radius, -radius, radius * 2, radius * 2);
    painter->restore();
}

void AverageSpeedGaugeView::drawInnerCircle(QPainter *painter)
{
    int radius = 90;
    painter->save();
    painter->setPen(Qt::NoPen);
    painter->setBrush(innerCircleColor);
    painter->drawEllipse(-radius, -radius, radius * 2, radius * 2);
    painter->restore();
}

void AverageSpeedGaugeView::drawColorPie(QPainter *painter)
{
    int radius = 60;
    painter->save();
    painter->setPen(Qt::NoPen);

    QRectF rect(-radius, -radius, radius * 2, radius * 2);

    if (pieStyle == PieStyle_Three) {
        //计算总范围角度,根据占比例自动计算三色圆环范围角度
        //可以更改比例
        double angleAll = 360.0 - startAngle - endAngle;
        double angleStart = angleAll * 0.7;
        double angleMid = angleAll * 0.15;
        double angleEnd = angleAll * 0.15;

        //增加偏移量使得看起来没有脱节
        int offset = 3;

        //绘制开始饼圆
        painter->setBrush(pieColorStart);
        painter->drawPie(rect, (270 - startAngle - angleStart) * 16, angleStart * 16);

        //绘制中间饼圆
        painter->setBrush(pieColorMid);
        painter->drawPie(rect, (270 - startAngle - angleStart - angleMid) * 16 + offset, angleMid * 16);

        //绘制结束饼圆
        painter->setBrush(pieColorEnd);
        painter->drawPie(rect, (270 - startAngle - angleStart - angleMid - angleEnd) * 16 + offset * 2, angleEnd * 16);
    } else if (pieStyle == PieStyle_Current) {
        //计算总范围角度,当前值范围角度,剩余值范围角度
        double angleAll = 360.0 - startAngle - endAngle;
        double angleCurrent = angleAll * ((currentValue - minValue) / (maxValue - minValue));
        double angleOther = angleAll - angleCurrent;

        //绘制当前值饼圆
        painter->setBrush(pieColorStart);
        painter->drawPie(rect, (270 - startAngle - angleCurrent) * 16, angleCurrent * 16);

        //绘制剩余值饼圆
        painter->setBrush(pieColorEnd);
        painter->drawPie(rect, (270 - startAngle - angleCurrent - angleOther) * 16, angleOther * 16);
    }

    painter->restore();
}

void AverageSpeedGaugeView::drawCoverCircle(QPainter *painter)
{
    int radius = 50;
    painter->save();
    painter->setPen(Qt::NoPen);
    painter->setBrush(coverCircleColor);
    painter->drawEllipse(-radius, -radius, radius * 2, radius * 2);
    painter->restore();
}

void AverageSpeedGaugeView::drawScale(QPainter *painter)
{
    int radius = 72;
    painter->save();

    painter->rotate(startAngle);
    int steps = (scaleMajor * scaleMinor);
    double angleStep = (360.0 - startAngle - endAngle) / steps;

    QPen pen;
    pen.setColor(scaleColor);
    pen.setCapStyle(Qt::RoundCap);

    for (int i = 0; i <= steps; i++) {
        if (i % scaleMinor == 0) {
            pen.setWidthF(1.5);
            painter->setPen(pen);
            painter->drawLine(0, radius - 10, 0, radius);
        } else {
            pen.setWidthF(0.5);
            painter->setPen(pen);
            painter->drawLine(0, radius - 5, 0, radius);
        }

        painter->rotate(angleStep);
    }

    painter->restore();
}

void AverageSpeedGaugeView::drawScaleNum(QPainter *painter)
{
    int radius = 82;
    painter->save();
    painter->setPen(scaleColor);

    double startRad = (360 - startAngle - 90) * (M_PI / 180);
    double deltaRad = (360 - startAngle - endAngle) * (M_PI / 180) / scaleMajor;

    for (int i = 0; i <= scaleMajor; i++) {
        double sina = qSin(startRad - i * deltaRad);
        double cosa = qCos(startRad - i * deltaRad);
        double value = 1.0 * i * ((maxValue - minValue) / scaleMajor) + minValue;

        QString strValue = QString("%1").arg((double)value, 0, 'f', precision);
        double textWidth = fontMetrics().boundingRect(strValue).width();
        double textHeight = fontMetrics().height();
        int x = radius * cosa - textWidth / 2;
        int y = -radius * sina + textHeight / 4;
        painter->drawText(x, y, strValue);
    }

    painter->restore();
}

void AverageSpeedGaugeView::drawPointerCircle(QPainter *painter)
{
    int radius = 6;
    int offset = 30;
    painter->save();
    painter->setPen(Qt::NoPen);
    painter->setBrush(pointerColor);

    painter->rotate(startAngle);
    double degRotate = (360.0 - startAngle - endAngle) / (maxValue - minValue) * (currentValue - minValue);
    painter->rotate(degRotate);
    painter->drawEllipse(-radius, radius + offset, radius * 2, radius * 2);

    painter->restore();
}

void AverageSpeedGaugeView::drawPointerIndicator(QPainter *painter)
{
    int radius = 75;
    painter->save();
    painter->setOpacity(0.8);
    painter->setPen(Qt::NoPen);
    painter->setBrush(pointerColor);

    QPolygon pts;
    pts.setPoints(3, -5, 0, 5, 0, 0, radius);

    painter->rotate(startAngle);
    double degRotate = (360.0 - startAngle - endAngle) / (maxValue - minValue) * (currentValue - minValue);
    painter->rotate(degRotate);
    painter->drawConvexPolygon(pts);

    painter->restore();
}

void AverageSpeedGaugeView::drawPointerIndicatorR(QPainter *painter)
{
    int radius = 75;
    painter->save();
    painter->setOpacity(1.0);

    QPen pen;
    pen.setWidth(1);
    pen.setColor(pointerColor);
    painter->setPen(pen);
    painter->setBrush(pointerColor);

    QPolygon pts;
    pts.setPoints(3, -5, 0, 5, 0, 0, radius);

    painter->rotate(startAngle);
    double degRotate = (360.0 - startAngle - endAngle) / (maxValue - minValue) * (currentValue - minValue);
    painter->rotate(degRotate);
    painter->drawConvexPolygon(pts);

    //增加绘制圆角直线,与之前三角形重叠,形成圆角指针
    pen.setCapStyle(Qt::RoundCap);
    pen.setWidthF(4);
    painter->setPen(pen);
    painter->drawLine(0, 0, 0, radius);

    painter->restore();
}

void AverageSpeedGaugeView::drawPointerTriangle(QPainter *painter)
{
    int radius = 10;
    int offset = 38;
    painter->save();
    painter->setPen(Qt::NoPen);
    painter->setBrush(pointerColor);

    QPolygon pts;
    pts.setPoints(3, -5, 0 + offset, 5, 0 + offset, 0, radius + offset);

    painter->rotate(startAngle);
    double degRotate = (360.0 - startAngle - endAngle) / (maxValue - minValue) * (currentValue - minValue);
    painter->rotate(degRotate);
    painter->drawConvexPolygon(pts);

    painter->restore();
}

void AverageSpeedGaugeView::drawRoundCircle(QPainter *painter)
{
    int radius = circleWidth + 3;
    painter->save();
    painter->setOpacity(0.8);
    painter->setPen(Qt::NoPen);
    painter->setBrush(pointerColor);
    painter->drawEllipse(-radius, -radius, radius * 2, radius * 2);
    painter->restore();
}

void AverageSpeedGaugeView::drawCenterCircle(QPainter *painter)
{
    int radius = circleWidth;
    painter->save();
    painter->setPen(Qt::NoPen);
    painter->setBrush(centerCircleColor);
    painter->drawEllipse(-radius, -radius, radius * 2, radius * 2);
    painter->restore();
}

void AverageSpeedGaugeView::drawValue(QPainter *painter)
{
    int radius = 100;
    painter->save();
    painter->setPen(textColor);

    QFont font;
    font.setPixelSize(18);
    painter->setFont(font);

    QRectF textRect(-radius, -radius, radius * 2, radius * 2);
    QString strValue = QString("%1").arg((double)currentValue, 0, 'f', precision);
    painter->drawText(textRect, Qt::AlignCenter, strValue);

    painter->restore();
}

void AverageSpeedGaugeView::drawOverlay(QPainter *painter)
{
    if (!showOverlay) {
        return;
    }

    int radius = 90;
    painter->save();
    painter->setPen(Qt::NoPen);

    QPainterPath smallCircle;
    QPainterPath bigCircle;
    radius -= 1;
    smallCircle.addEllipse(-radius, -radius, radius * 2, radius * 2);
    radius *= 2;
    bigCircle.addEllipse(-radius, -radius + 140, radius * 2, radius * 2);

    //高光的形状为小圆扣掉大圆的部分
    QPainterPath highlight = smallCircle - bigCircle;

    QLinearGradient linearGradient(0, -radius / 2, 0, 0);
    overlayColor.setAlpha(100);
    linearGradient.setColorAt(0.0, overlayColor);
    overlayColor.setAlpha(30);
    linearGradient.setColorAt(1.0, overlayColor);
    painter->setBrush(linearGradient);
    painter->rotate(-20);
    painter->drawPath(highlight);

    painter->restore();
}

void AverageSpeedGaugeView::drawBackground(QPainter *painter)
{
    painter->save();
    QPen pen;
    pen.setWidth(1); //设置线宽
    //pen.setColor(Qt::red); //设置颜色
    pen.setColor(Qt::white);//rgb设置颜色
    pen.setStyle(Qt::SolidLine); //设置风格
    //把画笔交给画家
    painter->setPen(pen);

    QBrush brush;
    brush.setColor(QColor(44, 46, 54));
    brush.setStyle(Qt::SolidPattern);
    painter->setBrush(brush);

    //画矩形
    painter->drawRect(-width(),-height(),width()*2,height()*2);

    painter->restore();
}

//void AverageSpeedGaugeView::updateValue()
//{
//    if (!reverse) {
//        if (currentValue >= value) {
//            timer->stop();
//        } else {
//            currentValue += animationStep;
//        }
//    } else {
//        if (currentValue <= value) {
//            timer->stop();
//        } else {
//            currentValue -= animationStep;
//        }
//    }

//    this->update();
//}

double AverageSpeedGaugeView::getMinValue() const
{
    return this->minValue;
}

double AverageSpeedGaugeView::getMaxValue() const
{
    return this->maxValue;
}

double AverageSpeedGaugeView::getValue() const
{
    return this->value;
}

int AverageSpeedGaugeView::getPrecision() const
{
    return this->precision;
}

int AverageSpeedGaugeView::getScaleMajor() const
{
    return this->scaleMajor;
}

int AverageSpeedGaugeView::getScaleMinor() const
{
    return this->scaleMinor;
}

int AverageSpeedGaugeView::getStartAngle() const
{
    return this->startAngle;
}

int AverageSpeedGaugeView::getEndAngle() const
{
    return this->endAngle;
}

bool AverageSpeedGaugeView::getAnimation() const
{
    return this->animation;
}

double AverageSpeedGaugeView::getAnimationStep() const
{
    return this->animationStep;
}

QColor AverageSpeedGaugeView::getOuterCircleColor() const
{
    return this->outerCircleColor;
}

QColor AverageSpeedGaugeView::getInnerCircleColor() const
{
    return this->innerCircleColor;
}

QColor AverageSpeedGaugeView::getPieColorStart() const
{
    return this->pieColorStart;
}

QColor AverageSpeedGaugeView::getPieColorMid() const
{
    return this->pieColorMid;
}

QColor AverageSpeedGaugeView::getPieColorEnd() const
{
    return this->pieColorEnd;
}

QColor AverageSpeedGaugeView::getCoverCircleColor() const
{
    return this->coverCircleColor;
}

QColor AverageSpeedGaugeView::getScaleColor() const
{
    return this->scaleColor;
}

QColor AverageSpeedGaugeView::getPointerColor() const
{
    return this->pointerColor;
}

QColor AverageSpeedGaugeView::getCenterCircleColor() const
{
    return this->centerCircleColor;
}

QColor AverageSpeedGaugeView::getTextColor() const
{
    return this->textColor;
}

bool AverageSpeedGaugeView::getShowOverlay() const
{
    return this->showOverlay;
}

QColor AverageSpeedGaugeView::getOverlayColor() const
{
    return this->overlayColor;
}

int AverageSpeedGaugeView::getCircleWidth() const
{
    return this->circleWidth;
}

AverageSpeedGaugeView::PieStyle AverageSpeedGaugeView::getPieStyle() const
{
    return this->pieStyle;
}

AverageSpeedGaugeView::PointerStyle AverageSpeedGaugeView::getPointerStyle() const
{
    return this->pointerStyle;
}

QSize AverageSpeedGaugeView::sizeHint() const
{
    return QSize(200, 200);
}

QSize AverageSpeedGaugeView::minimumSizeHint() const
{
    return QSize(50, 50);
}

void AverageSpeedGaugeView::setRange(double minValue, double maxValue)
{
    //如果最小值大于或者等于最大值则不设置
    if (minValue >= maxValue) {
        return;
    }

    this->minValue = minValue;
    this->maxValue = maxValue;

    //如果目标值不在范围值内,则重新设置目标值
    if (value < minValue || value > maxValue) {
        setValue(value);
    }

    this->update();
}

void AverageSpeedGaugeView::setRange(int minValue, int maxValue)
{
    setRange((double)minValue, (double)maxValue);
}

void AverageSpeedGaugeView::setMinValue(double minValue)
{
    setRange(minValue, maxValue);
}

void AverageSpeedGaugeView::setMaxValue(double maxValue)
{
    setRange(minValue, maxValue);
}

void AverageSpeedGaugeView::setValue(double value)
{
    //值和当前值一致则无需处理
    if (value == this->value) {
        return;
    }

    //值小于最小值则取最小值,大于最大值则取最大值
    if (value < minValue) {
        value = minValue;
    } else if (value > maxValue) {
        value = maxValue;
    }

    if (value > this->value) {
        reverse = false;
    } else if (value < this->value) {
        reverse = true;
    }

    this->value = value;
    emit valueChanged(value);

    if (!animation) {
        currentValue = this->value;
        this->update();
    } else {
//        timer->start();
    }
}

void AverageSpeedGaugeView::setValue(int value)
{
    setValue((double)value);
}

void AverageSpeedGaugeView::setPrecision(int precision)
{
    //最大精确度为 3
    if (precision <= 3 && this->precision != precision) {
        this->precision = precision;
        this->update();
    }
}

void AverageSpeedGaugeView::setScaleMajor(int scaleMajor)
{
    if (this->scaleMajor != scaleMajor) {
        this->scaleMajor = scaleMajor;
        this->update();
    }
}

void AverageSpeedGaugeView::setScaleMinor(int scaleMinor)
{
    if (this->scaleMinor != scaleMinor) {
        this->scaleMinor = scaleMinor;
        this->update();
    }
}

void AverageSpeedGaugeView::setStartAngle(int startAngle)
{
    if (this->startAngle != startAngle) {
        this->startAngle = startAngle;
        this->update();
    }
}

void AverageSpeedGaugeView::setEndAngle(int endAngle)
{
    if (this->endAngle != endAngle) {
        this->endAngle = endAngle;
        this->update();
    }
}

void AverageSpeedGaugeView::setAnimation(bool animation)
{
    if (this->animation != animation) {
        this->animation = animation;
        this->update();
    }
}

void AverageSpeedGaugeView::setAnimationStep(double animationStep)
{
    if (this->animationStep != animationStep) {
        this->animationStep = animationStep;
        this->update();
    }
}

void AverageSpeedGaugeView::setOuterCircleColor(const QColor &outerCircleColor)
{
    if (this->outerCircleColor != outerCircleColor) {
        this->outerCircleColor = outerCircleColor;
        this->update();
    }
}

void AverageSpeedGaugeView::setInnerCircleColor(const QColor &innerCircleColor)
{
    if (this->innerCircleColor != innerCircleColor) {
        this->innerCircleColor = innerCircleColor;
        this->update();
    }
}

void AverageSpeedGaugeView::setPieColorStart(const QColor &pieColorStart)
{
    if (this->pieColorStart != pieColorStart) {
        this->pieColorStart = pieColorStart;
        this->update();
    }
}

void AverageSpeedGaugeView::setPieColorMid(const QColor &pieColorMid)
{
    if (this->pieColorMid != pieColorMid) {
        this->pieColorMid = pieColorMid;
        this->update();
    }
}

void AverageSpeedGaugeView::setPieColorEnd(const QColor &pieColorEnd)
{
    if (this->pieColorEnd != pieColorEnd) {
        this->pieColorEnd = pieColorEnd;
        this->update();
    }
}

void AverageSpeedGaugeView::setCoverCircleColor(const QColor &coverCircleColor)
{
    if (this->coverCircleColor != coverCircleColor) {
        this->coverCircleColor = coverCircleColor;
        this->update();
    }
}

void AverageSpeedGaugeView::setScaleColor(const QColor &scaleColor)
{
    if (this->scaleColor != scaleColor) {
        this->scaleColor = scaleColor;
        this->update();
    }
}

void AverageSpeedGaugeView::setPointerColor(const QColor &pointerColor)
{
    if (this->pointerColor != pointerColor) {
        this->pointerColor = pointerColor;
        this->update();
    }
}

void AverageSpeedGaugeView::setCenterCircleColor(const QColor &centerCircleColor)
{
    if (this->centerCircleColor != centerCircleColor) {
        this->centerCircleColor = centerCircleColor;
        this->update();
    }
}

void AverageSpeedGaugeView::setTextColor(const QColor &textColor)
{
    if (this->textColor != textColor) {
        this->textColor = textColor;
        this->update();
    }
}

void AverageSpeedGaugeView::setShowOverlay(bool showOverlay)
{
    if (this->showOverlay != showOverlay) {
        this->showOverlay = showOverlay;
        this->update();
    }
}

void AverageSpeedGaugeView::setOverlayColor(const QColor &overlayColor)
{
    if (this->overlayColor != overlayColor) {
        this->overlayColor = overlayColor;
        this->update();
    }
}

void AverageSpeedGaugeView::setCircleWidth(int circleWidth)
{
    if (this->circleWidth != circleWidth) {
        this->circleWidth = circleWidth;
        this->update();
    }
}

void AverageSpeedGaugeView::setPieStyle(const AverageSpeedGaugeView::PieStyle &pieStyle)
{
    if (this->pieStyle != pieStyle) {
        this->pieStyle = pieStyle;
        this->update();
    }
}

void AverageSpeedGaugeView::setPointerStyle(const AverageSpeedGaugeView::PointerStyle &pointerStyle)
{
    if (this->pointerStyle != pointerStyle) {
        this->pointerStyle = pointerStyle;
        this->update();
    }
}
