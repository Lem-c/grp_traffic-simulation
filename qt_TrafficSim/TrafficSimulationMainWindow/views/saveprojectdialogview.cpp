#include "saveprojectdialogview.h"
#include "../models/projectmodel.h"
#include "ui_saveprojectdialogview.h"
#include <QFileDialog>
#include <QMessageBox>

SaveProjectDialogView::SaveProjectDialogView(QWidget *parent)
    : QDialog(parent), ui(new Ui::SaveProjectDialogView()), projectModel(nullptr)
{
    ui->setupUi(this);
}

SaveProjectDialogView::~SaveProjectDialogView() {
    delete ui;
}

void SaveProjectDialogView::setSourceModel(ProjectModel* projectModel) {
    this->projectModel = projectModel;

    ui->projectComboBox->addItems(projectModel->getProjectNames());

    connect(ui->saveFileButton, &QAbstractButton::clicked, this, &SaveProjectDialogView::handleSaveFileButtonOnClicked);
}

void SaveProjectDialogView::handleSaveFileButtonOnClicked() {
    const QFileDialog::Options options = QFlag(QFileDialog::DontUseCustomDirectoryIcons);
    QString selectedFilter;
    QString pathToFile = QFileDialog::getSaveFileName(this,
                                QString::fromStdString("Save Project"),
                                QString::fromStdString(""),
                                QString::fromStdString("All File (*);;Project File (*.json)"),
                                &selectedFilter,
                                options);

    if (!pathToFile.isEmpty()) {
        projectModel->saveProjectInto(ui->projectComboBox->currentText(), pathToFile);
    } else {
        QMessageBox::warning(this, "Warning", "File Path is empty.");
        this->reject();
    }

    this->accept();
}
