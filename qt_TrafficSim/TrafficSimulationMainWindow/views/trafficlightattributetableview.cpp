#include "trafficlightattributetableview.h"
#include "../models/projectmodel.h"
#include "ui_trafficlightattributetableview.h"
#include <qlistview.h>

TrafficLightAttributeTableView::TrafficLightAttributeTableView(QWidget* parent) :
    QDialog(parent), ui(new Ui::TrafficLightAttributeTableView())
{
    ui->setupUi(this);

    ui->trafficLightAttributeTableView->horizontalHeader()->setSectionsMovable(false);
    ui->trafficLightAttributeTableView->horizontalHeader()->setSectionsClickable(false);
    ui->trafficLightAttributeTableView->horizontalHeader()->setHighlightSections(false);

    ui->trafficLightAttributeTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

TrafficLightAttributeTableView::~TrafficLightAttributeTableView() {
    delete ui;
}

void TrafficLightAttributeTableView::setModels(ProjectAttributeTableModel* trafficLightAttributeTableModel,
                                               ProjectModel* projectModel, TrafficLightAttributeTableFilter* filterModel) {
    ui->projectComboBox->setView(new QListView());
    ui->projectComboBox->addItems(projectModel->getProjectNames());
    ui->projectComboBox->setCurrentIndex(0);


    trafficLightAttributeTableModel->setSourceModel(projectModel);
    trafficLightAttributeTableModel->setSelectedProjectName(ui->projectComboBox->currentText());

    trafficLightAttributeTableModel->setParent(ui->trafficLightAttributeTableView);
    filterModel->setParent(ui->trafficLightAttributeTableView);
    filterModel->setSourceModel(trafficLightAttributeTableModel);
    if (ui->filterCheckBox->isChecked()) {
        filterModel->setEnableFiltering(true);
    } else {
        filterModel->setEnableFiltering(false);
    }
    ui->trafficLightAttributeTableView->setModel(filterModel);

    ui->trafficLightAttributeTableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    ui->trafficLightAttributeTableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->trafficLightAttributeTableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->trafficLightAttributeTableView->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    ui->trafficLightAttributeTableView->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);

//    connect(ui->addButton, &QPushButton::clicked, this, &TrafficLightAttributeTableView::handleAddButtonOnClicked);
//    connect(ui->deleteButton, &QPushButton::clicked, this, &TrafficLightAttributeTableView::handleDeleteButtonOnClicked);
    connect(ui->quitButton, &QPushButton::clicked, this, &TrafficLightAttributeTableView::close);

    connect(ui->projectComboBox, &QComboBox::currentTextChanged,
            trafficLightAttributeTableModel, &ProjectAttributeTableModel::setSelectedProjectName);

    connect(ui->intersectionNumberSpinBox, &QSpinBox::valueChanged, filterModel, &TrafficLightAttributeTableFilter::setIntersectionNumber);
    connect(ui->sequenceNumberSpinBox, &QSpinBox::valueChanged, filterModel, &TrafficLightAttributeTableFilter::setSequenceNumber);
    connect(ui->filterCheckBox, &QCheckBox::stateChanged, filterModel, &TrafficLightAttributeTableFilter::setEnableFiltering);
}

//void TrafficLightAttributeTableView::handleAddButtonOnClicked() {
//    if (ui->trafficLightAttributeTableView->currentIndex().isValid()) {
//        ui->trafficLightAttributeTableView->model()->insertRow(ui->trafficLightAttributeTableView->currentIndex().row());
//    } else {
//        ui->trafficLightAttributeTableView->model()->insertRow(0);
//    }
//}

//void TrafficLightAttributeTableView::handleDeleteButtonOnClicked() {
//    if (ui->trafficLightAttributeTableView->currentIndex().isValid()) {
//        ui->trafficLightAttributeTableView->model()->removeRow(ui->trafficLightAttributeTableView->currentIndex().row());
//    } else {
//        ui->trafficLightAttributeTableView->model()->removeRow(0);
//    }

//}
