#ifndef ENTRYATTRIBUTETABLEVIEW_H
#define ENTRYATTRIBUTETABLEVIEW_H

#include <QDialog>

#include "../models/projectattributetablemodel.h"
#include "../models/entryattributetablefilter.h"

QT_BEGIN_NAMESPACE
namespace Ui { class EntryAttributeTableView; }
QT_END_NAMESPACE

/**
 *  @class EntryAttributeTableView entryattributetableview.h
 *  @brief This class is for table view of the entry attribute
 * 
 *  @author YiCun Duan
 */
class EntryAttributeTableView : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Construct a new Entry Attribute Table View object
     * 
     * @param parent : the parent class
     */
    explicit EntryAttributeTableView(QWidget* parent = nullptr);
    /**
     * @brief Destroy the Entry Attribute Table View object
     * 
     */
    ~EntryAttributeTableView();

    /**
     * @brief Set the Models object
     * 
     * @param entryAttributeTableModel : the entry attribute table model
     * @param projectModel : the project model
     * @param filterModel : the filter model
     */
    void setModels(ProjectAttributeTableModel* entryAttributeTableModel,
                   ProjectModel* projectModel, EntryAttributeTableFilter* filterModel);

private slots:
//    void handleAddButtonOnClicked();
//    void handleDeleteButtonOnClicked();

private:
    Ui::EntryAttributeTableView* ui;
};

#endif // ENTRYATTRIBUTETABLEVIEW_H
