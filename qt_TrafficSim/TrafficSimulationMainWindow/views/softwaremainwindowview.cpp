#include <QtWidgets>
#include <cstdlib>
#include <ctime>

#include "softwaremainwindowview.h"
#include "ui_softwaremainwindowview.h"
#include "glwidget.h"
#include "paintinghelper.h"
#include "../utilities/jsoncoder.h"
#include "../utilities/project.h"
#include "saveprojectdialogview.h"
#include "entryattributetableview.h"
#include "../models/projectattributetablemodel.h"
#include "trafficlightattributetableview.h"
#include "../models/trafficlightattributetablefilter.h"
#include "../models/entryattributetablefilter.h"
#include "../models/vehicleinputchartmodel.h"
#include "../utilities/uiexception.h"
#include "averagespeedgaugeview.h"
#include "throughputoverviewchartmodel.h"
#include "../simulation/trafficconfigurationmaker.h"
#include "../utilities/stylesheetparser.h"
#include "startsimulationdialogview.h"

const int SoftwareMainWindowView::simulationRefreshTimeInterval = 30;

SoftwareMainWindowView::SoftwareMainWindowView(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::SoftwareMainWindowView()), averageSpeedChartView(nullptr), throughputChartView(nullptr), vehicleInputChartView(nullptr),
      averageSpeedGaugeView(nullptr), throughputOverviewChartView(nullptr), trafficSimulationView(nullptr),
      actionRightClick(nullptr), recentlyUsedFilesMenu(nullptr), styleActionsGroup(nullptr), simulationControlToolBar(nullptr),
      actionStop(nullptr), actionPause(nullptr), actionStart(nullptr),
      projectTreeModel(nullptr), trafficDataModel(nullptr), averageSpeedChartModel(nullptr), throughputChartModel(nullptr),
      vehicleInputChartModel(nullptr), vehicleInputTableModel(nullptr), throughputTableModel(nullptr), throughputOverviewChartModel(nullptr),
      trafficSimulator(nullptr), trafficSimulatorThread(nullptr),
      firstRunSimulation(true), simulationIsStopped(true), simulationIsPaused(false), simulationIsRunning(false),
      simulationViewRefreshTimer(nullptr), totalTime(0),
      projectModel(nullptr), usedFilesTrackerModel(nullptr)
{
    ui->setupUi(this);
    actionRightClick = new QAction("Right Click Action", this);
    recentlyUsedFilesMenu = new QMenu("Recently Used Files", this);
    ui->fileMenu->addMenu(recentlyUsedFilesMenu);
    statusBar()->showMessage("Welcome!");
    setWindowTitle("Traffic Simulation");
    ui->plainTextEdit->setReadOnly(true);

//    paintingHelper = new PaintingHelper();
    simulationViewRefreshTimer = new QTimer(ui->simulationGroupBox);
//    glWidget = new GLWidget(ui->simulationGroupBox, paintingHelper);
    averageSpeedChartView = new QChartView(this); // add view
    throughputChartView = new QChartView(this); // add view
    vehicleInputChartView = new QChartView(this);
    averageSpeedGaugeView = new AverageSpeedGaugeView(this);
    throughputOverviewChartView = new QChartView(this);
    trafficSimulationView = new TrafficSimulationView(this);
    ui->simulationGroupBox->layout()->addWidget(trafficSimulationView);
    ui->averageSpeedHorizontalLayout->addWidget(averageSpeedChartView); // add to layout
    ui->throughputHorizontalLayout->addWidget(throughputChartView); // add to layout
    ui->vehicleInputHorizontalLayout->addWidget(vehicleInputChartView);
    ui->averageSpeedGaugeHorizontalLayout->addWidget(averageSpeedGaugeView);
    ui->throughputOverviewHorizontalLayout->addWidget(throughputOverviewChartView);

    ui->actionNew->setShortcut(QKeySequence::New);
    ui->actionNew->setStatusTip("Create a new file");

    ui->actionLoad->setShortcut(QKeySequence::Open);
    ui->actionLoad->setStatusTip("Load a file");

    ui->actionSave->setShortcut(QKeySequence::Save);
    ui->actionSave->setStatusTip("Save file");

    ui->actionManageEntryAttributes->setStatusTip("Manage Entry Attributes");

    ui->actionManageTrafficLightAttributes->setStatusTip("Manage Traffic Light Attributes");

    ui->actionUnityStyle->setCheckable(true);
    ui->actionUnityStyle->setChecked(true);
    ui->actionDefaultStyle->setCheckable(true);

    connect(ui->actionDefaultStyle, &QAction::triggered, this, &SoftwareMainWindowView::defaultStyle);
    connect(ui->actionUnityStyle, &QAction::triggered, this, &SoftwareMainWindowView::unityStyle);

    styleActionsGroup = new QActionGroup(this);
    styleActionsGroup->addAction(ui->actionUnityStyle);
    styleActionsGroup->addAction(ui->actionDefaultStyle);

    ui->actionManual->setShortcut(QKeySequence::HelpContents);
    ui->actionManual->setStatusTip("Show help manual");

    ui->actionAboutUs->setShortcut(QString::fromStdString("Ctrl+A"));
    ui->actionAboutUs->setStatusTip("About us");

    actionRightClick->setStatusTip("right-click shortcut");

    this->simulationControlToolBar = this->addToolBar("Simulation Control");
    this->actionStop = new QAction(QIcon(), "Stop", this);
    this->actionPause = new QAction(QIcon(), "Pause", this);
    this->actionStart = new QAction(QIcon(), "Start", this);
    connect(actionStop, &QAction::triggered, this, &SoftwareMainWindowView::stopSimulation);
    connect(actionPause, &QAction::triggered, this, &SoftwareMainWindowView::pauseSimulation);
    connect(actionStart, &QAction::triggered, this, &SoftwareMainWindowView::startSimulation);
    this->simulationControlToolBar->addActions({actionStop, actionPause, actionStart});
    this->actionStop->setEnabled(false);
    this->actionPause->setEnabled(false);
}

SoftwareMainWindowView::~SoftwareMainWindowView()
{
    delete ui;
    delete projectTreeModel;
    delete simulationViewRefreshTimer;
//    delete glWidget;
    delete averageSpeedChartModel;
    delete averageSpeedChartView;
    delete throughputChartModel;
    delete throughputChartView;
    delete vehicleInputChartModel;
    delete vehicleInputChartView;
    delete averageSpeedGaugeView;
    delete vehicleInputTableModel;
    delete throughputTableModel;
//    delete trafficDataModel;
//    delete paintingHelper;
    delete actionRightClick;
    delete styleActionsGroup;
    delete simulationControlToolBar;
    delete actionStop;
    delete actionPause;
    delete actionStart;

    for (QAction* action : recentlyUsedFilesMenu->actions()) {
        delete action;
    }

     delete recentlyUsedFilesMenu;
}

#ifndef QT_NO_CONTEXTMENU
void SoftwareMainWindowView::contextMenuEvent(QContextMenuEvent* event) {
    QMenu menu(this);
    menu.addAction(actionRightClick);
    menu.exec(event->globalPos());
}
#endif

void SoftwareMainWindowView::setModels(ProjectModel* projectModel, UsedFilesTrackerModel* usedFilesTrackerModel,
                                       TrafficDataModel* trafficDataModel) {
    this->projectModel = projectModel;
    this->usedFilesTrackerModel = usedFilesTrackerModel;
    this->projectTreeModel = new ProjectTreeModel(this);
    projectTreeModel->setSourceModel(projectModel);
    ui->projectsTreeView->setModel(projectTreeModel);

    ui->projectsTreeView->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->projectsTreeView->header()->setSectionResizeMode(1, QHeaderView::Stretch);

    this->trafficDataModel = trafficDataModel;
    trafficDataModel->addThroughputEntryBy(0, 0, 30);
    trafficDataModel->addThroughputEntryBy(0, 1, 20);
    trafficDataModel->addThroughputEntryBy(0, 2, 40);
    trafficDataModel->addThroughputEntryBy(0, 3, 40);
    trafficDataModel->addThroughputEntryBy(1, 0, 70);
    trafficDataModel->addThroughputEntryBy(1, 1, 90);
    trafficDataModel->addThroughputEntryBy(1, 2, 50);
    trafficDataModel->addThroughputEntryBy(1, 3, 90);
    trafficDataModel->addVehicleInputEntryBy(0, 1);
    trafficDataModel->addVehicleInputEntryBy(1, 99);
    trafficDataModel->addVehicleInputEntryBy(2, 50);

    this->averageSpeedChartModel = new AverageSpeedChartModel();
    this->averageSpeedChartModel->setSourceModel(trafficDataModel);
    this->throughputChartModel = new ThroughputChartModel();
    this->throughputChartModel->setSourceModel(trafficDataModel);
    this->vehicleInputChartModel = new VehicleInputChartModel();
    this->vehicleInputChartModel->setSourceModel(trafficDataModel);
    this->averageSpeedGaugeView->setSourceModel(trafficDataModel);
    this->averageSpeedChartView->setRenderHint(QPainter::Antialiasing);
    this->throughputChartView->setRenderHint(QPainter::Antialiasing);
    this->vehicleInputChartView->setRenderHint(QPainter::Antialiasing);
    this->averageSpeedChartView->setChart(averageSpeedChartModel);
    this->throughputChartView->setChart(throughputChartModel);
    this->vehicleInputChartView->setChart(vehicleInputChartModel);
    this->throughputOverviewChartModel = new ThroughputOverviewChartModel();
    this->throughputOverviewChartModel->setSourceModel(trafficDataModel);
    this->throughputOverviewChartView->setChart(throughputOverviewChartModel);

    this->vehicleInputTableModel = new VehicleInputTableModel();
    this->vehicleInputTableModel->setSourceModel(trafficDataModel);
    ui->vehicleInputTableView->setModel(vehicleInputTableModel);
    ui->vehicleInputTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    this->throughputTableModel = new ThroughputTableModel();
    this->throughputTableModel->setSourceModel(trafficDataModel);
    ui->throughputTableView->setModel(throughputTableModel);
    ui->throughputTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

//    connect(simulationTimer, &QTimer::timeout, glWidget, &GLWidget::animate);

    trafficSimulator = new TrafficSimulator();
    trafficSimulationView->setModel(trafficSimulator);

    trafficSimulatorThread = new TrafficSimulatorThread(trafficSimulator);

    connect(simulationViewRefreshTimer, &QTimer::timeout, trafficSimulationView, [&]() {
        trafficSimulationView->update();
    });

    connect(simulationViewRefreshTimer, &QTimer::timeout, trafficDataModel, [&]() {
        totalTime += simulationViewRefreshTimer->interval();
        trafficDataModel->addAverageSpeed(totalTime, rand() % 100);
        trafficDataModel->setThroughputEntryBy(0, rand() % 4, rand() % 100);
        trafficDataModel->setThroughputEntryBy(1, rand() % 4, rand() % 100);
        trafficDataModel->setVehicleInputEntryBy(rand() % 3, rand() % 100);
    });

    connect(ui->actionNew, &QAction::triggered, this, &SoftwareMainWindowView::createNewProject);

    connect(ui->actionLoad, &QAction::triggered, this, &SoftwareMainWindowView::loadProject);

    connect(ui->actionSave, &QAction::triggered, this, &SoftwareMainWindowView::saveProject);

    updateRecentlyUsedFilesMenuWith(usedFilesTrackerModel->getRecentlyUsedFilesRecords());
    connect(usedFilesTrackerModel, &UsedFilesTrackerModel::recordsSequenceChanged, this, &SoftwareMainWindowView::updateRecentlyUsedFilesMenuWith);
    connect(usedFilesTrackerModel, &UsedFilesTrackerModel::newRecordAdded, this, &SoftwareMainWindowView::addNewRecordIntoRecentlyUsedFilesMenu);

    connect(ui->actionManageEntryAttributes, &QAction::triggered, this, &SoftwareMainWindowView::manageEntryAttributes);

    connect(ui->actionManageTrafficLightAttributes, &QAction::triggered, this, &SoftwareMainWindowView::manageTrafficLightAttributes);

    connect(ui->actionManual, &QAction::triggered, this, &SoftwareMainWindowView::showHelpManual);

    connect(ui->actionAboutUs, &QAction::triggered, this, &SoftwareMainWindowView::aboutUs);

    connect(actionRightClick, &QAction::triggered, this, &SoftwareMainWindowView::showRightClickMenu);

    connect(projectModel, &ProjectModel::projectAddedAt, ui->plainTextEdit, [=]() {
        ui->plainTextEdit->setPlainText(getProjectsDetails());
    });
//    connect(projectModel, &ProjectModel::projectAttributeAddedAt, ui->plainTextEdit, [=]() {
//        ui->plainTextEdit->setPlainText(getProjectsDetails());
//    });
    connect(projectModel, &ProjectModel::projectAttributeChangedAt, ui->plainTextEdit, [=]() {
        ui->plainTextEdit->setPlainText(getProjectsDetails());
    });
//    connect(projectModel, &ProjectModel::projectAttributeRemovedAt, ui->plainTextEdit, [=]() {
//        ui->plainTextEdit->setPlainText(getProjectsDetails());
//    });
    connect(projectModel, &ProjectModel::projectReset, ui->plainTextEdit, [=]() {
        ui->plainTextEdit->setPlainText(getProjectsDetails());
    });

    connect(projectModel, &ProjectModel::projectAddedAt, projectTreeModel, &ProjectTreeModel::refreshModel);
//    connect(projectModel, &ProjectModel::projectAttributeAddedAt, projectTreeModel, &ProjectTreeModel::refreshModel);
    connect(projectModel, &ProjectModel::projectAttributeChangedAt, projectTreeModel, &ProjectTreeModel::refreshModel);
//    connect(projectModel, &ProjectModel::projectAttributeRemovedAt, projectTreeModel, &ProjectTreeModel::refreshModel);
    connect(projectModel, &ProjectModel::projectReset, projectTreeModel, &ProjectTreeModel::refreshModel);
}

QString SoftwareMainWindowView::getProjectsDetails() {
    std::ostringstream plainTextEditStringStream;

    for (const Project& project : projectModel->getProjects()) {
        plainTextEditStringStream << "Project Name: " << project.getProjectName().toStdString() + "\n"
                                  << "User Name: " << project.getUserName().toStdString() + "\n"
                                  << "Create Date: " << project.getCreateDate().toString().toStdString() + "\n"
                                  << "Last Modified Date: " << project.getLastModifiedDate().toString().toStdString() + "\n"
                                  << "Horizontal Intersection Number: " << std::to_string(project.getHorizontalIntersectionNumber()) + "\n"
                                  << "Vertical Intersection Numebr: " << std::to_string(project.getVerticalInersectionNumber()) + "\n"
                                  << "Entry Attributes: \n";

        for (const ProjectAttribute* entryAttribute : project.getEntryAttributes()) {
            auto entryAttributePtr = dynamic_cast<const EntryAttribute*>(entryAttribute);
            plainTextEditStringStream << "\t" + std::string("Entry Number: ") + std::to_string(entryAttributePtr->getEntryNumber()) + " " +
                               std::string("Vehicle Initial Speed: ") + std::to_string(entryAttributePtr->getVehicleInitialSpeed()) + "\n";
        }

        plainTextEditStringStream << "\nTraffic Light Attributes: \n";

        for (const ProjectAttribute* trafficLightAttribute : project.getTrafficLightAttributes()) {
            auto trafficLightAttributePtr = dynamic_cast<const TrafficLightAttribute*>(trafficLightAttribute);
            plainTextEditStringStream << "\t" + std::string("Intersection Number: ") + std::to_string(trafficLightAttributePtr->getIntersectionNumber()) + " " +
                               std::string("Sequence Number: ") + std::to_string(trafficLightAttributePtr->getRoadSequenceNumber()) + " " +
                               std::string("Green Duration: ") + std::to_string(trafficLightAttributePtr->getGreenDuration()) + " " +
                               std::string("Yellow Duration: ") + std::to_string(trafficLightAttributePtr->getYellowDuration()) + " " +
                               std::string("Red Duration: ") + std::to_string(trafficLightAttributePtr->getRedDuration()) + "\n";
        }

        plainTextEditStringStream << "\n\n";
    }

    return QString::fromStdString(plainTextEditStringStream.str());
}

void SoftwareMainWindowView::createNewProject() {
    CreateNewProjectDialogView createNewProjectDialogView(this);
    createNewProjectDialogView.setSourceModel(projectModel);
    createNewProjectDialogView.exec();
}

void SoftwareMainWindowView::loadProject() {
    const QFileDialog::Options options = QFlag(QFileDialog::DontUseCustomDirectoryIcons);
    QString selectedFilter;
    QString pathToFile = QFileDialog::getOpenFileName(this,
                                QString::fromStdString("Load Project"),
                                QString::fromStdString(""),
                                QString::fromStdString("Project File (*.json)"),
                                &selectedFilter,
                                options);
    if (!pathToFile.isEmpty()) {
        projectModel->loadProjectFrom(pathToFile);
        usedFilesTrackerModel->addNewRecord(pathToFile);
    } else {
        QMessageBox::warning(nullptr, "Warning", "File path is empty.");
    }
}

void SoftwareMainWindowView::saveProject() {
    if (!projectModel->isEmpty()) {
        SaveProjectDialogView saveProjectDialogView(this);
        saveProjectDialogView.setSourceModel(projectModel);
        saveProjectDialogView.exec();
    } else {
        QMessageBox warningBox(QMessageBox::Warning, QString::fromStdString("Warning"),
                           QString::fromStdString("There doesn't exist any project."), { }, this);

        warningBox.setDetailedText(QString::fromStdString("You should create/load a project before saving it."));
        warningBox.addButton(QString::fromStdString("Create"), QMessageBox::AcceptRole);
        warningBox.addButton(QString::fromStdString("Cancel"), QMessageBox::RejectRole);
        if (warningBox.exec() == QMessageBox::AcceptRole) {
            createNewProject();
        }
    }
}

void SoftwareMainWindowView::manageEntryAttributes() {
    if (!projectModel->isEmpty()) {
        EntryAttributeTableView entryAttributeTableView(this);
        ProjectAttributeTableModel entryAttributeTableModel(ProjectAttribute::Type::EntryAttributeType, this);
        EntryAttributeTableFilter filterModel;
        entryAttributeTableView.setModels(&entryAttributeTableModel, projectModel, &filterModel);
        entryAttributeTableView.exec();
    } else {
        //QMessageBox warningBox(QMessageBox::Warning,"Warning","<font size='3' color='black' align='left'>There is no project.</font>", {}, this);
         QMessageBox warningBox(QMessageBox::Warning,"Warning","There is no project.", {}, this);
        //QMessageBox warningBox(QMessageBox::Warning,"Warning","<p align='left'>There doesn't exist any project.</p>", {}, this);

        //warningBox.setWindowFlags(Qt::Dialog);
        warningBox.setStyleSheet("QLabel{"
                             "min-width: 205px;"
                             "min-height: 50px; "
                             "}");
        warningBox.setDetailedText(QString::fromStdString("You should create/load a project before managing its attribute."));
        warningBox.addButton(QString::fromStdString("Create a new project"), QMessageBox::AcceptRole);
        warningBox.addButton(QString::fromStdString("Cancel"), QMessageBox::RejectRole);
        if (warningBox.exec() == QMessageBox::AcceptRole) {
            createNewProject();
        }
    }
}

void SoftwareMainWindowView::manageTrafficLightAttributes() {
    if (!projectModel->isEmpty()) {
        TrafficLightAttributeTableView trafficLightAttributeTableView(this);
        ProjectAttributeTableModel trafficLightAttributeTableModel(ProjectAttribute::Type::TrafficLightAttributeType);
        TrafficLightAttributeTableFilter filterModel;
        trafficLightAttributeTableView.setModels(&trafficLightAttributeTableModel, projectModel, &filterModel);

        trafficLightAttributeTableView.exec();
    } else {
//        QMessageBox warningBox(QMessageBox::Warning, QString::fromStdString("Warning"),
//                           QString::fromStdString("There doesn't exist any project."), { }, this);

//        warningBox.setDetailedText(QString::fromStdString("You should create/load a project before managing its attribute."));
//        warningBox.addButton(QString::fromStdString("Create a new project"), QMessageBox::AcceptRole);
//        warningBox.addButton(QString::fromStdString("Cancel"), QMessageBox::RejectRole);
//        if (warningBox.exec() == QMessageBox::AcceptRole) {
//            createNewProject();
//        }
        //QMessageBox warningBox(QMessageBox::Warning,"Warning","<font size='3' color='black' align='center'>There is no project.</font>", {}, this);
        QMessageBox warningBox(QMessageBox::Warning,"Warning","There is no project.", {}, this);
        //QMessageBox warningBox(QMessageBox::Warning,"Warning","<p align='left'>There doesn't exist any project.</p>", {}, this);

        //warningBox.setWindowFlags(Qt::Dialog);
        warningBox.setStyleSheet("QLabel{"
                             "min-width: 205px;"
                             "min-height: 50px; "
                             "}");
        warningBox.setDetailedText(QString::fromStdString("You should create/load a project before managing its attribute."));
        warningBox.addButton(QString::fromStdString("Create a new project"), QMessageBox::AcceptRole);
        warningBox.addButton(QString::fromStdString("Cancel"), QMessageBox::RejectRole);
        if (warningBox.exec() == QMessageBox::AcceptRole) {
            createNewProject();
        }
    }
}

void SoftwareMainWindowView::defaultStyle() {
    dynamic_cast<QApplication*>(QApplication::instance())->setStyleSheet("");
}

void SoftwareMainWindowView::unityStyle() {
    dynamic_cast<QApplication*>(QApplication::instance())->setStyleSheet(StyleSheetParser::parseStyleSheet(":resources/styleSheets/Unity.qss"));
}

void SoftwareMainWindowView::showHelpManual() {
    QMessageBox::about(this, "Help Manual", "Showing help manual...");
}

void SoftwareMainWindowView::aboutUs() {
    QMessageBox::about(this, "About Us", "This is the <b>Traffic Simulation Project</b> "
                       "of <b>Group04</b>.<br>"
                       " If you have any questions and suggestions or encounter errors, please feel free to contact us via following email address.<br>"
                       "<b>GRPTeam04@outlook.com</b>"
                       );
}

void SoftwareMainWindowView::showRightClickMenu() {
     QMessageBox::about(this, "Right Click Shortcut", "This is the right-click shortcut.");
}

void SoftwareMainWindowView::updateRecentlyUsedFilesMenuWith(const QList<UsedFilesTrackerModel::Record>& recentlyUsedFilesRecords) {
    for (QAction* action : recentlyUsedFilesMenu->actions()) {
        disconnect(action, &QAction::triggered, nullptr, nullptr);
        delete action;
    }
    recentlyUsedFilesMenu->clear();

    for (const UsedFilesTrackerModel::Record& record : recentlyUsedFilesRecords) {
        QAction* action = new QAction(record.second, recentlyUsedFilesMenu);
        connect(action, &QAction::triggered, projectModel, [=]() {
            projectModel->loadProjectFrom(record.second);
        });
        recentlyUsedFilesMenu->addAction(action);
    }
}

void SoftwareMainWindowView::addNewRecordIntoRecentlyUsedFilesMenu(const UsedFilesTrackerModel::Record& newRecord) {
    QAction* action = new QAction(newRecord.second, recentlyUsedFilesMenu);
    connect(action, &QAction::triggered, projectModel, [=]() {
        projectModel->loadProjectFrom(newRecord.second);
    });
    recentlyUsedFilesMenu->addAction(action);
}

void SoftwareMainWindowView::stopSimulation() {
    if (simulationIsRunning || simulationIsPaused) {
        trafficSimulationView->setIsPaused(true);
        this->simulationViewRefreshTimer->stop();
        this->actionStart->setEnabled(true);
        this->actionPause->setEnabled(false);
        this->actionStop->setEnabled(false);
        ui->sceneMenu->setDisabled(false);
        ui->fileMenu->setDisabled(false);

        simulationIsRunning = false;
        simulationIsPaused = false;
        simulationIsStopped = true;
    }
}

void SoftwareMainWindowView::pauseSimulation() {
    if (simulationIsRunning) {
        trafficSimulationView->setIsPaused(true);
        this->simulationViewRefreshTimer->stop();
        this->actionStart->setEnabled(true);
        this->actionStop->setEnabled(true);
        this->actionPause->setEnabled(false);
        ui->sceneMenu->setDisabled(true);
        ui->fileMenu->setDisabled(true);

        simulationIsRunning = false;
        simulationIsPaused = true;
        simulationIsStopped = false;
    }
}

void SoftwareMainWindowView::startSimulation() {
    if (projectModel->isEmpty()) {
       QMessageBox warningBox(QMessageBox::Warning,"Warning","There is no project.", {}, this);
       warningBox.setStyleSheet("QLabel{"
                            "min-width: 205px;"
                            "min-height: 50px; "
                            "}");
       warningBox.setDetailedText(QString::fromStdString("You should create/load a project before starting simulation."));
       warningBox.addButton(QString::fromStdString("Create a new project"), QMessageBox::AcceptRole);
       warningBox.addButton(QString::fromStdString("Cancel"), QMessageBox::RejectRole);
       if (warningBox.exec() == QMessageBox::AcceptRole) {
           createNewProject();
       }
       return;
    }

    if (simulationIsStopped) {
        StartSimulationDialogView startSimulationDialogView(this);
        startSimulationDialogView.setSourceModel(projectModel);
        if (startSimulationDialogView.exec() == QDialog::Accepted) {
            trafficSimulator->setProjectToBeSyncWith(projectModel->getProjectBy(startSimulationDialogView.getSelectedProjectName()));
            if (firstRunSimulation) {
                trafficSimulatorThread->start(simulationRefreshTimeInterval);
                firstRunSimulation = false;
            }
            trafficSimulationView->setIsPaused(false);
            this->simulationViewRefreshTimer->start(simulationRefreshTimeInterval);
        } else {
            return;
        }
    } else if (simulationIsPaused) {
        trafficSimulationView->setIsPaused(false);
        this->simulationViewRefreshTimer->start(simulationRefreshTimeInterval);
    }

    this->actionStart->setEnabled(false);
    this->actionStop->setEnabled(true);
    this->actionPause->setEnabled(true);
    ui->sceneMenu->setDisabled(true);
    ui->fileMenu->setDisabled(true);

    simulationIsRunning = true;
    simulationIsStopped = false;
    simulationIsPaused = false;
}
