QT       += core gui openglwidgets charts testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ./models \
               ./views

SOURCES += \
    main.cpp \
    models/averagespeedchartmodel.cpp \
    models/entryattributetablefilter.cpp \
    models/projectattributetablemodel.cpp \
    models/projectmodel.cpp \
    models/projecttreemodel.cpp \
    models/throughputchartmodel.cpp \
    models/throughputtablemodel.cpp \
    models/trafficdatamodel.cpp \
    models/trafficlightattributetablefilter.cpp \
    models/usedfilestrackermodel.cpp \
    models/vehicleinputchartmodel.cpp \
    models/vehicleinputtablemodel.cpp \
    simulation/curveroad.cpp \
    simulation/frameblockingqueue.cpp \
    simulation/intersection.cpp \
    simulation/road.cpp \
    simulation/simulationframe.cpp \
    simulation/simulationpaintingengine.cpp \
    simulation/trafficconfigurationmaker.cpp \
    simulation/trafficlight.cpp \
    simulation/trafficsceneconfiguration.cpp \
    simulation/trafficsimulationview.cpp \
    simulation/trafficsimulator.cpp \
    simulation/trafficsimulatorthread.cpp \
    simulation/twowayroad.cpp \
    simulation/vehicle.cpp \
    simulation/vehiclegenerator.cpp \
    unitTest/interfacetest.cpp \
    unitTest/modelsTest.cpp \
    unitTest/simulationtest.cpp \
    unitTest/utilitiestest.cpp \
    unitTest/vehiclelogictest.cpp \
    utilities/entryattribute.cpp \
    utilities/jsoncoder.cpp \
    utilities/project.cpp \
    utilities/projectattribute.cpp \
    utilities/projectattributeindex.cpp \
    utilities/projecttreemodelitem.cpp \
    utilities/simulationexception.cpp \
    utilities/stylesheetparser.cpp \
    utilities/trafficlightattribute.cpp \
    utilities/uiexception.cpp \
    views/averagespeedgaugeview.cpp \
    views/createnewprojectdialogview.cpp \
    views/entryattributetableview.cpp \
    views/glwidget.cpp \
    views/paintinghelper.cpp \
    views/saveprojectdialogview.cpp \
    views/softwaremainwindowview.cpp \
    models/throughputoverviewchartmodel.cpp \
    views/startsimulationdialogview.cpp \
    views/trafficlightattributetableview.cpp

HEADERS += \
    models/averagespeedchartmodel.h \
    models/entryattributetablefilter.h \
    models/projectattributetablemodel.h \
    models/projectmodel.h \
    models/projecttreemodel.h \
    models/throughputchartmodel.h \
    models/throughputtablemodel.h \
    models/trafficdatamodel.h \
    models/trafficlightattributetablefilter.h \
    models/usedfilestrackermodel.h \
    models/vehicleinputchartmodel.h \
    models/vehicleinputtablemodel.h \
    simulation/curveroad.h \
    simulation/frameblockingqueue.h \
    simulation/intersection.h \
    simulation/road.h \
    simulation/simulationframe.h \
    simulation/simulationpaintingengine.h \
    simulation/trafficconfigurationmaker.h \
    simulation/trafficlight.h \
    simulation/trafficsceneconfiguration.h \
    simulation/trafficsimulationview.h \
    simulation/trafficsimulator.h \
    simulation/trafficsimulatorthread.h \
    simulation/twowayroad.h \
    simulation/vehicle.h \
    simulation/vehiclegenerator.h \
    unitTest/interfacetest.h \
    unitTest/modelsTest.h \
    unitTest/simulationtest.h \
    unitTest/utilitiestest.h \
    unitTest/vehiclelogictest.h \
    utilities/entryattribute.h \
    utilities/jsoncoder.h \
    utilities/project.h \
    utilities/projectattribute.h \
    utilities/projectattributeindex.h \
    utilities/projecttreemodelitem.h \
    utilities/simulationexception.h \
    utilities/stylesheetparser.h \
    utilities/trafficlightattribute.h \
    utilities/uiexception.h \
    views/averagespeedgaugeview.h \
    views/createnewprojectdialogview.h \
    views/entryattributetableview.h \
    views/glwidget.h \
    views/paintinghelper.h \
    views/saveprojectdialogview.h \
    views/softwaremainwindowview.h \
    models/throughputoverviewchartmodel.h \
    views/startsimulationdialogview.h \
    views/trafficlightattributetableview.h

FORMS += \
    views/createnewprojectdialogview.ui \
    views/entryattributetableview.ui \
    views/saveprojectdialogview.ui \
    views/softwaremainwindowview.ui \
    views/startsimulationdialogview.ui \
    views/trafficlightattributetableview.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc
