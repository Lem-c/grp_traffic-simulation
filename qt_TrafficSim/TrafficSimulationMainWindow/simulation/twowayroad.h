#ifndef TWOWAYROAD_H
#define TWOWAYROAD_H

#include "trafficsceneconfiguration.h"

/**
 * @class RoadWrapper twolineroad.h
 * @brief The configuration of the traffic scene
 * 
 * @author YiCun Duan
 */
class RoadWrapper {
public:
    /**
     * @brief Construct a new Road Wrapper 
     * 
     * @param roadSetting : the road setting
     * @param roadIndex : the road index
     */
    RoadWrapper(TrafficSceneConfiguration::RoadSetting* roadSetting, int roadIndex = -1)
        : roadSetting(roadSetting), roadIndex(roadIndex)
    {}

    /**
     * @brief Get the Road Setting object
     * 
     * @return the road setting
     */
    inline const TrafficSceneConfiguration::RoadSetting* getRoadSetting() const {
        return roadSetting;
    }
    
    /**
     * @brief Get the index of the road
     * 
     * @return the road index
     */
    inline int getRoadIndex() const {
        return roadIndex;
    }

    /**
     * @brief Set the Road Index object
     * 
     * @param roadIndex : the road index
     */
    inline void setRoadIndex(int roadIndex) {
        this->roadIndex = roadIndex;
    }

    /**
     * @brief Get the start point of the road
     * 
     * @return start point of the road
     */
    inline Road::Point2D getStartPoint() const {
        return std::get<TrafficSceneConfiguration::RoadSettingIndex::StartPoint>(*roadSetting);
    }

    /**
     * @brief Get the end point of the road
     * 
     * @return end point of the road
     */
    inline Road::Point2D getEndPoint() const {
        return std::get<TrafficSceneConfiguration::RoadSettingIndex::EndPoint>(*roadSetting);
    }

private:
    TrafficSceneConfiguration::RoadSetting* roadSetting;
    int roadIndex;
};

/**
 * @class TwolineRoad twolineroad.h
 * @brief This class creates a two line road
 * 
 * @author YiCun Duan
 */
class TwowayRoad
{
public:
    /**
     * @brief Enum of the default road setting configuration
     * 
     */
    enum class DefaultRoadSettingConfiguration {
        GreenLightWithoutEntry, /** @brief enum value GreenLightWithoutEntry */
        GreenLightWithEntry, /** @brief enum value GreenLightWithEntry */
        RedLightWithoutEntry, /** @brief enum value RedLightWithoutEntry */
        RedLightWithEntry, /** @brief enum value RedLightWithEntry */
        WithoutTrafficLightWithoutEntry /** @brief enum value WithoutTrafficLightWithoutEntry */
    };

    /**
     * @brief the width of the road
     * 
     */
    const static double roadWideDistance;
    
    /**
     * @brief Construct a new two line road
     * 
     * @param startPoint : the start point
     * @param endPoint : the end point
     * @param configuration : the configuration of the road setting
     * @param parent : The traffic scene configuration
     */
    TwowayRoad(Road::Point2D&& startPoint, Road::Point2D&& endPoint,
               DefaultRoadSettingConfiguration configuration, TrafficSceneConfiguration* parent);

    /**
     * @brief Get the start point of the left road
     * 
     * @return start point of left road
     */
    inline Road::Point2D getLeftRoadStartPoint() const;

    /**
     * @brief Get the end point of the left road
     * 
     * @return end point of left road
     */
    inline Road::Point2D getLeftRoadEndPoint() const;

    /**
     * @brief Get the start point of the right road
     * 
     * @return start point of right road
     */
    inline Road::Point2D getRightRoadStartPoint() const;

    /**
     * @brief Get the end point of the right road
     * 
     * @return end point of right road
     */
    inline Road::Point2D getRightRoadEndPoint() const;

    /**
     * @brief Get the start setting of the left road
     * 
     * @return start setting of the left road
     */
    inline const TrafficSceneConfiguration::RoadSetting* getLeftRoadStartRoadSetting() const;

    /**
     * @brief Get the end setting of the left road
     * 
     * @return end setting of the left road
     */
    inline const TrafficSceneConfiguration::RoadSetting* getLeftRoadEndRoadSetting() const;

    /**
     * @brief Get the start setting of the right road
     * 
     * @return start setting of the right road
     */
    inline const TrafficSceneConfiguration::RoadSetting* getRightRoadStartRoadSetting() const;

    /**
     * @brief Get the end setting of the right road
     * 
     * @return end setting of the right road
     */
    inline const TrafficSceneConfiguration::RoadSetting* getRightRoadEndRoadSetting() const;

    /**
     * @brief Get the left start road of the index
     * 
     * @return left start road of the index
     */
    inline unsigned int getLeftRoadStartRoadIndex() const;

    /**
     * @brief Get the left end road of the index
     * 
     * @return left end road of the index
     */
    inline unsigned int getLeftRoadEndRoadIndex() const;

    /**
     * @brief Get the right start road of the index
     * 
     * @return right start road of the index 
     */
    inline unsigned int getRightRoadStartRoadIndex() const;

    /**
     * @brief Get the right end road of the index
     * 
     * @return right end road of the index
     */
    inline unsigned int getRightRoadEndRoadIndex() const;

    /**
     * @brief Get the Route From To object
     * 
     * @param startLeft : whether its going left or right from the start of the route
     * @param endLeft : whether its going left or right at the end of the route
     * @return std::vector<unsigned int> 
     */
    std::vector<unsigned int> getRouteFromTo(bool startLeft, bool endLeft) const;

private:
    TrafficSceneConfiguration* parent;
    QList<RoadWrapper> roads;

    static const int leftRoadStartSegmentIndex;
    static const int leftRoadInternSegmentIndex;
    static const int leftRoadInternTurnSegmentIndex;
    static const int leftRoadEndSegmentIndex;

    static const int rightRoadStartSegmentIndex;
    static const int rightRoadInternSegmentIndex;
    static const int rightRoadInternTurnSegmentIndex;
    static const int rightRoadEndSegmentIndex;
};

inline Road::Point2D TwowayRoad::getLeftRoadStartPoint() const {
    return roads[leftRoadStartSegmentIndex].getStartPoint();
}

inline Road::Point2D TwowayRoad::getLeftRoadEndPoint() const {
    return roads[leftRoadEndSegmentIndex].getEndPoint();
}

inline Road::Point2D TwowayRoad::getRightRoadStartPoint() const {
    return roads[rightRoadStartSegmentIndex].getStartPoint();
}

inline Road::Point2D TwowayRoad::getRightRoadEndPoint() const {
    return roads[rightRoadEndSegmentIndex].getEndPoint();
}

const TrafficSceneConfiguration::RoadSetting* TwowayRoad::getLeftRoadStartRoadSetting() const {
    return roads[leftRoadStartSegmentIndex].getRoadSetting();
}

const TrafficSceneConfiguration::RoadSetting* TwowayRoad::getLeftRoadEndRoadSetting() const {
    return roads[leftRoadEndSegmentIndex].getRoadSetting();
}

const TrafficSceneConfiguration::RoadSetting* TwowayRoad::getRightRoadStartRoadSetting() const {
    return roads[rightRoadStartSegmentIndex].getRoadSetting();
}

const TrafficSceneConfiguration::RoadSetting* TwowayRoad::getRightRoadEndRoadSetting() const {
    return roads[rightRoadEndSegmentIndex].getRoadSetting();
}

unsigned int TwowayRoad::getLeftRoadStartRoadIndex() const {
    return roads[leftRoadStartSegmentIndex].getRoadIndex();
}

unsigned int TwowayRoad::getLeftRoadEndRoadIndex() const {
    return roads[leftRoadEndSegmentIndex].getRoadIndex();
}

unsigned int TwowayRoad::getRightRoadStartRoadIndex() const {
    return roads[rightRoadStartSegmentIndex].getRoadIndex();
}

unsigned int TwowayRoad::getRightRoadEndRoadIndex() const {
    return roads[rightRoadEndSegmentIndex].getRoadIndex();
}

#endif // TWOWAYROAD_H
