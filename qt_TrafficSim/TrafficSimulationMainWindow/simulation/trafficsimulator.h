#ifndef TRAFFICSIMULATOR_H
#define TRAFFICSIMULATOR_H

#include <QObject>
#include <QHash>
#include "road.h"
#include "vehicle.h"
#include "trafficlight.h"
#include "trafficsceneconfiguration.h"
#include "vehiclegenerator.h"
#include "frameblockingqueue.h"
#include "simulationframe.h"
#include "../utilities/project.h"

/**
 * @class TrafficSimulator trafficsimulator.h
 * @brief This class preserves the attributes for performing the traffic simulation
 * 
 * @author YiCun Duan
 */
class TrafficSimulator : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Construct a new Traffic Simulator object
     * 
     * @param parent : the parent class
     */
    explicit TrafficSimulator(QObject *parent = nullptr);
    
    /**
     * @brief Destroy the Traffic Simulator object
     * 
     */
    ~TrafficSimulator();

//    void setUpSimulatorWith(const TrafficSceneConfiguration& trafficMap);
    
    /**
     * @brief Set the project to be sync with
     * 
     * @param project : the project to be sync with
     */
    void setProjectToBeSyncWith(const Project* project);
    
    /**
     * @brief Get the total simulation time
     * 
     * @return total simulation time
     */
    inline unsigned long long getTotalSimulationTime() const;

    /**
     * @brief Get the width of the simulation view
     * 
     * @return view width
     */
    inline double getSimulationViewWidth() const;

    /**
     * @brief Get the height of the simulation view
     * 
     * @return view height
     */
    inline double getSimulationViewHeight() const;

    /**
     * @brief Get the roads of the simulator
     * 
     * @return QList<Road*> 
     */
    inline QList<Road*> getRoads() const;

    /**
     * @brief Get the vehicles of the simulator
     * 
     * @return const QList<Vehicle*>& 
     */
    inline const QList<Vehicle*>& getVehicles() const;

    /**
     * @brief Get the traffic lights of the simulator
     * 
     * @return const QList<TrafficLight*>& 
     */
    inline const QList<TrafficLight*>& getTrafficLights() const;

    /**
     * @brief Get the frame blocking queue of the simulator
     * 
     * @return the frame blocking queue
     */
    inline FrameBlockingQueue<SimulationFrame*>& getFrameBlockingQueue();

    /**
     * @brief Get the road by the road index
     * 
     * @param roadIndex : the index of the road
     * @return the road
     */
    inline Road* getRoadBy(unsigned int roadIndex);    

    /**
     * @brief Get the route by indexes
     * 
     * @param roadIndexes : the list of road indexes
     * @return the route based on the road indexes
     */
    Road::Route getRouteBy(const QList<unsigned int>& roadIndexes);

public slots:
    /**
     * @brief generate the simulation frame within delta time
     * 
     * @param deltaTimeInMS : the simulation time
     */
    void generateSimulationFrameWith(int deltaTimeInMS);

private:
    unsigned long long totalSimulationTime;

    double simulationViewWidth;
    double simulationViewHeight;

    QHash<unsigned int, Road*> roadsHashMap;
    QList<Vehicle*> vehicles;
    QList<TrafficLight*> trafficLights;

    VehicleGenerator* vehicleGenerator;

    FrameBlockingQueue<SimulationFrame*> frameBlockingQueque;

    const Project* projectToBeSyncWith;

    std::mutex setSyncProjectMutex;

    void reInitAllElements();
    void syncConfigurationWith(const Project* project);

    void addVehicleToRoad(Vehicle* vehicle, unsigned int roadIndex);
    friend void VehicleGenerator::generateVehicles(unsigned long long totalSimulationTime);

signals:

};

Road* TrafficSimulator::getRoadBy(unsigned int roadIndex) {
    return roadsHashMap.value(roadIndex);
}

unsigned long long TrafficSimulator::getTotalSimulationTime() const {
    return totalSimulationTime;
}

double TrafficSimulator::getSimulationViewWidth() const {
    return simulationViewWidth;
}

double TrafficSimulator::getSimulationViewHeight() const {
    return simulationViewHeight;
}

QList<Road*> TrafficSimulator::getRoads() const {
    return roadsHashMap.values();
}

const QList<Vehicle*>& TrafficSimulator::getVehicles() const {
    return vehicles;
}

const QList<TrafficLight*>& TrafficSimulator::getTrafficLights() const {
    return trafficLights;
}

FrameBlockingQueue<SimulationFrame*>& TrafficSimulator::getFrameBlockingQueue() {
    return frameBlockingQueque;
}

#endif // TRAFFICSIMULATOR_H
