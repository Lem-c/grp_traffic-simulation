#ifndef SIMULATIONFRAME_H
#define SIMULATIONFRAME_H

#include <QList>
#include "road.h"

/**
 * @class SimulationFrame simulationframe.h
 * @brief This class is the frame created by the simulator thread and pushed into
 * the frame blocking queue
 * 
 * @author YiCun Duan
 */
class SimulationFrame
{
public:
    /**
     * @brief Construct a new Simulation Frame object
     * 
     * @param roads : the roads to be painted
     * @param totalSimulationTime : the total simulation time
     * @param simulationViewWidth : the view width of the simulation
     * @param simulationViewHeight : the view height of the simulation
     */
    SimulationFrame(const QList<Road*>& roads, unsigned long long totalSimulationTime,
                    double simulationViewWidth, double simulationViewHeight);

    /**
     * @brief Destroy the Simulation Frame object
     * 
     */
    ~SimulationFrame();

    /**
     * @brief Get the roads in this frame
     * 
     * @return const QList<Road*>& 
     */
    inline const QList<Road*>& getRoads() const;

    /**
     * @brief Get the Total Simulation Time object
     * 
     * @return unsigned long long 
     */
    inline unsigned long long getTotalSimulationTime() const;

    /**
     * @brief Get the Simulation View Width object
     * 
     * @return double 
     */
    inline double getSimulationViewWidth() const;
    
    /**
     * @brief Get the Simulation View Height object
     * 
     * @return double 
     */
    inline double getSimulationViewHeight() const;

private:
    QList<Road*> roads;
    unsigned long long totalSimulationTime;
    double simulationViewWidth;
    double simulationViewHeight;
};

const QList<Road*>& SimulationFrame::getRoads() const {
    return roads;
}

unsigned long long SimulationFrame::getTotalSimulationTime() const {
    return totalSimulationTime;
}

double SimulationFrame::getSimulationViewWidth() const {
    return simulationViewWidth;
}

double SimulationFrame::getSimulationViewHeight() const {
    return simulationViewHeight;
}

#endif // SIMULATIONFRAME_H
