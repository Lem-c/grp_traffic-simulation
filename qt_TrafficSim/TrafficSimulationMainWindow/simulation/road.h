#ifndef ROAD_H
#define ROAD_H

#include "trafficlight.h"

class Vehicle;

/**
 * @class Road road.h
 * @brief This class creates a road on the scene
 * 
 * @author YiCun Duan
 */
class Road
{
public:
    /**
     * @brief The position of the road in the 2d scene
     * 
     */
    typedef std::pair<double, double> Point2D; // <m, m>

    /**
     * @brief all the roads created
     * 
     */
    typedef QList<Road*> Route;

    /**
     * @brief Create a new road based on the startpoint and the endpoint
     * 
     * @param startPoint : the start point of the road
     * @param endPoint : the end point of the road
     */
    explicit Road(const Point2D& startPoint, const Point2D& endPoint);

    /**
     * @brief Create a road based on another road
     * 
     * @param otherRoad 
     */
    Road(const Road& otherRoad);

    /**
     * @brief Destroy the Road object
     * 
     */
    virtual ~Road();

    /**
     * @brief Update the road
     * 
     * @param deltaTimeInMS : the delta of time to update the road
     */
    void update(int deltaTimeInMS);

    /**
     * @brief add a traffic light to the road
     * 
     * @param trafficLight : the traffic light object
     */
    inline void addTrafficLight(TrafficLight* trafficLight);

    /**
     * @brief check whether the road has traffic light
     * 
     * @return true 
     * @return false 
     */
    inline bool hasTrafficLight() const;
    
    /**
     * @brief Get the Attached Traffic Light object
     * 
     * @return const TrafficLight* 
     */
    inline const TrafficLight* getAttachedTrafficLight() const;

    /**
     * @brief add waiting vehicle to the road
     *  
     * @param vehicle : the vehicle to be added
     */
    inline void addWaitingVehicle(Vehicle* vehicle);
    
    /**
     * @brief add vehicle directly to the road
     * 
     * @param vehicle : the vehicle added to the road
     * @return true 
     * @return false 
     */
    bool addVehicleDirectly(Vehicle* vehicle);
    
    /**
     * @brief Get the list of the vehicle on the road
     * 
     * @return const QList<Vehicle*>& 
     */
    inline const QList<Vehicle*>& getVehiclesInRoad() const;

    /**
     * @brief Get the start point of the road
     * 
     * @return the start point of the road
     */
    inline Road::Point2D getStartPoint() const;

    /**
     * @brief Get the start point of the road
     * 
     * @return the start point of the road
     */
    inline Road::Point2D getEndPoint() const;

    /**
     * @brief Get the Road Index object
     * 
     * @return unsigned int 
     */
    inline unsigned int getRoadIndex() const;

    /**
     * @brief Get the length of the road
     * 
     * @return double 
     */
    inline double getRoadLength() const;

    /**
     * @brief Get the sin angle of the road
     * 
     * @return double 
     */
    inline double getAngleSin() const;

    /**
     * @brief Get the cos angle of the road
     * 
     * @return double 
     */
    inline double getAngleCos() const;

    /**
     * @brief set whether the road is in intersection
     * 
     * @param isInIntersection 
     */
    inline void setIsInIntersection(bool isInIntersection);

    /**
     * @brief get whether the road is in intersection
     * 
     * @return true 
     * @return false 
     */
    inline bool getIsInIntersection()const;

    /**
     * @brief get whether the road is a curve road or not
     * 
     * @return true 
     * @return false 
     */
    virtual bool isCurveRoad();

    /**
     * @brief reset the total roads to zero
     * 
     */
    static void resetTotalRoadsToZero();

    /**
     * @brief the road width
     * 
     */
    static const double roadWidth; // m

protected:
    static unsigned int totalRoads;
    unsigned int roadIndex;

    Point2D startPoint; // <m, m>
    Point2D endPoint;
    double roadLength; // m
    double angleSin;
    double angleCos;
    double slowDistance; // m
    double slowFactor; // 0 ~ 1
    double stopDistance; // m

    bool isInIntersection;

    QList<Vehicle*> vehicles;
    QList<Vehicle*> waitingVehicles;

    TrafficLight* trafficLight;    
};

bool Road::hasTrafficLight() const {
    return trafficLight;
}

Road::Point2D Road::getStartPoint() const {
    return startPoint;
}

Road::Point2D Road::getEndPoint() const {
    return endPoint;
}

unsigned int Road::getRoadIndex() const {
    return roadIndex;
}

double Road::getRoadLength() const {
    return roadLength;
}

double Road::getAngleSin() const {
    return angleSin;
}

double Road::getAngleCos() const {
    return angleCos;
}

void Road::setIsInIntersection(bool isInIntersection) {
    this->isInIntersection = isInIntersection;
}

bool Road::getIsInIntersection() const {
    return isInIntersection;
}

const TrafficLight* Road::getAttachedTrafficLight() const {
    return trafficLight;
}

const QList<Vehicle*>& Road::getVehiclesInRoad() const {
    return vehicles;
}

void Road::addTrafficLight(TrafficLight* trafficLight) {
    this->trafficLight = trafficLight;
}

void Road::addWaitingVehicle(Vehicle* vehicle) {
    waitingVehicles.append(vehicle);
}

#endif // ROAD_H
