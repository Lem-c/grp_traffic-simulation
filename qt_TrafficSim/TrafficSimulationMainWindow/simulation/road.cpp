#include <QTextStream>
#include "road.h"
#include "vehicle.h"

const double Road::roadWidth = 50.0;
unsigned int Road::totalRoads = 0;

Road::Road(const Point2D& startPoint, const Point2D& endPoint) :
    roadIndex(totalRoads), startPoint(startPoint), endPoint(endPoint),
    roadLength(std::sqrt(std::pow(startPoint.first - endPoint.first, 2.0) +
                     std::pow(startPoint.second - endPoint.second, 2.0))),
    angleSin((endPoint.second - startPoint.second) / roadLength),
    angleCos((endPoint.first - startPoint.first) / roadLength),
    slowDistance(100.0), slowFactor(0.3), stopDistance(50.0),
    isInIntersection(false),
    vehicles(), waitingVehicles(),
    trafficLight(nullptr)
{
    totalRoads++;
}

Road::Road(const Road& otherRoad) :
    roadIndex(otherRoad.roadIndex),
    startPoint(otherRoad.startPoint), endPoint(otherRoad.endPoint),
    roadLength(otherRoad.roadLength),
    angleSin(otherRoad.angleSin), angleCos(otherRoad.angleCos),
    slowDistance(otherRoad.slowDistance), slowFactor(otherRoad.slowFactor),
    stopDistance(otherRoad.stopDistance),
    isInIntersection(otherRoad.isInIntersection),
    vehicles(), waitingVehicles(), trafficLight(nullptr)
{
    for (Vehicle* vehicle : otherRoad.vehicles) {
        vehicles.append(new Vehicle(*vehicle));
    }

//    for (Vehicle* vehicle : otherRoad.waitingVehicles) {
//        waitingVehicles.append(new Vehicle(*vehicle));
//    }

    if (otherRoad.trafficLight) {
        trafficLight = new TrafficLight(*otherRoad.trafficLight);
    }
}

Road::~Road() {
    for (Vehicle* vehicle : vehicles) {
        delete vehicle;
    }

    for (Vehicle* vehicle : waitingVehicles) {
        delete vehicle;
    }

    if (trafficLight) {
        delete trafficLight;
    }
}

void Road::update(int deltaTimeInMS) {
    //add waiting vehicle to road
    if (!waitingVehicles.isEmpty()) {
        if (vehicles.isEmpty()) {
            vehicles.append(waitingVehicles[0]);
            waitingVehicles.removeFirst();
        } else {
            if (vehicles.last()->getPosition() >
                    waitingVehicles[0]->getVehicleLength() + waitingVehicles[0]->getMiniDesiredVehicleDistance()) {
                vehicles.append(waitingVehicles[0]);
                waitingVehicles.removeFirst();
            }
        }
    }

    if (vehicles.isEmpty()) {
        return;
    }

    //update all vehicles in road
    vehicles[0]->update(nullptr, deltaTimeInMS);

    int i = 1;
    while (i < vehicles.size()) {
        vehicles[i]->update(vehicles[i - 1], deltaTimeInMS);
        i++;
    }

    if (vehicles[0]->getPosition() >= roadLength) {
        if (vehicles[0]->hasNextRoad()) {
            if (vehicles[0]->getNextRoad()->addVehicleDirectly(vehicles[0])) {
                vehicles[0]->setPosition(0.0);
                vehicles[0]->setCurRoadToNextRoad();
                vehicles.removeFirst();
            } else {
                vehicles[0]->setVelocity(0.0);
                vehicles[0]->setAcceleration(0.0);
            }
        } else {
            vehicles.removeFirst();
        }
    }

    //test whether the leader vehicle is in slow/stop area
    if (!trafficLight) {
        for (Vehicle* vehicle : vehicles) {
            vehicle->setToBeStopped(false);
            vehicle->recoverCurMaxVelocity();
        }
    } else if (trafficLight->getCurrentState() == TrafficLight::TrafficLightState::Green) {
        for (Vehicle* vehicle : vehicles) {
            vehicle->setToBeStopped(false);
            vehicle->recoverCurMaxVelocity();
        }
    } else {
        if (!vehicles.isEmpty()) {
            if (vehicles[0]->getPosition() >= roadLength - slowDistance) {
                vehicles[0]->setCurMaxVelocity(vehicles[0]->getMaxVelocity() * slowFactor);
            }

            if (vehicles[0]->getPosition() >= roadLength - stopDistance) {
                vehicles[0]->setToBeStopped(true);
            }
        }
    }
}

bool Road::isCurveRoad() {
    return false;
}

bool Road::addVehicleDirectly(Vehicle* vehicle) {
    if (vehicles.isEmpty()) {
        vehicles.append(vehicle);
        return true;
    } else {
        if (vehicles.last()->getPosition() >
                vehicle->getVehicleLength() + vehicle->getMiniDesiredVehicleDistance()) {
            vehicles.append(vehicle);
            return true;
        }

        return false;
    }
}

void Road::resetTotalRoadsToZero() {
    totalRoads = 0;
}

//unsigned int Road::getRoadIndexBy(
//        int intersectionNumber, int sequenceNumber,
//        int subRoadNumber, int direction) {
//    if (intersectionNumber < 0 || intersectionNumber >= maxIntersectionNumber) {
//        throw SimulationException("Invalid intersection number.");
//    }

//    if (sequenceNumber < 0 || sequenceNumber >= maxSequenceNumber) {
//        throw SimulationException("Invalid sequence number");
//    }

//    if (subRoadNumber < 0 || subRoadNumber >= maxSubRoadNumber) {
//        throw SimulationException("Invalid subroad number.");
//    }

//    if (direction < 0 && direction >= maxDirectionNumber) {
//        throw SimulationException("Invalid direction.");
//    }

//    return intersectionNumber * maxSequenceNumber * maxSubRoadNumber * maxDirectionNumber
//            + sequenceNumber * maxSubRoadNumber * maxDirectionNumber
//            + subRoadNumber * maxDirectionNumber
//            + direction;
//}

//unsigned int Road::getLinkRoadIndexBy(unsigned int startRoadIndex, unsigned int endRoadIndex) {
//    unsigned int linkRoadMark = 1;
//    linkRoadMark <<= 31;
//    return linkRoadMark | (startRoadIndex << 15) | endRoadIndex;
//}

//unsigned int Road::addCurveRoadMarkTo(unsigned int roadIndex) {
//    unsigned int curveRoadMark = 1;
//    curveRoadMark <<= 30;
//    return curveRoadMark | roadIndex;
//}

//bool Road::isLinkRoad(unsigned int roadIndex) {
//    unsigned int linkRoadMark = 1;
//    linkRoadMark <<= 31;
//    return linkRoadMark & roadIndex;
//}

