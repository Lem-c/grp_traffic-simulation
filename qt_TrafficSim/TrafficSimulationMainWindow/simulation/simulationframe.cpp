#include "simulationframe.h"
#include "curveroad.h"
#include <QTextStream>

SimulationFrame::SimulationFrame(const QList<Road*>& roads, unsigned long long totalSimulationTime,
                                 double simulationViewWidth, double simulationViewHeight) :
    roads(), totalSimulationTime(totalSimulationTime),
    simulationViewWidth(simulationViewWidth), simulationViewHeight(simulationViewHeight)
{
    for (Road* road : roads) {
        if (!road->isCurveRoad()) {
            this->roads.append(new Road(*road));

        } else {
            this->roads.append(new CurveRoad(*dynamic_cast<CurveRoad*>(road)));
        }
    }
}

SimulationFrame::~SimulationFrame() {
    for (Road* road : roads) {
        delete road;
    }
}
