#ifndef INTERSECTION_H
#define INTERSECTION_H
#include <QList>
#include "twowayroad.h"
#include <memory>

class TrafficSceneConfiguration;

/**
 * @class Intersection intersection.h
 * @brief This class creates a intersection on the scene
 * 
 * @author YiCun Duan
 */
class Intersection
{
public:
    /**
     * @brief Enum of the road index between roads
     * 
     */
    enum ConnectionRoadIndex {
      Seq1RoadTurnLeft = 0, /** @brief enum value 0 */
      Seq1RoadLeftGoStraight = 1, /** @brief enum value 1 */ 
      Seq1RoadRightGoStraight = 2, /** @brief enum value 2 */
      Seq1RoadTurnRight = 3, /** @brief enum value 3 */
      Seq3RoadTurnLeft = 4, /** @brief enum value 4 */
      Seq3RoadLeftGoStraight = 5, /** @brief enum value 5 */
      Seq3RoadRightGoStraight = 6, /** @brief enum value 6 */
      Seq3RoadTurnRight = 7, /** @brief enum value 7 */
      Seq5RoadTurnLeft = 8, /** @brief enum value 8 */
      Seq5RoadLeftGoStraight = 9, /** @brief enum value 9 */
      Seq5RoadRightGoStraight = 10, /** @brief enum value 10 */
      Seq5RoadTurnRight = 11, /** @brief enum value 11 */
      Seq7RoadTurnLeft = 12, /** @brief enum value 12 */
      Seq7RoadLeftGoStraight = 13, /** @brief enum value 13 */
      Seq7RoadRightGoStraight = 14, /** @brief enum value 14 */
      Seq7RoadTurnRight = 15 /** @brief enum value 15 */
    };

    /**
     * @brief Creates a new intersection
     *  
     * @param intersectionNumber : the number of the intersection
     * @param parent : the parent class of the intersection
     */
    Intersection(int intersectionNumber, TrafficSceneConfiguration* parent);

    /**
     * @brief Get the shared pointer of the two line road by the sequence number
     *  
     * @param sequenceNumber : the sequence number of the two line road
     * @return shared pointer of the TwoLineRoad
     */
    inline std::shared_ptr<TwowayRoad> getTwowayRoadBy(int sequenceNumber);

    /**
     * @brief Set up the adjacent intersection
     * 
     * @param leftIntersection : left intersection
     * @param rightIntersection : right intersection
     * @param upIntersection : up intersection
     * @param downIntersection : down intersection
     */
    void initIntersectionWith(Intersection* leftIntersection, Intersection* rightIntersection,
                              Intersection* upIntersection, Intersection* downIntersection);

    /**
     * @brief Get the number of the intersection
     * 
     * @return int 
     */
    inline int getIntersectionNumber() const;

    /**
     * @brief Get the Left Intersection object
     * 
     * @return Intersection* 
     */
    inline Intersection* getLeftIntersection();

    /**
     * @brief Get the Right Intersection object
     * 
     * @return Intersection* 
     */
    inline Intersection* getRightIntersection();

    /**
     * @brief Get the Up Intersection object
     * 
     * @return Intersection* 
     */
    inline Intersection* getUpIntersection();

    /**
     * @brief Get the Down Intersection object
     * 
     * @return Intersection* 
     */
    inline Intersection* getDownIntersection();

    /**
     * @brief Get the Connection Roads object
     * 
     * @return const QList<RoadWrapper>& 
     */
    inline const QList<RoadWrapper>& getConnectionRoads() const;
    
    /**
     * @brief Get the Route From To object
     * 
     * @param srcSquenceNumber : the source sequence number of the road
     * @param left : tells whether the road is going left or right
     * @param targetSequenceNumber : the target sequence number of the road
     * @return std::vector<unsigned int> 
     */
    std::vector<unsigned int> getRouteFromTo(int srcSquenceNumber, bool left, int targetSequenceNumber);

private:
    static const double streetBlockSize;
    static const double intersectionPaddingDistance;
    static const double roadPaddingDistance;

    int intersectionNumber;
    TrafficSceneConfiguration* parent;

    Intersection* leftIntersection;
    Intersection* rightIntersection;
    Intersection* upIntersection;
    Intersection* downIntersection;

    std::shared_ptr<TwowayRoad> sequence0Road;
    std::shared_ptr<TwowayRoad> sequence1Road;
    std::shared_ptr<TwowayRoad> sequence2Road;
    std::shared_ptr<TwowayRoad> sequence3Road;
    std::shared_ptr<TwowayRoad> sequence4Road;
    std::shared_ptr<TwowayRoad> sequence5Road;
    std::shared_ptr<TwowayRoad> sequence6Road;
    std::shared_ptr<TwowayRoad> sequence7Road;

    QList<RoadWrapper> connectionRoads;

    void setLeftIntersection(Intersection* intersection);
    void setRightIntersection(Intersection* intersection);
    void setUpIntersection(Intersection* intersection);
    void setDownIntersection(Intersection* intersection);
    void initConnectionRoads();
    std::pair<Road::Point2D, Road::Point2D> calculatePointsFor(int sequenceNumber);
};

std::shared_ptr<TwowayRoad> Intersection::getTwowayRoadBy(int sequenceNumber) {
    switch(sequenceNumber) {
    case 0:
        return sequence0Road;
    case 1:
        return sequence1Road;
    case 2:
        return sequence2Road;
    case 3:
        return sequence3Road;
    case 4:
        return sequence4Road;
    case 5:
        return sequence5Road;
    case 6:
        return sequence6Road;
    case 7:
        return sequence7Road;
    default:
        throw SimulationException("Invalid sequence number for two-way road.");
    }
}

int Intersection::getIntersectionNumber() const {
    return intersectionNumber;
}

Intersection* Intersection::getLeftIntersection() {
    return leftIntersection;
}

Intersection* Intersection::getRightIntersection() {
    return rightIntersection;
}

Intersection* Intersection::getUpIntersection() {
    return upIntersection;
}

Intersection* Intersection::getDownIntersection() {
    return downIntersection;
}

const QList<RoadWrapper>& Intersection::getConnectionRoads() const {
    return connectionRoads;
}

#endif // INTERSECTION_H
