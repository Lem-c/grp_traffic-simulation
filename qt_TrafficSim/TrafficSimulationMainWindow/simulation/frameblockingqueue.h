#ifndef FRAMEBLOCKINGQUEUE_H
#define FRAMEBLOCKINGQUEUE_H

#include <deque>
#include <condition_variable>
#include <mutex>
#include <QTextStream>
#include "utilities/simulationexception.h"

/**
 * @class FrameBlockingQueue frameblockingqueue.h
 * @brief This class is a queue that maintains the 
 * frame created by the simulator thread, and the frame
 * will be popped and used by the painter thread 
 * 
 * @author YiCun Duan
 */
template<typename T>
class FrameBlockingQueue
{
public:
    /**
     * @brief Creates a new frame blocking queue
     * 
     * @param[in] maxCapacity : the max capacity of the queue
     */
    explicit FrameBlockingQueue(int maxCapacity = 100);

    /**
     * @brief Destroy the Frame Blocking Queue object
     * 
     */
    ~FrameBlockingQueue();

    /**
     * @brief Push new frame to the queue
     * 
     * @param value 
     */
    void pushBack(const T& value);

    /**
     * @brief Pop a frame from the queue
     * 
     * @return T 
     */
    T popFront();

    /**
     * @brief Clear the queue
     * 
     */
    void deepClear();

    /**
     * @brief returns the size of the queue
     * 
     * @return int 
     */
    inline int size();

    /**
     * @brief Check whether the queue is empty
     * 
     * @return true 
     * @return false 
     */
    inline bool isEmpty();

    /**
     * @brief Enforces the push back action to break
     * 
     */
    inline void enforcePushBackActionToBreak();

private:
    std::deque<T> dataDeque;
    int maxCapacity;
    bool enforceToBreak;

    std::mutex mutex;
    std::condition_variable notEmptyConditionVariable;
    std::condition_variable notFullConditionVariable;
};

template <typename T>
FrameBlockingQueue<T>::FrameBlockingQueue(int maxCapacity) :
    dataDeque(), maxCapacity(maxCapacity), enforceToBreak(false),
    mutex(), notEmptyConditionVariable(), notFullConditionVariable()
{
    if (maxCapacity <= 0) {
        throw SimulationException("Invalid maxCapacity.");
    }
}

template <typename T>
FrameBlockingQueue<T>::~FrameBlockingQueue() {
    std::unique_lock<std::mutex> lock(mutex);
    for (T item : dataDeque) {
        delete item;
    }
    dataDeque.clear();
}

template <typename T>
void FrameBlockingQueue<T>::pushBack(const T& value) {
    {
        std::unique_lock<std::mutex> lock(mutex);
        while (dataDeque.size() >= maxCapacity) {
            if (enforceToBreak) {
                return;
            }

            notFullConditionVariable.wait(lock);

            if (enforceToBreak) {
                return;
            }
        }
        dataDeque.push_back(value);
    }

//    pushBackTimes++;
//    QTextStream(stderr) << pushBackTimes << "\n";

    notEmptyConditionVariable.notify_one();
}

template <typename T>
T FrameBlockingQueue<T>::popFront() {
    T tempFrontValue;

    {
        std::unique_lock<std::mutex> lock(mutex);
        while (dataDeque.empty()) {
            notEmptyConditionVariable.wait(lock);
        }

        tempFrontValue = dataDeque.front();
        dataDeque.pop_front();
    }

    notFullConditionVariable.notify_one();

    return tempFrontValue;
}

template<typename T>
void FrameBlockingQueue<T>::deepClear() {
    std::unique_lock<std::mutex> lock(mutex);
    for (T item : dataDeque) {
        delete item;
    }
    dataDeque.clear();
}

template<typename T>
int FrameBlockingQueue<T>::size() {
    std::unique_lock<std::mutex> lock(mutex);
    return dataDeque.size();
}

template<typename T>
bool FrameBlockingQueue<T>::isEmpty() {
    std::unique_lock<std::mutex> lock(mutex);
    return dataDeque.empty();
}

template<typename T>
void FrameBlockingQueue<T>::enforcePushBackActionToBreak() {
    enforceToBreak = true;
    notFullConditionVariable.notify_all();
}

#endif // FRAMEBLOCKINGQUEUE_H
