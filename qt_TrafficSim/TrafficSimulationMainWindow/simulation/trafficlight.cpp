#include "trafficlight.h"

TrafficLight::TrafficLight(int greenDuration, int redDuration, TrafficLightState initState) :
    greenDuration(greenDuration), redDuration(redDuration),  initState(initState), curState(initState)
{

}

void TrafficLight::update(unsigned long long totalSimulationTime) {
    int curCycleTime = totalSimulationTime % (greenDuration + redDuration);
    if (initState == TrafficLightState::Green) {
        if (curCycleTime < greenDuration) {
            curState = TrafficLightState::Green;
        } else {
            curState = TrafficLightState::Red;
        }
    } else {
        if (curCycleTime < redDuration) {
            curState = TrafficLightState::Red;
        } else {
            curState = TrafficLightState::Green;
        }
    }
}
