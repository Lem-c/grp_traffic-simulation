#include "trafficsimulationview.h"
#include <QTimer>
#include <QPainter>

TrafficSimulationView::TrafficSimulationView(QWidget* parent) :
    QOpenGLWidget(parent), trafficSimulator(nullptr), simulationPaintingEngine(),
    isPaused(true)
{

}

void TrafficSimulationView::setModel(TrafficSimulator* trafficSimulator) {
    this->trafficSimulator = trafficSimulator;
}

void TrafficSimulationView::paintEvent(QPaintEvent* event) {
    if (!isPaused) {
        QPainter painter;
        painter.begin(this);
        SimulationFrame* curFrame = trafficSimulator->getFrameBlockingQueue().popFront();
        simulationPaintingEngine.paint(&painter, event, curFrame);
        delete curFrame;
        painter.end();
    }
}
