#include "vehiclegenerator.h"
#include "trafficsimulator.h"
#include <QTextStream>
#include <iostream>

VehicleGenerator::VehicleGenerator(TrafficSimulator* trafficSimulator)
    : trafficSimulator(trafficSimulator), routesMultiHashMap()
{

}

void VehicleGenerator::setUpGeneratorWith(const TrafficSceneConfiguration& trafficMap) {
    unsigned int roadIndex = 0;

    for (const TrafficSceneConfiguration::RoadSetting* roadSetting : trafficMap.getRoadSettings()) {
        if (std::get<TrafficSceneConfiguration::RoadSettingIndex::HasVehicleEntry>(*roadSetting)) {
            vehicleEnterSettings.append(VehicleEnterSetting(roadIndex,
                        (double) std::get<TrafficSceneConfiguration::RoadSettingIndex::VehicleEnterRate>(*roadSetting) / 60 / 1000, 0));

            if (trafficMap.getRoutesMultiHashMap().contains(roadIndex)) {
                QList<QList<unsigned int> > routes(trafficMap.getRoutesMultiHashMap().values(roadIndex));
                for (const QList<unsigned int>& route : routes) {
                    routesMultiHashMap.insert(roadIndex, trafficSimulator->getRouteBy(route));
                }
            } else {
                std::cerr << "Error road index: " << roadIndex << "\n";
                throw SimulationException("Vehicle generator can't figures out the routes for vehicles at one vehicle entry.");
            }
        }

        roadIndex++;
    }
}

void VehicleGenerator::generateVehicles(unsigned long long totalSimulationTime) {
//    srand((unsigned int)(time(NULL)));
    for(VehicleEnterSetting& vehicleEnterSetting : vehicleEnterSettings) {
        double vehicleGenerateProb =
                (totalSimulationTime - std::get<VehicleEnterSettingIndex::LastVehicleAddedTime>(vehicleEnterSetting))
                * std::get<VehicleEnterSettingIndex::VehicleEnterRate>(vehicleEnterSetting);
        if (vehicleGenerateProb >= 1.0) {
            QList<Road::Route> routes(routesMultiHashMap.values(std::get<VehicleEnterSettingIndex::RoadIndex>(vehicleEnterSetting)));
            Road::Route route(routes.at(rand() % routes.size()));
            trafficSimulator->addVehicleToRoad(new Vehicle(route), std::get<VehicleEnterSettingIndex::RoadIndex>(vehicleEnterSetting));
            std::get<VehicleEnterSettingIndex::LastVehicleAddedTime>(vehicleEnterSetting) = totalSimulationTime;            
        }
    }
}
