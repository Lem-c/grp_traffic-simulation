#ifndef SIMULATIONPAINTINGENGINE_H
#define SIMULATIONPAINTINGENGINE_H

#include <QPainter>
#include <QPaintEvent>
#include <ctime>
#include "trafficsimulator.h"
#include "simulationframe.h"

/**
 * @class SimulationPaintingEngine simulationpainting.h
 * @brief The class prints the frame on the scene
 * 
 * @author YiCun Duan
 */
class SimulationPaintingEngine
{
public:
    /**
     * @brief Construct a new Simulation Painting Engine object
     * 
     */
    SimulationPaintingEngine();

    /**
     * @brief Paints the frame on to the scene
     * 
     * @param painter : the painter class of qt
     * @param event : the painter event of qt
     * @param simulationFrame : the simulation frame to be painted
     */
    void paint(QPainter* painter, QPaintEvent* event, SimulationFrame* simulationFrame);

private:
    std::clock_t lastFrameRenderingTime;

    double widthScaling;
    double heightScaling;

    QBrush backgroundBrush;
    QBrush roadBrush;
    QBrush vehicleBrush;
    QBrush greenLightBrush;
    QBrush redLightBrush;
    QPen roadPen;
    QPen vehiclePen;
    QPen greenLightPen;
    QPen redLightPen;
    QPen textPen;
    QFont textFont;

    inline int convertXCoordinate(double xCoordinate) const;
    inline int convertYCoordinate(double yCoordinate) const;

    inline void setScaling(double widthScaling, double heightScaling);
};

int SimulationPaintingEngine::convertXCoordinate(double xCoordinate) const {
    return std::ceil(xCoordinate * widthScaling);
}

int SimulationPaintingEngine::convertYCoordinate(double yCoordinate) const {
    return std::ceil(yCoordinate * heightScaling);
}

void SimulationPaintingEngine::setScaling(double widthScaling, double heightScaling) {
    this->widthScaling = widthScaling;
    this->heightScaling = heightScaling;
}


#endif // SIMULATIONPAINTINGENGINE_H
