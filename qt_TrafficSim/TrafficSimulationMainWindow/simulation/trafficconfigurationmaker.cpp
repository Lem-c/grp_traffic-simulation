//#include "trafficconfigurationmaker.h"

//TrafficConfigurationMaker::TrafficConfigurationMaker()
//{
////    reFreshTime = 15000;
////    trafficLightTime = 1000;
////    roadLength = 500;
////    roadWidth = 45;
////    roadGap = 60;
////    interSize = 200;
//}

//TrafficConfigurationMaker::~TrafficConfigurationMaker(){

//}

//TrafficSceneConfiguration TrafficConfigurationMaker::make1X1Configuration() {
////    unsigned int road0RoadIndex = Road::getRoadIndexBy(0, 0, 0, 0);
////    unsigned int road1RoadIndex = Road::getRoadIndexBy(0, 0, 1, 1);
////    unsigned int road2RoadIndex = Road::getRoadIndexBy(0, 2, 0, 0);
////    unsigned int road3RoadIndex = Road::getRoadIndexBy(0, 2, 1, 1);
////    unsigned int road4RoadIndex = Road::getRoadIndexBy(0, 4, 0, 0);
////    unsigned int road5RoadIndex = Road::getRoadIndexBy(0, 4, 1, 1);
////    unsigned int road6RoadIndex = Road::getRoadIndexBy(0, 6, 0, 0);
////    unsigned int road7RoadIndex = Road::getRoadIndexBy(0, 6, 1, 1);

//    TrafficSceneConfiguration trafficSceneConfiguration;

//    //0
//    trafficSceneConfiguration.addRoadSetting(Road::Point2D(700, 500), Road::Point2D(700, 0));
//    //1
//    trafficSceneConfiguration.addRoadSetting(Road::Point2D(600, 0), Road::Point2D(600, 500),
//                                             true, 30000, 30000, TrafficLight::TrafficLightState::Green,
//                                             true, 100);

//    //2
//    trafficSceneConfiguration.addRoadSetting(Road::Point2D(500, 600), Road::Point2D(0, 600));
//    //3
//    trafficSceneConfiguration.addRoadSetting(Road::Point2D(0, 700), Road::Point2D(500, 700),
//                                             true, 30000, 30000, TrafficLight::TrafficLightState::Red,
//                                             true, 100);

//    //4
//    trafficSceneConfiguration.addRoadSetting(Road::Point2D(600, 800), Road::Point2D(600, 1300));
//    //5
//    trafficSceneConfiguration.addRoadSetting(Road::Point2D(700, 1300), Road::Point2D(700, 800),
//                                             true, 30000, 30000, TrafficLight::TrafficLightState::Green,
//                                             true, 100);

//    //6
//    trafficSceneConfiguration.addRoadSetting(Road::Point2D(800, 700), Road::Point2D(1300, 700));
//    //7
//    trafficSceneConfiguration.addRoadSetting(Road::Point2D(1300, 600), Road::Point2D(800, 600),
//                                             true, 30000, 30000, TrafficLight::TrafficLightState::Red,
//                                             true, 100);

//    //8
//    trafficSceneConfiguration.addRoadSetting({600, 500}, {500, 600},
//                                             false, -1, -1, TrafficLight::TrafficLightState::Green,
//                                             false, -1,
//                                              true, CurveRoad::TurnDirection::Right, 100);
//    //9
//    trafficSceneConfiguration.addRoadSetting({600, 500}, {600, 800});
//    //10
//    trafficSceneConfiguration.addRoadSetting({600, 500}, {800, 700},
//                                             false, -1, -1, TrafficLight::TrafficLightState::Green,
//                                             false, -1,
//                                              true, CurveRoad::TurnDirection::Left, 100);

//    //11
//    trafficSceneConfiguration.addRoadSetting({500, 700}, {700, 500},
//                                             false, -1, -1, TrafficLight::TrafficLightState::Green,
//                                             false, -1,
//                                              true, CurveRoad::TurnDirection::Left, 100);
//    //12
//    trafficSceneConfiguration.addRoadSetting({500, 700}, {600, 800},
//                                             false, -1, -1, TrafficLight::TrafficLightState::Green,
//                                             false, -1,
//                                              true, CurveRoad::TurnDirection::Right, 100);
//    //13
//    trafficSceneConfiguration.addRoadSetting({500, 700}, {800, 700});

//    //14
//    trafficSceneConfiguration.addRoadSetting({700, 800}, {700, 500});
//    //15
//    trafficSceneConfiguration.addRoadSetting({700, 800}, {500, 600},
//                                             false, -1, -1, TrafficLight::TrafficLightState::Green,
//                                             false, -1,
//                                              true, CurveRoad::TurnDirection::Left, 50);
//    //16
//    trafficSceneConfiguration.addRoadSetting({700, 800}, {800, 700},
//                                             false, -1, -1, TrafficLight::TrafficLightState::Green,
//                                             false, -1,
//                                              true, CurveRoad::TurnDirection::Right, 50);

//    //17
//    trafficSceneConfiguration.addRoadSetting({800, 600}, {700, 500},
//                                             false, -1, -1, TrafficLight::TrafficLightState::Green,
//                                             false, -1,
//                                              true, CurveRoad::TurnDirection::Right, 50);
//    //18
//    trafficSceneConfiguration.addRoadSetting({800, 600}, {500, 600});
//    //19
//    trafficSceneConfiguration.addRoadSetting({800, 600}, {600, 800},
//                                             false, -1, -1, TrafficLight::TrafficLightState::Green,
//                                             false, -1,
//                                              true, CurveRoad::TurnDirection::Left, 50);

//    trafficSceneConfiguration.addRoute({1, 8, 2});
//    trafficSceneConfiguration.addRoute({1, 9, 4});
//    trafficSceneConfiguration.addRoute({1, 10, 6});

//    trafficSceneConfiguration.addRoute({3, 11, 0});
//    trafficSceneConfiguration.addRoute({3, 12, 4});
//    trafficSceneConfiguration.addRoute({3, 13, 6});

//    trafficSceneConfiguration.addRoute({5, 14, 0});
//    trafficSceneConfiguration.addRoute({5, 15, 2});
//    trafficSceneConfiguration.addRoute({5, 16, 6});

//    trafficSceneConfiguration.addRoute({7, 17, 0});
//    trafficSceneConfiguration.addRoute({7, 18, 2});
//    trafficSceneConfiguration.addRoute({7, 19, 4});

//    return trafficSceneConfiguration;
//}

//TrafficSceneConfiguration TrafficConfigurationMaker::make3X3Configuration() {

//}

//The m*n model
//TrafficSceneConfiguration TrafficConfigurationMaker::autoConfigModel(TrafficConfigurationMaker* tc, int row, int col){
//    //int roadCount = 0;
//    tc->colNum = col;
//    tc->rowNum = row;

//    //The square traffic
//    //横向添加渲染
//    if(row==1){
//        if(col==1){
//            make1X1Configuration();
//        }else if(col==2){
//            tc->roadLength = 600;
//            make1_2(tc);
//        }else{
//            tc->roadLength = 500;
//            make1_3(tc);
//        }
//    }else if(row==2){
//        if(col==1){
//            tc->roadLength = 600;
//            make2_1(tc);
//        }else if(col==2){
//            tc->roadLength = 500;
//            make2_2(tc);
//        }else{
//            tc->roadLength = 500;
//            make2_3(tc);
//        }
//    }else if(row==3){
//        if(col==1){
//            tc->roadLength = 310;
//            make3_1(tc);
//        }else if(col==2){
//            tc->roadLength = 200;
//            make2_2(tc);
//        }else{
//            tc->roadLength = 200;
//            make3_3(tc);
//        }
//    }
//    else{
//        throw SimulationException("None generate model type!");;
//    }

//    return tc->modelTrafficSim;
//}


////The vertical add, give the right up coordinates
//void TrafficConfigurationMaker::addColVertival(int type, int oneBaseX, int oneBaseY){
//    if(type==0){
//        //Two down to up
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX, oneBaseY+roadLength)
//                                       , Road::Point2D(oneBaseX, oneBaseY));
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX-roadWidth, oneBaseY+roadLength)
//                                       , Road::Point2D(oneBaseX-roadWidth, oneBaseY));
//        //Two up to down
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX-roadWidth-roadGap, oneBaseY)
//                                       , Road::Point2D(oneBaseX-roadWidth-roadGap, oneBaseY+roadLength)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Green,
//                                       true, 100);
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX-2*roadWidth-roadGap, oneBaseY)
//                                       , Road::Point2D(oneBaseX-2*roadWidth-roadGap, oneBaseY+roadLength)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Green,
//                                       true, 100);
//    }else if(type==1){
//        //Two down to up
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX, oneBaseY+roadLength)
//                                       , Road::Point2D(oneBaseX, oneBaseY)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Green,
//                                       true, 100);
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX-roadWidth, oneBaseY+roadLength)
//                                       , Road::Point2D(oneBaseX-roadWidth, oneBaseY)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Green,
//                                       true, 100);
//        //Two up to down
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX-roadWidth-roadGap, oneBaseY)
//                                       , Road::Point2D(oneBaseX-roadWidth-roadGap, oneBaseY+roadLength));
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX-2*roadWidth-roadGap, oneBaseY)
//                                       , Road::Point2D(oneBaseX-2*roadWidth-roadGap, oneBaseY+roadLength));
//    }else if(type==2){
//        //Double light
//        //Two down to up
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX, oneBaseY+roadLength)
//                                       , Road::Point2D(oneBaseX, oneBaseY)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Green,
//                                       true, 100);
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX-roadWidth, oneBaseY+roadLength)
//                                       , Road::Point2D(oneBaseX-roadWidth, oneBaseY)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Green,
//                                       true, 100);
//        //Two up to down
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX-roadWidth-roadGap, oneBaseY)
//                                       , Road::Point2D(oneBaseX-roadWidth-roadGap, oneBaseY+roadLength)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Green,
//                                       true, 100);
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX-2*roadWidth-roadGap, oneBaseY)
//                                       , Road::Point2D(oneBaseX-2*roadWidth-roadGap, oneBaseY+roadLength)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Green,
//                                       true, 100);
//    }else{
//        //Two down to up
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX, oneBaseY+roadLength)
//                                       , Road::Point2D(oneBaseX, oneBaseY));
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX-roadWidth, oneBaseY+roadLength)
//                                       , Road::Point2D(oneBaseX-roadWidth, oneBaseY));
//        //Two up to down
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX-roadWidth-roadGap, oneBaseY)
//                                       , Road::Point2D(oneBaseX-roadWidth-roadGap, oneBaseY+roadLength));
//        modelTrafficSim.addRoadSetting(Road::Point2D(oneBaseX-2*roadWidth-roadGap, oneBaseY)
//                                       , Road::Point2D(oneBaseX-2*roadWidth-roadGap, oneBaseY+roadLength));
//    }
//}


////give the left up coordinates
//void TrafficConfigurationMaker::addRowHorizontal(int type, int oneBaseX, int oneBaseY){
//    int tempBaseX = oneBaseX;
//    int tempBaseY = oneBaseY;

//    if(type==0){
//        //Two right to left
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX+roadLength, tempBaseY)
//                                       , Road::Point2D(tempBaseX, tempBaseY));
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX+roadLength, tempBaseY+roadWidth)
//                                       , Road::Point2D(tempBaseX, tempBaseY+roadWidth));
//        //Two left to right
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX, tempBaseY+roadWidth+roadGap)
//                                       , Road::Point2D(tempBaseX+roadLength, tempBaseY+roadWidth+roadGap)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Red,
//                                       true, 100);
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX, tempBaseY+2*roadWidth+roadGap)
//                                       , Road::Point2D(tempBaseX+roadLength, tempBaseY+2*roadWidth+roadGap)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Red,
//                                       true, 100);
//    }else if(type==1){
//        //Two right to left
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX+roadLength, tempBaseY)
//                                       , Road::Point2D(tempBaseX, tempBaseY)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Red,
//                                       true, 100);
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX+roadLength, tempBaseY+roadWidth)
//                                       , Road::Point2D(tempBaseX, tempBaseY+roadWidth)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Red,
//                                       true, 100);
//        //Two left to right
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX, tempBaseY+roadWidth+roadGap)
//                                       , Road::Point2D(tempBaseX+roadLength, tempBaseY+roadWidth+roadGap));
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX, tempBaseY+2*roadWidth+roadGap)
//                                       , Road::Point2D(tempBaseX+roadLength, tempBaseY+2*roadWidth+roadGap));
//    }else if(type==2){
//        //Two right to left
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX+roadLength, tempBaseY)
//                                       , Road::Point2D(tempBaseX, tempBaseY)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Red,
//                                       true, 100);
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX+roadLength, tempBaseY+roadWidth)
//                                       , Road::Point2D(tempBaseX, tempBaseY+roadWidth)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Red,
//                                       true, 100);
//        //Two left to right
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX, tempBaseY+roadWidth+roadGap)
//                                       , Road::Point2D(tempBaseX+roadLength, tempBaseY+roadWidth+roadGap)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Red,
//                                       true, 100);
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX, tempBaseY+2*roadWidth+roadGap)
//                                       , Road::Point2D(tempBaseX+roadLength, tempBaseY+2*roadWidth+roadGap)
//                                       , true, 30000, 30000, TrafficLight::TrafficLightState::Red,
//                                       true, 100);
//    }else{
//        //Two right to left
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX+roadLength, tempBaseY)
//                                       , Road::Point2D(tempBaseX, tempBaseY));
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX+roadLength, tempBaseY+roadWidth)
//                                       , Road::Point2D(tempBaseX, tempBaseY+roadWidth));
//        //Two left to right
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX, tempBaseY+roadWidth+roadGap)
//                                       , Road::Point2D(tempBaseX+roadLength, tempBaseY+roadWidth+roadGap));
//        modelTrafficSim.addRoadSetting(Road::Point2D(tempBaseX, tempBaseY+2*roadWidth+roadGap)
//                                       , Road::Point2D(tempBaseX+roadLength, tempBaseY+2*roadWidth+roadGap));
//    }
//}


//void TrafficConfigurationMaker::connectRoute(uint road_1, uint road_2){
//    modelTrafficSim.addRoute({road_1, road_2});
//}

////Add turn round route connectionZ
//void TrafficConfigurationMaker::connectRoute(uint road_1, uint road_2, uint road_3){
//    modelTrafficSim.addRoute({road_1, road_2, road_3});
//}

//void TrafficConfigurationMaker::autoConnectRoute(int col, int row){
//    //int serialNum = modelTrafficSim.getSize();

//    //Connect the longitudinal
//    if(col==1){
//        if(row==1){
//            connectNS(3,2,12,1,8);
//            connectSN(13,12,12,1,8);
//            connectWE(7,6,4,5);
//            connectEW(9,8,4,8);
//        }else if(row==2){
//            connectNS(15,2,12,1,8);
//            connectSN(25,12,12,1,8);
//            //row_1
//            connectWE(7,6,4,5);
//            connectEW(9,8,4,8);
//            //row_2
//            connectWE(19,18,4,5);
//            connectEW(21,20,4,8);
//        }else if(row==3){
//            connectNS(39,2,12,1,8);
//            connectSN(37,12,12,1,8);
//            //row_1
//            connectWE(7,6,4,5);
//            connectEW(9,8,4,8);
//            //row_2
//            connectWE(19,18,4,5);
//            connectEW(21,20,4,8);
//            //row_2
//            connectWE(31,30,4,5);
//            connectEW(33,32,4,8);
//        }
//    }else if(col==2){
//        if(row==1){
//            connectNS(3,2,20,5,12);
//            connectNS(7,6,20,5,12);
//            connectSN(21,20,20,5,12);
//            connectSN(25,24,20,5,12);
//            connectWE(15,10,4,9);
//            connectEW(17,12,4,12);
//        }else if(row==2){
//            connectNS(23,2,20,5,12);
//            connectNS(27,6,20,5,12);
//            connectSN(41,20,20,5,12);
//            connectSN(45,24,20,5,12);
//            connectWE(15,10,4,9);
//            connectWE(35,30,4,9);
//            connectEW(17,12,4,12);
//            connectEW(37,32,4,12);
//        }else if(row==3){
//            connectNS(63,2,20,5,12);
//            connectNS(67,6,20,5,12);
//            connectSN(61,20,20,5,12);
//            connectSN(65,24,20,5,12);

//            connectWE(15,10,4,9);
//            connectWE(35,30,4,9);
//            connectWE(55,50,4,9);
//            connectEW(17,12,4,12);
//            connectEW(37,32,4,12);
//            connectEW(57,52,4,12);
//        }
//    }else if(col==3){
//        if(row==1){
//            connectNS(39,2,12,1,8);
//            connectSN(37,12,12,1,8);
//            connectWE(7,6,4,5);
//            connectWE(19,18,4,5);
//            connectWE(31,30,4,5);
//            connectEW(9,8,4,8);
//            connectEW(21,20,4,8);
//            connectEW(33,32,4,8);
//        }else if(row==3){
//            connectNS(87,2,28,9,16);
//            connectNS(91,6,28,9,16);
//            connectNS(95,10,28,9,16);
//            connectSN(85,28,28,9,16);
//            connectSN(89,32,28,9,16);
//            connectSN(93,36,28,9,16);

//            connectWE(23,14,4,13);
//            connectWE(51,42,4,13);
//            connectWE(79,70,4,13);
//            connectEW(25,16,4,16);
//            connectEW(53,44,4,16);
//            connectEW(81,72,4,16);
//        }
//    }
//}

////The route connection methods.
////Col start from up to down, row starts fron left to right
////

////turnGap is the inner-most meet gap
//void TrafficConfigurationMaker::connectNS(int limit, int base, int upGap, int turnGapR, int turnGapL){
//    int baseNum = base;
//    //count the add times
//    int count = 1;

//    while(baseNum<=limit){

//        if(count%2!=0){
//            connectRoute(baseNum, baseNum+upGap);
//            connectRoute(baseNum, baseNum+turnGapL);
//            //connectRoute(baseNum, baseNum+turnGapL+1);
//            baseNum += 1;

//            count += 1;
//        }else{
//            connectRoute(baseNum, baseNum+upGap);
//            connectRoute(baseNum, baseNum+turnGapR);
//            //connectRoute(baseNum, baseNum+turnGapR+1);
//            baseNum -= 1;
//            baseNum += upGap;

//            count += 1;
//        }
//    }
//}

////turnGap is the outer-most meet gap
//void TrafficConfigurationMaker::connectSN(int limit, int base, int upGap, int turnGapR, int turnGapL){
//    int baseNum = base;
//    //count the add times
//    int count = 1;

//    while(baseNum<=limit){

//        if(count%2!=0){
//            connectRoute(baseNum, baseNum-upGap);
//            connectRoute(baseNum, baseNum-turnGapR);
//            //connectRoute(baseNum, baseNum-turnGapR-1);
//            baseNum += 1;

//            count++;
//        }else{
//            connectRoute(baseNum, baseNum-upGap);
//            connectRoute(baseNum, baseNum-turnGapL);
//            //connectRoute(baseNum, baseNum+turnGapL+1);
//            baseNum -= 1;
//            baseNum += upGap;

//            count++;
//        }
//    }
//}

////turnGap is the outer-most meet gap
//void TrafficConfigurationMaker::connectWE(int limit, int base, int upGap, int turnGap){
//    int baseNum = base;
//    //count the add times
//    int count = 1;

//    while(baseNum<=limit){

//        if(count%2!=0){
//            connectRoute(baseNum, baseNum+upGap);
//            connectRoute(baseNum, baseNum-turnGap);
//            baseNum += 1;

//            count++;
//        }else{
//            connectRoute(baseNum, baseNum+upGap);
//            connectRoute(baseNum, baseNum+turnGap+3);
//            baseNum -= 1;
//            baseNum += upGap;

//            count++;
//        }
//    }
//}

////turnGap is teh first meet gap, always turn the inner-most road
//void TrafficConfigurationMaker::connectEW(int limit, int base, int upGap, int turnGap){
//    int baseNum = base;
//    //count the add times
//    int count = 1;

//    while(baseNum<=limit){

//        if(count%2!=0){
//            connectRoute(baseNum, baseNum-upGap);
//            connectRoute(baseNum, baseNum-turnGap);
//            baseNum += 1;

//            count++;
//        }else{
//            connectRoute(baseNum, baseNum-upGap);
//            connectRoute(baseNum, baseNum+turnGap-3);
//            baseNum -= 1;
//            baseNum += upGap;

//            count++;
//        }
//    }
//}

//void TrafficConfigurationMaker::make1_2(TrafficConfigurationMaker *tc){
//    tc->addColVertival(0,830,0);
//    tc->addRowHorizontal(0,0, 670);
//    tc->addRowHorizontal(1,900,670);
//    tc->addColVertival(3,830,940);
//    tc->addRowHorizontal(0,0, 1610);
//    tc->addRowHorizontal(1,900,1610);
//    tc->addColVertival(1,830, 1880);
//    //Add route
//    tc->autoConnectRoute(1,2);
//}

//void TrafficConfigurationMaker::make1_3(TrafficConfigurationMaker *tc){
//    tc->addColVertival(0,680,0);
//    tc->addRowHorizontal(0,0, 560);
//    tc->addRowHorizontal(1,710,560);
//    tc->addColVertival(2,680,810);
//    tc->addRowHorizontal(0,0,1390);
//    tc->addRowHorizontal(1,710,1390);
//    tc->addColVertival(2,680, 1640);
//    tc->addRowHorizontal(0,0,2230);
//    tc->addRowHorizontal(1,710,2230);
//    tc->addColVertival(1,680,2520);
//    //Add route
//    tc->autoConnectRoute(1,3);
//}

//void TrafficConfigurationMaker::make2_1(TrafficConfigurationMaker *tc){
//    tc->addColVertival(0,850,0);
//    tc->addColVertival(0,1800,0);
//    tc->addRowHorizontal(0,0,680);
//    tc->addRowHorizontal(2,950,680);
//    tc->addRowHorizontal(1,1900,680);
//    tc->addColVertival(1,850,940);
//    tc->addColVertival(1,1800,940);
//    //Add route
//    tc->autoConnectRoute(2,1);
//}

//void TrafficConfigurationMaker::make2_2(TrafficConfigurationMaker *tc){
//    tc->addColVertival(0,740,0);
//    tc->addColVertival(0,1580,0);

//    tc->addRowHorizontal(0,0,570);
//    tc->addRowHorizontal(2,850,570);
//    tc->addRowHorizontal(1,1680,570);

//    tc->addColVertival(2,740,850);
//    tc->addColVertival(2,1580,850);
//    //row_2
//    tc->addRowHorizontal(0,0,1500);
//    tc->addRowHorizontal(2,850,1500);
//    tc->addRowHorizontal(1,1680,1500);

//    tc->addColVertival(1,740,1790);
//    tc->addColVertival(1,1580,1790);
//    //Add route
//    tc->autoConnectRoute(2,2);
//}

//void TrafficConfigurationMaker::make2_3(TrafficConfigurationMaker *tc){
//    tc->addColVertival(0,715,0);
//    tc->addColVertival(0,1570,0);

//    tc->addRowHorizontal(0,0,520);
//    tc->addRowHorizontal(2,835,520);
//    tc->addRowHorizontal(1,1635,520);

//    tc->addColVertival(2,715,790);
//    tc->addColVertival(1,1570,790);

//    tc->addRowHorizontal(0,0,1340);
//    tc->addRowHorizontal(2,835,1340);
//    tc->addRowHorizontal(1,1635,1340);

//    tc->addColVertival(0,715,1640);
//    tc->addColVertival(2,1570,1640);

//    tc->addRowHorizontal(0,0,2210);
//    tc->addRowHorizontal(2,835,2210);
//    tc->addRowHorizontal(1,1635,2210);

//    tc->addColVertival(1,715,2440);
//    tc->addColVertival(1,1570,2440);
//    //Add route
//    tc->autoConnectRoute(2,3);
//}

//void TrafficConfigurationMaker::make3_1(TrafficConfigurationMaker *tc){
//    tc->addColVertival(0,640,0);
//    tc->addRowHorizontal(0,0,330);
//    tc->addRowHorizontal(1,800,330);
//    tc->addColVertival(1,640,400);

//    tc->addRowHorizontal(0,0,670);
//    tc->addRowHorizontal(1,800,670);
//    tc->addColVertival(0,640,700);

//    tc->addRowHorizontal(0,0,1000);
//    tc->addRowHorizontal(1,800,1000);
//    tc->addColVertival(1,640,1100);
//    //Add route
//    tc->autoConnectRoute(3,1);
//}

//void TrafficConfigurationMaker::make3_3(TrafficConfigurationMaker *tc){
//    tc->addColVertival(0,315,0);
//    tc->addColVertival(0,670,0);
//    tc->addColVertival(0,1030,0);

//    tc->addRowHorizontal(0,0,280);
//    tc->addRowHorizontal(0,335,280);
//    tc->addRowHorizontal(1,700,280);
//    tc->addRowHorizontal(1,1030,280);

//    tc->addColVertival(2,315,510);
//    tc->addColVertival(2,670,510);
//    tc->addColVertival(2,1030,510);

//    tc->addRowHorizontal(0,0,760);
//    tc->addRowHorizontal(0,335,760);
//    tc->addRowHorizontal(1,700,760);
//    tc->addRowHorizontal(1,1030,760);

//    tc->addColVertival(2,315,970);
//    tc->addColVertival(2,670,970);
//    tc->addColVertival(2,1030,970);

//    tc->addRowHorizontal(0,0,1250);
//    tc->addRowHorizontal(0,335,1250);
//    tc->addRowHorizontal(1,700,1250);
//    tc->addRowHorizontal(1,1030,1250);

//    tc->addColVertival(1,315,1460);
//    tc->addColVertival(1,670,1460);
//    tc->addColVertival(1,1030,1460);
//    //Add route
//    tc->autoConnectRoute(3,3);
//}

////According to the intersection No. and the road No.
////give the direct road-index
////Non-static method, needs to be called through an object
////The sequence is clockwise
//int TrafficConfigurationMaker::searchThroughInterNo(int interNo, int roadNum){
//    if(colNum==1){
//        if(roadNum < 8+4*3*(interNo-1)){
//            return roadNum+12*(interNo-1);
//        }else{
//            switch (roadNum){
//                case 8:
//                    return 15 + 12*(interNo-1);
//                case 9:
//                    return 14 + 12*(interNo-1);
//                case 10:
//                    return 13 + 12*(interNo-1);
//                case 11:
//                    return 12 + 12*(interNo-1);
//                case 12:
//                    return 11 + 12*(interNo-1);
//                case 13:
//                    return 10 + 12*(interNo-1);
//                case 14:
//                    return 9 + 12*(interNo-1);
//                case 15:
//                    return 8 + 12*(interNo-1);
//            }

//        }
//    }

//        int index;

//        if(interNo <=2){
//            index=0;
//        }else if(interNo <= 4){
//            index = 1;
//        }else{
//            index =2;
//        }

//        switch (roadNum){
//            case 0:
//                return 0 + 4*((interNo-1)%2) + 20*index;
//            case 1:
//                return 1 + 4*((interNo-1)%2) + 20*index;
//            case 2:
//                return 2 + 4*((interNo-1)%2) + 20*index;
//            case 3:
//                return 3 + 4*((interNo-1)%2) + 20*index;
//            case 4:
//                return 8 + 4*((interNo-1)%2) + 20*index;
//            case 5:
//                return 9 + 4*((interNo-1)%2) + 20*index;
//            case 6:
//                return 10 + 4*((interNo-1)%2) + 20*index;
//            case 7:
//                return 11 + 4*((interNo-1)%2) + 20*index;
//            case 8:
//                return 20 + 4*((interNo-1)%2) + 20*index;
//            case 9:
//                return 21 + 4*((interNo-1)%2) + 20*index;
//            case 10:
//                return 22 + 4*((interNo-1)%2) + 20*index;
//            case 11:
//                return 23 + 4*((interNo-1)%2) + 20*index;
//            case 12:
//                return 12 + 4*((interNo-1)%2) + 20*index;
//            case 13:
//                return 13 + 4*((interNo-1)%2) + 20*index;
//            case 14:
//                return 14 + 4*((interNo-1)%2) + 20*index;
//            case 15:
//                return 15 + 4*((interNo-1)%2) + 20*index;
//    }

//    return 0;
//}
