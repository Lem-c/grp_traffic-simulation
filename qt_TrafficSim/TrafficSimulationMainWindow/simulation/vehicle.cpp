#include <QTextStream>
#include "vehicle.h"

const double Vehicle::vehicleWidth = 20.0;

Vehicle::Vehicle(const Road::Route& route) :
    position(0.0), velocity(0.0), acceleration(0.0),
    curMaxVelocity(50.0), maxVelocity(50.0), maxAcceleration(15.0),
    vehicleLength(4.0),
    reactTime(1000), miniDesiredVehicleDistance(50.0), comfortableDeceleration(50.0),
    toBeStopped(false),
    route(), curRoad(nullptr)
{
    if (route.isEmpty()) {
        throw SimulationException("Vehicle's route can't be empty.");
    }

    this->route = route;
    this->curRoad = route[0];
}

Vehicle::Vehicle(const Vehicle& otherVehicle) :
    position(otherVehicle.position), velocity(otherVehicle.velocity),
    acceleration(otherVehicle.acceleration), curMaxVelocity(otherVehicle.curMaxVelocity),
    maxVelocity(otherVehicle.curMaxVelocity), maxAcceleration(otherVehicle.maxAcceleration),
    vehicleLength(otherVehicle.vehicleLength),
    reactTime(otherVehicle.reactTime), miniDesiredVehicleDistance(otherVehicle.miniDesiredVehicleDistance),
    comfortableDeceleration(otherVehicle.comfortableDeceleration),
    toBeStopped(otherVehicle.toBeStopped),
    route(), curRoad(nullptr)
{

}

void Vehicle::update(const Vehicle* frontVehicle, int deltaTimeInMS) {
    double deltaTime = (double) deltaTimeInMS / 1000; // s

    if (velocity + acceleration * deltaTime < 0) {
        position -= 0.5 * velocity * velocity / acceleration;
        velocity = 0.0;
    } else {
        velocity += acceleration * deltaTime;
        position += velocity * deltaTime + acceleration * deltaTime * deltaTime / 2;
    }

    if (toBeStopped) {
        acceleration = - comfortableDeceleration * (velocity / curMaxVelocity);
    } else {
        double desiredVehicleDistanceRatio = 0.0;
        //Vehicle auto slow down
        if (frontVehicle) {
            double deltaX = frontVehicle->getPosition() - position - vehicleLength;
            double deltaV = velocity - frontVehicle->getVelocity();
            desiredVehicleDistanceRatio = (miniDesiredVehicleDistance +
                                            velocity * reactTime / 1000 +
                                            velocity * deltaV / (2 * std::sqrt(maxAcceleration * comfortableDeceleration))
                                          ) / deltaX;
        }

        acceleration = maxAcceleration *
                (1 - std::pow(velocity / curMaxVelocity, 4.0)
                   - std::pow(desiredVehicleDistanceRatio, 2.0));
    }

}
