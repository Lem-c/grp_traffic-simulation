#include "trafficsimulatorthread.h"
#include <QTextStream>

TrafficSimulatorThread::TrafficSimulatorThread(TrafficSimulator* trafficSimulator,
                                                     QObject* parent)
    : QObject(parent), workerThread(), trafficSimulator(trafficSimulator)
{
    trafficSimulator->moveToThread(&workerThread);
//    connect(&workerThread, &QThread::finished, trafficSimulator, &QObject::deleteLater);
    connect(this, &TrafficSimulatorThread::operate, trafficSimulator, &TrafficSimulator::generateSimulationFrameWith);
}

void TrafficSimulatorThread::start(int refreshTimeInterval) {
    workerThread.start();
    emit operate(refreshTimeInterval);
}

TrafficSimulatorThread::~TrafficSimulatorThread() {
    workerThread.quit();
    workerThread.requestInterruption();
    trafficSimulator->getFrameBlockingQueue().enforcePushBackActionToBreak();
    workerThread.wait();
}
