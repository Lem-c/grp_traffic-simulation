#ifndef TRAFFICSCENECONFIGURATION_H
#define TRAFFICSCENECONFIGURATION_H

#include <QMultiHash>
#include "road.h"
#include "../utilities/simulationexception.h"
#include "curveroad.h"
#include <iostream>
#include <set>
#include <QVariant>

class Intersection;

/**
 * @class TrafficSceneConfiguration trafficsceneconfiguration.h
 * @brief The configuration of the traffic scene
 * 
 * @author YiCun Duan
 */
class TrafficSceneConfiguration
{
public:
    /**
     * @brief Enum of the setting index of the road
     * 
     */
    enum RoadSettingIndex {
        StartPoint = 0, /** @brief enum value 0 */
        EndPoint = 1, /** @brief enum value 1 */
        HasTrafficLight = 2, /** @brief enum value 2 */
        GreenDuration = 3, /** @brief enum value 3 */
        RedDuration = 4, /** @brief enum value 4 */
        InitState = 5, /** @brief enum value 5 */
        HasVehicleEntry = 6, /** @brief enum value 6 */
        VehicleEnterRate = 7, /** @brief enum value 7 vehicles/minute */
        IsCurveRoad = 8, /** @brief enum value 8 */
        TurnDirection = 9, /** @brief enum value 9 */
        InternalPointsNumber = 10 /** @brief enum value 10 */
    };

    typedef std::tuple<Road::Point2D, Road::Point2D,
                        bool, int, int, TrafficLight::TrafficLightState,
                        bool, int,
                        bool, CurveRoad::TurnDirection, int> RoadSetting;

    /**
     * @brief Construct a new Traffic Scene Configuration object
     * 
     * @param horizontalIntersectionNumber : the number of horizontal intersection
     * @param verticalIntersectionNumber : the number of vertical intersection
     */
    explicit TrafficSceneConfiguration(int horizontalIntersectionNumber, int verticalIntersectionNumber);

    /**
     * @brief Add setting of the road
     * 
     * @param roadSetting : the road setting
     * @return unsigned int 
     */
    inline unsigned int addRoadSetting(RoadSetting* roadSetting);
//    inline unsigned int addRoadSetting(const Road::Point2D& startPoint, const Road::Point2D& endPoint,
//                               bool hasTrafficLight = false, int greenDuration = -1, int redDuration = -1,
//                               TrafficLight::TrafficLightState initState = TrafficLight::TrafficLightState::Green,
//                               bool hasVehicleEntry = false, int vehicleEnterRate = -1,
//                               bool isCurveRoad = false, CurveRoad::TurnDirection turnDirection = CurveRoad::TurnDirection::Left, int internalPointsNumber = -1);

    /**
     * @brief Add route to the traffic scene
     * 
     * @param roadIndexes : the index of the road
     */
    inline void addRoute(const QList<unsigned int>& roadIndexes);

    /**
     * @brief Get the number of horizontal intersection
     * 
     * @return number of horizontal intersection
     */
    inline int getHorizontalIntersectionNumber() const;

    /**
     * @brief Get the number of vertical intersection
     * 
     * @return number of vertical intersection
     */
    inline int getVerticalIntersectionNumber() const;

    /**
     * @brief Get the list of intersections
     * 
     * @return list of intersections
     */
    inline const QList<Intersection*>& getIntersections() const;

    /**
     * @brief Get the road settings
     * 
     * @return road settings
     */
    inline const QList<RoadSetting*>& getRoadSettings() const;

    /**
     * @brief Get the Routes Multi Hash Map object
     * 
     * @return Routes Multi Hash Map object
     */
    inline const QMultiHash<unsigned int, QList<unsigned int> >& getRoutesMultiHashMap() const;

    /**
     * @brief Get the road index that are connected
     * 
     * @return std::set<unsigned int> 
     */
    std::set<unsigned int> getConnectionRoadIndexes() const;

    /**
     * @brief Chaneg the road setting
     * 
     * @param roadIndex : the road index
     * @param roadSettingIndex : the road setting index
     * @param newValue : the new value entered
     */
    void changeRoadSettingBy(unsigned int roadIndex, RoadSettingIndex roadSettingIndex, const QVariant& newValue);

private:
    static const int maxRandomRoutesNumberForOneEntry;

    int horizontalIntersectionNumber;
    int verticalIntersectionNumber;
    QList<Intersection*> intersections;
    QList<RoadSetting*> roadSettings;
    QMultiHash<unsigned int, QList<unsigned int> > routesMultiHashMap;

    QList<unsigned int> generateOneRandomRouteFrom(Intersection* intersection, int sequenceNumber, bool left);
};

unsigned int TrafficSceneConfiguration::addRoadSetting(RoadSetting* roadSetting) {
    roadSettings.append(roadSetting);
    return roadSettings.size() - 1;
}

//unsigned int TrafficSceneConfiguration::addRoadSetting(const Road::Point2D &startPoint, const Road::Point2D &endPoint,
//                                               bool hasTrafficLight, int greenDuration, int redDuration, TrafficLight::TrafficLightState initState,
//                                               bool hasVehicleEntry, int vehicleEnterRate,
//                                               bool isCurveRoad, CurveRoad::TurnDirection turnDirection, int internalPointsNumber) {
//   if (hasTrafficLight && (greenDuration <= 0 || redDuration <= 0)) {
//       throw SimulationException("greenDuration or redDuration can't be <= 0.");
//   }

//   if (hasVehicleEntry && vehicleEnterRate <= 0) {
//       throw SimulationException("vehicle enter rate can't be <= 0.");
//   }

//   if (isCurveRoad && turnDirection != CurveRoad::TurnDirection::Left && turnDirection != CurveRoad::Right) {
//       throw SimulationException("Invalid turnDirection.");
//   }

//   roadSettings.append(new RoadSetting(startPoint, endPoint,
//                                   hasTrafficLight, greenDuration, redDuration, initState,
//                                   hasVehicleEntry, vehicleEnterRate,
//                                   isCurveRoad, turnDirection, internalPointsNumber));

//   return roadSettings.size() - 1;
//}

void TrafficSceneConfiguration::addRoute(const QList<unsigned int>& roadIndexes) {
    routesMultiHashMap.insert(roadIndexes[0], roadIndexes);
}

const QList<Intersection*>& TrafficSceneConfiguration::getIntersections() const {
    return intersections;
}

const QList<TrafficSceneConfiguration::RoadSetting*>& TrafficSceneConfiguration::getRoadSettings() const {
    return roadSettings;
}

const QMultiHash<unsigned int, QList<unsigned int> >& TrafficSceneConfiguration::getRoutesMultiHashMap() const {
    return routesMultiHashMap;
}

int TrafficSceneConfiguration::getHorizontalIntersectionNumber() const {
    return horizontalIntersectionNumber;
}

int TrafficSceneConfiguration::getVerticalIntersectionNumber() const {
    return verticalIntersectionNumber;
}

#endif // TRAFFICSCENECONFIGURATION_H
