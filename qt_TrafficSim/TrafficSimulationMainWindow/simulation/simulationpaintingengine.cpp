#include "simulationpaintingengine.h"
#include "simulationframe.h"
#include <cmath>
#include <vector>
#include <QPainter>

SimulationPaintingEngine::SimulationPaintingEngine() :
    lastFrameRenderingTime(std::clock()),
    widthScaling(1.0), heightScaling(1.0),
    backgroundBrush(QColor(224, 224, 224, 255)), roadBrush(QColor(116, 116, 116)),
    vehicleBrush(QColor(46, 255, 244)), greenLightBrush(QColor(0, 255, 0, 225)),
    redLightBrush(QColor(255, 0, 0, 225)), roadPen(QColor(0, 0, 0, 175)),
    vehiclePen(QColor(255, 255, 0, 255)), greenLightPen(QColor(0, 255, 0, 225), 10),
    redLightPen(QColor(255, 0, 0, 225), 10), textPen(Qt::black), textFont()
{
    textPen.setWidth(1);
    textFont.setPixelSize(15);
}

void SimulationPaintingEngine::paint(QPainter* painter, QPaintEvent* event, SimulationFrame* simulationFrame) {
//    std::clock_t paintingTime = std::clock();
    painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

    this->setScaling(event->rect().width() / simulationFrame->getSimulationViewWidth(),
                     event->rect().height() / simulationFrame->getSimulationViewHeight());
//    painter->scale(widthScaling, heightScaling);

    //draw background
    painter->fillRect(event->rect(), backgroundBrush);

    //show time
    painter->save();

    painter->setPen(textPen);
    painter->setFont(textFont);

    painter->drawText(0, 15, QString("Total time: %1 ms").arg(simulationFrame->getTotalSimulationTime()));

    painter->drawText(0, 32, QString("FPS: %1").arg(std::ceil(1.0 / ((double) (std::clock() - lastFrameRenderingTime) / CLOCKS_PER_SEC))));
    lastFrameRenderingTime = std::clock();

//    int textYCoordination = 49;
//    for (Road* road : simulationFrame->getRoads()) {
//        painter->drawText(0, textYCoordination, QString("Vehicles in road %1: %2").arg(road->getRoadIndex()).arg(road->getVehiclesInRoad().size()));
//        textYCoordination += 17;
//    }

    painter->restore();

    //draw road, vehicles and traffic lights
    roadPen.setWidth(convertYCoordinate(Road::roadWidth));
    vehiclePen.setWidth(convertYCoordinate(Vehicle::vehicleWidth));

    for (Road* road : simulationFrame->getRoads()) {
        if (!road->isCurveRoad()) {
            int roadX1 = convertXCoordinate(road->getStartPoint().first);
            int roadY1 = convertYCoordinate(road->getStartPoint().second);
            int roadX2 = convertXCoordinate(road->getEndPoint().first);
            int roadY2 = convertYCoordinate(road->getEndPoint().second);

            if (!road->getIsInIntersection()) {
                painter->save();

                painter->setPen(roadPen);
                painter->setBrush(roadBrush);

                painter->drawLine(roadX1, roadY1, roadX2, roadY2);

                painter->restore();
            }

            painter->save();

            painter->setPen(vehiclePen);
            painter->setBrush(vehicleBrush);

            for (Vehicle* vehicle : road->getVehiclesInRoad()) {
                int vehicleX1 = convertXCoordinate(vehicle->getPosition() * road->getAngleCos() + road->getStartPoint().first);
                int vehicleY1 = convertYCoordinate(vehicle->getPosition() * road->getAngleSin() + road->getStartPoint().second);
                int vehicleX2 = convertXCoordinate((vehicle->getPosition() + vehicle->getVehicleLength()) * road->getAngleCos() + road->getStartPoint().first);
                int vehicleY2 = convertYCoordinate((vehicle->getPosition() + vehicle->getVehicleLength()) * road->getAngleSin() + road->getStartPoint().second);
                painter->drawLine(vehicleX1, vehicleY1, vehicleX2, vehicleY2);
            }

            painter->restore();

            if (road->hasTrafficLight()) {
                painter->save();

                if (road->getAttachedTrafficLight()->getCurrentState() ==
                        TrafficLight::TrafficLightState::Green) {
                      painter->setPen(greenLightPen);
                      painter->setBrush(greenLightBrush);
                } else {
                    painter->setPen(redLightPen);
                    painter->setBrush(redLightBrush);
                }

                painter->drawEllipse(roadX2, roadY2, 3, 3);
//                painter->drawPoint(roadX2, roadY2);

                painter->restore();
            }
        } else {
            CurveRoad* curveRoad = dynamic_cast<CurveRoad*>(road);

//            painter->save();

//            painter->setPen(roadPen);
//            painter->setBrush(roadBrush);

            int pointsNumber = curveRoad->getAllPointsInRoad().size();

            std::vector<QPoint> pointsInRoad;

            int i = 0;
            while (i < pointsNumber - 1) {
                pointsInRoad.push_back({convertXCoordinate(curveRoad->getAllPointsInRoad()[i].first),
                                        convertYCoordinate(curveRoad->getAllPointsInRoad()[i].second)});
                i++;
            }

//            painter->drawPolyline(pointsInRoad.data(), pointsNumber - 1);

//            painter->restore();

            painter->save();

            painter->setPen(vehiclePen);
            painter->setBrush(vehicleBrush);

            double tangentLineLength = curveRoad->getRoadLength() / (curveRoad->getAllPointsInRoad().size() - 1);

            for (Vehicle* vehicle : curveRoad->getVehiclesInRoad()) {
                double positionRatio = vehicle->getPosition() / curveRoad->getRoadLength();
                if (positionRatio >= 1.0) {
                    positionRatio = 0.99;
                }
                int sectionNumber = std::floor(positionRatio * (pointsNumber - 1));
                double deltaY = curveRoad->getAllPointsInRoad()[sectionNumber + 1].second - curveRoad->getAllPointsInRoad()[sectionNumber].second;
                double deltaX = curveRoad->getAllPointsInRoad()[sectionNumber + 1].first - curveRoad->getAllPointsInRoad()[sectionNumber].first;
//                double tangentLineLength = std::sqrt(
//                            std::pow(deltaY, 2.0)
//                            + std::pow(deltaX, 2.0));

                double tangentLineSin = deltaY / tangentLineLength;
                double tangentLineCos = deltaX / tangentLineLength;

//                QTextStream(stderr) << "deltaX: " << deltaX << " deltaY: " << deltaY << "\n";

                int vehicleEndPointX = convertXCoordinate(curveRoad->getAllPointsInRoad()[sectionNumber].first + vehicle->getVehicleLength() * tangentLineCos);
                int vehicleEndPointY = convertYCoordinate(curveRoad->getAllPointsInRoad()[sectionNumber].second + vehicle->getVehicleLength() * tangentLineSin);

//                if (vehicleEndPointX < 0 || vehicleEndPointY < 0) {
//                    QTextStream(stderr) << "vehicleEndX: " << vehicleEndPointX << " vehicleEndY: " << vehicleEndPointY << "\n";
//                    QTextStream(stderr) << curveRoad->getAllPointsInRoad()[sectionNumber].first << " " << tangentLineCos << "\n";
//                    QTextStream(stderr) << curveRoad->getAllPointsInRoad()[sectionNumber].second << " " << tangentLineSin << "\n";
//                }

                painter->drawLine(pointsInRoad[sectionNumber].x(), pointsInRoad[sectionNumber].y(), vehicleEndPointX, vehicleEndPointY);
            }

            painter->restore();

            if (curveRoad->hasTrafficLight()) {
                painter->save();

                if (curveRoad->getAttachedTrafficLight()->getCurrentState() ==
                        TrafficLight::TrafficLightState::Green) {
                      painter->setPen(greenLightPen);
                      painter->setBrush(greenLightBrush);
                } else {
                    painter->setPen(redLightPen);
                    painter->setBrush(redLightBrush);
                }

                painter->drawPoint(pointsInRoad[pointsNumber - 1].x(), pointsInRoad[pointsNumber - 1].y());

                painter->restore();
            }
        }
    }
//    QTextStream(stdout) << "Painting Time: " << ((double) (std::clock() - paintingTime) / CLOCKS_PER_SEC) * 1000 << "\n";
}

