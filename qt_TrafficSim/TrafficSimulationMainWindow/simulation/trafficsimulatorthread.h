#ifndef TRAFFICSIMULATORTHREAD_H
#define TRAFFICSIMULATORTHREAD_H

#include <QObject>
#include <QThread>
#include "trafficsimulator.h"

/**
 * @class TrafficSimulatorThread trafficsimulatorthread.h
 * @brief The simulator thread that creates simulation frames
 * 
 * @author YiCun Duan
 */
class TrafficSimulatorThread : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Construct a new simulator thread
     * 
     * @param trafficSimulator : the traffic simulator
     * @param parent : the parent class
     */
    explicit TrafficSimulatorThread(TrafficSimulator* trafficSimulator,
                                       QObject *parent = nullptr);

    /**
     * @brief Destroy the Traffic Simulator Thread object
     * 
     */
    ~TrafficSimulatorThread();

    /**
     * @brief Start the simulator thread
     * 
     * @param refreshTimeInterval : the refreash time interval
     */
    void start(int refreshTimeInterval);

private:
    QThread workerThread;
    TrafficSimulator* trafficSimulator;

signals:
    void operate(int refreshTimeInterval);
};

#endif // TRAFFICSIMULATORTHREAD_H
