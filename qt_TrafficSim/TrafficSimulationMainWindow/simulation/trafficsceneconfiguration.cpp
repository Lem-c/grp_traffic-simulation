#include "trafficsceneconfiguration.h"
#include "intersection.h"
#include <iostream>

const int TrafficSceneConfiguration::maxRandomRoutesNumberForOneEntry = 30;

TrafficSceneConfiguration::TrafficSceneConfiguration(int horizontalIntersectionNumber, int verticalIntersectionNumber)
    : horizontalIntersectionNumber(horizontalIntersectionNumber), verticalIntersectionNumber(verticalIntersectionNumber),
      intersections(), roadSettings(), routesMultiHashMap()
{
    if (horizontalIntersectionNumber <= 0 || verticalIntersectionNumber <= 0) {
        throw SimulationException("Horizontal Intersection Number / Vertical Intersection Number can't be <= 0.");
    }

    int i = 0;
    int j = 0;
    while (i < verticalIntersectionNumber) {
        while (j < horizontalIntersectionNumber) {
            intersections.append(new Intersection(i * horizontalIntersectionNumber + j, this));
            j++;
        }

        j = 0;
        i++;
    }

    for (Intersection* intersection : intersections) {
        Intersection* leftIntersection = nullptr;
        Intersection* rightIntersection = nullptr;
        Intersection* upIntersection = nullptr;
        Intersection* downIntersection = nullptr;

        if (intersection->getIntersectionNumber() % horizontalIntersectionNumber != 0) {
            leftIntersection = intersections[intersection->getIntersectionNumber() - 1];
        }

        if ((intersection->getIntersectionNumber() + 1) % horizontalIntersectionNumber != 0) {
            rightIntersection = intersections[intersection->getIntersectionNumber() + 1];
        }

        if (intersection->getIntersectionNumber() / horizontalIntersectionNumber != 0) {
            upIntersection = intersections[intersection->getIntersectionNumber() - horizontalIntersectionNumber];
        }

        if (intersection->getIntersectionNumber() / horizontalIntersectionNumber != verticalIntersectionNumber - 1) {
            downIntersection = intersections[intersection->getIntersectionNumber() + horizontalIntersectionNumber];
        }

        intersection->initIntersectionWith(leftIntersection, rightIntersection, upIntersection, downIntersection);
    }

    int k = 0;
    while (k < horizontalIntersectionNumber * verticalIntersectionNumber) {
        if (!intersections[k]->getLeftIntersection()) {
            int m = 0;
            while (m < maxRandomRoutesNumberForOneEntry) {
                this->addRoute(generateOneRandomRouteFrom(intersections[k], 3, false));
                this->addRoute(generateOneRandomRouteFrom(intersections[k], 3, true));
                m++;
            }
        }

        if (!intersections[k]->getRightIntersection()) {
            int m = 0;
            while (m < maxRandomRoutesNumberForOneEntry) {
                this->addRoute(generateOneRandomRouteFrom(intersections[k], 7, false));
                this->addRoute(generateOneRandomRouteFrom(intersections[k], 7, true));
                m++;
            }
        }

        if (!intersections[k]->getUpIntersection()) {
            int m = 0;
            while (m < maxRandomRoutesNumberForOneEntry) {
                this->addRoute(generateOneRandomRouteFrom(intersections[k], 1, false));
                this->addRoute(generateOneRandomRouteFrom(intersections[k], 1, true));
                m++;
            }
        }

        if (!intersections[k]->getDownIntersection()) {
            int m = 0;
            while (m < maxRandomRoutesNumberForOneEntry) {
                this->addRoute(generateOneRandomRouteFrom(intersections[k], 5, false));
                this->addRoute(generateOneRandomRouteFrom(intersections[k], 5, true));
                m++;
            }
        }

        k++;
    }
}

QList<unsigned int> TrafficSceneConfiguration::generateOneRandomRouteFrom(Intersection* intersection, int sequenceNumber, bool left) {
    if (sequenceNumber != 1 && sequenceNumber != 3 &&
            sequenceNumber != 5 && sequenceNumber != 7) {
        throw SimulationException("Invalid sequence number.");
    }

    if (!intersection) {
        throw SimulationException("Intersection pointer can't be null.");
    }

    Intersection* curIntersection = intersection;
    Intersection* preIntersection = curIntersection;
    int curSequenceNumber = sequenceNumber;
    bool curLeft = left;

    std::vector<unsigned int> route;
    std::vector<unsigned int> routeToBeAdded;

    while (curIntersection) {
        preIntersection = curIntersection;

        if (curSequenceNumber == 1) {
            switch (rand() % 3) {
            case 0:
                routeToBeAdded = curIntersection->getRouteFromTo(curSequenceNumber, curLeft, 2);
                route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());
                curIntersection = curIntersection->getLeftIntersection();
                curSequenceNumber = 7;
                curLeft = false;
                break;
            case 1:
                routeToBeAdded = curIntersection->getRouteFromTo(curSequenceNumber, curLeft, 4);
                route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());
                curIntersection = curIntersection->getDownIntersection();
                curSequenceNumber = 1;
                break;
            case 2:
                routeToBeAdded = curIntersection->getRouteFromTo(curSequenceNumber, curLeft, 6);
                route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());
                curIntersection = curIntersection->getRightIntersection();
                curSequenceNumber = 3;
                curLeft = true;
                break;
            }
        } else if (curSequenceNumber == 3) {
            switch (rand() % 3) {
            case 0:
                routeToBeAdded = curIntersection->getRouteFromTo(curSequenceNumber, curLeft, 0);
                route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());
                curIntersection = curIntersection->getUpIntersection();
                curSequenceNumber = 5;
                curLeft = true;
                break;
            case 1:
                routeToBeAdded = curIntersection->getRouteFromTo(curSequenceNumber, curLeft, 6);
                route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());
                curIntersection = curIntersection->getRightIntersection();
                curSequenceNumber = 3;
                break;
            case 2:
                routeToBeAdded = curIntersection->getRouteFromTo(curSequenceNumber, curLeft, 4);
                route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());
                curIntersection = curIntersection->getDownIntersection();
                curSequenceNumber = 1;
                curLeft = false;
                break;
            }
        } else if (curSequenceNumber == 5) {
            switch (rand() % 3) {
            case 0:
                routeToBeAdded = curIntersection->getRouteFromTo(curSequenceNumber, curLeft, 6);
                route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());
                curIntersection = curIntersection->getRightIntersection();
                curSequenceNumber = 3;
                curLeft = false;
                break;
            case 1:
                routeToBeAdded = curIntersection->getRouteFromTo(curSequenceNumber, curLeft, 0);
                route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());
                curIntersection = curIntersection->getUpIntersection();
                curSequenceNumber = 5;
                break;
            case 2:
                routeToBeAdded = curIntersection->getRouteFromTo(curSequenceNumber, curLeft, 2);
                route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());
                curIntersection = curIntersection->getLeftIntersection();
                curSequenceNumber = 7;
                curLeft = true;
                break;
            }
        } else if (curSequenceNumber == 7) {
            switch (rand() % 3) {
            case 0:
                routeToBeAdded = curIntersection->getRouteFromTo(curSequenceNumber, curLeft, 0);
                route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());
                curIntersection = curIntersection->getUpIntersection();
                curSequenceNumber = 5;
                curLeft = false;
                break;
            case 1:
                routeToBeAdded = curIntersection->getRouteFromTo(curSequenceNumber, curLeft, 2);
                route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());
                curIntersection = curIntersection->getLeftIntersection();
                curSequenceNumber = 7;
                break;
            case 2:
                routeToBeAdded = curIntersection->getRouteFromTo(curSequenceNumber, curLeft, 4);
                route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());
                curIntersection = curIntersection->getDownIntersection();
                curSequenceNumber = 1;
                curLeft = true;
                break;
            }
        }
    }

    int preSequenceNumber = -1;
    if (curSequenceNumber == 1) {
        preSequenceNumber = 4;
    } else if (curSequenceNumber == 3) {
        preSequenceNumber = 6;
    } else if (curSequenceNumber == 5) {
        preSequenceNumber = 0;
    } else if (curSequenceNumber == 7) {
        preSequenceNumber = 2;
    }

    routeToBeAdded = preIntersection->getRouteFromTo(preSequenceNumber, curLeft, preSequenceNumber);
    route.insert(route.end(), routeToBeAdded.begin(), routeToBeAdded.end());

//    for (unsigned int roadIndex : route) {
//        std::cerr << roadIndex << ", ";
//    }

//    std::cerr << "\n";

    return QList<unsigned int>(route.begin(), route.end());
}

std::set<unsigned int> TrafficSceneConfiguration::getConnectionRoadIndexes() const {
    std::set<unsigned int> connectionRoadIndexes;
    for (Intersection* intersection : intersections) {
        for (const RoadWrapper& roadWrapper : intersection->getConnectionRoads()) {
            connectionRoadIndexes.insert(roadWrapper.getRoadIndex());
        }
    }

    return connectionRoadIndexes;
}

void TrafficSceneConfiguration::changeRoadSettingBy(unsigned int roadIndex, RoadSettingIndex roadSettingIndex, const QVariant& newValue) {
    switch (roadSettingIndex) {
    case RoadSettingIndex::GreenDuration:
        std::get<RoadSettingIndex::GreenDuration>(*roadSettings[roadIndex]) = newValue.toInt();
        break;
    case RoadSettingIndex::RedDuration:
        std::get<RoadSettingIndex::RedDuration>(*roadSettings[roadIndex]) = newValue.toInt();
        break;
    case RoadSettingIndex::VehicleEnterRate:
        std::get<RoadSettingIndex::VehicleEnterRate>(*roadSettings[roadIndex]) = newValue.toInt();
        break;
    case RoadSettingIndex::StartPoint:
    case RoadSettingIndex::EndPoint:
    case RoadSettingIndex::HasTrafficLight:
    case RoadSettingIndex::HasVehicleEntry:
    case RoadSettingIndex::IsCurveRoad:
    case RoadSettingIndex::TurnDirection:
    case RoadSettingIndex::InitState:
    case RoadSettingIndex::InternalPointsNumber:
        throw SimulationException("Can't change some road setting attributes that are forbided to be changed.");
    }
}
