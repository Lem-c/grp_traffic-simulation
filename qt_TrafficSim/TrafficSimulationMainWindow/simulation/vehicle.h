#ifndef VEHICLE_H
#define VEHICLE_H

#include <QList>
#include "../utilities/simulationexception.h"
#include "road.h"

/**
 * @class Vehicle vehicle.h
 * @brief This class creates vehicles for the simulation
 * 
 * @author YiCun Duan
 */
class Vehicle {

private:
    /**
     * @brief The position of the vehicle
     * 
     */
    double position;// m
    /**
     * @brief The velocity of the vehicle
     * 
     */
    double velocity;// m/s 
    /**
     * @brief The acceleration of the vehicle
     * 
     */
    double acceleration;// m/s^2

    /**
     * @brief The current max velocity of the vehicle
     * 
     */
    double curMaxVelocity;
    /**
     * @brief The max velocity of the vehicle
     * 
     */
    double maxVelocity;
    /**
     * @brief The max acceleration of the vehicle
     * 
     */
    double maxAcceleration;

    /**
     * @brief The length of the vehicle
     * 
     */
    double vehicleLength;// m
    
    /**
     * @brief The react time of the vehicle when encountered another vehicle
     * 
     */
    int reactTime;// ms
    /**
     * @brief The expected distance maintained between two vehicles
     * 
     */
    double miniDesiredVehicleDistance;// m
    /**
     * @brief The comfortable deceleration of the vehicle
     * 
     */
    double comfortableDeceleration;// m/s^2

    /**
     * @brief If the vehicle is stopped
     * 
     */
    bool toBeStopped;

    /**
     * @brief The route of this vehicle
     * 
     */
    Road::Route route;

    /**
     * @brief The road the vehicle is currently on
     * 
     */
    Road* curRoad;

public:
    /**
     * @brief Width of the vehicle
     * 
     */
    static const double vehicleWidth;

    /**
     * @brief Creates a vehicle with designated route
     * 
     * @param route : the route the vehicle follows
     */
    explicit Vehicle(const Road::Route& route);

    /**
     * @brief Creates a new vehicle based on another vehicle
     * 
     * @param otherVehicle : the vehicle to be copied
     */
    Vehicle(const Vehicle& otherVehicle);

    /**
     * @brief Get the Position of the vehicle
     * 
     * @return position of the vehicle
     */
    inline double getPosition() const;
    /**
     * @brief Get the Velocity of the vehicle
     * 
     * @return velocity of the vehicle
     */
    inline double getVelocity() const;
    /**
     * @brief Get the Acceleration of the vehicle
     * 
     * @return acceleration of the vehicle
     */
    inline double getAcceleration() const;
    /**
     * @brief Get the current max velocity of the vehicle
     * 
     * @return current max velocity of the vehicle
     */
    inline double getCurMaxVelocity() const;
    /**
     * @brief Get the max velocity of the vehicle
     * 
     * @return max velocity of the vehicle
     */
    inline double getMaxVelocity() const;
    /**
     * @brief Get the max acceleration of the vehicle
     * 
     * @return max acceleration of the vehicle
     */
    inline double getMaxAcceleration() const;
    /**
     * @brief Get the expected distance maintained between two vehicles
     * 
     * @return the expected distance maintained between two vehicles
     */
    inline double getMiniDesiredVehicleDistance() const;
    /**
     * @brief Get the length of the vehicle
     * 
     * @return length of the vehicle
     */
    inline double getVehicleLength() const;

    /**
     * @brief Set the Position object
     * 
     * @param position : position of the vehicle
     */
    inline void setPosition(double position);
    /**
     * @brief Set the Velocity object
     * 
     * @param velocity : the velocity of the vehicle
     */
    inline void setVelocity(double velocity);
    /**
     * @brief Set the Acceleration object
     * 
     * @param acceleration : the acceleration of the vehicle
     */
    inline void setAcceleration(double acceleration);
    /**
     * @brief Set the current max velocity of the vehicle
     * 
     * @param curMaxVelocity : the current max velocity of the vehicle
     */
    inline void setCurMaxVelocity(double curMaxVelocity);
    /**
     * @brief Set the max velocity of the vehicle
     * 
     * @param maxVelocity : max velocity of the vehicle
     */
    inline void setMaxVelocity(double maxVelocity);
    /**
     * @brief Set the max acceleration of the vehicle
     * 
     * @param maxAcceleration : max acceleration of the vehicle
     */
    inline void setMaxAcceleration(double maxAcceleration);
    /**
     * @brief Set the length of the vehicle
     * 
     * @param vehicleLength : length of the vehicle
     */
    inline void setVehicleLength(double vehicleLength);

    /**
     * @brief Get the Route object
     * 
     * @return the route of the vehicle
     */
    inline const Road::Route& getRoute() const;
    /**
     * @brief Get the Cur Road object
     * 
     * @return current road
     */
    inline Road* getCurRoad();
    /**
     * @brief Whether there are next road the move
     * 
     * @return true 
     * @return false 
     */
    inline bool hasNextRoad() const;
    /**
     * @brief Get the Next Road object
     * 
     * @return the next road the vehicle moves to
     */
    inline Road* getNextRoad();
    /**
     * @brief Set the vehicle to the next road
     * 
     */
    inline void setCurRoadToNextRoad();

    /**
     * @brief recover the car after breaking
     * 
     */
    inline void recoverCurMaxVelocity();
    /**
     * @brief Set the car to stop
     * 
     * @param toBeStopped : whether the car should be stopped
     */
    inline void setToBeStopped(bool toBeStopped = true);    
    
    /**
     * @brief Updates the behavior of the car
     * 
     * @param frontVehicle : the vehicle infront
     * @param deltaTimeInMS : the delta time of the simulation
     */
    void update(const Vehicle* frontVehicle, int deltaTimeInMS);
};

double Vehicle::getPosition() const {
    return position;
}

double Vehicle::getVelocity() const {
    return velocity;
}

double Vehicle::getAcceleration() const {
    return acceleration;
}

double Vehicle::getCurMaxVelocity() const {
    return curMaxVelocity;
}

double Vehicle::getMaxVelocity() const {
    return maxVelocity;
}

double Vehicle::getMaxAcceleration() const {
    return maxAcceleration;
}

double Vehicle::getVehicleLength() const {
    return vehicleLength;
}

const Road::Route& Vehicle::getRoute() const {
    return route;
}

Road* Vehicle::getCurRoad() {
    return curRoad;
}

double Vehicle::getMiniDesiredVehicleDistance() const {
    return miniDesiredVehicleDistance;
}

void Vehicle::setPosition(double position) {
    this->position = position;
}

void Vehicle::setVelocity(double velocity) {
    this->velocity = velocity;
}

void Vehicle::setAcceleration(double acceleration) {
    this->acceleration = acceleration;
}

void Vehicle::setCurMaxVelocity(double curMaxVelocity) {
    this->curMaxVelocity = curMaxVelocity;
}

void Vehicle::setMaxVelocity(double maxVelocity) {
    this->maxVelocity = maxVelocity;
}

void Vehicle::setMaxAcceleration(double maxAcceleration) {
    this->maxAcceleration = maxAcceleration;
}

void Vehicle::setVehicleLength(double vehicleLength) {
    this->vehicleLength = vehicleLength;
}

bool Vehicle::hasNextRoad() const {
    if (!curRoad) {
        throw SimulationException("curRoad is nullptr.");
    }

    if (route.indexOf(curRoad) == route.size() - 1) {
        return false;
    }

    return true;
}

Road* Vehicle::getNextRoad() {
    if (!hasNextRoad()) {
        throw SimulationException("Vehicle doesn't have next road.");
    }

    return route.at(route.indexOf(curRoad) + 1);
}

void Vehicle::setCurRoadToNextRoad() {
    if (!hasNextRoad()) {
        throw SimulationException("Vehicle doesn't have next road.");
    }

    curRoad = route.at(route.indexOf(curRoad) + 1);
}

void Vehicle::recoverCurMaxVelocity() {
    curMaxVelocity = maxVelocity;
}

void Vehicle::setToBeStopped(bool toBeStopped) {
    this->toBeStopped = toBeStopped;
}

#endif //VEHICLE_H
