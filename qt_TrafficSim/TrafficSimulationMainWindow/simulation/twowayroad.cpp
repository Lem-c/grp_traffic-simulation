#include "twowayroad.h"
#include <cmath>

const double TwowayRoad::roadWideDistance = 50.0;

const int TwowayRoad::leftRoadStartSegmentIndex = 0;
const int TwowayRoad::leftRoadInternSegmentIndex = 1;
const int TwowayRoad::leftRoadInternTurnSegmentIndex = 6;
const int TwowayRoad::leftRoadEndSegmentIndex = 2;

const int TwowayRoad::rightRoadStartSegmentIndex = 3;
const int TwowayRoad::rightRoadInternSegmentIndex = 4;
const int TwowayRoad::rightRoadInternTurnSegmentIndex = 7;
const int TwowayRoad::rightRoadEndSegmentIndex = 5;

TwowayRoad::TwowayRoad(Road::Point2D&& startPoint, Road::Point2D&& endPoint,
                       DefaultRoadSettingConfiguration configuration, TrafficSceneConfiguration* parent)
    : parent(parent), roads()
{
    double deltaX = endPoint.first - startPoint.first;
    double deltaY = endPoint.second - startPoint.second;

    double roadLength = std::sqrt(deltaX * deltaX + deltaY * deltaY);

    double cosTheta = deltaX / std::sqrt(deltaX * deltaX + deltaY * deltaY);
    double sinTheta = deltaY / std::sqrt(deltaX * deltaX + deltaY * deltaY);

    double vectorX = -sinTheta;
    double vectorY = cosTheta;

    Road::Point2D leftRoadStartPoint(startPoint.first - roadWideDistance * vectorX, startPoint.second - roadWideDistance * vectorY);
    Road::Point2D leftRoadInterPoint0(leftRoadStartPoint.first + roadLength * 0.25 * cosTheta, leftRoadStartPoint.second + roadLength * 0.25 * sinTheta);
    Road::Point2D leftRoadInterPoint1(leftRoadStartPoint.first + roadLength * 0.75 * cosTheta, leftRoadStartPoint.second + roadLength * 0.75 * sinTheta);
    Road::Point2D leftRoadEndPoint(endPoint.first - roadWideDistance * vectorX, endPoint.second - roadWideDistance * vectorY);

    Road::Point2D rightRoadStartPoint(startPoint.first + roadWideDistance * vectorX, startPoint.second + roadWideDistance * vectorY);
    Road::Point2D rightRoadInterPoint0(rightRoadStartPoint.first + roadLength * 0.375 * cosTheta, rightRoadStartPoint.second + roadLength * 0.375 * sinTheta);
    Road::Point2D rightRoadInterPoint1(rightRoadStartPoint.first + roadLength * 0.625 * cosTheta, rightRoadStartPoint.second + roadLength * 0.625 * sinTheta);
    Road::Point2D rightRoadEndPoint(endPoint.first + roadWideDistance * vectorX, endPoint.second + roadWideDistance * vectorY);

    TrafficSceneConfiguration::RoadSetting* tempRoadSetting = nullptr;
    switch(configuration) {
        case DefaultRoadSettingConfiguration::GreenLightWithEntry:
            tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadStartPoint, leftRoadInterPoint0,
                                                                          false, -1, -1,  TrafficLight::TrafficLightState::Green,
                                                                          true, 100,
                                                                          false, CurveRoad::TurnDirection::Left, -1);
            roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

            tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint0, leftRoadInterPoint1,
                                                                        false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                        false, -1,
                                                                        false, CurveRoad::TurnDirection::Left, -1);
            roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

            tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint1, leftRoadEndPoint,
                                                                        true, 30000, 30000, TrafficLight::TrafficLightState::Green,
                                                                        false, -1,
                                                                        false, CurveRoad::TurnDirection::Left, -1);
            roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

            tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadStartPoint, rightRoadInterPoint0,
                                                                        false, -1, -1,  TrafficLight::TrafficLightState::Green,
                                                                        true, 100,
                                                                        false, CurveRoad::TurnDirection::Left, -1);
            roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

            tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint0, rightRoadInterPoint1,
                                                                        false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                        false, -1,
                                                                        false, CurveRoad::TurnDirection::Left, -1);
            roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

            tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint1, rightRoadEndPoint,
                                                                        true, 30000, 30000, TrafficLight::TrafficLightState::Green,
                                                                        false, -1,
                                                                        false, CurveRoad::TurnDirection::Left, -1);
            roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

            tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint0, rightRoadInterPoint0,
                                                                        false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                        false, -1,
                                                                        true, CurveRoad::TurnDirection::Left, 100);
            roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

            tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint1, leftRoadInterPoint1,
                                                                        false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                        false, -1,
                                                                        true, CurveRoad::TurnDirection::Right, 100);
            roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

            break;

    case DefaultRoadSettingConfiguration::GreenLightWithoutEntry:
        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadStartPoint, leftRoadInterPoint0,
                                                                     false, -1, -1,  TrafficLight::TrafficLightState::Green,
                                                                     false, -1,
                                                                     false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint0, leftRoadInterPoint1,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint1, leftRoadEndPoint,
                                                                    true, 30000, 30000, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadStartPoint, rightRoadInterPoint0,
                                                                    false, -1, -1,  TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint0, rightRoadInterPoint1,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint1, rightRoadEndPoint,
                                                                    true, 30000, 30000, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint0, rightRoadInterPoint0,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    true, CurveRoad::TurnDirection::Left, 100);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint1, leftRoadInterPoint1,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    true, CurveRoad::TurnDirection::Right, 100);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        break;

    case DefaultRoadSettingConfiguration::RedLightWithEntry:
        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadStartPoint, leftRoadInterPoint0,
                                                                     false, -1, -1,  TrafficLight::TrafficLightState::Green,
                                                                     true, 100,
                                                                     false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint0, leftRoadInterPoint1,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint1, leftRoadEndPoint,
                                                                    true, 30000, 30000, TrafficLight::TrafficLightState::Red,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadStartPoint, rightRoadInterPoint0,
                                                                    false, -1, -1,  TrafficLight::TrafficLightState::Green,
                                                                    true, 100,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint0, rightRoadInterPoint1,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint1, rightRoadEndPoint,
                                                                    true, 30000, 30000, TrafficLight::TrafficLightState::Red,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint0, rightRoadInterPoint0,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    true, CurveRoad::TurnDirection::Left, 100);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint1, leftRoadInterPoint1,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    true, CurveRoad::TurnDirection::Right, 100);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        break;
    case DefaultRoadSettingConfiguration::RedLightWithoutEntry:
        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadStartPoint, leftRoadInterPoint0,
                                                                     false, -1, -1,  TrafficLight::TrafficLightState::Green,
                                                                     false, -1,
                                                                     false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint0, leftRoadInterPoint1,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint1, leftRoadEndPoint,
                                                                    true, 30000, 30000, TrafficLight::TrafficLightState::Red,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadStartPoint, rightRoadInterPoint0,
                                                                    false, -1, -1,  TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint0, rightRoadInterPoint1,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint1, rightRoadEndPoint,
                                                                    true, 30000, 30000, TrafficLight::TrafficLightState::Red,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint0, rightRoadInterPoint0,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    true, CurveRoad::TurnDirection::Left, 100);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint1, leftRoadInterPoint1,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    true, CurveRoad::TurnDirection::Right, 100);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        break;
    case DefaultRoadSettingConfiguration::WithoutTrafficLightWithoutEntry:
        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadStartPoint, leftRoadInterPoint0,
                                                                     false, -1, -1,  TrafficLight::TrafficLightState::Green,
                                                                     false, -1,
                                                                     false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint0, leftRoadInterPoint1,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint1, leftRoadEndPoint,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadStartPoint, rightRoadInterPoint0,
                                                                    false, -1, -1,  TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint0, rightRoadInterPoint1,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint1, rightRoadEndPoint,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    false, CurveRoad::TurnDirection::Left, -1);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(leftRoadInterPoint0, rightRoadInterPoint0,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    true, CurveRoad::TurnDirection::Left, 100);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(rightRoadInterPoint1, leftRoadInterPoint1,
                                                                    false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                    false, -1,
                                                                    true, CurveRoad::TurnDirection::Right, 100);
        roads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

        break;
    }
}

std::vector<unsigned int> TwowayRoad::getRouteFromTo(bool startLeft, bool endLeft) const {
    std::vector<unsigned int> route;

    if (startLeft) {
        route.push_back(roads[leftRoadStartSegmentIndex].getRoadIndex());
        if (endLeft) {
            route.push_back(roads[leftRoadInternSegmentIndex].getRoadIndex());
            route.push_back(roads[leftRoadEndSegmentIndex].getRoadIndex());
        } else {
            route.push_back(roads[leftRoadInternTurnSegmentIndex].getRoadIndex());
            route.push_back(roads[rightRoadInternSegmentIndex].getRoadIndex());
            route.push_back(roads[rightRoadEndSegmentIndex].getRoadIndex());
        }
    } else {
        route.push_back(roads[rightRoadStartSegmentIndex].getRoadIndex());
        if (endLeft) {
            route.push_back(roads[rightRoadInternSegmentIndex].getRoadIndex());
            route.push_back(roads[rightRoadInternTurnSegmentIndex].getRoadIndex());
            route.push_back(roads[leftRoadEndSegmentIndex].getRoadIndex());
        } else {
            route.push_back(roads[rightRoadInternSegmentIndex].getRoadIndex());
            route.push_back(roads[rightRoadEndSegmentIndex].getRoadIndex());
        }
    }

    return route;
}
