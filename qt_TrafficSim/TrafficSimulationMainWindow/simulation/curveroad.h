#ifndef CURVEROAD_H
#define CURVEROAD_H

#include "road.h"

/**
 * @class CurveRoad curveroad.h
 * @brief This class creates a curve road on the scene
 * 
 * @author YiCun Duan
 */
class CurveRoad : public Road
{
public:
    /**
     * @brief Enum, of the turn direction
     * 
     */
    enum TurnDirection {
        Left, /** @brief enum value Left */
        Right /** @brief enum value Right */
    };

    /**
     * @brief Creates a curve road on the scene
     * 
     * @param[in] startPoint : the start point of the curve road
     * @param[in] endPoint : the start point of the curve road
     * @param[in] turnDirction : the turn direction of the curve road
     * @param[in] internalPointsNumber : the number of internal points
     */
    explicit CurveRoad(const Road::Point2D& startPoint, const Road::Point2D& endPoint,
                       TurnDirection turnDirction, int internalPointsNumber = 50);

    /**
     * @brief Construct a new Curve Road base on another curve road
     * 
     * @param[in] otherCurveRoad : the other curve road object
     */
    CurveRoad(const CurveRoad& otherCurveRoad);
    
    /**
     * @brief Return whether this object is a curve road or not
     * 
     * @return true 
     * @return false 
     */
    virtual bool isCurveRoad() override;

    /**
     * @brief Get the turn direction of the curve road
     * 
     * @return TurnDirection 
     */
    inline TurnDirection getTurnDirection() const;

    /**
     * @brief Get the All Points In Road object
     * 
     * @return const QList<Point2D>& 
     */
    inline const QList<Point2D>& getAllPointsInRoad() const;

protected:
    TurnDirection turnDirection;
    QList<Point2D> allPointsInRoad;
};

CurveRoad::TurnDirection CurveRoad::getTurnDirection() const {
    return turnDirection;
}

const QList<CurveRoad::Point2D>& CurveRoad::getAllPointsInRoad() const {
    return allPointsInRoad;
}

#endif // CURVEROAD_H
