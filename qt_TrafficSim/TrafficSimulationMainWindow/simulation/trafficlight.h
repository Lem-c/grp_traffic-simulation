#ifndef TRAFFICLIGHT_H
#define TRAFFICLIGHT_H

#include <QObject>

/**
 * @class TrafficLight traffilight.h
 * @brief This class creates a road on the scene
 * 
 * @author YiCun Duan
 */
class TrafficLight
{
public:
    /**
     * @brief Enum of the state of the traffic light
     * 
     */
    enum TrafficLightState {
        Red, /** @brief enum value Red */
        Green /** @brief enum value Green */
    };

    /**
     * @brief Construct a new Traffic Light object
     * 
     * @param greenDuration : the duration of the green light
     * @param redDuration : the duration of the red light
     * @param initState : the initial state of the traffic light
     */
    explicit TrafficLight(int greenDuration, int redDuration,
                          TrafficLightState initState);

    /**
     * @brief update the cycle time of the traffic light
     * 
     * @param totalSimulationTime 
     */
    void update(unsigned long long totalSimulationTime); // ms

    /**
     * @brief Get the current state of the traffic light
     * 
     * @return state of the traffic light
     */
    inline TrafficLightState getCurrentState() const;

private:
    int greenDuration;// ms
    int redDuration;// ms
    TrafficLightState initState;
    TrafficLightState curState;
};

TrafficLight::TrafficLightState TrafficLight::getCurrentState() const {
    return curState;
}

#endif // TRAFFICLIGHT_H
