#include "intersection.h"
#include <iostream>

const double Intersection::streetBlockSize = 1000;
const double Intersection::intersectionPaddingDistance = 200;
const double Intersection::roadPaddingDistance = 100;

Intersection::Intersection(int intersectionNumber, TrafficSceneConfiguration* parent)
    : intersectionNumber(intersectionNumber), parent(parent),
      leftIntersection(nullptr), rightIntersection(nullptr),
      upIntersection(nullptr), downIntersection(nullptr),
      sequence0Road(nullptr), sequence1Road(nullptr),
      sequence2Road(nullptr), sequence3Road(nullptr),
      sequence4Road(nullptr), sequence5Road(nullptr),
      sequence6Road(nullptr), sequence7Road(nullptr),
      connectionRoads()
{

}

void Intersection::initIntersectionWith(Intersection *leftIntersection, Intersection *rightIntersection,
                                        Intersection *upIntersection, Intersection *downIntersection) {
    setLeftIntersection(leftIntersection);
    setRightIntersection(rightIntersection);
    setUpIntersection(upIntersection);
    setDownIntersection(downIntersection);

    initConnectionRoads();
}

void Intersection::setLeftIntersection(Intersection* intersection) {
    leftIntersection = intersection;

    if (intersection) {
        if (intersection->getTwowayRoadBy(7)) {
            sequence2Road = intersection->getTwowayRoadBy(7);
        } else {
            std::pair<Road::Point2D, Road::Point2D> road2Points(calculatePointsFor(2));
            sequence2Road = std::make_shared<TwowayRoad>(std::move(road2Points.first), std::move(road2Points.second),
                                                         TwowayRoad::DefaultRoadSettingConfiguration::GreenLightWithoutEntry, parent);
        }

        if (intersection->getTwowayRoadBy(6)) {
            sequence3Road = intersection->getTwowayRoadBy(6);
        } else {
            std::pair<Road::Point2D, Road::Point2D> road3Points(calculatePointsFor(3));
            sequence3Road = std::make_shared<TwowayRoad>(std::move(road3Points.first), std::move(road3Points.second),
                                                         TwowayRoad::DefaultRoadSettingConfiguration::GreenLightWithoutEntry, parent);
        }
    } else { // intersection == nullptr
        std::pair<Road::Point2D, Road::Point2D> road2Points(calculatePointsFor(2));
        sequence2Road = std::make_shared<TwowayRoad>(std::move(road2Points.first), std::move(road2Points.second),
                                                     TwowayRoad::DefaultRoadSettingConfiguration::WithoutTrafficLightWithoutEntry, parent);
        std::pair<Road::Point2D, Road::Point2D> road3Points(calculatePointsFor(3));
        sequence3Road = std::make_shared<TwowayRoad>(std::move(road3Points.first), std::move(road3Points.second),
                                                     TwowayRoad::DefaultRoadSettingConfiguration::GreenLightWithEntry, parent);
    }
}

void Intersection::setRightIntersection(Intersection* intersection) {
    rightIntersection = intersection;

    if (intersection) {
        if (intersection->getTwowayRoadBy(3)) {
            sequence6Road = intersection->getTwowayRoadBy(3);
        } else {
            std::pair<Road::Point2D, Road::Point2D> road6Points(calculatePointsFor(6));
            sequence6Road = std::make_shared<TwowayRoad>(std::move(road6Points.first), std::move(road6Points.second),
                                                         TwowayRoad::DefaultRoadSettingConfiguration::GreenLightWithoutEntry, parent);
        }

        if (intersection->getTwowayRoadBy(2)) {
            sequence7Road = intersection->getTwowayRoadBy(2);
        } else {
            std::pair<Road::Point2D, Road::Point2D> road7Points(calculatePointsFor(7));
            sequence7Road = std::make_shared<TwowayRoad>(std::move(road7Points.first), std::move(road7Points.second),
                                                         TwowayRoad::DefaultRoadSettingConfiguration::GreenLightWithoutEntry, parent);
        }
    } else {
        std::pair<Road::Point2D, Road::Point2D> road6Points(calculatePointsFor(6));
        sequence6Road = std::make_shared<TwowayRoad>(std::move(road6Points.first), std::move(road6Points.second),
                                                     TwowayRoad::DefaultRoadSettingConfiguration::WithoutTrafficLightWithoutEntry, parent);
        std::pair<Road::Point2D, Road::Point2D> road7Points(calculatePointsFor(7));
        sequence7Road = std::make_shared<TwowayRoad>(std::move(road7Points.first), std::move(road7Points.second),
                                                     TwowayRoad::DefaultRoadSettingConfiguration::GreenLightWithEntry, parent);
    }
}

void Intersection::setUpIntersection(Intersection* intersection) {
    upIntersection = intersection;

    if (intersection) {
        if (intersection->getTwowayRoadBy(5)) {
            sequence0Road = intersection->getTwowayRoadBy(5);
        } else {
            std::pair<Road::Point2D, Road::Point2D> road0Points(calculatePointsFor(0));
            sequence0Road = std::make_shared<TwowayRoad>(std::move(road0Points.first), std::move(road0Points).second,
                                                         TwowayRoad::DefaultRoadSettingConfiguration::RedLightWithoutEntry, parent);
        }

        if (intersection->getTwowayRoadBy(4)) {
            sequence1Road = intersection->getTwowayRoadBy(4);
        } else {
            std::pair<Road::Point2D, Road::Point2D> road1Points(calculatePointsFor(1));
            sequence1Road = std::make_shared<TwowayRoad>(std::move(road1Points.first), std::move(road1Points.second),
                                                         TwowayRoad::DefaultRoadSettingConfiguration::RedLightWithoutEntry, parent);
        }
    } else {
        std::pair<Road::Point2D, Road::Point2D> road0Points(calculatePointsFor(0));
        sequence0Road = std::make_shared<TwowayRoad>(std::move(road0Points.first), std::move(road0Points).second,
                                                     TwowayRoad::DefaultRoadSettingConfiguration::WithoutTrafficLightWithoutEntry, parent);
        std::pair<Road::Point2D, Road::Point2D> road1Points(calculatePointsFor(1));
        sequence1Road = std::make_shared<TwowayRoad>(std::move(road1Points.first), std::move(road1Points.second),
                                                     TwowayRoad::DefaultRoadSettingConfiguration::RedLightWithEntry, parent);
    }
}

void Intersection::setDownIntersection(Intersection* intersection) {
    downIntersection = intersection;

    if (intersection) {
        if (intersection->getTwowayRoadBy(1)) {
            sequence4Road = intersection->getTwowayRoadBy(1);
        } else {
            std::pair<Road::Point2D, Road::Point2D> road4Points(calculatePointsFor(4));
            sequence4Road = std::make_shared<TwowayRoad>(std::move(road4Points.first), std::move(road4Points.second),
                                                         TwowayRoad::DefaultRoadSettingConfiguration::RedLightWithoutEntry, parent);
        }

        if (intersection->getTwowayRoadBy(0)) {
            sequence5Road = intersection->getTwowayRoadBy(0);
        } else {
            std::pair<Road::Point2D, Road::Point2D> road5Points(calculatePointsFor(5));
            sequence5Road = std::make_shared<TwowayRoad>(std::move(road5Points.first), std::move(road5Points.second),
                                                         TwowayRoad::DefaultRoadSettingConfiguration::RedLightWithoutEntry, parent);
        }
    } else {
        std::pair<Road::Point2D, Road::Point2D> road4Points(calculatePointsFor(4));
        sequence4Road = std::make_shared<TwowayRoad>(std::move(road4Points.first), std::move(road4Points.second),
                                                     TwowayRoad::DefaultRoadSettingConfiguration::WithoutTrafficLightWithoutEntry, parent);
        std::pair<Road::Point2D, Road::Point2D> road5Points(calculatePointsFor(5));
        sequence5Road = std::make_shared<TwowayRoad>(std::move(road5Points.first), std::move(road5Points.second),
                                                     TwowayRoad::DefaultRoadSettingConfiguration::RedLightWithEntry, parent);
    }
}

void Intersection::initConnectionRoads() {
    //for sequence1Road
    TrafficSceneConfiguration::RoadSetting* tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence1Road->getLeftRoadEndPoint(), sequence6Road->getLeftRoadStartPoint(),
                                                                                                         false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                                                         false, -1,
                                                                                                         true, CurveRoad::TurnDirection::Left, 100);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence1Road->getLeftRoadEndPoint(), sequence4Road->getLeftRoadStartPoint(),
                                                                 false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                 false, -1,
                                                                 false, CurveRoad::TurnDirection::Left, -1);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence1Road->getRightRoadEndPoint(), sequence4Road->getRightRoadStartPoint(),
                                                                 false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                 false, -1,
                                                                 false, CurveRoad::TurnDirection::Left, -1);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence1Road->getRightRoadEndPoint(), sequence2Road->getRightRoadStartPoint(),
                                                                 false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                 false, -1,
                                                                 true, CurveRoad::TurnDirection::Right, 100);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    //for sequence3Road
    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence3Road->getLeftRoadEndPoint(), sequence0Road->getLeftRoadStartPoint(),
                                                                                                             false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                                                             false, -1,
                                                                                                             true, CurveRoad::TurnDirection::Left, 100);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence3Road->getLeftRoadEndPoint(), sequence6Road->getLeftRoadStartPoint(),
                                                                 false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                 false, -1,
                                                                 false, CurveRoad::TurnDirection::Left, -1);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence3Road->getRightRoadEndPoint(), sequence6Road->getRightRoadStartPoint(),
                                                                 false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                 false, -1,
                                                                 false, CurveRoad::TurnDirection::Left, -1);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence3Road->getRightRoadEndPoint(), sequence4Road->getRightRoadStartPoint(),
                                                                 false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                 false, -1,
                                                                 true, CurveRoad::TurnDirection::Right, 100);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    //for sequence5Road
    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence5Road->getLeftRoadEndPoint(), sequence2Road->getLeftRoadStartPoint(),
                                                                                                             false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                                                             false, -1,
                                                                                                             true, CurveRoad::TurnDirection::Left, 100);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence5Road->getLeftRoadEndPoint(), sequence0Road->getLeftRoadStartPoint(),
                                                                 false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                 false, -1,
                                                                 false, CurveRoad::TurnDirection::Left, -1);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence5Road->getRightRoadEndPoint(), sequence0Road->getRightRoadStartPoint(),
                                                                 false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                 false, -1,
                                                                 false, CurveRoad::TurnDirection::Left, -1);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence5Road->getRightRoadEndPoint(), sequence6Road->getRightRoadStartPoint(),
                                                                 false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                 false, -1,
                                                                 true, CurveRoad::TurnDirection::Right, 100);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    //for sequence7Road
    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence7Road->getLeftRoadEndPoint(), sequence4Road->getLeftRoadStartPoint(),
                                                                                                             false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                                                             false, -1,
                                                                                                             true, CurveRoad::TurnDirection::Left, 100);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence7Road->getLeftRoadEndPoint(), sequence2Road->getLeftRoadStartPoint(),
                                                                 false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                 false, -1,
                                                                 false, CurveRoad::TurnDirection::Left, -1);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence7Road->getRightRoadEndPoint(), sequence2Road->getRightRoadStartPoint(),
                                                                 false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                 false, -1,
                                                                 false, CurveRoad::TurnDirection::Left, -1);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));

    tempRoadSetting = new TrafficSceneConfiguration::RoadSetting(sequence7Road->getRightRoadEndPoint(), sequence0Road->getRightRoadStartPoint(),
                                                                 false, -1, -1, TrafficLight::TrafficLightState::Green,
                                                                 false, -1,
                                                                 true, CurveRoad::TurnDirection::Right, 100);
    connectionRoads.append(RoadWrapper(tempRoadSetting, parent->addRoadSetting(tempRoadSetting)));
}

std::pair<Road::Point2D, Road::Point2D> Intersection::calculatePointsFor(int sequenceNumber) {
    double centralPointX = (intersectionNumber % parent->getHorizontalIntersectionNumber() + 1) * streetBlockSize;
    double centralPointY = (intersectionNumber / parent->getHorizontalIntersectionNumber() + 1) * streetBlockSize;

    double roadLength = streetBlockSize - intersectionPaddingDistance;
    double internRoadLength = streetBlockSize - 2 * intersectionPaddingDistance;

    Road::Point2D startPoint;
    Road::Point2D endPoint;

    if (sequenceNumber == 0) {
        if (!upIntersection) {
            startPoint = {centralPointX + roadPaddingDistance, centralPointY - intersectionPaddingDistance};
            endPoint = {centralPointX + roadPaddingDistance, centralPointY - intersectionPaddingDistance - roadLength};
        } else {
            startPoint = {centralPointX + roadPaddingDistance, centralPointY - intersectionPaddingDistance};
            endPoint = {centralPointX + roadPaddingDistance, centralPointY - intersectionPaddingDistance - internRoadLength};
        }
    } else if (sequenceNumber == 1) {
        if (!upIntersection) {
            endPoint = {centralPointX - roadPaddingDistance, centralPointY - intersectionPaddingDistance};
            startPoint = {centralPointX - roadPaddingDistance, centralPointY - intersectionPaddingDistance - roadLength};
        } else {
            endPoint = {centralPointX - roadPaddingDistance, centralPointY - intersectionPaddingDistance};
            startPoint = {centralPointX - roadPaddingDistance, centralPointY - intersectionPaddingDistance - internRoadLength};
        }
    } else if (sequenceNumber == 2) {
        if (!leftIntersection) {
            startPoint = {centralPointX - intersectionPaddingDistance, centralPointY - roadPaddingDistance};
            endPoint = {centralPointX - intersectionPaddingDistance - roadLength, centralPointY - roadPaddingDistance};
        } else {
            startPoint = {centralPointX - intersectionPaddingDistance, centralPointY - roadPaddingDistance};
            endPoint = {centralPointX - intersectionPaddingDistance - internRoadLength, centralPointY - roadPaddingDistance};
        }
    } else if (sequenceNumber == 3) {
        if (!leftIntersection) {
            endPoint = {centralPointX - intersectionPaddingDistance, centralPointY + roadPaddingDistance};
            startPoint = {centralPointX - intersectionPaddingDistance - roadLength, centralPointY + roadPaddingDistance};
        } else {
            endPoint = {centralPointX - intersectionPaddingDistance, centralPointY + roadPaddingDistance};
            startPoint = {centralPointX - intersectionPaddingDistance - internRoadLength, centralPointY + roadPaddingDistance};
        }
    } else if (sequenceNumber == 4) {
        if (!downIntersection) {
            startPoint = {centralPointX - roadPaddingDistance, centralPointY + intersectionPaddingDistance};
            endPoint = {centralPointX - roadPaddingDistance, centralPointY + intersectionPaddingDistance + roadLength};
        } else {
            startPoint = {centralPointX - roadPaddingDistance, centralPointY + intersectionPaddingDistance};
            endPoint = {centralPointX - roadPaddingDistance, centralPointY + intersectionPaddingDistance + internRoadLength};
        }
    } else if (sequenceNumber == 5) {
        if (!downIntersection) {
            endPoint = {centralPointX + roadPaddingDistance, centralPointY + intersectionPaddingDistance};
            startPoint = {centralPointX + roadPaddingDistance, centralPointY + intersectionPaddingDistance + roadLength};
        } else {
            endPoint = {centralPointX + roadPaddingDistance, centralPointY + intersectionPaddingDistance};
            startPoint = {centralPointX + roadPaddingDistance, centralPointY + intersectionPaddingDistance + internRoadLength};
        }
    } else if (sequenceNumber == 6) {
        if (!rightIntersection) {
            startPoint = {centralPointX + intersectionPaddingDistance, centralPointY + roadPaddingDistance};
            endPoint = {centralPointX + intersectionPaddingDistance + roadLength, centralPointY + roadPaddingDistance};
        } else {
            startPoint = {centralPointX + intersectionPaddingDistance, centralPointY + roadPaddingDistance};
            endPoint = {centralPointX + intersectionPaddingDistance + internRoadLength, centralPointY + roadPaddingDistance};
        }
    } else if (sequenceNumber == 7) {
        if (!rightIntersection) {
            endPoint = {centralPointX + intersectionPaddingDistance, centralPointY - roadPaddingDistance};
            startPoint = {centralPointX + intersectionPaddingDistance + roadLength, centralPointY - roadPaddingDistance};
        } else {
            endPoint = {centralPointX + intersectionPaddingDistance, centralPointY - roadPaddingDistance};
            startPoint = {centralPointX + intersectionPaddingDistance + internRoadLength, centralPointY - roadPaddingDistance};
        }
    } else {
        throw SimulationException("Invalid sequence number.");
    }

    return std::pair<Road::Point2D, Road::Point2D>(startPoint, endPoint);
}

std::vector<unsigned int> Intersection::getRouteFromTo(int srcSequenceNumber, bool left, int targetSequenceNumber) {
    if (srcSequenceNumber == targetSequenceNumber) {
        if (srcSequenceNumber >= 0 && srcSequenceNumber <= 7) {
            return this->getTwowayRoadBy(srcSequenceNumber)->getRouteFromTo(left, left);
        } else {
            throw SimulationException("Invalid sequence number.");
        }
    } else {
        if (srcSequenceNumber == 1 &&
                (targetSequenceNumber == 2 || targetSequenceNumber == 4 || targetSequenceNumber == 6)) {
            if (targetSequenceNumber == 2) {
                std::vector<unsigned int> preRoute(this->getTwowayRoadBy(1)->getRouteFromTo(left, false));
                preRoute.push_back(connectionRoads[Seq1RoadTurnRight].getRoadIndex());
                return preRoute;
            } else if (targetSequenceNumber == 4) {
                std::vector<unsigned int> preRoute(this->getTwowayRoadBy(1)->getRouteFromTo(left, left));
                if (left) {
                    preRoute.push_back(connectionRoads[Seq1RoadLeftGoStraight].getRoadIndex());
                } else {
                    preRoute.push_back(connectionRoads[Seq1RoadRightGoStraight].getRoadIndex());
                }
                return preRoute;
            } else if (targetSequenceNumber == 6) {
                std::vector<unsigned int> preRoute(this->getTwowayRoadBy(1)->getRouteFromTo(left, true));
                preRoute.push_back(connectionRoads[Seq1RoadTurnLeft].getRoadIndex());
                return preRoute;
            }
        } else if (srcSequenceNumber == 3 &&
                   (targetSequenceNumber == 0 || targetSequenceNumber == 4 || targetSequenceNumber == 6)) {
            if (targetSequenceNumber == 4) {
                std::vector<unsigned int> preRoute(this->getTwowayRoadBy(3)->getRouteFromTo(left, false));
                preRoute.push_back(connectionRoads[Seq3RoadTurnRight].getRoadIndex());
                return preRoute;
            } else if (targetSequenceNumber == 6) {
                std::vector<unsigned int> preRoute(this->getTwowayRoadBy(3)->getRouteFromTo(left, left));
                if (left) {
                    preRoute.push_back(connectionRoads[Seq3RoadLeftGoStraight].getRoadIndex());
                } else {
                    preRoute.push_back(connectionRoads[Seq3RoadRightGoStraight].getRoadIndex());
                }
                return preRoute;
            } else if (targetSequenceNumber == 0) {
                std::vector<unsigned int> preRoute(this->getTwowayRoadBy(3)->getRouteFromTo(left, true));
                preRoute.push_back(connectionRoads[Seq3RoadTurnLeft].getRoadIndex());
                return preRoute;
            }
        } else if (srcSequenceNumber == 5 &&
                   (targetSequenceNumber == 0 || targetSequenceNumber == 2 || targetSequenceNumber == 6)) {
            if (targetSequenceNumber == 6) {
                std::vector<unsigned int> preRoute(this->getTwowayRoadBy(5)->getRouteFromTo(left, false));
                preRoute.push_back(connectionRoads[Seq5RoadTurnRight].getRoadIndex());
                return preRoute;
            } else if (targetSequenceNumber == 0) {
                std::vector<unsigned int> preRoute(this->getTwowayRoadBy(5)->getRouteFromTo(left, left));
                if (left) {
                    preRoute.push_back(connectionRoads[Seq5RoadLeftGoStraight].getRoadIndex());
                } else {
                    preRoute.push_back(connectionRoads[Seq5RoadRightGoStraight].getRoadIndex());
                }
                return preRoute;
            } else if (targetSequenceNumber == 2) {
                std::vector<unsigned int> preRoute(this->getTwowayRoadBy(5)->getRouteFromTo(left, true));
                preRoute.push_back(connectionRoads[Seq5RoadTurnLeft].getRoadIndex());
                return preRoute;
            }
        } else if (srcSequenceNumber == 7 &&
                   (targetSequenceNumber == 0 || targetSequenceNumber == 2 || targetSequenceNumber == 4)) {
            if (targetSequenceNumber == 0) {
                std::vector<unsigned int> preRoute(this->getTwowayRoadBy(7)->getRouteFromTo(left, false));
                preRoute.push_back(connectionRoads[Seq7RoadTurnRight].getRoadIndex());
                return preRoute;
            } else if (targetSequenceNumber == 2) {
                std::vector<unsigned int> preRoute(this->getTwowayRoadBy(7)->getRouteFromTo(left, left));
                if (left) {
                    preRoute.push_back(connectionRoads[Seq7RoadLeftGoStraight].getRoadIndex());
                } else {
                    preRoute.push_back(connectionRoads[Seq7RoadRightGoStraight].getRoadIndex());
                }
                return preRoute;
            } else if (targetSequenceNumber == 4) {
                std::vector<unsigned int> preRoute(this->getTwowayRoadBy(7)->getRouteFromTo(left, true));
                preRoute.push_back(connectionRoads[Seq7RoadTurnLeft].getRoadIndex());
                return preRoute;
            }
        } else {
            throw SimulationException("Invalid srcSequenceNumber and targetSequenceNumber.");
        }
    }

    return std::vector<unsigned int>();
}
