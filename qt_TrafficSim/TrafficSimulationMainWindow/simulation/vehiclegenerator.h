#ifndef VEHICLEGENERATOR_H
#define VEHICLEGENERATOR_H

#include <QObject>
#include <QMultiHash>
#include "trafficsceneconfiguration.h"

class TrafficSimulator;

class VehicleGenerator
{
public:
    enum VehicleEnterSettingIndex {
      RoadIndex = 0,
      VehicleEnterRate = 1, // vehicles/ms
      LastVehicleAddedTime = 2 // ms
    };

    typedef std::tuple<unsigned int, double, unsigned long long> VehicleEnterSetting;

    explicit VehicleGenerator(TrafficSimulator* trafficSimulator);

    void setUpGeneratorWith(const TrafficSceneConfiguration& trafficMap);
    void generateVehicles(unsigned long long totalSimulationTime); // ms

private:
    TrafficSimulator* trafficSimulator;
    QList<VehicleEnterSetting> vehicleEnterSettings;
    QMultiHash<unsigned int, Road::Route> routesMultiHashMap;

signals:

};

#endif // VEHICLEGENERATOR_H
