#include "trafficsimulator.h"
#include <QTextStream>
#include <QThread>

TrafficSimulator::TrafficSimulator(QObject *parent)
    : QObject(parent), totalSimulationTime(0),
      simulationViewWidth(0.0), simulationViewHeight(0.0),
      roadsHashMap(), vehicles(), trafficLights(),
      vehicleGenerator(new VehicleGenerator(this)), frameBlockingQueque(100),
      projectToBeSyncWith(nullptr),
      setSyncProjectMutex()
{

}

TrafficSimulator::~TrafficSimulator() {
    QHash<unsigned int, Road*>::iterator roadsHashMapIterator = roadsHashMap.begin();
    QHash<unsigned int, Road*>::iterator roadsHashMapEnd = roadsHashMap.end();
    while (roadsHashMapIterator != roadsHashMapEnd) {
        delete *roadsHashMapIterator;
        roadsHashMapIterator++;
    }

//    for (Vehicle* vehicle : vehicles) {
//        delete vehicle;
//    }

//    for (TrafficLight* trafficLight : trafficLights) {
//        delete trafficLight;
//    }

    delete vehicleGenerator;
}

//void TrafficSimulator::setUpSimulatorWith(const TrafficSceneConfiguration& trafficMap) {
//    if (trafficMap.getRoadSettings().isEmpty()) {
//        throw SimulationException("trafficMap can't be empty.");
//    }

//    double maxSimulationWidth = 0.0;
//    double maxSimulationHeight = 0.0;

//    std::set<unsigned int> connectionRoadIndexes(trafficMap.getConnectionRoadIndexes());

//    for (const TrafficSceneConfiguration::RoadSetting* roadSetting : trafficMap.getRoadSettings()) {
//        Road* road = nullptr;
//        if (std::get<TrafficSceneConfiguration::RoadSettingIndex::IsCurveRoad>(*roadSetting)) {
//            road = new CurveRoad(std::get<TrafficSceneConfiguration::RoadSettingIndex::StartPoint>(*roadSetting),
//                                 std::get<TrafficSceneConfiguration::RoadSettingIndex::EndPoint>(*roadSetting),
//                                 std::get<TrafficSceneConfiguration::RoadSettingIndex::TurnDirection>(*roadSetting),
//                                 std::get<TrafficSceneConfiguration::RoadSettingIndex::InternalPointsNumber>(*roadSetting));
//        } else {
//            road = new Road(std::get<TrafficSceneConfiguration::RoadSettingIndex::StartPoint>(*roadSetting),
//                            std::get<TrafficSceneConfiguration::RoadSettingIndex::EndPoint>(*roadSetting));
//        }

//        if (connectionRoadIndexes.find(road->getRoadIndex()) != connectionRoadIndexes.end()) {
//            road->setIsInIntersection(true);
//        }

//        if (std::get<TrafficSceneConfiguration::RoadSettingIndex::HasTrafficLight>(*roadSetting)) {
//            TrafficLight* trafficLight = new TrafficLight(
//                        std::get<TrafficSceneConfiguration::RoadSettingIndex::GreenDuration>(*roadSetting),
//                        std::get<TrafficSceneConfiguration::RoadSettingIndex::RedDuration>(*roadSetting),
//                        std::get<TrafficSceneConfiguration::RoadSettingIndex::InitState>(*roadSetting));
//            trafficLights.append(trafficLight);
//            road->addTrafficLight(trafficLight);
//        }

//        Road::Point2D startPoint(std::get<TrafficSceneConfiguration::RoadSettingIndex::StartPoint>(*roadSetting));
//        Road::Point2D endPoint(std::get<TrafficSceneConfiguration::RoadSettingIndex::EndPoint>(*roadSetting));
//        double positionMaxWidth = 0.0;
//        double positionMaxHeight = 0.0;
//        if (!std::get<TrafficSceneConfiguration::RoadSettingIndex::IsCurveRoad>(*roadSetting)) {
//            positionMaxWidth = std::max(startPoint.first, endPoint.first);
//            positionMaxHeight = std::max(startPoint.second, endPoint.second);
//        } else {
//            double roadPointsMaxWidth = 0.0;
//            double roadPointsMaxHeight = 0.0;
//            for (Road::Point2D roadPoint : dynamic_cast<CurveRoad*>(road)->getAllPointsInRoad()) {
//                if (roadPoint.first > roadPointsMaxWidth) {
//                    roadPointsMaxWidth = roadPoint.first;
//                }

//                if (roadPoint.second > roadPointsMaxHeight) {
//                    roadPointsMaxHeight = roadPoint.second;
//                }
//            }
//            positionMaxWidth = roadPointsMaxWidth;
//            positionMaxHeight = roadPointsMaxHeight;
//        }

//        if (positionMaxWidth > maxSimulationWidth) {
//            maxSimulationWidth = positionMaxWidth;
//        }

//        if (positionMaxHeight > maxSimulationHeight) {
//            maxSimulationHeight = positionMaxHeight;
//        }

//        roadsHashMap.insert(road->getRoadIndex(), road);
//    }

//    simulationViewWidth = maxSimulationWidth;
//    simulationViewHeight = maxSimulationHeight;

//    vehicleGenerator.setUpGeneratorWith(trafficMap);
//}

void TrafficSimulator::syncConfigurationWith(const Project* project) {
    reInitAllElements();

    const TrafficSceneConfiguration& trafficMap = project->getTrafficSceneConfiguration();
    if (trafficMap.getRoadSettings().isEmpty()) {
        throw SimulationException("trafficMap can't be empty.");
    }

    double maxSimulationWidth = 0.0;
    double maxSimulationHeight = 0.0;

    std::set<unsigned int> connectionRoadIndexes(trafficMap.getConnectionRoadIndexes());

    for (const TrafficSceneConfiguration::RoadSetting* roadSetting : trafficMap.getRoadSettings()) {
        Road* road = nullptr;
        if (std::get<TrafficSceneConfiguration::RoadSettingIndex::IsCurveRoad>(*roadSetting)) {
            road = new CurveRoad(std::get<TrafficSceneConfiguration::RoadSettingIndex::StartPoint>(*roadSetting),
                                 std::get<TrafficSceneConfiguration::RoadSettingIndex::EndPoint>(*roadSetting),
                                 std::get<TrafficSceneConfiguration::RoadSettingIndex::TurnDirection>(*roadSetting),
                                 std::get<TrafficSceneConfiguration::RoadSettingIndex::InternalPointsNumber>(*roadSetting));
        } else {
            road = new Road(std::get<TrafficSceneConfiguration::RoadSettingIndex::StartPoint>(*roadSetting),
                            std::get<TrafficSceneConfiguration::RoadSettingIndex::EndPoint>(*roadSetting));
        }

        if (connectionRoadIndexes.find(road->getRoadIndex()) != connectionRoadIndexes.end()) {
            road->setIsInIntersection(true);
        }

        if (std::get<TrafficSceneConfiguration::RoadSettingIndex::HasTrafficLight>(*roadSetting)) {
            TrafficLight* trafficLight = new TrafficLight(
                        std::get<TrafficSceneConfiguration::RoadSettingIndex::GreenDuration>(*roadSetting),
                        std::get<TrafficSceneConfiguration::RoadSettingIndex::RedDuration>(*roadSetting),
                        std::get<TrafficSceneConfiguration::RoadSettingIndex::InitState>(*roadSetting));
            trafficLights.append(trafficLight);
            road->addTrafficLight(trafficLight);
        }

        Road::Point2D startPoint(std::get<TrafficSceneConfiguration::RoadSettingIndex::StartPoint>(*roadSetting));
        Road::Point2D endPoint(std::get<TrafficSceneConfiguration::RoadSettingIndex::EndPoint>(*roadSetting));
        double positionMaxWidth = 0.0;
        double positionMaxHeight = 0.0;
        if (!std::get<TrafficSceneConfiguration::RoadSettingIndex::IsCurveRoad>(*roadSetting)) {
            positionMaxWidth = std::max(startPoint.first, endPoint.first);
            positionMaxHeight = std::max(startPoint.second, endPoint.second);
        } else {
            double roadPointsMaxWidth = 0.0;
            double roadPointsMaxHeight = 0.0;
            for (Road::Point2D roadPoint : dynamic_cast<CurveRoad*>(road)->getAllPointsInRoad()) {
                if (roadPoint.first > roadPointsMaxWidth) {
                    roadPointsMaxWidth = roadPoint.first;
                }

                if (roadPoint.second > roadPointsMaxHeight) {
                    roadPointsMaxHeight = roadPoint.second;
                }
            }
            positionMaxWidth = roadPointsMaxWidth;
            positionMaxHeight = roadPointsMaxHeight;
        }

        if (positionMaxWidth > maxSimulationWidth) {
            maxSimulationWidth = positionMaxWidth;
        }

        if (positionMaxHeight > maxSimulationHeight) {
            maxSimulationHeight = positionMaxHeight;
        }

        roadsHashMap.insert(road->getRoadIndex(), road);
    }

    simulationViewWidth = maxSimulationWidth;
    simulationViewHeight = maxSimulationHeight;

    vehicleGenerator->setUpGeneratorWith(trafficMap);

    Road::resetTotalRoadsToZero();
}

Road::Route TrafficSimulator::getRouteBy(const QList<unsigned int>& roadIndexes) {
    Road::Route route;
    for (unsigned int roadIndex : roadIndexes) {
        if (roadsHashMap.contains(roadIndex)) {
            route.append(roadsHashMap.value(roadIndex));
        } else {
            throw SimulationException("Try to find a non-existed road by roadIndex.");
        }
    }

    return route;
}

void TrafficSimulator::addVehicleToRoad(Vehicle* vehicle, unsigned int roadIndex) {
    if (roadsHashMap.contains(roadIndex)) {
        vehicles.append(vehicle);
        roadsHashMap.value(roadIndex)->addWaitingVehicle(vehicle);
    } else {
        throw SimulationException("Try to find a non-existed road by roadIndex.");
    }
}

void TrafficSimulator::generateSimulationFrameWith(int deltaTimeInMS) {
    while (true) {
        if (QThread::currentThread()->isInterruptionRequested()) {
            return;
        }

        {
            std::unique_lock<std::mutex> lock(setSyncProjectMutex);
            if (projectToBeSyncWith) {
                syncConfigurationWith(projectToBeSyncWith);
                projectToBeSyncWith = nullptr;
            }
        }

        vehicleGenerator->generateVehicles(totalSimulationTime);

        for (TrafficLight* trafficLight : trafficLights) {
            trafficLight->update(totalSimulationTime);
        }

        QHash<unsigned int, Road*>::iterator roadsHashMapIterator = roadsHashMap.begin();
        QHash<unsigned int, Road*>::iterator roadsHashMapEnd = roadsHashMap.end();
        while (roadsHashMapIterator != roadsHashMapEnd) {
            (*roadsHashMapIterator)->update(deltaTimeInMS);
            roadsHashMapIterator++;
        }

        totalSimulationTime += deltaTimeInMS;

        frameBlockingQueque.pushBack(new SimulationFrame(roadsHashMap.values(), totalSimulationTime,
                                                         simulationViewWidth, simulationViewHeight));
    }
}

void TrafficSimulator::reInitAllElements() {
    totalSimulationTime = 0;

    simulationViewWidth = 0.0;
    simulationViewHeight = 0.0;

    QHash<unsigned int, Road*>::iterator roadsHashMapIterator = roadsHashMap.begin();
    QHash<unsigned int, Road*>::iterator roadsHashMapEnd = roadsHashMap.end();
    while (roadsHashMapIterator != roadsHashMapEnd) {
        delete *roadsHashMapIterator;
        roadsHashMapIterator++;
    }

    roadsHashMap.clear();
    vehicles.clear();
    trafficLights.clear();

    delete vehicleGenerator;

    vehicleGenerator = new VehicleGenerator(this);

    frameBlockingQueque.deepClear();
}

void TrafficSimulator::setProjectToBeSyncWith(const Project* project) {
    std::unique_lock<std::mutex> lock(setSyncProjectMutex);
    projectToBeSyncWith = project;
}
