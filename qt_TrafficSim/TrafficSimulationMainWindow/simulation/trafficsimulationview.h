#ifndef TRAFFICSIMULATIONVIEW_H
#define TRAFFICSIMULATIONVIEW_H

#include <QOpenGLWidget>
#include <QObject>
#include "trafficsimulator.h"
#include "simulationpaintingengine.h"

/**
 * @class TrafficSimulationView trafficsimulationview.h
 * @brief The traffic simulation view
 * 
 * @author YiCun Duan
 */
class TrafficSimulationView : public QOpenGLWidget
{
    Q_OBJECT
public:
    /**
     * @brief Construct a new Traffic Simulation View object
     * 
     * @param parent : the parent class
     */
    explicit TrafficSimulationView(QWidget* parent = nullptr);

    /**
     * @brief Set the simulator model to run
     * 
     * @param trafficSimulator : the traffic simulator
     */
    void setModel(TrafficSimulator* trafficSimulator);

    /**
     * @brief Choose whether to pause the simulation
     * 
     * @param paused : whether the simulation is paused
     */
    inline void setIsPaused(bool paused);

protected:
    /**
     * @brief Paint the event on to the scene
     *  
     * @param event : the event
     */
    virtual void paintEvent(QPaintEvent* event) override;

private:
    TrafficSimulator* trafficSimulator;
    SimulationPaintingEngine simulationPaintingEngine;

    bool isPaused;
};

void TrafficSimulationView::setIsPaused(bool paused) {
    isPaused = paused;
}

#endif // TRAFFICSIMULATIONVIEW_H
