#include "curveroad.h"
#include "../utilities/simulationexception.h"

CurveRoad::CurveRoad(const Road::Point2D& startPoint, const Road::Point2D& endPoint,
                     TurnDirection turnDirection, int internalPointsNumber) :
    Road(startPoint, endPoint), turnDirection(turnDirection), allPointsInRoad()
{
    if (internalPointsNumber <= 1) {
        throw SimulationException("Invalid internal points number.");
    }

    double minX = std::min(startPoint.first, endPoint.first);
    double minY = std::min(startPoint.second, endPoint.second);

    double alpha = 0.0;
    double beta = 0.0;

    if (turnDirection == CurveRoad::TurnDirection::Left) {
        alpha = minX - minY + startPoint.second;
        beta = minY - minX + endPoint.first;
    } else if (turnDirection == CurveRoad::TurnDirection::Right) {
        alpha = minX - minY + endPoint.second;
        beta = minY - minX + startPoint.first;
    } else {
        throw SimulationException("Invalid turn direction.");
    }

    allPointsInRoad.append(startPoint);

    int i = 1;
    roadLength = 0.0;
    while (i <= internalPointsNumber) {
        double sectionRatio = (double) i / internalPointsNumber;
        double internalPointX = std::pow((1 - sectionRatio), 2.0) * startPoint.first
                + 2 * (1 - sectionRatio) * sectionRatio * alpha
                + std::pow(sectionRatio, 2.0) * endPoint.first;

        double internalPointY = std::pow((1 - sectionRatio), 2.0) * startPoint.second
                + 2 * (1 - sectionRatio) * sectionRatio * beta
                + std::pow(sectionRatio, 2.0) * endPoint.second;

        roadLength += std::sqrt(std::pow((allPointsInRoad.last().first - internalPointX), 2.0)
                                + std::pow((allPointsInRoad.last().second - internalPointY), 2.0));

        if (i == internalPointsNumber) {
            roadLength += std::sqrt(std::pow((endPoint.first - internalPointX), 2.0)
                                    + std::pow((endPoint.second - internalPointY), 2.0));
        }

        allPointsInRoad.append({internalPointX, internalPointY});

        i++;
    }

    allPointsInRoad.append(endPoint);
}

CurveRoad::CurveRoad(const CurveRoad& otherCurveRoad) :
    Road(otherCurveRoad), turnDirection(otherCurveRoad.turnDirection),
    allPointsInRoad(otherCurveRoad.allPointsInRoad)
{

}

bool CurveRoad::isCurveRoad() {
    return true;
}
