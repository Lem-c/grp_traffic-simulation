#include "../unitTest/modelsTest.h"


#include "../models/averagespeedchartmodel.h"
#include "../models/entryattributetablefilter.h"
#include "../models/trafficlightattributetablefilter.h"

//#pragma comment  (lib, "User32.lib")

void modelsTest::testAverageSpeedData(){
    int isEmpty = 0;
    //Test whether the model could be created
    AverageSpeedChartModel* test = new AverageSpeedChartModel();
    if(test!=NULL){
        isEmpty = 1;
    }

    delete  test;
    QVERIFY(isEmpty==1);
}

void modelsTest::testSetEnableFiltering(){
    EntryAttributeTableFilter test2;

    test2.setEnableFiltering(true);

    QCOMPARE(test2.getEnableFiltering(), true);

    //delete test2;
}

void modelsTest::testSetEntryNumber(){
    EntryAttributeTableFilter test3;

    test3.setEntryNumber(3);

    QCOMPARE(test3.getEntryNumber(), 3);
}

void modelsTest::test_setIntersectionNumber(){
    TrafficLightAttributeTableFilter test4;

    test4.setIntersectionNumber(4);

    QCOMPARE(test4.getIntersectionNumber(), 4);

}

void modelsTest::test_setSequenceNumber(){
    TrafficLightAttributeTableFilter test5;

    test5.setSequenceNumber(5);

    QCOMPARE(test5.getSequenceNumber(),5);
}

void modelsTest::test_setEnableFiltering(){
    TrafficLightAttributeTableFilter test6;

    test6.setEnableFiltering(false);

    QCOMPARE(test6.getEnableFiltering(), false);
}

