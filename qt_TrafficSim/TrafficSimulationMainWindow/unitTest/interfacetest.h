#ifndef INTERFACETEST_H
#define INTERFACETEST_H

#include <QtTest/QtTest>

class interfaceTest : public QObject
{
    Q_OBJECT

private slots:
    void fullProijecttest();
    void creatProjectTest();
    void processWarningTest();
    void simulationTest();
    void chartTest();
    void formTest();
};

#endif // INTERFACETEST_H
