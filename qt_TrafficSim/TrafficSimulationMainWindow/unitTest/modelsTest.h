#ifndef __MODELSTEST_H__
#define __MODELSTEST_H__

#include <QtTest/QtTest>

class modelsTest : public QObject
{
    Q_OBJECT

private slots:
    void testAverageSpeedData();

    void testSetEnableFiltering();
    void testSetEntryNumber();

    void test_setIntersectionNumber();
    void test_setSequenceNumber();
    void test_setEnableFiltering();
};


#endif
