#ifndef UTILITIESTEST_H
#define UTILITIESTEST_H


#include <QtTest/QtTest>

class utilitiesTest : public QObject
{
    Q_OBJECT

private slots:
    void test_EntryAttribute();
    void test_JsonCoder();
    void test_Project();
    void test_ProjectAttribute();
    void test_ProjectAttributeIndex();
    void test_ProjectTreeModelItem();
    void test_SimulationException();
    void test_TrafficLightAttribute();
    void test_ui();
};

#endif // UTILITIESTEST_H
