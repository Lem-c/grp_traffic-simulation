#include "simulationtest.h"

#include <deque>
#include <simulation/frameblockingqueue.h>
#include <simulation/road.h>
#include <simulation/trafficlight.h>
#include <simulation/trafficsceneconfiguration.h>

std::deque<int> test;

void simulationTest::test_pushBack()
{
    test.push_back(1);
}

void simulationTest::test_popFront()
{
    test.pop_front();
    int testNum = test.size();
    QVERIFY(testNum==0);
}

void simulationTest::test_getSize(){
    test.push_back(1);
    test.push_back(2);
    test.push_back(3);
    int testSieze = test.size();

    QVERIFY(testSieze==3);
}

void simulationTest::test_isEmpty(){
    test.pop_front();
    test.pop_front();
    test.pop_front();
    int testEmpty = test.size();


    QVERIFY(testEmpty==0);
}

void simulationTest::test_getMethods(){
    Road testRoad(Road::Point2D(0, 0), Road::Point2D(100, 0));

    testRoad.hasTrafficLight();
    testRoad.getStartPoint();
    testRoad.getEndPoint();
}

void simulationTest::test_trafficLight(){
    TrafficLight testLight(20,20, TrafficLight::TrafficLightState::Red);
}

void simulationTest::test_RoadSetting(){
//    TrafficSceneConfiguration testConfig;
}

void simulationTest::test_setAndGetMethods(){

}
