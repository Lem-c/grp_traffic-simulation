#ifndef SIMULATIONTEST_H
#define SIMULATIONTEST_H

#include <QtTest/QtTest>

class simulationTest : public QObject
{
    Q_OBJECT

private slots:
    //BlockingDeque class test
    void test_pushBack();
    void test_popFront();
    void test_getSize();
    void test_isEmpty();
    //Road class test
    void test_getMethods();
    //TrafficLight test
    void test_trafficLight();
    //TrafficSceneConfiguration class test
    void test_RoadSetting();
    //Vehilce class test
    void test_setAndGetMethods();
};

#endif // SIMULATIONTEST_H
