#ifndef VEHICLELOGICTEST_H
#define VEHICLELOGICTEST_H

#include <QtTest/QtTest>

class vehicleLogicTest : public QObject
{
    Q_OBJECT

private slots:
    void testAverageSpeedData();
    void testAddVehicle();
};

#endif // VEHICLELOGICTEST_H
