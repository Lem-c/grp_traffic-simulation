#include "widget.h"
#include "ui_widget.h"
//Import to use qpainter
#include"qpainter.h"
//#include <QPropertyAnimation>
//#include <QSequentialAnimationGroup>


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    //init param
    carCount = 0;

    ui->setupUi(this);
    this->setMaximumSize(WINDOWS_WIDTH*ZOOM_SIZE, WINDOWS_HEIGHT*ZOOM_SIZE);
    this->setMinimumSize(WINDOWS_WIDTH*ZOOM_SIZE, WINDOWS_HEIGHT*ZOOM_SIZE);

    m_map.load(":cross_2.png");

    //creat a crossroad
    CrossRoad* cr1 = new CrossRoad(440, 450, 400, 300);

    initTwoCrossRoad();

    ts_array = new Transportation[10];
    sp1 = new SpawnPoint(1820, 510, 0, 2);

    //Add cars
    addVehicle();
    addVehicle(":car2.png", 400, 0, 1);
    addVehicle(":car3.png", 1264, 0, 1);
    addVehicle(":car5.png", 475, 1080, 1);
    addVehicle(":car4.png", 10, 405, 0);
    addVehicle();
    //test turn right
    addVehicle(":car7.png", 320, 0, 1);

    ts_array[1].setSpeed(1);
    ts_array[2].setSpeed(1);
    ts_array[4].setSpeed(10);
    ts_array[3].setMoveWay(1);
    ts_array[5].setSpeed(2);

    ts_array[6].setSpeed(1);
    ts_array[6].setTurn(true);
    ts_array[6].setRow(1);

    startSim();

    //free objs
    delete cr1;
}

Widget::~Widget()
{
    delete ui;
}

void Widget::startSim(){
    m_Timer.start();

    connect(&m_Timer,&QTimer::timeout,[=](){
    //update car
    updatePosition();
    //重新绘制图片
    update();
    });
}

void Widget::updatePosition(){
    //ts.run();

    for(int i=0; i<carCount; i++){
        ts_array[i].run();
    }

    //update spawn point cars
    for(int index=0; index<sp1->getSpawnNum(); index++){
        sp1->spawnArray[index].run();
    }
}

//The medthod load the world map
void Widget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.drawPixmap(0,0,m_map);
//    painter.drawPixmap(ts.car_x, ts.car_y, ts.car);

    for(int i=0; i<carCount; i++){
        painter.drawPixmap(ts_array[i].car_x, ts_array[i].car_y, ts_array[i].car);
    }

    //Draw cars of spawn points
    for(int index=0; index<sp1->getSpawnNum(); index++){
        painter.drawPixmap(sp1->spawnArray[index].car_x, sp1->spawnArray[index].car_y, sp1->spawnArray[index].car);
    }
}

////绘制道路地图方法
//void Widget::drawWorldMap(QPainter *paint)
//{
//    //load world map
//    QPixmap pix;
//    pix.load(":cross_2.png");

//    paint->drawPixmap(0, 0, 1920, 1080, pix);

//    this->paint->end();
//}

void Widget::initTwoCrossRoad(){
    //纵向灯
    TrafficLight *tl1 = new TrafficLight(1, 20, 3);
    tl1->addTrafficLight(this, 417, 280);

    TrafficLight *tl3 = new TrafficLight(1,20,3);
    tl3->addTrafficLight(this,1381, 280);

    TrafficLight *tl4 = new TrafficLight(1,20,3);
    tl4->addTrafficLight(this,417, 650);

    TrafficLight *tl8 = new TrafficLight(1,20,3);
    tl8->addTrafficLight(this,1381, 650);

    //横向灯
    TrafficLight* tl2 = new TrafficLight(0, 20 ,3);
    tl2->addTrafficLight(this, 290, 435);

    TrafficLight *tl7 = new TrafficLight(0, 20 ,3);
    tl7->addTrafficLight(this, 591, 435);

    TrafficLight *tl5 = new TrafficLight(0, 20 ,3);
    tl5->addTrafficLight(this, 1254, 435);

    TrafficLight *tl6 = new TrafficLight(0, 20 ,3);
    tl6->addTrafficLight(this, 1596, 435);

}

void Widget::addVehicle(){
    if(carCount < 10){
        ts_array[carCount] = Transportation();
        carCount++;
    }else{
        return;
    }
}

void Widget::addVehicle(std::string imgUrl, int x, int y, int type){
    if(carCount < 10){
        ts_array[carCount] = Transportation(imgUrl, x, y, type);
        carCount++;
    }
}
