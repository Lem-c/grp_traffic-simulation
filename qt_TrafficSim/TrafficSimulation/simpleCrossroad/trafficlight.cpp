#include "trafficlight.h"

#include "widget.h"
#include "ui_widget.h"


bool can_row_move;
bool can_col_move;

TrafficLight::TrafficLight()
{
    //The default create of traffic light is row-like
    type = 0;
    initRowTime(25,3);

    can_row_move = false;
}

/**
 * @brief TrafficLight::TrafficLight
 * @param rc        1 是纵向路径红绿灯， 0 是横向红绿灯
 * @param rgTime red/green light time
 * @param yTime  yellow light time
 */
TrafficLight::TrafficLight(int rc, int rgTime, int yTime){
    type = rc;

    if(rc==0){
        initRowTime(rgTime, yTime);

        can_row_move = false;
    }else{
        initColumeTime(rgTime, yTime);

        can_col_move = true;
    }

}

void TrafficLight::initRowTime(int red, int yellow){
    count_red = red;
    count_green = -1;
    count_yellow = yellow;

    redTime = count_red;
    yellowTime = count_yellow;
}

void TrafficLight::initColumeTime(int green, int yellow){
    count_red = -1;
    count_green = green;
    count_yellow = yellow;


    greenTime = count_green;
    yellowTime = count_yellow;
}


void TrafficLight::columnLightSim_timeout(){
    //如果红灯时间到，绿灯亮
    if(count_red == 0){
        //init timer
        initColumeTime(greenTime, yellowTime);

        //重启定时器
        lightTimer->start(1000);
        greenLight->show();
        yellowLight->hide();
        redLight->hide();
    }
    //如果红灯还有时间
    else if(count_red > 0){
        can_col_move = false;

        count_red -= 1;
        redLight->show();
        greenLight->hide();
        yellowLight->hide();
    }
    //如果绿灯时间到，黄灯亮
    if(count_green == 0){
        count_yellow -= 1;
        greenLight->hide();
        redLight->hide();
        yellowLight->show();

        //如果黄灯时间到
        if(count_yellow == 0){
            //re-init timer
            initRowTime(greenTime, yellowTime);

            //重启定时器
            lightTimer->start(1000);
            redLight->show();
            yellowLight->hide();
            greenLight->hide();

        }
    }
    //如果绿灯还有时间
    else if(count_green > 0){
        can_col_move = true;

        count_green -= 1;

        greenLight->show();
        yellowLight->hide();
        redLight->hide();
    }
}

void TrafficLight::rowLightSim_timeout(){
    //如果绿灯时间到，黄灯亮
    if(count_green == 0){
        count_yellow--;

        yellowLight->show();
        greenLight->hide();
        redLight->hide();

        //如果黄灯时间到，红灯亮
        if(count_yellow == 0){
            initRowTime(redTime, yellowTime);

            //重启定时器
            lightTimer->start(1000);
            greenLight->hide();
            yellowLight->hide();
            redLight->show();
        }
    }
    //如果绿灯还有时间
    else if(count_green > 0){
        can_row_move = true;

        count_green--;
        redLight->hide();
        greenLight->show();
        yellowLight->hide();
    }
    //如果红灯时间到，绿灯亮
    if(count_red == 0){
        //init timer
        initColumeTime(redTime, yellowTime);

        //重启定时器
        lightTimer->start(1000);
        greenLight->show();
        yellowLight->hide();
        redLight->hide();
    }
    //如果红灯还有时间
    else if(count_red > 0){
        can_row_move = false;

        count_red--;

        greenLight->hide();
        yellowLight->hide();
        redLight->show();
    }
}



void TrafficLight::startTimeout(){
    lightTimer = new QTimer();

    //Connect timer
    lightTimer->start(1000);
    //inherit from QObject

    if(type==1){
        connect(lightTimer,&QTimer::timeout,
               this, &TrafficLight::columnLightSim_timeout);//绑定函数
    }else{
        connect(lightTimer,&QTimer::timeout,
               this, &TrafficLight::rowLightSim_timeout);//绑定函数
    }
}

void TrafficLight::addTrafficLight(QWidget *ui, int xp, int yp){
    greenLight = new QLabel(ui);
    redLight = new QLabel(ui);
    yellowLight = new QLabel(ui);

    //Set images of traffic light
    //The images
    QPixmap green;
    QPixmap yellow;
    QPixmap red;

    if(type==0){
        green.load(":/greenLight.png");
        yellow.load(":/yellowLight.png");
        red.load(":/redLight.png");

        greenLight->setGeometry(xp, yp, 30, 84);
        redLight->setGeometry(xp, yp, 30, 84);
        yellowLight->setGeometry(xp, yp, 30, 84);

        initRowLight();
    }else{
        green.load(":/greenLight_c.png");
        yellow.load(":/yellowLight_c.png");
        red.load(":/redLight_c.png");

        greenLight->setGeometry(xp, yp, 84, 30);
        redLight->setGeometry(xp, yp, 84, 30);
        yellowLight->setGeometry(xp, yp, 84, 30);

        initColLight();

        //set the column images rotation
//        QTransform transform = QTransform();
//        transform.rotate(90);

//        green.transformed(transform);
//        red.transformed(transform);
//        yellow.transformed(transform);
    }

    greenLight->setPixmap(green);
    redLight->setPixmap(red);
    yellowLight->setPixmap(yellow);

    greenLight->setScaledContents(true);
    yellowLight->setScaledContents(true);
    redLight->setScaledContents(true);

    //Start the timer
    startTimeout();
}


void TrafficLight::initRowLight(){
    greenLight->hide();
    redLight->show();
    yellowLight->hide();
}

void TrafficLight::initColLight(){
    greenLight->show();
    redLight->hide();
    yellowLight->hide();
}

int* TrafficLight::getLightTime(){
    int* lightTime = new int[3];
    lightTime[0] = count_green;
    lightTime[1] = count_red;
    lightTime[2] = count_yellow;
    return lightTime;
}

bool TrafficLight::canGo(){
    if(count_green > 0){
        return true;
    }

    return false;
}
