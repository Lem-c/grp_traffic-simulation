#include "vehicle.h"
//Need import
#include <qmath.h>
#include <QtGui>

#define PI 3.1415926

//构造函数
/**
 * @brief Vehicle::Vehicle 车辆构造函数
 * @param parent 设置画布
 */
Vehicle::Vehicle(QWidget *parent) : QToolButton(parent)
{
    l_pixSize = QSize(32,32);
        InitData();
}

/**
 * @brief Vehicle::Vehicle 车辆构造函数2
 * @param pix 设置车辆图片
 * @param pixS 设置图片大小
 * @param parent 设置画布
 */
Vehicle::Vehicle(QImage pix,QSize pixS,QWidget *parent)
    : QToolButton(parent),l_image(pix),l_pixSize(pixS)
{
    InitData();
}

Vehicle::~Vehicle(){

}



void Vehicle::InitData(){
    this->setAutoRaise(true);
    this->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    this->setStyleSheet("color: rgb(255, 0, 0);\nfont: 75 9pt \"黑体\";");
    this->setIconSize(l_pixSize);
    this->setIcon(QIcon(QPixmap::fromImage(l_image)));
}

QImage Vehicle::filp(const QImage& image,bool bIsHorizon){
    return image.mirrored(bIsHorizon,!bIsHorizon);
}

QImage Vehicle::rotateImage(const QImage& image,qreal fAngle)		//将图片按顺时针方向旋转一定的角度，fAngle为角度值
{
    QTransform transform = QTransform();
    transform.rotate(fAngle);
    return image.transformed(transform,Qt::FastTransformation);
}

void Vehicle::setImageRote(qreal r_x,qreal r_y)					//根据弧度值（角度值）r_x,r_y确定图片旋转的角度
{
    //坐标和理论坐标轴不同,X轴正向为水平右，Y轴正向为垂直向下
    QImage tmp;
    if(r_x == 0 &&  r_y == 0)
        return;
    if(r_x == 0 )				//Y轴
    {
        if(r_y > 0)
        {
            tmp = rotateImage(l_image,90.0);
        }
        else
        {
            tmp = rotateImage(l_image,270.0);
        }
    }
    else if(r_y == 0)			//X轴
    {
        if(r_x >0)
        {
            ;
        }
        else
        {
            tmp = filp(l_image,true);
        }
    }
    else if(r_x > 0 && r_y > 0) //第一象限
    {
        qreal k = qAtan(r_y/r_x)*180/PI;
        tmp = rotateImage(l_image,k);
    }
    else if(r_x < 0 && r_y > 0) //第二象限
    {
        QImage _filp = filp(l_image,true);
        qreal k = qAtan(r_y/r_x)*180/PI;
        tmp = rotateImage(_filp,360+k);
    }else if(r_x < 0 && r_y < 0) //第三象限
    {
        QImage _filp = filp(l_image,true);
        qreal k = qAtan(r_y/r_x)*180/PI;
        tmp = rotateImage(_filp,k);
    }
    else if(r_x > 0 && r_y < 0) //第四象限
    {
        qreal k = qAtan(r_y/r_x)*180/PI;
        tmp = rotateImage(l_image,360.0+k);
    }
    this->setIcon(QIcon(QPixmap::fromImage(tmp)));
}

void Vehicle::setImageRote(int x1,int y1,int x2,int y2)	//根据弧度值（角度值）起点（x1,y1)和终点(x2,y2)确定图片旋转的角度
{
    setImageRote(qreal(x2-x1),qreal(y2-y1));
}

QPoint Vehicle::getPoswithLinedistance(qreal distance,QPoint start,QPoint end)	//计算从起点到终点方向距离distance的坐标点
{
    QPoint p;
    qreal t = qSqrt((end.x() - start.x())*(end.x() - start.x()) + (end.y() - start.y())*(end.y() - start.y()));
    p.setX((end.x() - start.x())*distance/t + start.x());
    p.setY((end.y() - start.y())*distance/t + start.y());
    return p;
}

void Vehicle::setData(QVector<QPoint> _v_point,qreal _distance,qreal _speed)//设置行进参数
{
    v_point.clear();
    v_linedistance.clear();
    v_point = _v_point;
    distance = _distance;
    speed = _speed;
    linedistance = 0;
    curposindex = -1;
    curposx = -1;
    curposy = -1;
    curlinetotledistance = 0;
    for(int i = 1;i<v_point.size();i++)
    {
        v_linedistance.push_back(qSqrt((v_point[i].x() - v_point[i-1].x())*(v_point[i].x() - v_point[i-1].x())+
            (v_point[i].y() - v_point[i-1].y())*(v_point[i].y() - v_point[i-1].y())));
        linedistance += v_linedistance.back();
    }
    if(v_point.size() > 0 )
    {
        curposindex = 0;
        curlinedistance = 0;
        curposx = v_point[0].x();
        curposy = v_point[0].y();
        if(v_point.size() >1)
            setImageRote(v_point[0].x(),v_point[0].y(),v_point[1].x(),v_point[1].y());
    }
}

void Vehicle::setSpeed(qreal _speed)//改变速度
{
    speed = _speed;
}

void Vehicle::setPos(int x,int y)//强制设置显示位置
{
    curposx = x;
    curposy = y;
}

void Vehicle::getCurrentPos(qreal time,int& x,int& y)//根据车速和运动轨迹计算time时间之后位置，timer事件调用move()函数移动到该位置，
{
    if(linedistance <=0 )//图像显示总长度为0
        return;
    qreal move_line = (time * speed)*linedistance/distance;
    if(curposindex == v_point.size()-1)//当前点在终点
    {

    }
    else
    {
        if(v_linedistance[curposindex] <= curlinedistance + move_line)//超越该点
        {
            curlinedistance = curlinedistance + move_line - v_linedistance[curposindex];
            curposindex++;
            if(curposindex == v_point.size()-1)//当前点在终点
            {
                curlinedistance = 0;
                curposx = v_point.back().x();
                curposy = v_point.back().y();
            }
            else
            {
                QPoint pt = getPoswithLinedistance(curlinedistance,v_point[curposindex],v_point[curposindex+1]);
                curposx = pt.x();
                curposy = pt.y();
                //旋转图标位置
                setImageRote(v_point[curposindex].x(),v_point[curposindex].y(),v_point[curposindex+1].x(),v_point[curposindex+1].y());
            }
        }
        else
        {
            curlinedistance += move_line;
            QPoint pt = getPoswithLinedistance(curlinedistance,v_point[curposindex],v_point[curposindex+1]);
            curposx = pt.x();
            curposy = pt.y();
        }
        curlinetotledistance += move_line;
    }
    x = curposx;
    y = curposy;
}

void Vehicle::startTimer(int _msec)
{
    msec = _msec;
    connect(&timer,SIGNAL(timeout()),this,SLOT(updatedisplay()));
    timer.start(_msec);
}

void Vehicle::updatedisplay()
{
    int x,y;
    getCurrentPos(msec*0.001,x,y);
    this->move(x,y);
    if(curposindex == v_point.size() -1)
        timer.stop();
}
