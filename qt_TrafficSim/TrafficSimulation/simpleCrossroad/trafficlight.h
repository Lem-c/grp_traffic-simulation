#ifndef TRAFFICLIGHT_H
#define TRAFFICLIGHT_H

#include <qlabel.h>


extern bool can_row_move;
extern bool can_col_move;

class TrafficLight : public QObject
{
public:
    TrafficLight();
    TrafficLight(int rc, int rgTimer, int yTime);

    void columnLightSim_timeout();
    void rowLightSim_timeout();
    void initRowTime(int redTime, int yellowTime);
    void initColumeTime(int greenTime, int yellowTime);
    void startTimeout();

    void addTrafficLight(QWidget *ui, int x, int y);

    int* getLightTime();
    bool canGo();

public:
    int count_red;
    int count_green;
    int count_yellow;


private:
    //whether row or column
    int type;

    //The lights
    QLabel *greenLight;
    QLabel *redLight;
    QLabel *yellowLight;

    int redTime;
    int greenTime;
    int yellowTime;

    //红绿灯计时器
    QTimer *lightTimer;

private:
    void initRowLight();
    void initColLight();
};

#endif // TRAFFICLIGHT_H
