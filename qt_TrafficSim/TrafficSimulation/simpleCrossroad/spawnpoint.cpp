#include "spawnpoint.h"

SpawnPoint::SpawnPoint(int x, int y, int type, int uNum)
{
    upNum = uNum;
    spawnArray = new Transportation[upNum];

    spawn_x = x;
    spawn_y = y;
    spawn_type = type;

    generateVehicle();
}

int SpawnPoint::getSpawnNum(){
    return upNum;
}

void SpawnPoint::generateVehicle(){
    //random num to select random car
    srand((unsigned)time(NULL));

    int index;

    //Add vehicle into the spawn array
    for(index = 0; index < upNum; index++){
        //get number from 0 to 10
        int random = rand()%11;
        //get img for the cars
        std::string carImg = getColorOfCar(random);
        spawnArray[index] = Transportation(carImg, spawn_x, spawn_y, spawn_type);
        //speed random
        if(random != 0){
            if(spawn_x>=1500){
                spawnArray[index].setSpeed(-random);
            }else{
                spawnArray[index].setSpeed(random);
            }
        }else{
            if(spawn_x>=1500){
                spawnArray[index].setSpeed(-5);
            }else{
                spawnArray[index].setSpeed(5);
            }
        }
    }

    return;
}

std::string SpawnPoint::getColorOfCar(int sel){
    const char* result = NULL;

    if(sel<=1){
        char temp[] = ":car1.png";
        result = temp;
    }else if(sel>1 && sel<=3){
        char temp[] = ":car2.png";
        result = temp;
    }else if(sel>3 && sel<=5){
        char temp[] = ":car3.png";
        result = temp;
    }else if(sel>5 && sel<=7){
        char temp[] = ":car4.png";
        result = temp;
    }else if(sel>7 && sel<=9){
        char temp[] = ":car5.png";
        result = temp;
    }else{
        char temp[] = ":car7.png";
        result = temp;
    }

    return result;
}
