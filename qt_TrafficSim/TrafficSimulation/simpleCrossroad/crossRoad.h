#pragma once
#ifndef CROSSROAD_H
#define CROSSROAD_H

class CrossRoad {
private:
    int cross_x;
    int cross_y;
    int c_width;
    int c_height;
	int fixNum;

public:
	CrossRoad() {
        cross_x = 0;
        cross_y = 0;
        c_height = 10;
        c_width = 10;
		fixNum = 5;
	}

    CrossRoad(int x, int y, int w, int h);
	void setMidPoint(int x, int y);
	void setArea(int width, int height);

    int getX();
    int getY();
    int getWidth();
    int getHeight();
	int* getInfo();
};

#endif
