#include "transportation.h"

Transportation::Transportation(){
    car.load(":car1.png");

    car_x = 10;
    car_y = 315;

    carRect.setWidth(car.width());
    carRect.setHeight(car.height());
    carRect.moveTo(car_x, car_y);

    setMoveType(0);
    revers = false;

    speed = 5;
    tempSpeed = 5;

    crArray = new CrossRoad[2];
    crArray[0] = CrossRoad(415, 480, 480, 320);
    crArray[1] = CrossRoad(1350, 480, 460, 320);
//    temp = new CrossRoad(440, 440, 480, 320);
//    temp2 = new CrossRoad(1350, 490, 460, 320);
}

Transportation::Transportation(std::string imgUrl, int x, int y, int type)
{
    const char *url = imgUrl.c_str();
    car.load(url);

    car_x = x;
    car_y = y;

    carRect.setWidth(car.width());
    carRect.setHeight(car.height());
    carRect.moveTo(car_x, car_y);

    crArray = new CrossRoad[2];
    crArray[0] = CrossRoad(440, 440, 480, 320);
    crArray[1] = CrossRoad(1350, 490, 460, 320);
//    temp = new CrossRoad(440, 440, 480, 300);
//    temp2 = new CrossRoad(1350, 490, 460, 320);

    setMoveType(type);
    revers = false;
    canTurn = false;

    speed = 3;
    tempSpeed = 5;
}


void Transportation::setStartPoint(int x, int y){
    startPoint[0] = x;
    startPoint[1] = y;
}

void Transportation::setEndPoint(int x, int y){
    endPoint[0] = x;
    endPoint[1] = y;
}

void Transportation::setMoveType(int type){
    if(type==0){
        rowMove = true;
        colMove = false;
    }else{
        rowMove = false;
        colMove = true;
    }
}

void Transportation::setMoveWay(int type){
    if(type==0){
        revers = false;
    }else{
        revers = true;
    }
}

void Transportation::setSpeed(double num){
    speed = num;
    tempSpeed = num;
}

void Transportation::setRow(int num){
    row = num;
}

void Transportation::setTurn(bool can){
    canTurn = can;
}

void Transportation::moveForward(){

    if(rowMove){
        car_x += speed;
    }else{
        car_y += speed;
    }

    carRect.moveTo(car_x, car_y);
}

/**
 * The duplicated method that used to change the revrsed vehicle movement
 * @brief Transportation::moveUp
 */
void Transportation::moveUp(){
    if(rowMove){
        car_x -= speed;
    }else{
        car_y -= speed;
    }

    carRect.moveTo(car_x, car_y);
}

/**
 * Unfinished turn method
 * @brief Transportation::moveTurnLeft
 */
void Transportation::moveTurnLeft(){
    if(row==1){
        if(colMove){

//            if(isInINtersection()){
//                if(car_y>350 && car_x>417){
//                    car_x -= speed;
//                    car_y += speed;
//                    carRect.moveTo(car_x, car_y);
//                }
//            }
//            else
            if(canTurn == true && car_y>=510){
                canTurn = false;
                setMoveType(0);

                setSpeed(-tempSpeed);
                //carRect rotate
                QTransform turnTran;
                turnTran.rotate(90);
                car.transformed(turnTran);
                carRect.setWidth(car.width());
                carRect.setHeight(car.height());

                return;
            }

            moveForward();
        }
    }else{

    }

}

void Transportation::slowDown(){
    if(speed>0){
        speed -= 1;
    }else if(speed<0){
        speed += 1;
    }
    else{
        speed = 0;
    }
}

void Transportation::speedUP(){
    if(tempSpeed < 0){
        if(speed >= tempSpeed){
            speed--;
        }
    }else{
        if(speed < tempSpeed){
            speed++;
        }
    }
}

//run method check the state and change the speed
void Transportation::run(){
    if(isInINtersection()){
        if(rowMove && can_row_move==false){
            slowDown();
        }else if(can_row_move==true && rowMove){
            speedUP();
        }

        if(colMove && can_col_move==false ){
            slowDown();
        }else if(can_col_move==true && colMove){
            speedUP();
        }
    }

    if(revers){
        moveUp();
    }else if(canTurn){
        moveTurnLeft();
    }
    else{
        moveForward();
    }
}

bool Transportation::isInINtersection(){
    int carW = 50;
    int carH = 15;

    bool* tempresult = new bool[2];
    bool result = true;
    for(int i=0; i<2; i++){
        if(car_x+carW/2 < crArray[i].getX()-crArray[i].getWidth()/2 || car_y + carH/2 < crArray[i].getY()-crArray[i].getHeight()/2 ||
           car_x-carW/2 > crArray[i].getX()+crArray[i].getWidth()/2 || car_y - carH/2 > crArray[i].getY()+crArray[i].getHeight()/2){
            tempresult[i] = false;
        }

       if(i==1){
            result = tempresult[0] || tempresult[1];
       }
    }

    delete[]  tempresult;
//    if(car_x+carW/2 < temp2->getX()-temp->getWidth()/2 || car_y + carH/2 < temp2->getY()-temp->getHeight()/2 ||
//       car_x-carW/2 > temp2->getX()+temp->getWidth()/2 || car_y - carH/2 > temp2->getY()+temp->getHeight()/2){
//        t2 = false;
//    }
    return result;
}
