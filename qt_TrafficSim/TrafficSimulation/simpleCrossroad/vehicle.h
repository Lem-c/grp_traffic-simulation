#ifndef VEHICLE_H
#define VEHICLE_H

#include <QWidget>
#include <QToolButton>
#include <qtimer.h>

class Vehicle : public QToolButton
{
   Q_OBJECT

public:
    Vehicle(QWidget *parent = NULL);
    Vehicle(QImage pix,QSize pixS,QWidget *parent = NULL);
    ~Vehicle();

    void InitData();
    QImage filp(const QImage& image,bool bIsHorizon);						//按水平轴或者垂直线作镜像翻转，bIsHorizon为true按水平轴，false按垂直方向
    QImage rotateImage(const QImage& image,qreal fAngle);					//将图片按顺时针方向旋转一定的角度，fAngle为角度值
    void setImageRote(qreal r_x,qreal r_y);									//根据弧度值（角度值）r_x,r_y确定图片旋转的角度
    void setImageRote(int x1,int y1,int x2,int y2);							//根据弧度值（角度值）起点（x1,y1)和终点(x2,y2)确定图片旋转的角度
    void setData(QVector<QPoint> _v_point,qreal _distance,qreal _speed);	//设置行进参数
    void setSpeed(qreal _speed);											//改变速度
    void setPos(int x,int y);												//强制设置显示位置
    QPoint getPoswithLinedistance(qreal distance,QPoint start,QPoint end);	//计算从起点到终点方向距离distance的坐标点
    void getCurrentPos(qreal time,int& x,int& y);							//根据车速和运动轨迹计算time时间之后位置，timer事件调用move()函数移动到该位置，
    //测试_时间触发器
    void startTimer(int _msec);
public slots:
    void updatedisplay();

private:
    QSize l_pixSize;
    QImage l_image;

    //车辆行进数据结构
    QVector<QPoint> v_point;												//行驶路径点集合（图上位置）
    QVector<qreal> v_linedistance;											//行驶路径段在图上的线段长度
    qreal distance;															//行驶路径总长度（单位m)
    qreal linedistance;														//行驶路径在图上的总长度
    int	   curposindex;														//当前所在点的下标
    qreal  curlinedistance;													//当前所在线段上距离
    qreal  curlinetotledistance;											//当前行驶完成的路径长度总和
    int	   curposx;															//当前在图上的点X坐标
    int    curposy;															//当前在图上的点Y坐标
    qreal speed;															//当前车速

    //测试_时间触发器
    QTimer timer;
    int msec;

};

#endif // VEHICLE_H
