#include "widget.h"
#include <QApplication>

#include <QtGui>
#include "vehicle.h"
#include "transportation.h"

/**
 * @author Yuhao CHEN
 * @brief main The program entry
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;

    w.show();

    //The main do the widget window method
    //The public method can be realized here
    //Decalre the pubilc method in the Headers
    return a.exec();

}
