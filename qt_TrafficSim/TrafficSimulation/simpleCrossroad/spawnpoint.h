#ifndef SPAWNPOINT_H
#define SPAWNPOINT_H

//include self defined class
#include <transportation.h>
//include random gen lib
#include <iostream>
#include <stdlib.h>
#include <time.h>

/**
 * @brief The SpawnPoint class generate the vehicles automatically
 * accroding to the x,y axis, the vehicle type and the max number
 * of car
 */
class SpawnPoint
{
public:
    SpawnPoint(int x, int y, int type, int uNum);
    int getSpawnNum();
    void generateVehicle();
    std::string getColorOfCar(int sel);

public:
    //车辆数组
    Transportation* spawnArray;

private:

    int spawn_x;
    int spawn_y;
    int spawn_type;

    //出生点总数限制
    int upNum;
};

#endif // SPAWNPOINT_H
