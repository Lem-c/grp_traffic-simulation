#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QParallelAnimationGroup>
#include <QGridLayout>
//Import timer
#include <QTimer>

//import the self write class
#include "trafficlight.h"
#include "transportation.h"
#include "spawnpoint.h"

#define WINDOWS_WIDTH 960
#define WINDOWS_HEIGHT 540
#define ZOOM_SIZE 2

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void startSim();

public:
    //定义画笔
    QPainter *paint;

    //红绿灯时间交替
    void LightTime();
    //初始化红绿灯时长
    void initLightParam(int green, int red);

    //更新位置
    void updatePosition();

    //Vehicles
    //Transportation ts;

    //车辆数组
    Transportation* ts_array;
    //车辆数目
    int carCount;
    //出生点
    SpawnPoint* sp1;

protected:
    //Draw the world map
    void paintEvent(QPaintEvent *);
    void drawWorldMap(QPainter *paint);
    //void loadTrafiicLight();

    //The map init
    void initState();
    void initTwoCrossRoad();

    void addVehicle();
    void addVehicle(std::string imgUrl, int x, int y, int type);

private:
    Ui::Widget *ui;

    QPixmap m_map;
    QTimer m_Timer;
};


#endif // WIDGET_H
