#ifndef TRANSPORTATION_H
#define TRANSPORTATION_H


#include "vehicle.h"
#include <QLabel>

#include <iostream>
#include <string>
#include <QImage>
#include <QPixmap>
#include <QGridLayout>
#include "crossRoad.h"
#include "trafficlight.h"

#define UNIT_SIZE 50;

class Transportation
{
public:
    //Constructor
    Transportation();
    Transportation(std::string imgUrl, int x, int y, int type);
    //void setImage(string url);
    void setStartPoint(int x, int y);
    void setEndPoint(int x, int y);
    void setSpeed(double num);
    void setRow(int num);
    void setTurn(bool canTurn);

    void moveForward();
    void moveUp();
    void moveTurnLeft();
    void slowDown();
    void speedUP();
    void turn();
    void run();

    void setMoveType(int type);
    void setMoveWay(int type);
    bool isInINtersection();

public:
    QPixmap car;
    QRect carRect;

    int car_x;
    int car_y;

private:
    //QImage carImg;
    int startPoint[2];
    int endPoint[2];
    double speed;
    double tempSpeed;

    CrossRoad* crArray;

    //The different state
    bool canTurn;
    bool turnLeft;
    bool turnRight;

    bool rowMove;
    bool colMove;
    bool revers;

    //on which road
    int row;
};

#endif // TRANSPORTATION_H
