#include "crossRoad.h"

CrossRoad::CrossRoad(int x, int y, int w, int h) {
    cross_x = x;
    cross_y = y;
    c_width = w;
    c_height = h;
    fixNum = 10;
}

void CrossRoad::setMidPoint(int x, int y) {
    cross_x = x;
    cross_y = y;
}

void CrossRoad::setArea(int w, int h) {
    c_width = w;
    c_height = h;
}

int CrossRoad:: getX(){
    return cross_x;
}

int CrossRoad:: getY(){
    return cross_y;
}

int CrossRoad:: getWidth(){
    return c_width + fixNum;
}

int CrossRoad:: getHeight(){
    return c_height + fixNum;
}

int* CrossRoad::getInfo() {
    int* infoArr = new int[4];
    infoArr[0] = cross_x;
    infoArr[1] = cross_y;
    infoArr[2] = c_width;
    infoArr[3] = c_height;

	return infoArr;
}

