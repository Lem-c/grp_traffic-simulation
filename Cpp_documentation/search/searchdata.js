var indexSectionsWithContent =
{
  0: "acdefghijlmnoprstuvw~",
  1: "acefgijmprstuv",
  2: "jsu",
  3: "acefgijmprstuv",
  4: "acdefghilmnoprstuvw~",
  5: "aceinprstvw",
  6: "aeprtv",
  7: "cdprtv",
  8: "eghilnprstvw",
  9: "jv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends"
};

